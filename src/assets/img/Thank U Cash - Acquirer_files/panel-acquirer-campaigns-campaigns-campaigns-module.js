(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-acquirer-campaigns-campaigns-campaigns-module"],{

/***/ "./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--campaign--list -->\n\n<div class=\"card\">\n    <div [className]=\"_HelperService._Assets.Box.header\">\n        <div class=\"tx-14 tx-bold tx-uppercase\"> {{UserAccounts_Config.Title}} </div>\n        <div [className]=\"_HelperService._Assets.Box.headerTitleRight\">\n            <nav class=\"nav nav-icon-only mg-l-auto\">\n\n                <button class=\"btn tx-12 btn-primary pd-x-20 rounded-50 \" (click)=\"Open_AddCampaign()\"> New Campaign\n                </button>\n            </nav>\n        </div>\n    </div>\n    <div class=\"box-header px-3 pd-t-15 pd-b-10 d-flex align-items-center justify-content-between\">\n        <div class=\"form-inline \">\n            <div class=\"input-group input-group-sm mr-3\">\n                <input type=\"text\" class=\"form-control\" [value]=\"UserAccounts_Config.SearchParameter\" maxlength=\"100\"\n                    class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n                    (keyup)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n                <div class=\"input-group-append\">\n                    <button class=\"btn btn-outline-light\" type=\"button\"\n                        (click)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n                            data-feather=\"search\"></i> </button>\n                </div>\n            </div>\n\n        </div>\n        <div class=\"d-flex ml-auto\">\n            <button type=\"button\" class=\"btn  tx-12 rounded-0 bd-gray-200 bd-r-0  btn-outline-danger btn-icon\"\n              (click)=\"Delete_Filter()\">\n              <i data-feather=\"minus-circle\"></i> </button>\n      \n            <div class=\"form-group my-auto shadow-none ml-auto \" *ngIf=\"_HelperService._RefreshUI\">\n              <select2 name=\"itemId0\" [options]=\"_DataHelperService.S2_NewSavedFilters_Option\" class=\"wd-30 select2\"\n                [data]=\"_HelperService.Active_FilterOptions\" [value]=\"_HelperService.Active_FilterOptions[0]\"\n                (valueChanged)=\"Active_FilterValueChanged( $event )\">\n              </select2>\n            </div>\n            <button type=\"button\" class=\" btn btn-outline-success tx-12 bd-l-0 mr-3 rounded-0 bd-gray-200 btn-icon\"\n              (click)=\"Save_NewFilter()\"> <i data-feather=\"plus-circle\"></i> </button>\n      \n          </div>\n        <nav class=\"nav nav-with-icon tx-20\">\n            <div class=\"dropdown dropleft\">\n                <a href=\"\" class=\"nav-link mr-2\" id=\"UserAccounts_sdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n                    aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n                <div class=\"dropdown-menu tx-13\">\n                    <form class=\"wd-250 pd-15\">\n                        <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n                            <div class=\"float-right tx-20\" *ngIf=\"UserAccounts_Config.Sort.SortOrder == 'desc'\"\n                                (click)=\"UserAccounts_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                                <i class=\"fas fa-sort-alpha-down-alt\"></i>\n                            </div>\n                            <div class=\"float-right tx-20\" *ngIf=\"UserAccounts_Config.Sort.SortOrder == 'asc'\"\n                                (click)=\"UserAccounts_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                                <i class=\"fas fa-sort-alpha-up-alt\"></i>\n                            </div>\n                        </h6>\n                        <a class=\"dropdown-item\" *ngFor=\"let TItem of UserAccounts_Config.Sort.SortOptions\"\n                            (click)=\"UserAccounts_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n                            {{TItem.Name}}\n                        </a>\n\n                        <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                        (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                        Reset</button>\n                      <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                        (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n\n                    </form>\n                </div>\n            </div>\n            <div class=\"dropdown dropleft\">\n                <a href=\"\" class=\"nav-link\" id=\"UserAccounts_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n                    aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n                <div class=\"dropdown-menu tx-13\">\n                    <form class=\"wd-250 pd-15\">\n                        <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Filters\n                        </h6>\n                        <div>\n                            <div class=\"form-group\">\n                                <label>Date</label>\n                                <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\"\n                                    daterangepicker [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                                    (selected)=\"UserAccounts_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n                            </div>\n                        </div>\n                        <div>\n                            <div class=\"form-group\">\n                                <label>Status</label>\n                                <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                                    [data]=\"UserAccounts_Config.StatusOptions\" [value]=\"UserAccounts_Config.Status\"\n                                    (valueChanged)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                                </select2>\n                            </div>\n                        </div>\n\n                        <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                            (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                            Reset</button>\n                        <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                            (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n                    </form>\n                </div>\n            </div>\n            <a class=\"nav-link ml-2 RefreshColor\"\n                (click)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n                    data-feather=\"refresh-cw\"></i> </a>\n        </nav>\n    </div>\n\n    <div class=\"pt-0 pd-x-15 pd-b-15\">\n\n        <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"(!_HelperService.FilterSnap.Status) || (_HelperService.FilterSnap.Status==28)\"\n        (click)=\"RemoveFilterComponent('Status')\">\n        {{_HelperService.FilterSnap.StatusName}}\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n                class=\"fas fa-times\"></i> </span>\n    </div>\n    <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"!_HelperService.FilterSnap.Sort.SortColumn || (_HelperService.FilterSnap.Sort.SortColumn=='CreateDate' && _HelperService.FilterSnap.Sort.SortOrder=='desc')\"\n        (click)=\"RemoveFilterComponent('Sort')\">\n        <span>\n            {{_HelperService.FilterSnap.Sort.SortColumn + \"--\" + _HelperService.FilterSnap.Sort.SortOrder}}</span>\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n                class=\"fas fa-times\"></i> </span>\n    </div>\n    <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"!_HelperService.FilterSnap.StartTime\" (click)=\"RemoveFilterComponent('Time')\">\n        {{_HelperService.GetDateTimeS(_HelperService.FilterSnap.StartTime) + \"--\" + _HelperService.GetDateTimeS(_HelperService.FilterSnap.EndTime)}}\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n                class=\"fas fa-times\"></i> </span>\n    </div>\n\n    <span class=\"badge badge-pill badge-primary\"\n        *ngFor=\"let TItem of UserAccounts_Config.Filters\">{{TItem.Title}}</span>\n    </div>\n    <div class=\"card-body p-0\">\n        <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n            <thead>\n                <tr>\n\n                    <th class=\"text-center\" *ngIf=\"EditColoum[0].Value\">\n                        Status\n                    </th>\n                    <th class=\"text-center\" *ngIf=\"EditColoum[1].Value\">\n                        Campaign ID\n                    </th>\n\n                    <th>\n                        Campaign Name\n                    </th>\n                    <th *ngIf=\"EditColoum[2].Value\">\n                        Type\n                    </th>\n                    <th *ngIf=\"EditColoum[3].Value\">\n                        Reward\n                    </th>\n                    <th *ngIf=\"EditColoum[4].Value\">\n                        Start\n                    </th>\n                    <th *ngIf=\"EditColoum[5].Value\">\n                        End\n                    </th>\n                    <th class=\"d-flex\">\n                        <span class=\"btn cursor-pointer text-primary p-0\"\n                            style=\"position: absolute; z-index: 3;right: -10px;\" data-toggle=\"tooltip\"\n                            title=\"Edit Your Column\" (click)=\"EditCol()\">\n                            <i style=\"height: 20px; width: 20px;\" data-feather=\"plus-circle\"></i>\n                        </span>\n                    </th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr class=\"cursor-pointer\" (click)=\"UserAccounts_RowSelected(TItem)\"\n                    *ngFor=\"let TItem of UserAccounts_Config.Data  | paginate: { id: 'UserAccounts_Paginator',itemsPerPage: UserAccounts_Config.PageRecordLimit, currentPage: UserAccounts_Config.ActivePage,  totalItems: UserAccounts_Config.TotalRecords }\">\n                    <!-- <td class=\"text-center\">\n                        <img class=\"br\" src=\"{{TItem.IconUrl}}\" class=\"img wd-40 ht-40 rounded-circle bd bd-2\"\n                            [style.border-color]=\"TItem.StatusC\">\n                    </td> -->\n                    <td *ngIf=\"EditColoum[0].Value\">\n                        <div class=\"text-center\">\n                            <span class=\"\" [ngClass]=\"TItem.StatusBadge\">{{ TItem.StatusName || \"--\" }}</span>\n                        </div>\n                    </td>\n                    <td *ngIf=\"EditColoum[1].Value\">\n                        <div class=\"tx-semibold text-center\">{{TItem.ReferenceId  || \"--\"}}</div>\n\n                    </td>\n                    <td class=\"\">\n                        <div class=\"tx-14\"> {{TItem.Name || \"--\"}}</div>\n                        <div class=\"tx-12 text-muted\"> All access card customers will get 2%. Cash back </div>\n                    </td>\n                    <td class=\"\" *ngIf=\"EditColoum[2].Value\">\n                        {{ (TItem.TypeName | campaignText)  || \"--\"}}\n                    </td>\n\n                    <td class=\"\" *ngIf=\"EditColoum[3].Value\">\n                        {{TItem.SubTypeValue  || \"--\"}}\n                        <div class=\"tx-12 text-muted\"> {{TItem.SubTypeName}}</div>\n\n                    </td>\n                    <td class=\"text-muted\" *ngIf=\"EditColoum[4].Value\">\n                        {{TItem.StartDate  || \"--\"}}\n                    </td>\n                    <td class=\"text-muted\" *ngIf=\"EditColoum[5].Value\">\n                        {{TItem.EndDate  || \"--\"}}\n                    </td>\n                    <td></td>\n\n                </tr>\n            </tbody>\n        </table>\n    </div>\n    <div class=\"card-footer pd-20\">\n        <span class=\"mr-3\">\n            {{ 'Common.Showing' | translate }}\n            <span class=\"bold\">{{UserAccounts_Config.ShowingStart}} {{ 'Common.To' | translate }}\n                {{UserAccounts_Config.ShowingEnd}}\n            </span> {{ 'Common.Of' | translate }} {{UserAccounts_Config.TotalRecords}}\n        </span>\n        <span>\n            {{ 'Common.Show' | translate }}\n            <select class=\"wd-40 \" [(ngModel)]=\"UserAccounts_Config.PageRecordLimit\"\n                (ngModelChange)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n                <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n                    {{TItem}}\n                </option>\n            </select>\n        </span>\n        <span class=\"float-right\">\n            <pagination-controls id=\"UserAccounts_Paginator\" class=\"pagination pagination-space\"\n                (pageChange)=\"UserAccounts_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n                nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n            </pagination-controls>\n        </span>\n    </div>\n</div>\n\n<div class=\"modal\" id=\"EditCol\" tabindex=\"-1\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Edit Column</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group\" *ngFor=\"let Col of SaveEditValue\">\n                    <div class=\"form-check\">\n                        <input class=\"form-check-input\" type=\"checkbox\" id=\"gridCheck\" [(ngModel)]=Col.Value>\n                        <label class=\"form-check-label\" for=\"\">\n                            {{Col.Name}}\n                        </label>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"SaveEditCol()\">Save</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.ts ***!
  \***************************************************************************/
/*! exports provided: TUCampaignsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignsComponent", function() { return TUCampaignsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var TUCampaignsComponent = /** @class */ (function () {
    function TUCampaignsComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _ChangeDetectorRef, _FilterHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._ChangeDetectorRef = _ChangeDetectorRef;
        this._FilterHelperService = _FilterHelperService;
        this.SaveEditValue = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "Campaign ID ",
                Value: true,
            },
            {
                Name: "Type ",
                Value: true,
            },
            {
                Name: "Reward",
                Value: true,
            },
            {
                Name: "Start",
                Value: true,
            },
            {
                Name: "End",
                Value: true,
            },
        ];
        this.EditColoum = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "Campaign ID ",
                Value: true,
            },
            {
                Name: "Type ",
                Value: true,
            },
            {
                Name: "Reward",
                Value: true,
            },
            {
                Name: "Start",
                Value: true,
            },
            {
                Name: "End",
                Value: true,
            },
        ];
    }
    TUCampaignsComponent.prototype.ngOnInit = function () {
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this.UserAccounts_Setup();
        var temp = this._HelperService.GetStorage("CampaignTable");
        if (temp != undefined && temp != null) {
            this.EditColoum = temp.config;
            this.SaveEditValue = JSON.parse(JSON.stringify(temp.config));
        }
    };
    TUCampaignsComponent.prototype.SetOtherFilters = function () {
        this.UserAccounts_Config.SearchBaseConditions = [];
        this.UserAccounts_Config.SearchBaseCondition = null;
    };
    TUCampaignsComponent.prototype.EditCol = function () {
        this._HelperService.OpenModal("EditCol");
    };
    TUCampaignsComponent.prototype.SaveEditCol = function () {
        this.EditColoum = JSON.parse(JSON.stringify(this.SaveEditValue));
        this._HelperService.SaveStorage("CampaignTable", {
            config: this.EditColoum,
        });
        this._HelperService.CloseModal("EditCol");
    };
    TUCampaignsComponent.prototype.Open_AddCampaign = function () {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns
                .AddCampaign,
        ]);
    };
    TUCampaignsComponent.prototype.UserAccounts_Setup = function () {
        this.UserAccounts_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaigns,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
            Title: "Available Campaigns",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict("", "UserAccountKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, "="),
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: "CreateDate desc",
            TableFields: [
                {
                    DisplayName: "Name",
                    SystemName: "Name",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "Type",
                    SystemName: "TypeName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "Reward Type",
                    SystemName: "SubTypeName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "Start",
                    SystemName: "StartDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "End",
                    SystemName: "EndDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ],
        };
        this.UserAccounts_Config = this._DataHelperService.List_Initialize(this.UserAccounts_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.Campaign, this.UserAccounts_Config);
        this.UserAccounts_GetData();
    };
    TUCampaignsComponent.prototype.UserAccounts_ToggleOption = function (event, Type) {
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.UserAccounts_Config);
        this.UserAccounts_Config = this._DataHelperService.List_Operations(this.UserAccounts_Config, event, Type);
        if ((this.UserAccounts_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)) {
            this.UserAccounts_GetData();
        }
    };
    TUCampaignsComponent.prototype.UserAccounts_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.UserAccounts_Config);
        this.UserAccounts_Config = TConfig;
    };
    TUCampaignsComponent.prototype.UserAccounts_RowSelected = function (ReferenceData) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveCampaign, {
            ReferenceKey: ReferenceData.ReferenceKey,
            ReferenceId: ReferenceData.ReferenceId,
            DisplayName: ReferenceData.DisplayName,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Campaign,
        });
        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns
                .Sales,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
        ]);
    };
    //#region filterOperations
    TUCampaignsComponent.prototype.Active_FilterValueChanged = function (event) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.UserAccounts_GetData();
    };
    TUCampaignsComponent.prototype.Save_NewFilter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._FilterHelperService._BuildFilterName_Store(result.value);
                _this._HelperService.Save_NewFilter(_this._HelperService.AppConfig.FilterTypeOption.Campaign);
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    TUCampaignsComponent.prototype.Delete_Filter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._HelperService.Delete_Filter(_this._HelperService.AppConfig.FilterTypeOption.Campaign);
                _this._FilterHelperService.SetStoreConfig(_this.UserAccounts_Config);
                _this.UserAccounts_GetData();
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    TUCampaignsComponent.prototype.ApplyFilters = function (event, Type) {
        this._HelperService.MakeFilterSnapPermanent();
        this.UserAccounts_GetData();
    };
    TUCampaignsComponent.prototype.ResetFilters = function (event, Type) {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.UserAccounts_GetData();
    };
    TUCampaignsComponent.prototype.RemoveFilterComponent = function (Type, index) {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.UserAccounts_GetData();
    };
    TUCampaignsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-campaigns",
            template: __webpack_require__(/*! ./campaigns.component.html */ "./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["FilterHelperService"]])
    ], TUCampaignsComponent);
    return TUCampaignsComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/campaigns/campaigns.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaigns/campaigns.module.ts ***!
  \************************************************************************/
/*! exports provided: TUCampaignsRoutingModule, TUCampaignsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignsRoutingModule", function() { return TUCampaignsRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignsModule", function() { return TUCampaignsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _campaigns_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./campaigns.component */ "./src/app/panel/acquirer/campaigns/campaigns/campaigns.component.ts");
/* harmony import */ var src_app_service_main_pipe_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/service/main-pipe.module */ "./src/app/service/main-pipe.module.ts");












var routes = [{ path: "", component: _campaigns_component__WEBPACK_IMPORTED_MODULE_10__["TUCampaignsComponent"] }];
var TUCampaignsRoutingModule = /** @class */ (function () {
    function TUCampaignsRoutingModule() {
    }
    TUCampaignsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUCampaignsRoutingModule);
    return TUCampaignsRoutingModule;
}());

var TUCampaignsModule = /** @class */ (function () {
    function TUCampaignsModule() {
    }
    TUCampaignsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUCampaignsRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                src_app_service_main_pipe_module__WEBPACK_IMPORTED_MODULE_11__["MainPipe"]
            ],
            declarations: [_campaigns_component__WEBPACK_IMPORTED_MODULE_10__["TUCampaignsComponent"]]
        })
    ], TUCampaignsModule);
    return TUCampaignsModule;
}());



/***/ })

}]);
//# sourceMappingURL=panel-acquirer-campaigns-campaigns-campaigns-module.js.map