(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-acquirer-campaigns-sales-tusale-module"],{

/***/ "./src/app/panel/acquirer/campaigns/sales/tusale.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/sales/tusale.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card bd-t-0\">\n  <!-- <div class=\"card-header pt-0\">   \n    <div class=\"pd-y-10 px-2  no-gutters\">\n        <div class=\"d-flex justify-content-between\">\n            <div class=\" media\">\n              <div class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-primary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                <i data-feather=\"shopping-cart\"></i>\n              </div>\n              <div class=\"media-body bd-pink\">\n                <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold tx-nowrap mg-b-5 mg-md-b-8\">Transactions</h6>\n                <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">$1,958,104</h4>\n              </div>\n            </div>\n            <div class=\" media\">\n              <div class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-success tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded \">\n                <span class=\"tx-20\">  <i class=\"fas fa-money-bill-alt\"></i></span>\n              </div>\n              <div class=\"media-body\">\n                <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">Sale Amount</h6>\n                <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">$234,769<small>.50</small></h4>\n              </div>\n            </div>\n            <div class=\" media\">\n              <div class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-verve tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                <span class=\"tx-20\">  <i class=\"fas fa-money-bill-alt\"></i></span>\n              </div>\n              <div class=\"media-body\">\n                <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">Rewards given</h6>\n                <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">$1,608,469<small>.50</small></h4>\n              </div>\n            </div>\n            <div class=\" media\">\n                <div class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-teal tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                  <span class=\"tx-20\">  <i class=\"fas fa-money-bill-alt\"></i></span>\n                </div>\n                <div class=\"media-body\">\n                  <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">Total Budget</h6>\n                  <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">$1,608,469<small>.50</small></h4>\n                </div>\n              </div>\n          </div>\n   \n   </div>\n</div> -->\n   <div class=\"box-header px-3 pb-3 d-flex align-items-center justify-content-between\">\n     <div class=\"form-inline \">\n       <div class=\"input-group input-group-sm mr-3\">\n         <input type=\"text\" class=\"form-control\" [value]=\"TUTr_Config.SearchParameter\" maxlength=\"100\"\n           class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n           (keyup)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n         <div class=\"input-group-append\">\n           <button class=\"btn btn-outline-light\" type=\"button\"\n             (click)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n               data-feather=\"search\"></i> </button>\n         </div>\n       </div>\n       <span class=\"badge badge-pill badge-primary\"\n         *ngFor=\"let TItem of TUTr_Config.Filters\">{{TItem.Title}}</span>\n     </div>\n     <nav class=\"nav nav-with-icon tx-20\">\n       <div class=\"dropdown dropleft\">\n         <a href=\"\" class=\"nav-link mr-2\" id=\"TUTr_sdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n           aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n         <div class=\"dropdown-menu tx-13\">\n           <form class=\"wd-250 pd-15\">\n             <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n               <div class=\"float-right tx-20\" *ngIf=\"TUTr_Config.Sort.SortOrder == 'desc'\"\n                 (click)=\"TUTr_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                 <i class=\"fas fa-sort-alpha-down-alt\"></i>\n               </div>\n               <div class=\"float-right tx-20\" *ngIf=\"TUTr_Config.Sort.SortOrder == 'asc'\"\n                 (click)=\"TUTr_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                 <i class=\"fas fa-sort-alpha-up-alt\"></i>\n               </div>\n             </h6>\n             <a class=\"dropdown-item\" *ngFor=\"let TItem of TUTr_Config.Sort.SortOptions\"\n               (click)=\"TUTr_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n               {{TItem.Name}}\n             </a>\n           </form>\n         </div>\n       </div>\n       <div class=\"dropdown dropleft\">\n         <a href=\"\" class=\"nav-link\" id=\"TUTr_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n           aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n           <div class=\"dropdown-menu pd-0 tx-13\">\n            <form class=\"wd-300 \">\n              <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse  pd-15 pd-b-20 pd-l-0 pd-t-0 bd bd-b-1\">Filters\n              </h6>\n              <div class=\"pd-25\" style=\"overflow: auto; height: 80vh;\">\n              <div>\n                <div class=\"form-group\">\n                  <label>Date</label>\n                  <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                    [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                    (selected)=\"TUTr_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n                </div>\n              </div>\n              <div>\n                <div class=\"form-group\">\n                  <label>Status</label>\n                  <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                    [data]=\"TUTr_Config.StatusOptions\" [value]=\"TUTr_Config.Status\"\n                    (valueChanged)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                  </select2>\n                </div>\n              </div>\n              <div>\n               <div class=\"form-group\">\n                 <label>Merchant</label>\n                 <div class=\"form-group\">\n                  <select2 class=\"full-width\" [options]=\"TUTr_Filter_Merchant_Option\"\n                    (valueChanged)=\"TUTr_Filter_Merchants_Change($event)\">\n                  </select2>\n                </div>\n               </div>\n             </div>\n              <div>\n                <div class=\"form-group\">\n                  <label>Store</label>\n                  <div class=\"form-group\">\n                   <select2 class=\"full-width\" [options]=\"TUTr_Filter_Store_Option\"\n                     (valueChanged)=\"TUTr_Filter_Stores_Change($event)\">\n                   </select2>\n                 </div>\n                </div>\n              </div>\n              <div>\n               <div class=\"form-group\">\n                 <label>Card Bank</label>\n                 <select2 class=\"full-width\" [options]=\"TUTr_Filter_CardBank_Option\"\n                   (valueChanged)=\"TUTr_Filter_CardBanks_Change($event)\">\n                 </select2>\n               </div>\n              </div>\n              <div>\n               <div class=\"form-group\">\n                 <label>Transaction Type</label>\n                 <select2 class=\"full-width\" [options]=\"TUTr_Filter_TransactionType_Option\"\n                   (valueChanged)=\"TUTr_Filter_TransactionTypes_Change($event)\">\n                 </select2>\n               </div>\n              </div>\n              <div>\n               <div class=\"form-group\">\n                 <label>Card Brand</label>\n                 <select2 class=\"full-width\" [options]=\"TUTr_Filter_CardBrand_Option\"\n                   (valueChanged)=\"TUTr_Filter_CardBrands_Change($event)\">\n                 </select2>\n               </div>\n              </div>\n              <div class=\"mg-b-70\">\n               <div class=\"form-group\">\n                 <label for=\"\"> Provider</label>\n                 <select2 class=\"full-width\" [options]=\"TUTr_Filter_Provider_Option\"\n                   (valueChanged)=\"TUTr_Filter_Providers_Change($event)\">\n                 </select2>\n               </div>\n              </div>\n </div>\n \n              <div class=\"aside-log  bg-white bd wd-100p pd-15 mg-0\" style=\"position: fixed; bottom:0%; margin-top: -2.5em ;\"> \n              <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                (click)=\"TUTr_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                Reset</button>\n              <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                (click)=\"TUTr_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n               </div>\n            </form>\n          </div>\n       </div>\n       <a class=\"nav-link ml-2 RefreshColor\"\n         (click)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n           data-feather=\"refresh-cw\"></i> </a>\n     </nav>\n   </div>\n   <div class=\"card-body p-0\">\n     <table class=\"{{ _HelperService.AppConfig.TablesConfig.DefaultClass }}\">\n       <thead>\n         <tr>\n           <th class=\"\">Status</th>\n           <th class=\"td-date\">Date</th>\n           <th>  Customer</th>\n           <th>Card Details</th>\n           <th class=\"td-number\">Ref Id</th>\n           <th class=\"text-right\">Sale and Reward</th>\n\n           <th class=\"td-amount text-right\">Amount</th>\n           <th class=\"\">Merchant</th>\n           <th>POS</th>\n         </tr>\n       </thead>\n       <tbody>\n         <tr *ngFor=\"\n               let TItem of (TUTr_Config.Data\n                 | paginate\n                   : {\n                       id: 'TUTr_Paginator',\n                       itemsPerPage: TUTr_Config.PageRecordLimit,\n                       currentPage: TUTr_Config.ActivePage,\n                       totalItems: TUTr_Config.TotalRecords\n                     })\n             \">\n <td>\n   <div class=\"\">\n     <span class=\"text-lowercase\" [ngClass]=\"TItem.StatusBadge\">{{ TItem.StatusName || \"--\" }}</span>\n   </div>\n </td>\n \n           <td class=\"td-date\">\n             <div>{{ TItem.TransactionDateD || \"--\" }}\n               <span class=\"\">{{ TItem.TransactionDateT || \"--\" }}</span>\n             </div>\n            \n           </td>\n           <td>\n             <div class=\"\">\n               {{ TItem.UserDisplayName || \"--\" }}\n             </div>\n             <div class=\"tx-12 text-muted\">\n               {{ TItem.UserMobileNumber || \"--\" }}\n             </div>\n           </td>\n           <td>\n             <div class=\"tx-14 text-uppercase\" *ngIf=\"TItem.TypeName != 'Cash '\">\n              {{ TItem.CardBrandName || \"--\" }}\n               <span *ngIf=\"TItem.CardBrandName != undefined\">\n                 <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'visa'\"\n                   class=\"fab fa-cc-visa tx-teal text-sm \"></i>\n                 <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'mastercard'\"\n                   class=\"fab fa-cc-mastercard tx-MasterCard text-sm  \"></i>\n                 <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'verve'\"\n                   class=\"fas fa-credit-card tx-verve text-sm  \"></i>\n               </span>\n             </div>\n             <div class=\"  text-uppercase\" *ngIf=\"TItem.TypeName == 'Cash '\">\n               Cash Payment\n \n               <i class=\"fab fas fa-money-bill-alt text-cash  tx-primary\"></i>\n             </div>\n             <div class=\"tx-12 text-muted\">\n             <span class=\"\">  {{ TItem.AccountNumber  }}</span>\n             </div>\n           </td>\n           <td>\n             <div class=\"text-muted\">\n               {{ TItem.ReferenceId  }}\n             </div>\n           </td>\n           <td>\n              <div class=\"tx-14\"> {{TItem.Name || \"--\"}}</div>\n              <div class=\"tx-12 text-teal\"> N9000 </div>\n          </td>\n           <td class=\"text-right\">\n             <div>\n               <span [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>\n               {{ TItem.InvoiceAmount | number: \"1.2-2\" || \"--\" }}\n             </div>\n           </td>\n \n           <td class=\"\">\n             <div>{{ TItem.ParentDisplayName || \"--\" }}</div>\n             <div class=\"tx-12 text-muted\">\n               {{ TItem.SubParentDisplayName || \"--\" }}\n             </div>\n           </td>\n \n           <td>\n             <div class=\"tx-bold\">{{ TItem.CreatedByDisplayName || \"--\" }}\n             </div>\n             <div class=\"tx-12 text-muted\">\n               {{ TItem.AcquirerDisplayName || \"--\" }}\n               <span>- {{ TItem.ProviderDisplayName || \"--\" }}</span>\n             </div>\n           </td>\n         </tr>\n       </tbody>\n     </table>\n   </div>\n   <div class=\"card-footer pd-20\">\n     <span class=\"mr-3\">\n       {{ 'Common.Showing' | translate }}\n       <span class=\"bold\">{{TUTr_Config.ShowingStart}} {{ 'Common.To' | translate }}\n         {{TUTr_Config.ShowingEnd}}\n       </span> {{ 'Common.Of' | translate }} {{TUTr_Config.TotalRecords}}\n     </span>\n     <span>\n       {{ 'Common.Show' | translate }}\n       <select class=\"wd-40 \" [(ngModel)]=\"TUTr_Config.PageRecordLimit\"\n         (ngModelChange)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n         <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n           {{TItem}}\n         </option>\n       </select>\n     </span>\n     <span class=\"float-right\">\n       <pagination-controls id=\"TUTr_Paginator\" class=\"pagination pagination-space\"\n         (pageChange)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n         nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n       </pagination-controls>\n     </span>\n   </div>\n </div>"

/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/sales/tusale.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/sales/tusale.component.ts ***!
  \********************************************************************/
/*! exports provided: TUSaleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUSaleComponent", function() { return TUSaleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_5__);






var TUSaleComponent = /** @class */ (function () {
    function TUSaleComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this.TUTr_Filter_Merchant_Selected = 0;
        this.TUTr_Filter_Store_Toggle = false;
        this.TUTr_Filter_Store_Selected = 0;
        this.TUTr_Filter_UserAccount_Selected = 0;
        this.TUTr_Filter_Bank_Selected = 0;
        this.TUTr_Filter_Provider_Selected = 0;
        this.TUTr_Filter_Issuer_Selected = 0;
        this.TUTr_Filter_TransactionType_Selected = 0;
        this.TUTr_Filter_CardBrand_Selected = 0;
        this.TUTr_Filter_CardBank_Selected = 0;
    }
    TUSaleComponent.prototype.ngOnInit = function () {
        var _this = this;
        feather_icons__WEBPACK_IMPORTED_MODULE_5__["replace"]();
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            _this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null || _this._HelperService.AppConfig.ActiveReferenceId == null) {
                _this._Router.navigate([_this._HelperService.AppConfig.Pages.System.NotFound]);
            }
            else {
                //this._HelperService.Get_UserAccountDetails(true);
                _this.TUTr_Setup();
                //#region DropdownInits 
                _this.TUTr_Filter_UserAccounts_Load();
                _this.TUTr_Filter_Banks_Load();
                _this.TUTr_Filter_Providers_Load();
                _this.TUTr_Filter_Issuers_Load();
                _this.TUTr_Filter_CardBrands_Load();
                _this.TUTr_Filter_TransactionTypes_Load();
                _this.TUTr_Filter_CardBanks_Load();
                _this.TUTr_Filter_Stores_Load();
                _this.TUTr_Filter_Merchants_Load();
                //#endregion
            }
        });
    };
    TUSaleComponent.prototype.TUTr_Setup = function () {
        this.TUTr_Config =
            {
                Id: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Sales History',
                StatusType: 'transaction',
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                Type: this._HelperService.AppConfig.ListType.All,
                Sort: {
                    SortDefaultName: null,
                    SortDefaultColumn: 'TransactionDate',
                    SortName: null,
                    SortColumn: null,
                    SortOrder: 'desc',
                    SortOptions: []
                },
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Transaction Status',
                        SystemName: 'StatusName',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'User',
                        SystemName: 'UserDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Mobile Number',
                        SystemName: 'UserMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Invoice Amount',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Reward Amount',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Reward Amount',
                        SystemName: 'UserAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Commission Amount',
                        SystemName: 'CommissionAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Card Number',
                        SystemName: 'AccountNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Type',
                        SystemName: 'CardBrandName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Bank Provider',
                        SystemName: 'CardBankName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'TUC Card Number',
                        SystemName: 'TUCCardNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Merchant',
                        SystemName: 'ParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Acquirer / Bank',
                        SystemName: 'AcquirerDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Terminal Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Issuer (Done by)',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Reference',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                ]
            };
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    };
    TUSaleComponent.prototype.TUTr_ToggleOption_Date = function (event, Type) {
        this.TUTr_ToggleOption(event, Type);
    };
    TUSaleComponent.prototype.TUTr_ToggleOption = function (event, Type) {
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (this.TUTr_Config.RefreshData == true) {
            this.TUTr_GetData();
        }
    };
    TUSaleComponent.prototype.TUTr_GetData = function () {
        var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    };
    TUSaleComponent.prototype.TUTr_RowSelected = function (ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    };
    TUSaleComponent.prototype.TUTr_Filter_Merchants_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_Merchants_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.TUTr_Config, this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant);
        this.MerchantEventProcessing(event);
    };
    TUSaleComponent.prototype.MerchantEventProcessing = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
            _this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    };
    TUSaleComponent.prototype.TUTr_Filter_Stores_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Store
                // }
            ]
        };
        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_Stores_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.TUTr_Config, this._HelperService.AppConfig.OtherFilters.MerchantSales.Store);
        this.StoresEventProcessing(event);
    };
    TUSaleComponent.prototype.StoresEventProcessing = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
        }, 500);
    };
    TUSaleComponent.prototype.TUTr_Filter_UserAccounts_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_UserAccounts_Change = function (event) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUSaleComponent.prototype.TUTr_Filter_Banks_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Search By Bank / Acquirer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_Banks_Change = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Bank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
        }, 500);
    };
    TUSaleComponent.prototype.TUTr_Filter_Providers_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_Providers_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.TUTr_Config, this._HelperService.AppConfig.OtherFilters.MerchantSales.Provider);
        this.ProvidersEventProcessing(event);
    };
    TUSaleComponent.prototype.ProvidersEventProcessing = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
        }, 500);
    };
    TUSaleComponent.prototype.TUTr_Filter_Issuers_Load = function () {
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text, [
            this._HelperService.AppConfig.AccountType.PosTerminal,
            this._HelperService.AppConfig.AccountType.PGAccount,
            this._HelperService.AppConfig.AccountType.Cashier,
        ], "=");
        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        if (this.TUTr_Filter_Bank_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Bank_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_Issuers_Change = function (event) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUSaleComponent.prototype.TUTr_Filter_TransactionTypes_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_TransactionTypes_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.TUTr_Config, this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType);
        this.TransTypeEventProcessing(event);
    };
    TUSaleComponent.prototype.TransTypeEventProcessing = function (event) {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUSaleComponent.prototype.TUTr_Filter_CardBrands_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_CardBrands_Change = function (event) {
        this.CardBrandEventProcessing(event);
    };
    TUSaleComponent.prototype.CardBrandEventProcessing = function (event) {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUSaleComponent.prototype.TUTr_Filter_CardBanks_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUSaleComponent.prototype.TUTr_Filter_CardBanks_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.TUTr_Config, this._HelperService.AppConfig.OtherFilters.Merchant.Owner);
        this.CardBankEventProcessing(event);
    };
    TUSaleComponent.prototype.CardBankEventProcessing = function (event) {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUSaleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tu-sale',
            template: __webpack_require__(/*! ./tusale.component.html */ "./src/app/panel/acquirer/campaigns/sales/tusale.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_4__["DataHelperService"]])
    ], TUSaleComponent);
    return TUSaleComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/sales/tusale.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/sales/tusale.module.ts ***!
  \*****************************************************************/
/*! exports provided: TUSaleRoutingModule, TUSaleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUSaleRoutingModule", function() { return TUSaleRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUSaleModule", function() { return TUSaleModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var _tusale_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./tusale.component */ "./src/app/panel/acquirer/campaigns/sales/tusale.component.ts");












var routes = [{ path: "", component: _tusale_component__WEBPACK_IMPORTED_MODULE_11__["TUSaleComponent"] }];
var TUSaleRoutingModule = /** @class */ (function () {
    function TUSaleRoutingModule() {
    }
    TUSaleRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUSaleRoutingModule);
    return TUSaleRoutingModule;
}());

var TUSaleModule = /** @class */ (function () {
    function TUSaleModule() {
    }
    TUSaleModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                ng5_slider__WEBPACK_IMPORTED_MODULE_10__["Ng5SliderModule"],
                TUSaleRoutingModule,
            ],
            declarations: [_tusale_component__WEBPACK_IMPORTED_MODULE_11__["TUSaleComponent"]]
        })
    ], TUSaleModule);
    return TUSaleModule;
}());



/***/ })

}]);
//# sourceMappingURL=panel-acquirer-campaigns-sales-tusale-module.js.map