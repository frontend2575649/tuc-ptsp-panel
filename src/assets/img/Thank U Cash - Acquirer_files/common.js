(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/service/main-pipe.module.ts":
/*!*********************************************!*\
  !*** ./src/app/service/main-pipe.module.ts ***!
  \*********************************************/
/*! exports provided: MainPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPipe", function() { return MainPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _truncate_text_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./truncate-text.pipe */ "./src/app/service/truncate-text.pipe.ts");




var MainPipe = /** @class */ (function () {
    function MainPipe() {
    }
    MainPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_truncate_text_pipe__WEBPACK_IMPORTED_MODULE_3__["TruncateTextPipe"], _truncate_text_pipe__WEBPACK_IMPORTED_MODULE_3__["CampaignTypePipe"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [_truncate_text_pipe__WEBPACK_IMPORTED_MODULE_3__["TruncateTextPipe"], _truncate_text_pipe__WEBPACK_IMPORTED_MODULE_3__["CampaignTypePipe"]]
        })
    ], MainPipe);
    return MainPipe;
}());



/***/ }),

/***/ "./src/app/service/truncate-text.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/service/truncate-text.pipe.ts ***!
  \***********************************************/
/*! exports provided: TruncateTextPipe, CampaignTypePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TruncateTextPipe", function() { return TruncateTextPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignTypePipe", function() { return CampaignTypePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TruncateTextPipe = /** @class */ (function () {
    function TruncateTextPipe() {
    }
    TruncateTextPipe.prototype.transform = function (value, length) {
        var biggestWord = 50;
        var elipses = "...";
        if (typeof value === "undefined")
            return value;
        if (value.length <= length)
            return value;
        //.. truncate to about correct lenght
        var truncatedText = value.slice(0, length + biggestWord);
        //.. now nibble ends till correct length
        while (truncatedText.length > length - elipses.length) {
            var lastSpace = truncatedText.lastIndexOf(" ");
            if (lastSpace === -1)
                break;
            truncatedText = truncatedText.slice(0, lastSpace).replace(/[!,.?;:]$/, '');
        }
        ;
        return truncatedText + elipses;
    };
    TruncateTextPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'truncateText'
        })
    ], TruncateTextPipe);
    return TruncateTextPipe;
}());

var CampaignTypePipe = /** @class */ (function () {
    function CampaignTypePipe() {
    }
    CampaignTypePipe.prototype.transform = function (value) {
        if (typeof value === "undefined")
            return value;
        var truncatedText = '';
        if (value == 'Own Card Customers') {
            truncatedText = 'Card';
        }
        else if (value == 'Other Card Customers') {
            truncatedText = 'POS';
        }
        return truncatedText;
    };
    CampaignTypePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'campaignText'
        })
    ], CampaignTypePipe);
    return CampaignTypePipe;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map