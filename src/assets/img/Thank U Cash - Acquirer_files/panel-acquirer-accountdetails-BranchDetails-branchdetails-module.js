(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-acquirer-accountdetails-BranchDetails-branchdetails-module"],{

/***/ "./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--branch--details -->\n<div id=\"accordion\">\n    <div class=\"card shadow-none gredientBorder\">\n        <div class=\"card-header bd-b-0\" id=\"headingOne\">\n\n            <div class=\"row \">\n\n                <div class=\"col-sm-12 col-md-12 col-lg-1 d-flex\">\n                    <div class=\"media m-auto align-sm-items-center\">\n                        <div class=\"avatar avatar-xl    avatar-online\"><img\n                                src=\"{{ _HelperService.UserAccount.IconUrl }}\" class=\"rounded-circle \" alt=\"\"> </div>\n                    </div><!-- media -->\n                </div>\n\n                <div class=\"col-sm-12 col-md-12 col-lg-5 d-md-flex d-lg-flex Hm-AlignCenter mg-t-15  mg-lg-t-0\">\n                    <div class=\"media  Hm-textCenterMd\">\n                        <div class=\"media-body\">\n                            <h6 class=\"tx-20 tx-lg-24 tx-bold text-capitalize mg-b-2\">\n                                {{_BranchDetails.DisplayName}}</h6>\n                            <div class=\"d-md-flex d-lg-flex Hm-mg-y Hm-AlignCenter align-items-baseline\">\n                                <h2 class=\"tx-14 tx-lg-14 tx-color-03 tx-normal tx-rubik lh-2 mg-b-5\">\n                                    <span class=\"tx-MasterCard mg-r-5\"> <i style=\"height: 18px; width: 18px;;\"\n                                        data-feather=\"codesandbox\"></i> </span>Branch\n                                    Id : {{_BranchDetails.BranchCode}}</h2>\n                            </div>\n                            <div class=\"d-md-flex d-lg-flex Hm-AlignCenter align-items-baseline\">\n                                <h2\n                                    class=\"tx-14 tx-lg-14 tx-color-03 tx-normal lh-2 mg-b-0 d-flex\">\n                                    <span class=\"text-danger mg-r-5\"> <i style=\"height: 18px; width: 18px;;\"\n                                            data-feather=\"map-pin\"></i> </span> <span> {{_BranchDetails.Address}}</span>\n                                </h2>\n                            </div>\n                        </div><!-- media-body -->\n                    </div><!-- media -->\n                </div>\n                <div class=\"col-lg-6 mg-t-15  mg-lg-t-0 \" id=\"StoresHide\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                            <div class=\"row flex-nowrap\">\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n\n                                    <div class=\" text-center stores\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\"tx-bold tx-40  tx-rubik tx-verve mg-b-5 mg-r-5 lh-1\">\n                                                {{_BranchDetails.Stores}}</h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0 \"> Stores</p>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n                                    <div class=\" text-center Terminals\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\"tx-bold tx-40 tx-rubik tx-teal mg-b-5 mg-r-5 lh-1 \">\n                                                {{_BranchDetails.Terminals}}</h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0\"> Pos Terminal</p>\n\n                                    </div>\n\n                                </div>\n\n\n                            </div>\n                        </div>\n                        <div class=\"col-sm-12 col-md-6 col-lg-6 col-lg-6 d-flex mg-t-15  mg-lg-t-0 justify-content-end\">\n                            <div class=\"row\">\n                                <div class=\"manageButton mg-t-0 pd-t-0\">\n                                    <button (click)=\"EditBranch()\" type=\"button\"\n                                        class=\"btn btn-xs btn-transparent tx-color-03 bd bd-secondary rounded-pill\">Manage\n                                        Account</button>\n                                </div>\n\n                                <div class=\"collapseButton\">\n                                    <button [class.button-open]=\"!slideOpen\" [class.button-close]=\"slideOpen\"\n                                        (click)=\"changeSlide() ; _HideStoreDetail()\" class=\"btn  btn-link\"\n                                        data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"false\"\n                                        aria-controls=\"collapseOne\">\n                                        <div class=\"tx-15 tx-lg-18 lh-0 tx-color-03\"><i class=\"fas fa-chevron-down\"\n                                                [class.clicked]=\"slideOpen\"></i>\n                                        </div>\n                                    </button>\n                                </div>\n\n                            </div>\n\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n\n        <div id=\"collapseOne\" class=\"collapse\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">\n            <div class=\"card-body pt-0\">\n                <div class=\"row\">\n\n                    <div class=\"col-lg-1\">\n\n                    </div>\n                    <div class=\"col-lg-11\">\n                        <div class=\"row\">\n\n\n                            <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                                <!-- <agm-map class=\"Hm-height ht-100p\" [zoom]=\"14\"\n                                    [latitude]=\"_HelperService._UserAccount.Latitude\"\n                                    [longitude]=\"_HelperService._UserAccount.Longitude\">\n                                    <agm-marker [latitude]=\"_HelperService._UserAccount.Latitude\"\n                                        [longitude]=\"_HelperService._UserAccount.Longitude\"></agm-marker>\n                                </agm-map> -->\n                                <div style=\"z-index: 3 !important;\" class=\"Hm-height   ht-100p\" leaflet *ngIf=\"_ShowMap\"\n                                    [leafletOptions]=\"_HelperService.lefletoptions\"\n                                    [leafletLayers]=\"_HelperService.layers\"\n                                    (leafletMapReady)=\"_HelperService.onMapReady($event,false)\">\n                                </div>\n                            </div>\n\n                            <div class=\"col-sm-12 col-md-6  col-lg-6\">\n                                <div class=\"row\">\n                                    <div class=\"col-sm-12 col-md-6  col-lg-6 mg-t-15  mg-lg-t-0\">\n                                        <label\n                                            class=\"tx-14 tx-bold text-capitalize Hm-HeaderColor mg-b-15\">Contact\n                                            information</label>\n                                        <ul class=\"list-unstyled profile-info-list\">\n                                            <li class=\"tx-color-03\"><i data-feather=\"user\"></i> <span\n                                                    class=\"tx-color-03\">{{_BranchDetails.Name || \"--\"}}</span></li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"smartphone\"></i>\n                                                <a>{{_BranchDetails.PhoneNumber}}</a></li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"mail\"></i>\n                                                <a>{{_BranchDetails.EmailAddress}}</a>\n                                            </li>\n                                        </ul>\n\n                                    </div>\n                                    <div class=\"col-sm-12 col-md-6  col-lg-6 mg-t-15  mg-lg-t-0\">\n                                        <label\n                                            class=\"tx-14 tx-bold text-capitalize Hm-HeaderColor mg-b-15\">\n                                            Manager</label>\n                                        <ul class=\"list-unstyled profile-info-list\">\n                                            <li class=\"tx-color-03\"><i data-feather=\"user\"></i> <span\n                                                    class=\"tx-color-03\">{{_BranchDetails.ManagerName}}</span></li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"smartphone\"></i> <a\n                                                    class=\"tx-color-03\">{{_BranchDetails.ManagerMobileNumber || \"--\"}}</a>\n                                            </li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"mail\"></i>\n                                                <a>{{_BranchDetails.ManagerEmailAddress || \"--\"}}</a>\n                                            </li>\n                                        </ul>\n\n                                    </div>\n                                    <!-- <div class=\"col-lg-12 mg-t-15  mg-lg-t-0\">\n                                        <div class=\"d-flex\">\n                                            <button type=\"button\"\n                                                class=\"btn btn-xs  tx-color-03 btn-transparent bd bd-primary rounded-pill mg-r-10\">Food</button>\n                                            <button type=\"button\"\n                                                class=\"btn btn-xs  tx-color-03 btn-transparent bd bd-primary rounded-pill mg-r-10\">Drinks</button>\n                                            <button type=\"button\"\n                                                class=\"btn btn-xs  tx-color-03 btn-transparent bd bd-primary rounded-pill mg-r-10\">Grocessory</button>\n                                            <button type=\"button\"\n                                                class=\"btn  btn-xs tx-color-03  btn-transparent bd bd-primary rounded-pill mg-r-10\">Drinks</button>\n\n\n                                        </div>\n                                    </div> -->\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row mg-t-15  mg-lg-t-0 d-block d-sm-none\">\n                    <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                        <div class=\"row flex-nowrap\">\n\n                            <div class=\"col-sm-6 col-md-6 col-lg-6\">\n\n                                <div class=\" text-center stores\">\n                                    <div class=\"text-center\">\n                                        <h3 class=\"tx-bold tx-40  tx-rubik tx-verve mg-b-5 mg-r-5 lh-1\">\n                                            {{_BranchDetails.Stores}}</h3>\n                                    </div>\n                                    <p class=\"tx-12 tx-color-03 mg-b-0 \"> Stores</p>\n                                </div>\n                            </div>\n\n                            <div class=\"col-sm-6 col-md-6 col-lg-6\">\n                                <div class=\" text-center Terminals\">\n                                    <div class=\"text-center\">\n                                        <h3 class=\"tx-bold tx-40 tx-rubik tx-teal mg-b-5 mg-r-5 lh-1 \">\n                                            {{_BranchDetails.Terminals}}</h3>\n                                    </div>\n                                    <p class=\"tx-12 tx-color-03 mg-b-0\"> Pos Terminal</p>\n\n                                </div>\n\n                            </div>\n\n\n                        </div>\n                    </div>\n                    <div class=\"col-sm-12 col-md-6 col-lg-6 col-lg-6 d-flex mg-t-15  mg-lg-t-0 justify-content-end\">\n                        <div class=\"row\">\n                            <div class=\"manageButton mg-t-0 pd-t-0\">\n                                <button (click)=\"EditBranch()\" type=\"button\"\n                                    class=\"btn btn-xs btn-transparent tx-color-03 bd bd-secondary rounded-pill\">Manage\n                                    Account</button>\n                            </div>\n\n                            <div class=\"collapseButton\">\n                                <button [class.button-open]=\"!slideOpen\" [class.button-close]=\"slideOpen\"\n                                    (click)=\"changeSlide() ; _ShowStoreDetails()\" class=\"btn  btn-link\" data-toggle=\"collapse\"\n                                    data-target=\"#collapseOne\" aria-expanded=\"false\" aria-controls=\"collapseOne\">\n                                    <div class=\"tx-15 tx-lg-18 lh-0 tx-color-03\"><i class=\"fas fa-chevron-down\"\n                                            [class.clicked]=\"slideOpen\"></i>\n                                    </div>\n                                </button>\n                            </div>\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"card bd-b-0 pd-20 mt-3\">\n    <ul class=\"nav nav-line\" id=\"myTab5\" role=\"tablist\">\n        <!-- <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n            <a class=\"nav-link\" [routerLinkActive]=\"['active']\" routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches.BranchUpdate}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"  >\n                <span class=\"nav-text\">{{\"System.Menu.Overview\" | translate}}</span>\n            </a>\n        </li> \n        <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n            <a class=\"nav-link\" id=\"Transactions-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"  routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Manager.SalesHistory}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\" role=\"tab\"\n                aria-controls=\"Transactions\" aria-selected=\"false\">Transactions</a>\n        </li> -->\n        <li class=\"nav-item cursor-pointer\"  [routerLinkActive]=\"['active']\">\n            <a class=\"nav-link\" id=\"Terminal-tab5\"  [routerLinkActive]=\"['active']\" data-toggle=\"tab\" role=\"tab\" routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches.BranchTerminals}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                aria-controls=\"Terminal\" aria-selected=\"false\">Terminal</a>\n        </li>\n        <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n            <a class=\"nav-link\" id=\"Stores-tab5\" data-toggle=\"tab\" role=\"tab\"  [routerLinkActive]=\"['active']\" routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Manager.Stores}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                aria-controls=\"Stores\" aria-selected=\"false\">Stores</a>\n        </li>\n        <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n            <a class=\"nav-link\" id=\"Team-tab5\" data-toggle=\"tab\" role=\"tab\" [routerLinkActive]=\"['active']\" routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches.Manager}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                aria-controls=\"Team\" aria-selected=\"false\">Team</a>\n        </li>\n\n        <div class=\"ml-auto d-flex\" *ngIf=\"_HelperService.ShowDateRange\">\n            <div class=\"  mg-r-10 order-1\">\n                <input type=\"text\" class=\"form-control tx-13 form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                    [options]=\"_HelperService.AppConfig.DatePickerOptions\"\n                    (selected)=\"_HelperService.SetDateRange($event)\" />\n            </div>\n        </div>\n\n        <div class=\"btn-group\" data-toggle=\"buttons\" *ngIf=\"_HelperService.ShowDateRange\">\n            <label class=\"btn btn-xs pb-0 mb-0 tx-13 btn-white  active d-flex align-items-center\"\n                (click)=\"_HelperService.ToogleRange('A')\">\n                <input type=\"radio\" name=\"options\" style=\"display: none;\" checked>\n                <span class=\"tx-13 mg-b-5\"> Month </span>\n            </label>\n            <label class=\"btn btn-xs  pb-0 mb-0 tx-13 btn-white d-flex align-items-center\"\n                (click)=\"_HelperService.ToogleRange('B')\">\n                <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n                <span class=\"tx-13 mg-b-5\">Week</span>\n            </label>\n            <label class=\"btn pb-0 mb-0 btn-xs tx-13 btn-white  d-flex align-items-center\"\n                (click)=\"_HelperService.ToogleRange('C')\">\n                <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n                <span class=\"tx-13 mg-b-5\"> Day </span>\n            </label>\n        </div>\n\n    </ul>\n</div>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.ts ***!
  \****************************************************************************************/
/*! exports provided: BranchDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchDetailsComponent", function() { return BranchDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");






var BranchDetailsComponent = /** @class */ (function () {
    function BranchDetailsComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._ShowMap = false;
        //#region MapCorrection 
        this.slideOpen = false;
        //#endregion
        //#region BranchDetails 
        this._BranchDetails = {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        };
        this._DateDiff = {};
    }
    BranchDetailsComponent.prototype.ngOnInit = function () {
        //#region StorageDetail 
        var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveBranch);
        if (StorageDetails != null) {
            this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
            this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
            this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
            this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
        }
        //#endregion
        this._ShowMap = true;
        this._HelperService._InitMap();
        //#region UIInit 
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Acquirer;
        this._HelperService.ContainerHeight = window.innerHeight;
        //#endregion
        this.GetBranchDetails();
    };
    BranchDetailsComponent.prototype.changeSlide = function () {
        this.slideOpen = !this.slideOpen;
        if (this.slideOpen) {
            this._HelperService._MapCorrection();
        }
    };
    //#endregion
    //#region ToogleStoreDetail 
    BranchDetailsComponent.prototype._HideStoreDetail = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-HideDiv");
        element.classList.remove("Hm-DisplayDiv");
    };
    BranchDetailsComponent.prototype._ShowStoreDetails = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-DisplayDiv");
        element.classList.remove("Hm-HideDiv");
    };
    BranchDetailsComponent.prototype.EditBranch = function () {
        this._HelperService.AppConfig.ActiveReferenceKey = this._BranchDetails.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = this._BranchDetails.ReferenceId;
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches.EditBranch, this._BranchDetails.ReferenceKey, this._BranchDetails.ReferenceId]);
    };
    BranchDetailsComponent.prototype.GetBranchDetails = function () {
        var _this = this;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetBranch,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.IsFormProcessing = false;
                _this._BranchDetails = _Response.Result;
                if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                    _this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                    _this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                }
                else {
                    _this._HelperService._UserAccount.Longitude = _this._HelperService._UserAccount.Latitude;
                    _this._HelperService._UserAccount.Longitude = _this._HelperService._UserAccount.Longitude;
                }
                _this._HelperService._ReLocate();
                _this._DateDiff = _this._HelperService._DateRangeDifference(_this._BranchDetails.StartDate, _this._BranchDetails.EndDate);
                _this._BranchDetails.StartDateS = _this._HelperService.GetDateS(_this._BranchDetails.StartDate);
                _this._BranchDetails.EndDateS = _this._HelperService.GetDateS(_this._BranchDetails.EndDate);
                _this._BranchDetails.CreateDateS = _this._HelperService.GetDateTimeS(_this._BranchDetails.CreateDate);
                _this._BranchDetails.ModifyDateS = _this._HelperService.GetDateTimeS(_this._BranchDetails.ModifyDate);
                _this._BranchDetails.StatusI = _this._HelperService.GetStatusIcon(_this._BranchDetails.StatusCode);
                _this._BranchDetails.StatusB = _this._HelperService.GetStatusBadge(_this._BranchDetails.StatusCode);
                _this._BranchDetails.StatusC = _this._HelperService.GetStatusColor(_this._BranchDetails.StatusCode);
            }
            else {
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.HandleException(_Error);
        });
    };
    BranchDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-branchdetails",
            template: __webpack_require__(/*! ./branchdetails.component.html */ "./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"]])
    ], BranchDetailsComponent);
    return BranchDetailsComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.module.ts ***!
  \*************************************************************************************/
/*! exports provided: BranchDeatilsRoutingModule, BranchDetailsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchDeatilsRoutingModule", function() { return BranchDeatilsRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchDetailsModule", function() { return BranchDetailsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _branchdetails_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./branchdetails.component */ "./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.component.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");













var routes = [
    {
        path: "",
        component: _branchdetails_component__WEBPACK_IMPORTED_MODULE_11__["BranchDetailsComponent"],
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../modules/accounts/tuterminals/branch/tuterminals.module#TUTerminalsModule" },
            { path: "branchupdate/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../panel/acquirer/BranchDivision/dashboard/dashboard.module#BranchUpdateModule" },
            { path: "sales/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", PageName: "System.Menu.Branch", accounttypecode: "merchant" }, loadChildren: "../../../../modules/transactions/tusale/branch/acquirer/tusale.module#TUSaleModule" },
            { path: "manager/:referencekey/:referenceid", data: { permission: "manager", PageName: "System.Menu.Branch" }, loadChildren: "../../../acquirer/BranchDivision/manager/manager.module#ManagerModule" },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Branch' }, loadChildren: '../../../../modules/accounts/tuterminals/branch/tuterminals.module#TUTerminalsModule' },
            { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Branch' }, loadChildren: '../../../../modules/accounts/tustores/branch/tustores.module#TUStoresModule' }
        ]
    }
];
var BranchDeatilsRoutingModule = /** @class */ (function () {
    function BranchDeatilsRoutingModule() {
    }
    BranchDeatilsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], BranchDeatilsRoutingModule);
    return BranchDeatilsRoutingModule;
}());

var BranchDetailsModule = /** @class */ (function () {
    function BranchDetailsModule() {
    }
    BranchDetailsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                BranchDeatilsRoutingModule,
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_12__["LeafletModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_10__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
                }),
            ],
            declarations: [_branchdetails_component__WEBPACK_IMPORTED_MODULE_11__["BranchDetailsComponent"]]
        })
    ], BranchDetailsModule);
    return BranchDetailsModule;
}());



/***/ })

}]);
//# sourceMappingURL=panel-acquirer-accountdetails-BranchDetails-branchdetails-module.js.map