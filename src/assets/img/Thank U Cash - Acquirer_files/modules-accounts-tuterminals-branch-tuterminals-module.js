(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-accounts-tuterminals-branch-tuterminals-module"],{

/***/ "./src/app/modules/accounts/tuterminals/branch/tuterminals.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/modules/accounts/tuterminals/branch/tuterminals.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--branches--details--terminal -->\n<div class=\"row row-xs pb-3\" *ngIf=\"TerminalsList_Config.OriginalResponse != undefined\">\n    <div class=\"col-sm-6 col-lg-3\">\n        <div class=\"card p-3 mb-3 r-1x\" (click)=\"ToggleActivityStatus(2)\">\n            <div class=\"media cursor-pointer\">\n                <h4\n                    class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-success tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6\">\n                    <i class=\"fa fa-play\"></i>\n                </h4>\n                <div class=\"media-body\">\n                    <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1  tx-semibold tx-nowrap mg-md-b-2\">Active</h6>\n                    <p class=\"tx-12 tx-sm-12 tx-md-12 tx-normal tx-color-03 tx-rubik mg-b-0\">Terminals</p>\n                    <div class=\"db-title-count-xl tx-20 tx-bold\">\n                        {{ _AccountOverview.ActiveTerminals  | number: \"1.0-1\" || \"--\" }}\n                    </div>\n                </div>\n            </div>\n            <!-- <div class=\"db-title color-merchant\">\n              <i class=\"fa fa-briefcase db-title-icon\"></i> Merchants\n            </div>\n            <div class=\" db-title-sub color-merchant\">Total merchants\n            </div> -->\n\n        </div>\n    </div>\n    <div class=\"col-sm-6 col-lg-3\">\n        <div class=\"card p-3 mb-3 r-1x\" (click)=\"ToggleActivityStatus(3)\">\n            <div class=\"media cursor-pointer\">\n                <h4\n                    class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-warning tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6\">\n                    <i class=\"fa fa-clock\"></i>\n                </h4>\n                <div class=\"media-body\">\n                    <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1  tx-semibold tx-nowrap mg-md-b-2\">Idle Terminals\n                    </h6>\n                    <p class=\"tx-12 tx-sm-12 tx-md-12 tx-normal tx-color-03 tx-rubik mg-b-0\">Unused for a week</p>\n                    <div class=\"db-title-count-xl tx-20 tx-bold\">\n                        {{ _AccountOverview.IdleTerminals  | number: \"1.0-1\" || \"--\" }}\n                    </div>\n                </div>\n            </div>\n            <!-- <div class=\"db-title color-merchant\">\n              <i class=\"fa fa-briefcase db-title-icon\"></i> Merchants\n            </div>\n            <div class=\" db-title-sub color-merchant\">Total merchants\n            </div> -->\n\n        </div>\n    </div>\n    <div class=\"col-sm-6 col-lg-3\">\n        <div class=\" card p-3 mb-3 r-1x \" (click)=\"ToggleActivityStatus(4)\">\n            <div class=\"media cursor-pointer\">\n                <h4\n                    class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-danger tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6\">\n                    <i class=\"fa fa-ban\"></i>\n                </h4>\n                <div class=\"media-body\">\n                    <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1  tx-semibold tx-nowrap mg-md-b-2\">Dead Terminals\n                    </h6>\n                    <p class=\"tx-12 tx-sm-12 tx-md-12 tx-normal tx-color-03 tx-rubik mg-b-0\">Unused for a week</p>\n                    <div class=\"db-title-count-xl tx-20 tx-bold\">\n                        {{ _AccountOverview.DeadTerminals| number: \"1.0-1\" || \"--\"}}\n                    </div>\n                </div>\n            </div>\n            <!-- <div class=\"db-title color-merchant\">\n              <i class=\"fa fa-briefcase db-title-icon\"></i> Merchants\n            </div>\n            <div class=\" db-title-sub color-merchant\">Total merchants\n            </div> -->\n        </div>\n    </div>\n    <div class=\"col-sm-6 col-lg-3\">\n        <div class=\"card p-3 mb-3 r-1x\" (click)=\"ToggleActivityStatus(1)\">\n            <div class=\"media cursor-pointer\">\n                <h4\n                    class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-primary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6\">\n                    <i class=\"fa fa-exclamation-triangle\"></i>\n                </h4>\n                <div class=\"media-body\">\n                    <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1  tx-semibold tx-nowrap mg-md-b-2\">Unused</h6>\n                    <p class=\"tx-12 tx-sm-12 tx-md-12 tx-normal tx-color-03 tx-rubik mg-b-0\">Software not updated</p>\n                    <div class=\"db-title-count-xl tx-20 tx-bold\">\n                        {{ _AccountOverview.UnusedTerminals| number: \"1.0-1\" || \"--\"}}\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n\n</div>\n\n\n<!-- <div [className]=\"_HelperService._Assets.Box.root\">\n    <div [className]=\"_HelperService._Assets.Box.header\">\n        <div [className]=\"_HelperService._Assets.Box.headerTitle\">\n            {{TerminalsList_Config.Title}} :  \n            <span *ngIf=\"TerminalsList_Config.TotalRecords != undefined\">\n                {{TerminalsList_Config.TotalRecords | number: \"1.0-1\" || \"--\"}}</span>\n           \n        </div>\n        <div [className]=\"_HelperService._Assets.Box.headerTitleRight\">\n            <nav class=\"nav nav-icon-only mg-l-auto\">\n            <a (click)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Csv)\" data-toggle=\"tooltip\" title=\"Download\" class=\"nav-link d-none px-2 d-sm-block iconColor cursor-pointer\"><i data-feather=\"download-cloud\"></i></a>\n            <a (click)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\" data-toggle=\"tooltip\" title=\"Refresh\" class=\"nav-link d-none d-sm-block iconColor cursor-pointer mr-3\"><i data-feather=\"refresh-cw\"></i></a>\n            <button class=\"btn btn-primary  tx-13\" (click)=\"Form_AddUser_Open()\"> Add New </button>\n            </nav>\n            </div>\n    </div>\n    <div [className]=\"_HelperService._Assets.Box.bodyFilter\">\n        <div class=\"form-inline \">\n            <div class=\"input-group mr-2 \">\n                \n                <input type=\"text\" [value]=\"TerminalsList_Config.SearchParameter\" maxlength=\"100\" class=\"form-control\"\n                    placeholder=\"{{ 'Common.Search' | translate }}\"\n                    (keyup)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n                <div class=\"input-group-append\"><button class=\"btn btn-white\" type=\"button\"\n                        (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.Search)\">{{\n                                'Common.Go' | translate }}</button></div>\n            </div>\n            <div class=\"form-group mr-2 \">\n                <input type=\"text\" class=\"form-control  form-daterangepicker mr-2\" name=\"daterangeInput\" daterangepicker\n                    [options]=\"_HelperService.AppConfig.DatePickerOptions\"\n                    (selected)=\"TerminalsList_ToggleOptionPre($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n            </div>\n\n            <div class=\"form-group mr-2 \">\n                <select2 [options]=\"TerminalsList_Filter_Store_Option\"\n                    (valueChanged)=\"TerminalsList_Filter_Stores_Change($event)\">\n                </select2>\n            </div>\n            <div class=\"form-group mr-2 \">\n                <select2 [options]=\"TerminalsList_Filter_Provider_Option\"\n                    (valueChanged)=\"TerminalsList_Filter_Stores_Change($event)\">\n                </select2>\n            </div>\n\n        </div>\n        <div class=\"mt-2\" *ngIf=\"TerminalsList_Config.ToggleFilter\">\n\n        </div>\n        <div class=\"table-responsive mt-3\">\n            <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n                <thead>\n                    <tr>\n                        <th class=\"td-status\">\n                        </th>\n                        <th *ngFor=\"let TItem of TerminalsList_Config.VisibleHeaders\" class=\"{{TItem.Class}}\">\n                            {{TItem.DisplayName}}\n                        </th>\n                        <th class=\"td-status\">\n                            Status\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr class=\"cursor-pointer\" (click)=\"TerminalsList_RowSelected(TItem)\" *ngFor=\"let TItem of TerminalsList_Config.Data\n                                                            | paginate: { id: 'TerminalsList',itemsPerPage: TerminalsList_Config.PageRecordLimit, currentPage: TerminalsList_Config.ActivePage,\n                                                            totalItems: TerminalsList_Config.TotalRecords }\">\n\n                        <td class=\"td-status \">\n                            <img class=\"br\" src=\"{{TItem.IconUrl}}\" class=\"avatar avatar-sm\"\n                                [style.border-color]=\"TItem.StatusC\">\n                        </td>\n                        <td *ngFor=\"let HTItem of TerminalsList_Config.VisibleHeaders\" class=\" {{HTItem.Class}} rowvertical\"\n                            title=\"{{TItem[HTItem.SystemName]}}\">\n                            <span *ngIf=\"HTItem.NavigateLink != undefined\">\n                                <span *ngIf=\"HTItem.NavigateLink != undefined && HTItem.NavigateField == undefined\">\n                                    <a routerLink=\"{{HTItem.NavigateLink}}\"> {{TItem[HTItem.SystemName] || \"--\"}}</a>\n                                </span>\n                                <span *ngIf=\"HTItem.NavigateField != undefined\">\n                                    <a routerLink=\"{{HTItem.NavigateLink}}/{{TItem[HTItem.NavigateField]}}\">\n                                        <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Text\">\n                                            {{TItem[HTItem.SystemName] || \"--\"}}\n                                        </span>\n                                        <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Number\">\n                                            {{TItem[HTItem.SystemName]|| \"0\"}}\n                                        </span>\n                                        <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Decimal\">\n                                            {{TItem[HTItem.SystemName] | number:'1.2-2' || \"--\"}}\n                                        </span>\n                                        <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Date\">\n                                            {{TItem[HTItem.SystemName] || \"--\"}}\n                                        </span></a>\n                                </span>\n                            </span>\n                            <span *ngIf=\"HTItem.NavigateLink == undefined\">\n                                <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Text\">\n                                    {{TItem[HTItem.SystemName] || \"--\"}}\n                                </span>\n                                <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Number\">\n                                    {{TItem[HTItem.SystemName]|| \"0\"}}\n                                </span>\n                                <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Decimal\">\n                                    {{TItem[HTItem.SystemName] | number:'1.2-2' || \"--\"}}\n                                </span>\n                                <span *ngIf=\"HTItem.DataType == _HelperService.AppConfig.DataType.Date\">\n                                    {{TItem[HTItem.SystemName] || \"--\"}}\n                                </span>\n                            </span>\n                        </td>\n                        <td>\n                            <div class=\"badge grey full-width\" *ngIf=\"TItem.ApplicationStatusId == undefined\">\n                                n/a\n                            </div>\n                            <div class=\"badge grey full-width\" *ngIf=\"TItem.ApplicationStatusId == 1\">\n                                unused\n                            </div>\n                            <div class=\"badge success full-width\" *ngIf=\"TItem.ApplicationStatusId == 2\">\n                                active\n                            </div>\n                            <div class=\"badge warning full-width\" *ngIf=\"TItem.ApplicationStatusId == 3\">\n                                idle\n                            </div>\n                            <div class=\"badge danger full-width\" *ngIf=\"TItem.ApplicationStatusId == 4\">\n                                dead\n                            </div>\n\n                        </td>\n                     \n                    </tr>\n                </tbody>\n            </table>\n\n            <div class=\"  p-4\">\n                <span class=\"mr-1\">\n                    {{ 'Common.Showing' | translate }}\n                    <span class=\"bold\">{{TerminalsList_Config.ShowingStart}} {{ 'Common.To' | translate }}\n                        {{TerminalsList_Config.ShowingEnd}}\n                    </span> {{ 'Common.Of' | translate }} {{TerminalsList_Config.TotalRecords}}\n                </span>\n                |\n                <span class=\"ml-1\">\n                    {{ 'Common.Show' | translate }}\n                    <select class=\" w-64 \" [(ngModel)]=\"TerminalsList_Config.PageRecordLimit\"\n                        (ngModelChange)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n                        <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n                            {{TItem}}\n                        </option>\n                    </select> {{ 'Common.Entries' | translate }}\n                </span>\n                <span class=\"float-right\">\n                    <pagination-controls id=\"TerminalsList\"\n                        (pageChange)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n                        nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n                    </pagination-controls>\n                </span>\n            </div>\n        </div>\n    </div>\n\n\n</div> -->\n\n\n<div class=\"card bd-t-0\">\n    <!-- <div class=\"card-header pt-0\" >\n        <button class=\"px-4 float-right btn tx-12 btn-success rounded-50\" (click)=\"Form_AddUser_Open()\">  Add New Terminal </button>\n\n    </div> -->\n    <!-- <div class=\"card-header pt-0\">\n        <div class=\" no-gutters\">\n            <div class=\"d-sm-flex justify-content-between\">\n                <div class=\" media\">\n                    <div type=\"button\" (click)=\"FilterByStatus('All')\"\n                        class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-primary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                        <i data-feather=\"smartphone\"></i>\n                    </div>\n                    <div class=\"media-body bd-pink\">\n                        <h6\n                            class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold tx-nowrap mg-b-5 mg-md-b-8\">\n                            ALL</h6>\n                        <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">\n                            {{_AOverview.TerminalStatus.Total  | number: \"1.0-1\" || \"--\"}}</h4>\n                    </div>\n                </div>\n                <div type=\"button\" class=\" media mg-t-20 mg-sm-t-0 mg-sm-l-15 mg-md-l-40\">\n                    <div class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-success tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded \"\n                        (click)=\"FilterByStatus('Active')\">\n                        <i data-feather=\"smartphone\"></i>\n                    </div>\n                    <div class=\"media-body\">\n                        <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">\n                            Active</h6>\n                        <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">\n                            {{_AOverview.TerminalStatus.Active  | number: \"1.0-1\" || \"--\"}}</h4>\n                    </div>\n                </div>\n                <div type=\"button\" class=\" media mg-t-20 mg-sm-t-0 mg-sm-l-15 mg-md-l-40\">\n                    <div (click)=\"FilterByStatus('Idle')\"\n                        class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-MasterCard tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                        <i data-feather=\"smartphone\"></i>\n                    </div>\n                    <div class=\"media-body\">\n                        <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">\n                            Idle</h6>\n                        <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">\n                            {{_AOverview.TerminalStatus.Idle  | number: \"1.0-1\" || \"--\"}}</h4>\n                    </div>\n                </div>\n                <div type=\"button\" class=\" media mg-t-20 mg-sm-t-0 mg-sm-l-15 mg-md-l-40\">\n                    <div (click)=\"FilterByStatus('Dead')\"\n                        class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-danger tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                        <i data-feather=\"smartphone\"></i>\n                    </div>\n                    <div class=\"media-body\">\n                        <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">\n                            Dead</h6>\n                        <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">\n                            {{_AOverview.TerminalStatus.Dead | number: \"1.0-1\" || \"--\"}}</h4>\n                    </div>\n                </div>\n                <div type=\"button\" class=\" media mg-t-20 mg-sm-t-0 mg-sm-l-15 mg-md-l-40\">\n                    <div (click)=\"FilterByStatus('Unused')\"\n                        class=\"wd-40 wd-md-50 ht-40 ht-md-50 bg-secondary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded\">\n                        <i data-feather=\"smartphone\"></i>\n                    </div>\n                    <div class=\"media-body\">\n                        <h6 class=\"tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8\">\n                            Unused</h6>\n                        <h4 class=\"tx-20 tx-sm-18 tx-md-20 tx-normal tx-rubik mg-b-0\">\n                            {{ '0' | number: \"1.0-1\" || \"--\"}}</h4>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div> -->\n\n    <div class=\"box-header px-3 pb-3 pt-0 d-flex align-items-center justify-content-between\">\n        <div class=\"form-inline \">\n            <div class=\"input-group input-group-sm mr-3\">\n                <input type=\"text\" class=\"form-control\" [value]=\"TerminalsList_Config.SearchParameter\" maxlength=\"100\"\n                    class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n                    (keyup)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n                <div class=\"input-group-append\">\n                    <button class=\"btn btn-outline-light\" type=\"button\"\n                        (click)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n                            data-feather=\"search\"></i> </button>\n                </div>\n            </div>\n            <span class=\"badge badge-pill badge-primary\"\n                *ngFor=\"let TItem of TerminalsList_Config.Filters\">{{TItem.Title}}</span>\n        </div>\n        <nav class=\"nav nav-with-icon tx-20\">\n            <div class=\"dropdown dropleft\">\n                <a href=\"\" class=\"nav-link mr-2\" id=\"TerminalsList_sdropdown\" data-toggle=\"dropdown\"\n                    aria-haspopup=\"true\" aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n                <div class=\"dropdown-menu tx-13\">\n                    <form class=\"wd-250 pd-15\">\n                        <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n                            <div class=\"float-right tx-20\" *ngIf=\"TerminalsList_Config.Sort.SortOrder == 'desc'\"\n                                (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                                <i class=\"fas fa-sort-alpha-down-alt\"></i>\n                            </div>\n                            <div class=\"float-right tx-20\" *ngIf=\"TerminalsList_Config.Sort.SortOrder == 'asc'\"\n                                (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                                <i class=\"fas fa-sort-alpha-up-alt\"></i>\n                            </div>\n                        </h6>\n                        <a class=\"dropdown-item\" *ngFor=\"let TItem of TerminalsList_Config.Sort.SortOptions\"\n                            (click)=\"TerminalsList_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n                            {{TItem.Name}}\n                        </a>\n                        <!-- <div *ngFor=\"let TItem of TerminalsList_Config.SortExpressionOption\"\n                            (click)=\"TerminalsList_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n                            {{TItem.Name}}\n                        </div> -->\n                        <!-- <div class=\"mt-2\">\n                            <button type=\"button\" class=\"btn btn-primary btn-sm mr-2\"\n                                (click)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.SortApply)\">Apply</button>\n\n                            <button type=\"button\" class=\"btn btn-light btn-sm \"\n                                (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortReset)\">\n                                Reset</button>\n                        </div> -->\n                    </form>\n                </div>\n            </div>\n            <div class=\"dropdown dropleft\">\n                <a href=\"\" class=\"nav-link\" id=\"TerminalsList_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n                    aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n                <div class=\"dropdown-menu tx-13\">\n                    <form class=\"wd-250 pd-15\">\n                        <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Filters\n                        </h6>\n                        <div>\n                            <div class=\"form-group\">\n                                <label>Date</label>\n                                <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\"\n                                    daterangepicker [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                                    (selected)=\"TerminalsList_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n                            </div>\n                        </div>\n                        <div>\n                            <div class=\"form-group\">\n                                <label>Status</label>\n                                <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                                    [data]=\"TerminalsList_Config.StatusOptions\" [value]=\"TerminalsList_Config.Status\"\n                                    (valueChanged)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                                </select2>\n                            </div>\n                        </div>\n                        <!-- <div>\n                            <div class=\"form-group wd-100p\">\n                                <label>Store</label>\n                                <select2 [options]=\"TerminalsList_Filter_Store_Option\"\n                                (valueChanged)=\"TerminalsList_Filter_Stores_Change($event)\">\n                            </select2>\n                            </div>\n                        </div>\n                        <div>\n                            <label for=\"\"> Provider</label>\n                            <div class=\"form-group wd-100p\">\n                                <select2 [options]=\"TerminalsList_Filter_Provider_Option\"\n                                    (valueChanged)=\"TerminalsList_Filter_Providers_Change($event)\">\n                                </select2>\n                            </div>\n                        </div> -->\n                        <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                            (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                            Reset</button>\n                        <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                            (click)=\"TerminalsList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n\n                    </form>\n                </div>\n            </div>\n            <a class=\"nav-link ml-2 RefreshColor\"\n                (click)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n                    data-feather=\"refresh-cw\"></i> </a>\n        </nav>\n    </div>\n    <div class=\"card-body p-0\">\n        <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n            <thead>\n                <tr>\n                    <th class=\"text-center\">\n                    </th>\n                    <th>\n                        Terminal ID\n                    </th>\n                    <th>\n                        Provider\n                    </th>\n                    <th>\n                        Store\n                    </th>\n                    <th class=\"text-center\">\n                        Last Transactions\n                    </th>\n                    <th>\n                        Sale Amount\n                    </th>\n                    <th class=\"text-center\">\n                        RM\n                    </th>\n                    <th class=\"text-center\">\n                        Added On\n                    </th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr class=\"cursor-pointer\" (click)=\"TerminalsList_RowSelected(TItem)\"\n                    *ngFor=\"let TItem of TerminalsList_Config.Data  | paginate: { id: 'TerminalsList_Paginator',itemsPerPage: TerminalsList_Config.PageRecordLimit, currentPage: TerminalsList_Config.ActivePage,  totalItems: TerminalsList_Config.TotalRecords }\">\n                    <td class=\"text-center\">\n                        <img class=\"br\" src=\"{{TItem.ProviderIconUrl}}\" class=\"img wd-40 ht-40 rounded-circle bd bd-2\"\n                            [style.border-color]=\"TItem.StatusC\">\n                    </td>\n                    <td>\n                        <div class=\"tx-semibold\">{{TItem.TerminalId  || \"--\"}}</div>\n                    </td>\n                    <td>\n                        <div class=\"\">{{TItem.ProviderName  || \"--\"}}</div>\n\n                    </td>\n                    <td>\n                        <div class=\"\">{{TItem.StoreName  || \"--\"}}</div>\n\n                    </td>\n                    <td class=\"text-muted text-center\">\n                        {{TItem.LastTransactionDate  || \"--\"}}\n                    </td>\n                    <td class=\"text-center\">\n                        {{TItem.SaleAmount || \"--\"}}\n                    </td>\n                    <td class=\"text-muted\">\n                        {{TItem.RmName  || \"--\"}}\n                    </td>\n                    <td class=\"text-muted text-center\">\n                        {{TItem.CreateDate  || \"--\"}}\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n    <div class=\"card-footer pd-20\">\n        <span class=\"mr-3\">\n            {{ 'Common.Showing' | translate }}\n            <span class=\"bold\">{{TerminalsList_Config.ShowingStart}} {{ 'Common.To' | translate }}\n                {{TerminalsList_Config.ShowingEnd}}\n            </span> {{ 'Common.Of' | translate }} {{TerminalsList_Config.TotalRecords}}\n        </span>\n        <span>\n            {{ 'Common.Show' | translate }}\n            <select class=\"wd-40 \" [(ngModel)]=\"TerminalsList_Config.PageRecordLimit\"\n                (ngModelChange)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n                <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n                    {{TItem}}\n                </option>\n            </select>\n        </span>\n        <span class=\"float-right\">\n            <pagination-controls id=\"TerminalsList_Paginator\" class=\"pagination pagination-space\"\n                (pageChange)=\"TerminalsList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n                nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n            </pagination-controls>\n        </span>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/accounts/tuterminals/branch/tuterminals.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/accounts/tuterminals/branch/tuterminals.component.ts ***!
  \******************************************************************************/
/*! exports provided: TUTerminalsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUTerminalsComponent", function() { return TUTerminalsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");






var TUTerminalsComponent = /** @class */ (function () {
    function TUTerminalsComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this.TerminalsList_Filter_Bank_Selected = 0;
        this.TerminalsList_Filter_Store_Selected = 0;
        this.TerminalsList_Filter_Provider_Selected = 0;
        //#endregion
        //#region Terminals Count 
        this._AOverview = {
            TerminalStatus: {
                Active: 0,
                Dead: 0,
                Idle: 0,
                Inactive: 0,
                Total: 0
            }
        };
        this._HelperService.ShowDateRange = false;
    }
    TUTerminalsComponent.prototype.ngOnInit = function () {
        var _this = this;
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            _this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null || _this._HelperService.AppConfig.ActiveReferenceId == null) {
                _this._Router.navigate([_this._HelperService.AppConfig.Pages.System.NotFound]);
            }
            else {
                // this._HelperService.Get_UserAccountDetails(true);
                _this.TerminalsList_Filter_Banks_Load();
                _this.TerminalsList_Filter_Providers_Load();
                _this.TerminalsList_Setup();
                _this.TerminalsList_Filter_Stores_Load();
                // this.GetTerminalCount();
            }
        });
    };
    TUTerminalsComponent.prototype.TerminalsList_Setup = function () {
        this.TerminalsList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            Title: "Available Terminals",
            StatusType: "default",
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
            Type: this._HelperService.AppConfig.ListType.Branch,
            DefaultSortExpression: 'CreateDate desc',
            TableFields: [
                {
                    DisplayName: 'TID',
                    SystemName: 'TerminalId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'Provider',
                    SystemName: 'ProviderName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'StoreName',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'TodaysTransactionAmount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Last Tr',
                    SystemName: 'LastTransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.TerminalsList_Config = this._DataHelperService.List_Initialize(this.TerminalsList_Config);
        this.TerminalsList_GetData();
    };
    TUTerminalsComponent.prototype.TerminalsList_ToggleOption = function (event, Type) {
        this.TerminalsList_Config = this._DataHelperService.List_Operations(this.TerminalsList_Config, event, Type);
        if (this.TerminalsList_Config.RefreshData == true) {
            this.TerminalsList_GetData();
        }
    };
    TUTerminalsComponent.prototype.TerminalsList_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.TerminalsList_Config);
        this.TerminalsList_Config = TConfig;
    };
    TUTerminalsComponent.prototype.TerminalsList_RowSelected = function (ReferenceData) {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Banks_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TerminalsList_Filter_Bank_Option = {
            placeholder: 'Sort by Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Banks_Change = function (event) {
        if (event.value == this.TerminalsList_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Bank_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Bank_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
        }
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Stores_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TerminalsList_Filter_Store_Option = {
            placeholder: "Filter by Store",
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Stores_Change = function (event) {
        this.StoreEventProcessing(event);
    };
    TUTerminalsComponent.prototype.StoreEventProcessing = function (event) {
        if (event.value == this.TerminalsList_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "StoreId", this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, "=");
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "StoreId", this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, "=");
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Store_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict("", "StoreId", this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, "="));
        }
        console.log("=>", this.TerminalsList_Config);
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Providers_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TerminalsList_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUTerminalsComponent.prototype.TerminalsList_Filter_Providers_Change = function (event) {
        if (event.value == this.TerminalsList_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
            this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
            this.TerminalsList_Filter_Provider_Selected = event.value;
            this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '='));
        }
        this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUTerminalsComponent.prototype.GetTerminalCount = function () {
        var _this = this;
        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getaccountoverview',
            StartTime: new Date(2017, 0, 1, 0, 0, 0, 0),
            EndDate: moment().endOf('day'),
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
            SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this._HelperService.IsFormProcessing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._AOverview = _Response.Result;
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.IsFormProcessing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    TUTerminalsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-tuterminals",
            template: __webpack_require__(/*! ./tuterminals.component.html */ "./src/app/modules/accounts/tuterminals/branch/tuterminals.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"]])
    ], TUTerminalsComponent);
    return TUTerminalsComponent;
}());



/***/ }),

/***/ "./src/app/modules/accounts/tuterminals/branch/tuterminals.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/accounts/tuterminals/branch/tuterminals.module.ts ***!
  \***************************************************************************/
/*! exports provided: TUTerminalsRoutingModule, TUTerminalsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUTerminalsRoutingModule", function() { return TUTerminalsRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUTerminalsModule", function() { return TUTerminalsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _tuterminals_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tuterminals.component */ "./src/app/modules/accounts/tuterminals/branch/tuterminals.component.ts");











var routes = [
    { path: '', component: _tuterminals_component__WEBPACK_IMPORTED_MODULE_10__["TUTerminalsComponent"] }
];
var TUTerminalsRoutingModule = /** @class */ (function () {
    function TUTerminalsRoutingModule() {
    }
    TUTerminalsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUTerminalsRoutingModule);
    return TUTerminalsRoutingModule;
}());

var TUTerminalsModule = /** @class */ (function () {
    function TUTerminalsModule() {
    }
    TUTerminalsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUTerminalsRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
            ],
            declarations: [_tuterminals_component__WEBPACK_IMPORTED_MODULE_10__["TUTerminalsComponent"]]
        })
    ], TUTerminalsModule);
    return TUTerminalsModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-accounts-tuterminals-branch-tuterminals-module.js.map