(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboards-store-acquirer-dashboard-module"],{

/***/ "./src/app/modules/dashboards/store/acquirer/dashboard.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/modules/dashboards/store/acquirer/dashboard.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--stores--overview -->\n<div class=\"card bd-t-0 px-3 pb-3 pt-0\">\n\n  <div class=\"row row-xs mg-t-15  mg-lg-t-0\">\n    <div class=\"col-sm-6 col-lg-3 \">\n      <div class=\"card card-body shadow-none ht-100p\">\n        <div class=\"d-flex d-lg-block d-xl-flex align-items-end\">\n          <h3 class=\"tx-bold tx-rubik tx-primary mg-b-8 mg-r-5 lh-1\">\n            {{_AccountOverview.TotalTransactions | number: \"1.0-1\" || \"--\"}}</h3>\n          <!-- <p class=\"tx-11 tx-color-03 mg-b-0 mg-l-20 mg-b-10\"><span class=\"tx-medium tx-success\">1.2% <i\n                class=\"icon ion-md-arrow-up\"></i></span></p> -->\n\n        </div>\n        <h6 class=\"tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8\">TRANSACTIONS</h6>\n\n        <p class=\"tx-12 tx-color-03 mg-b-0\"> Total transactions on all pos during selected period </p>\n      </div>\n    </div><!-- col -->\n    <div class=\"col-sm-6 col-lg-3 mg-t-10  mg-sm-t-0\">\n      <div class=\"card card-body shadow-none ht-100p\">\n        <div class=\"d-flex d-lg-block d-xl-flex align-items-end\">\n          <h3 class=\"tx-bold tx-teal tx-rubik mg-b-8 mg-r-5 lh-1\"><span class=\" text-sm mg-r-2\"\n            [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>\n            {{_AccountOverview.TotalSale | number: \"1.0-1\" || \"--\"}}</h3>\n          <!-- <p class=\"tx-11 tx-color-03 mg-b-0 mg-l-20 mg-b-10\"><span class=\"tx-medium tx-danger\">0.7% <i\n                class=\"icon ion-md-arrow-down\"></i></span></p> -->\n\n        </div>\n        <h6 class=\"tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8\">TOTAL SALE</h6>\n\n        <p class=\"tx-12 tx-color-03 mg-b-0\"> Total transaction amount on all terminals during selected period</p>\n\n\n      </div>\n    </div><!-- col -->\n    <div class=\"col-sm-6 col-lg-3 mg-t-10  mg-lg-t-0\">\n      <div class=\"card card-body shadow-none ht-100p\">\n        <div class=\"d-flex d-lg-block d-xl-flex align-items-end\">\n          <h3 class=\"tx-bold tx-rubik tx-verve mg-b-8 mg-r-5 lh-1\">\n            {{_AccountOverview.AverageTransactions | number: \"1.0-1\" || \"--\"}} </h3>\n          <!-- <p class=\"tx-11 tx-color-03 mg-b-0 mg-l-20 mg-b-10\"><span class=\"tx-medium tx-danger\">0.7% <i\n                class=\"icon ion-md-arrow-down\"></i></span></p> -->\n\n        </div>\n        <h6 class=\"tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8\">AVG TRANSACTIONS</h6>\n\n        <p class=\"tx-12 tx-color-03 mg-b-0\"> Average transaction count per POS at all locations</p>\n\n\n      </div>\n    </div><!-- col -->\n    <div class=\"col-sm-6 col-lg-3 mg-t-10  mg-lg-t-0\">\n      <div class=\"card card-body shadow-none ht-100p\">\n        <div class=\"d-flex d-lg-block d-xl-flex align-items-end\">\n          <h3 class=\"tx-bold tx-rubik tx-MasterCard mg-b-8 mg-r-5 lh-1\"><span class=\" text-sm mg-r-2\"\n            [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>\n            {{_AccountOverview.AverageTransactionAmount | number: \"1.0-1\" || \"--\"}}</h3>\n          <!-- <p class=\"tx-11 tx-color-03 mg-b-0 mg-l-20 mg-b-10\"><span class=\"tx-medium tx-danger\">0.7% <i\n                class=\"icon ion-md-arrow-down\"></i></span></p> -->\n\n        </div>\n        <h6 class=\" tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8\">AVG. TRANSACTIONS AMT</h6>\n\n        <p class=\"tx-12 tx-color-03 mg-b-0\"> Average transaction amount per POS in all POS</p>\n      </div>\n    </div><!-- col -->\n  </div>\n\n  <div class=\"row row-xs mg-t-15  \">\n    <div class=\"col-lg-4 col-md-12 col-sm-12\">\n      <div class=\"card ht-100p\">\n        <!-- <div class=\"card-header\">\n        <h6 class=\"mg-b-0\">Sessions By Channel</h6>\n      </div> -->\n        <div class=\"card-body d-flex justify-content-center align-items-center\">\n          <div class=\"chart-seven\"> <canvas style=\"height: 250px;\" baseChart [chartType]=\"'doughnut'\"\n              [datasets]=\"datapieType.datasets\" [labels]=\"datapieType.labels\" [options]=\"dougnoutOptions\" [colors]=\"doughnuttype\"\n              [legend]=\"false\" (chartClick)=\"onChartClick($event)\">\n            </canvas></div>\n        </div><!-- card-body -->\n        <div class=\"card-footer pd-20\">\n          <div class=\"row\">\n            <div class=\"col-6\">\n              <p class=\"tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5\">CASH</p>\n              <div class=\"d-flex align-items-center\">\n                <h5 class=\"tx-bold tx-rubik tx-teal mg-b-0\"><span class=\" text-sm mg-r-5\"\n                    [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{_AccountOverview.CashTransactionAmount}}\n                  <br>\n\n                  <small class=\"tx-color-04 tx-11\"> {{_AccountOverview.CashTransPerTerm | number: \"1.2-2\" || \"--\"}}% Of Total Transactions</small>\n\n                </h5>\n              </div>\n            </div><!-- col -->\n            <div class=\"col-6\">\n              <p class=\"tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 mg-b-5\">CARDS</p>\n              <div class=\"d-flex align-items-center\">\n                <h5 class=\"tx-bold tx-rubik tx-verve mg-b-0\"><span class=\" text-sm mg-r-5\"\n                    [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{_AccountOverview.CardTransactionsAmount}}\n                  <br>\n\n                  <small class=\"tx-color-04 tx-11\"> {{_AccountOverview.CardTransPerTerm | number: \"1.2-2\" || \"--\"}}% Of Total Transactions</small>\n\n                </h5>\n              </div>\n            </div><!-- col -->\n          </div><!-- row -->\n        </div><!-- card-footer -->\n      </div><!-- card -->\n    </div>\n    <div class=\"col-lg-4 col-md-12 col-sm-12 mg-t-15  mg-lg-t-0\">\n      <div class=\"card ht-100p\">\n        <!-- <div class=\"card-header\">\n        <h6 class=\"mg-b-0\">Sessions By Channel</h6>\n      </div> -->\n        <div class=\"card-body d-flex justify-content-center align-items-center\">\n          <div class=\"chart-seven\"> <canvas style=\"height: 250px;\" baseChart [chartType]=\"'doughnut'\"\n              [datasets]=\"datapieCard.datasets\" [labels]=\"datapieCard.labels\" [options]=\"dougnoutOptions\" [colors]=\"doughnutcard\"\n              [legend]=\"false\" (chartClick)=\"onChartClick($event)\">\n            </canvas></div>\n        </div><!-- card-body -->\n        <div class=\"card-footer pd-20\">\n          <div class=\"row\">\n            <div class=\"col-6\" *ngIf=\"_AccountOverview.OwnerCardTotal\">\n              <p class=\"tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5\">{{_HelperService.UserAccount.DisplayName}}} CARDS</p>\n              <div class=\"d-flex align-items-center\"  *ngIf=\"_AccountOverview.OwnerCardTotal\">\n                <h5 class=\"tx-bold tx-rubik tx-success mg-b-0\"><span class=\" text-sm mg-r-5\"\n                    [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{_AccountOverview.OwnerCardTotal.Amount}} <br>\n\n                  <small class=\"tx-color-04 tx-11\"> {{_AccountOverview.OwnerCardTotal.TransactionsPerc | number: \"1.2-2\" || \"--\"}}% Of Total Transactions</small>\n\n                </h5>\n              </div>\n            </div><!-- col -->\n            <div class=\"col-6\">\n              <p class=\"tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 mg-b-5\">OTHER BANK CARDS</p>\n              <div class=\"d-flex align-items-center\" *ngIf=\"_AccountOverview.OtherCardTotal\">\n                <h5 class=\"tx-bold tx-rubik tx-MasterCard mg-b-0\"><span class=\" text-sm mg-r-5\"\n                    [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span> {{_AccountOverview.OtherCardTotal.Amount}}<br>\n\n                  <small class=\"tx-color-04 tx-11\">{{_AccountOverview.OtherCardTotal.TransactionsPerc | number: \"1.2-2\" || \"--\"}}% Of Total Transactions</small>\n\n                </h5>\n              </div>\n            </div><!-- col -->\n          </div><!-- row -->\n        </div><!-- card-footer -->\n      </div><!-- card -->\n\n    </div>\n    <div class=\"col-lg-4 col-md-12 col-sm-12 mg-t-15  mg-lg-t-0\">\n      <div class=\"card ht-100p\">\n\n        <div class=\"card-body\">\n          <div class=\"row\">\n            <div class=\"col-12 mt-4 d-flex\" *ngFor=\"let TItem of _AccountOverview.CardTypeSale\">\n              <div class=\"mr-3 d-flex align-items-center\">\n                <span class=\"tx-30\"\n                  [ngClass]=\"{'tx-MasterCard': TItem.Name  == 'Mastercard',  'tx-teal': TItem.Name  == 'VISA',  'tx-verve': TItem.Name  == 'Verve'}\">\n                  <i class=\"{{TItem.Name  == 'Verve'? 'far':'fab'}}\"\n                    [ngClass]=\"{'fa-cc-mastercard': TItem.Name  == 'Mastercard', 'fa-credit-card': TItem.Name  == 'Verve','fa-cc-visa': TItem.Name  == 'VISA'}\"></i>\n                </span>\n              </div>\n\n              <div class=\"flex-fill\">\n                <p class=\"tx-10 tx-uppercase tx-semibold link-01tx-spacing-1 tx-nowrap mg-b-5\">{{ TItem.Name || \"--\" }}\n                </p>\n                <div class=\"d-flex align-items-center bd-b\">\n                  <!-- <div class=\"wd-10 ht-10 rounded-circle bg-warning mg-r-5\"></div> -->\n                  <h5 class=\"tx-bold tx-rubik  tx-MasterCard mg-b-0 \"\n                    [ngClass]=\"{'tx-MasterCard': TItem.Name  == 'Mastercard',  'tx-teal': TItem.Name  == 'VISA',  'tx-verve': TItem.Name  == 'Verve'}\">\n                    <span class=\" text-sm mg-r-5\"\n                      [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{TItem.Amount | number: \"1.2-2\" || \"--\"}}\n                    <small class=\"tx-color-04 tx-11\"><br> {{TItem.TransactionPer | number: \"1.2-2\" || \"--\"}}% Of Total\n                      from\n                      {{TItem.Transactions | number: \"1.0-0\" || \"--\"}} Transactions</small> </h5>\n                </div>\n              </div>\n            </div><!-- col -->\n\n            <div class=\"col-12 mt-4 d-flex\">\n              <div class=\"mr-3 d-flex align-items-center\">\n                <span class=\"tx-30 tx-primary\">\n                  <i class=\"fas fa-credit-card\"></i>\n                </span>\n              </div>\n              <div>\n                <p class=\"tx-10 tx-uppercase tx-semibold link-01 tx-spacing-1 mg-b-5\">Other Cards</p>\n                <div class=\"d-flex align-items-center\" *ngIf=\"_AccountOverview.Others\" >\n                  <!-- <div class=\"wd-10 ht-10 rounded-circle bg-primary mg-r-5\"></div> -->\n                  <h5 class=\"tx-bold tx-rubik tx-primary mg-b-0\"><span class=\" text-sm mg-r-5\"\n                      [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span> {{_AccountOverview.Others.Amount}} <br>\n                    <small class=\"tx-color-04 tx-11\">{{_AccountOverview.Others.TransactionPer | number: \"1.2-2\" || \"--\"}}  % Of Total From {{_AccountOverview.Others.Transactions}} Transactions</small>\n\n\n\n                  </h5>\n                </div>\n              </div>\n            </div><!-- col -->\n          </div><!-- row -->\n        </div><!-- card-footer -->\n      </div>\n\n\n    </div>\n\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/modules/dashboards/store/acquirer/dashboard.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/modules/dashboards/store/acquirer/dashboard.component.ts ***!
  \**************************************************************************/
/*! exports provided: TUDashboardComponent, OAccountOverview */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUDashboardComponent", function() { return TUDashboardComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccountOverview", function() { return OAccountOverview; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");






var TUDashboardComponent = /** @class */ (function () {
    function TUDashboardComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _ChangeDetectorRef) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._ChangeDetectorRef = _ChangeDetectorRef;
        //#region DougnutConfig 
        this.dougnoutOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false,
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };
        this.doughnutcard = [{ backgroundColor: ['#FFC20A', '#10b759'] }];
        this.datapieCard = {
            labels: ['OtherCard', 'Owner Card'],
            datasets: [{
                    data: [0, 0],
                }],
        };
        this.doughnuttype = [{ backgroundColor: ['#00cccc', '#f10075'] }];
        this.datapieType = {
            labels: ['Cash', 'Card'],
            datasets: [{
                    data: [0, 0],
                    backgroundColor: ['#66a4fb', '#4cebb5']
                }]
        };
        //#endregion
        this._DateSubscription = null;
        this._AccountOverview = {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
        };
        this._HelperService.ShowDateRange = true;
    }
    TUDashboardComponent.prototype.ngOnDestroy = function () {
        this._DateSubscription.unsubscribe();
    };
    TUDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._HelperService.FullContainer = false;
        this._HelperService.ResetDateRange();
        this._DateSubscription = this._HelperService.RangeAltered.subscribe(function (value) {
            _this.GetAccountOverviewLite();
        });
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            _this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null) {
                _this._Router.navigate([_this._HelperService.AppConfig.Pages.System.NotFound]);
            }
            else {
                _this._HelperService.ReloadEventEmit.emit(0);
                _this.GetAccountOverviewLite();
            }
        });
    };
    TUDashboardComponent.prototype.GetAccountOverviewLite = function () {
        var _this = this;
        this._HelperService.toogleFormProcessingFlag(true);
        var Data = {
            Task: 'getaccountoverview',
            StartTime: this._HelperService.DateRangeStart,
            EndTime: this._HelperService.DateRangeEnd,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this._HelperService.toogleFormProcessingFlag(false);
            var OwnerCardTotal = {
                "Name": "OwnerAllCards",
                "Transactions": 0,
                "TransactionsPerc": 0.0,
                "Amount": 0.0
            };
            var OtherCardTotal = {
                "Name": "OtherAllCards",
                "Transactions": 0,
                "TransactionsPerc": 0.0,
                "Amount": 0.0
            };
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._AccountOverview = _Response.Result;
                _this.datapieType.datasets[0].data[1] = _this._AccountOverview['CardTransactions'];
                _this.datapieType.datasets[0].data[0] = _this._AccountOverview['CashTransactions'];
                //#region CardCashTransPerc 
                _this._AccountOverview["CardTransPerTerm"] = (_this._HelperService.DivideTwoNumbers(_this._AccountOverview['CardTransactions'], _this._AccountOverview['TotalTransactions'])) * 100;
                _this._AccountOverview["CashTransPerTerm"] = (_this._HelperService.DivideTwoNumbers(_this._AccountOverview['CashTransactions'], _this._AccountOverview['TotalTransactions'])) * 100;
                //#endregion
                //#region OwnerCardTotals 
                for (var index = 0; index < _this._AccountOverview['CardTypeSale'].length; index++) {
                    var element = _this._AccountOverview['CardTypeSale'][index];
                    var _TempVal = _this._HelperService.DivideTwoNumbers(100, _this._AccountOverview['TotalTransactions']);
                    element['TransactionPer'] = element['Transactions'] * _TempVal;
                }
                for (var index = 0; index < _this._AccountOverview['OwnerCardTypeSale'].length; index++) {
                    var element = _this._AccountOverview['OwnerCardTypeSale'][index];
                    OwnerCardTotal.Transactions += element.Transactions;
                    OwnerCardTotal.Amount += element.Amount;
                }
                //#endregion
                //#region OwnerOtherCardTransPerc 
                OwnerCardTotal.TransactionsPerc = (_this._HelperService.DivideTwoNumbers(OwnerCardTotal.Transactions, _this._AccountOverview['OwnerCardTransactions']) * 100);
                OtherCardTotal.TransactionsPerc = (_this._HelperService.DivideTwoNumbers(OtherCardTotal.Transactions, _this._AccountOverview['OwnerCardTransactions']) * 100);
                //#endregion
                //#region OtherCardTotals 
                OtherCardTotal.Transactions = _this._AccountOverview['OwnerCardTransactions'] - OwnerCardTotal.Transactions;
                OtherCardTotal.Amount = _this._AccountOverview['OwnerCardTransactionsAmount'] - OwnerCardTotal.Amount;
                //#endregion
                _this.datapieCard.datasets[0].data[1] = OwnerCardTotal.Transactions;
                _this.datapieCard.datasets[0].data[0] = OtherCardTotal.Transactions;
                _this._AccountOverview['OwnerCardTotal'] = OwnerCardTotal;
                _this._AccountOverview['OtherCardTotal'] = OtherCardTotal;
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
            var other = {
                Name: "Other",
                Transactions: 0,
                Amount: 0.0,
                TransactionPer: 0
            };
            //#region otherTotals 
            for (var index = 0; index < _this._AccountOverview['CardTypeSale'].length; index++) {
                var element = _this._AccountOverview['CardTypeSale'][index];
                other.Transactions += element['Transactions'];
                other.Amount += element['Amount'];
            }
            other.Transactions = _this._AccountOverview["TotalTransactions"] - other.Transactions;
            other.Amount = _this._AccountOverview["TotalSale"] - other.Amount;
            other.TransactionPer = (_this._HelperService.DivideTwoNumbers(other.Transactions, _this._AccountOverview['TotalTransactions']) * 100);
            //#endregion
            _this._AccountOverview['Others'] = other;
            _this.components.forEach(function (a) {
                try {
                    if (a.chart)
                        a.chart.update();
                }
                catch (error) {
                    console.log('chartjs error');
                }
            });
        }, function (_Error) {
            _this._HelperService.toogleFormProcessingFlag(false);
            _this._HelperService.HandleException(_Error);
        });
    };
    TUDashboardComponent.prototype.TUTr_Setup = function () {
        this.TUTr_Config =
            {
                Id: null, Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Sales History',
                StatusType: 'transaction',
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: 'TransactionDate desc',
                RefreshCount: false,
                PageRecordLimit: 6,
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                ]
            };
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    };
    TUDashboardComponent.prototype.TUTr_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])(ng2_charts__WEBPACK_IMPORTED_MODULE_4__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], TUDashboardComponent.prototype, "components", void 0);
    TUDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/modules/dashboards/store/acquirer/dashboard.component.html"),
            styles: ["\n    agm-map {\n      height: 300px;\n    }\n"]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], TUDashboardComponent);
    return TUDashboardComponent;
}());

var OAccountOverview = /** @class */ (function () {
    function OAccountOverview() {
    }
    return OAccountOverview;
}());



/***/ }),

/***/ "./src/app/modules/dashboards/store/acquirer/dashboard.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/dashboards/store/acquirer/dashboard.module.ts ***!
  \***********************************************************************/
/*! exports provided: TUDashboardRoutingModule, TUDashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUDashboardRoutingModule", function() { return TUDashboardRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUDashboardModule", function() { return TUDashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/modules/dashboards/store/acquirer/dashboard.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var agm_overlays__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! agm-overlays */ "./node_modules/agm-overlays/index.js");
/* harmony import */ var agm_overlays__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(agm_overlays__WEBPACK_IMPORTED_MODULE_13__);














var routes = [
    { path: '', component: _dashboard_component__WEBPACK_IMPORTED_MODULE_11__["TUDashboardComponent"] }
];
var TUDashboardRoutingModule = /** @class */ (function () {
    function TUDashboardRoutingModule() {
    }
    TUDashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUDashboardRoutingModule);
    return TUDashboardRoutingModule;
}());

var TUDashboardModule = /** @class */ (function () {
    function TUDashboardModule() {
    }
    TUDashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                TUDashboardRoutingModule,
                agm_overlays__WEBPACK_IMPORTED_MODULE_13__["AgmOverlays"],
                _agm_core__WEBPACK_IMPORTED_MODULE_12__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
                }),
                ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"],
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_11__["TUDashboardComponent"]]
        })
    ], TUDashboardModule);
    return TUDashboardModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboards-store-acquirer-dashboard-module.js.map