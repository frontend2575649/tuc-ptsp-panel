(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-accounts-tustores-acquirer-tustores-module"],{

/***/ "./src/app/modules/accounts/tustores/acquirer/tustores.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/modules/accounts/tustores/acquirer/tustores.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card \">\n  <div class=\"card-header\">\n    <h6 class=\"tx-uppercase tx-semibold mg-b-0\"> {{StoresList_Config.Title}}</h6>\n  </div>\n  <div class=\"box-header px-3 pd-t-15 pd-b-10  d-flex align-items-center justify-content-between\">\n    <div class=\"form-inline \">\n      <div class=\"input-group input-group-sm mr-3\">\n        <input type=\"text\" class=\"form-control\" [value]=\"StoresList_Config.SearchParameter\" maxlength=\"100\"\n          class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n          (keyup)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n        <div class=\"input-group-append\">\n          <button class=\"btn btn-outline-light\" type=\"button\"\n            (click)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n              data-feather=\"search\"></i> </button>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"d-flex ml-auto\">\n      <button type=\"button\" class=\"btn  tx-12 rounded-0 bd-gray-200 bd-r-0  btn-outline-danger btn-icon\"\n        (click)=\"Delete_Filter()\">\n        <i data-feather=\"minus-circle\"></i> </button>\n\n      <div class=\"form-group my-auto shadow-none ml-auto \" *ngIf=\"_HelperService._RefreshUI\">\n        <select2 name=\"itemId0\" [options]=\"_DataHelperService.S2_NewSavedFilters_Option\" class=\"wd-30 select2\"\n          [data]=\"_HelperService.Active_FilterOptions\" [value]=\"_HelperService.Active_FilterOptions[0]\"\n          (valueChanged)=\"Active_FilterValueChanged( $event )\">\n        </select2>\n      </div>\n      <button type=\"button\" class=\" btn btn-outline-success tx-12 bd-l-0 mr-3 rounded-0 bd-gray-200 btn-icon\"\n        (click)=\"Save_NewFilter()\"> <i data-feather=\"plus-circle\"></i> </button>\n\n    </div>\n\n    <nav class=\"nav nav-with-icon tx-20\">\n      <div class=\"dropdown dropleft\">\n        <a href=\"\" class=\"nav-link mr-2\" id=\"StoresList_sdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n          aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n        <div class=\"dropdown-menu tx-13\">\n          <form class=\"wd-250 pd-15\">\n            <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n              <div class=\"float-right tx-20\" *ngIf=\"StoresList_Config.Sort.SortOrder == 'desc'\"\n                (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                <i class=\"fas fa-sort-alpha-down-alt\"></i>\n              </div>\n              <div class=\"float-right tx-20\" *ngIf=\"StoresList_Config.Sort.SortOrder == 'asc'\"\n                (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                <i class=\"fas fa-sort-alpha-up-alt\"></i>\n              </div>\n            </h6>\n            <a class=\"dropdown-item\" *ngFor=\"let TItem of StoresList_Config.Sort.SortOptions\"\n              (click)=\"StoresList_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n              {{TItem.Name}}\n            </a>\n            <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n              (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n              Reset</button>\n            <button type=\"button\" class=\"btn btn-primary btn-sm \"\n              (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n          </form>\n        </div>\n      </div>\n      <div class=\"dropdown dropleft\">\n        <a href=\"\" class=\"nav-link\" id=\"StoresList_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n          aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n        <div class=\"dropdown-menu tx-13\">\n          <form class=\"wd-250 pd-15\">\n            <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Filters\n            </h6>\n            <div>\n              <div class=\"form-group\">\n                <label>Date</label>\n                <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                  [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                  (selected)=\"StoresList_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n              </div>\n            </div>\n            <div>\n              <div class=\"form-group\">\n                <label>Status</label>\n                <!-- <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                  [data]=\"StoresList_Config.StatusOptions\" [value]=\"StoresList_Config.Status\"\n                  (valueChanged)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                </select2> -->\n                <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                  [data]=\"StoresList_Config.StatusOptions\"\n                  (valueChanged)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                </select2>\n              </div>\n            </div>\n            <div>\n              <div class=\"form-group\">\n                <label>Owner</label>\n                <select2 [options]=\"StoresList_Filter_Owners_Option\" class=\"wd-100p\"\n                  (valueChanged)=\"StoresList_Filter_Owners_Change($event)\">\n                </select2>\n              </div>\n            </div>\n            <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n              (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n              Reset</button>\n            <button type=\"button\" class=\"btn btn-primary btn-sm \"\n              (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n\n          </form>\n        </div>\n      </div>\n      <a class=\"nav-link ml-2 RefreshColor\"\n        (click)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n          data-feather=\"refresh-cw\"></i> </a>\n    </nav>\n  </div>\n\n<div class=\"pt-0 pd-x-15 pd-b-15\">\n  <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n  [hidden]=\"(!_HelperService.FilterSnap.Status) || (_HelperService.FilterSnap.Status==0)\"\n  (click)=\"RemoveFilterComponent('Status')\">\n  {{_HelperService.FilterSnap.StatusName}}\n  <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n      class=\"fas fa-times\"></i> </span>\n</div>\n<div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n  [hidden]=\"!_HelperService.FilterSnap.Sort.SortColumn || (_HelperService.FilterSnap.Sort.SortColumn=='CreateDate' && _HelperService.FilterSnap.Sort.SortOrder=='desc')\"\n  (click)=\"RemoveFilterComponent('Sort')\">\n  <span>\n    {{_HelperService.FilterSnap.Sort.SortColumn + \"--\" + _HelperService.FilterSnap.Sort.SortOrder}}</span>\n  <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n      class=\"fas fa-times\"></i> </span>\n</div>\n<div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n  [hidden]=\"!_HelperService.FilterSnap.StartTime\" (click)=\"RemoveFilterComponent('Time')\">\n  {{_HelperService.GetDateTimeS(_HelperService.FilterSnap.StartTime) + \"--\" + _HelperService.GetDateTimeS(_HelperService.FilterSnap.EndTime)}}\n  <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n      class=\"fas fa-times\"></i> </span>\n</div>\n<div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n  *ngFor=\"let TItem of _HelperService.FilterSnap.OtherFilters; let i = index\" (click)=\"RemoveFilterComponent('Other',i)\">\n  {{TItem.data[0].DisplayName}}\n  <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n      class=\"fas fa-times\"></i> </span>\n</div>\n<span class=\"badge badge-pill badge-primary\"\n  *ngFor=\"let TItem of StoresList_Config.Filters\">{{TItem.Title}}</span>\n</div>\n\n  <div class=\"card-body p-0\">\n    <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n      <thead>\n        <tr>\n          <th class=\"td-status\" *ngIf=\"ColumnConfig[0].Value\">\n            <!-- <i class=\"{{ 'Common.StatusI' | translate }}\"></i> -->\n          </th>\n          <th class=\"\">\n            Store Name\n          </th>\n          <th class=\"text-center\" *ngIf=\"ColumnConfig[1].Value\">\n            City\n          </th>\n          <th class=\"\" *ngIf=\"ColumnConfig[2].Value\">\n            Merchant\n          </th>\n          <th class=\"text-center\" *ngIf=\"ColumnConfig[3].Value\">\n            R. %\n          </th>\n          <th class=\"text-center\" *ngIf=\"ColumnConfig[4].Value\">\n            POS\n          </th>\n          <th class=\"text-center\" *ngIf=\"ColumnConfig[5].Value\">\n            Active POS\n          </th>\n          <th class=\"text-center\" *ngIf=\"ColumnConfig[6].Value\">\n            RM\n          </th>\n          <th class=\"td-date\" *ngIf=\"ColumnConfig[7].Value\">\n            Active Date\n          </th>\n\n          <th class=\"d-flex\">\n            <span class=\"btn cursor-pointer text-primary p-0\" style=\"position: absolute; z-index: 3;right: -10px;\"\n              data-toggle=\"tooltip\" title=\"Edit Your Column\" (click)=\"EditColumnConfig()\">\n              <i style=\"height: 20px; width: 20px;\" data-feather=\"plus-circle\"></i>\n            </span>\n          </th>\n\n        </tr>\n      </thead>\n      <tbody>\n        <tr class=\"cursor-pointer\" (click)=\"StoresList_RowSelected(TItem)\" *ngFor=\"let TItem of StoresList_Config.Data\n                                                | paginate: { id: 'StoresList_Paginator',itemsPerPage: StoresList_Config.PageRecordLimit, currentPage: StoresList_Config.ActivePage,\n                                                totalItems: StoresList_Config.TotalRecords }\">\n          <td class=\"td-status text-center pl-0 ml-0\" *ngIf=\"ColumnConfig[0].Value\">\n            <img src=\"{{TItem.IconUrl}}\" class=\"avatar-sm rounded-circle bd bd-2\" [style.border-color]=\"TItem.StatusC\">\n          </td>\n          <td>\n            <div class=\"\">\n\n              <div class=\"tx-14 tx-semibold\"> {{TItem.DisplayName}}</div>\n            </div>\n            <div class=\"tx-12 text-muted\">\n              {{ TItem.Address | truncateText:40  }}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[1].Value\">\n            <div class=\"text-center\">\n\n              {{TItem.City || \"--\"}}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[2].Value\">\n            <div class=\"text-muted\">\n\n              {{TItem.MerchantDisplayName || \"--\"}}\n            </div>\n            <div class=\"text-muted tx-12\">\n              {{ TItem.MerchantEmailAddress || \"--\"}}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[3].Value\">\n            <div class=\"text-center\">\n\n              {{TItem.RewardPercentage || \"--\" }}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[4].Value\">\n            <div class=\"text-center\">\n\n              {{TItem.Terminals || \"--\"}}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[5].Value\">\n            <div class=\"text-primary text-center\">\n\n              {{TItem.ActiveTerminals || \"--\" }}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[6].Value\">\n            <div class=\"text-center\">\n\n              {{TItem.RmDisplayName || \"--\"}}\n            </div>\n          </td>\n\n          <td *ngIf=\"ColumnConfig[7].Value\">\n            <div class=\"text-muted text-uppercase\">\n              {{TItem.CreateDate }}\n            </div>\n          </td>\n          <td></td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"card-footer pd-20\">\n    <span class=\"mr-3\">\n      {{ 'Common.Showing' | translate }}\n      <span class=\"bold\">{{StoresList_Config.ShowingStart}} {{ 'Common.To' | translate }}\n        {{StoresList_Config.ShowingEnd}}\n      </span> {{ 'Common.Of' | translate }} {{StoresList_Config.TotalRecords}}\n    </span>\n    <span>\n      {{ 'Common.Show' | translate }}\n      <select class=\"wd-40 \" [(ngModel)]=\"StoresList_Config.PageRecordLimit\"\n        (ngModelChange)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n        <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n          {{TItem}}\n        </option>\n      </select>\n    </span>\n    <span class=\"float-right\">\n      <pagination-controls id=\"StoresList_Paginator\" class=\"pagination pagination-space\"\n        (pageChange)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n        nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n      </pagination-controls>\n    </span>\n  </div>\n</div>\n\n<div class=\"modal\" id=\"EditCol\" tabindex=\"-1\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\">Edit Column</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"form-group\" *ngFor=\"let Col of TempColumnConfig\">\n          <div class=\"form-check\">\n            <input class=\"form-check-input\" type=\"checkbox\" id=\"gridCheck\" [(ngModel)]=Col.Value>\n            <label class=\"form-check-label\" for=\"\">\n              {{Col.Name}}\n            </label>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"SaveColumnConfig()\">Save</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/accounts/tustores/acquirer/tustores.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/modules/accounts/tustores/acquirer/tustores.component.ts ***!
  \**************************************************************************/
/*! exports provided: TUStoresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoresComponent", function() { return TUStoresComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");







var TUStoresComponent = /** @class */ (function () {
    function TUStoresComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _FilterHelperService, _ChangeDetectorRef) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._FilterHelperService = _FilterHelperService;
        this._ChangeDetectorRef = _ChangeDetectorRef;
        //#region ColumnConfig
        this.TempColumnConfig = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "City",
                Value: true,
            },
            {
                Name: "Merchant Contact",
                Value: true,
            },
            {
                Name: "Reward",
                Value: true,
            },
            {
                Name: "POS",
                Value: true,
            },
            {
                Name: "ActivePOS",
                Value: true,
            },
            {
                Name: "RM",
                Value: true,
            },
            {
                Name: "Added",
                Value: true,
            },
        ];
        this.ColumnConfig = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "City",
                Value: true,
            },
            {
                Name: "Merchant Contact",
                Value: true,
            },
            {
                Name: "Reward",
                Value: true,
            },
            {
                Name: "POS",
                Value: true,
            },
            {
                Name: "ActivePOS",
                Value: true,
            },
            {
                Name: "RM",
                Value: true,
            },
            {
                Name: "Added",
                Value: true,
            },
        ];
        this.StoresList_Filter_Owners_Selected = 0;
    }
    TUStoresComponent.prototype.ngOnInit = function () {
        var _this = this;
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this.InitColumnConfig();
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            _this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null ||
                _this._HelperService.AppConfig.ActiveReferenceId == null) {
                _this.StoresList_Filter_Owners_Load();
                _this.StoresList_Setup();
            }
            else {
                _this.StoresList_Filter_Owners_Load();
                _this.StoresList_Setup();
            }
        });
    };
    TUStoresComponent.prototype.InitColumnConfig = function () {
        var temp = this._HelperService.GetStorage("StoreTable");
        if (temp != undefined && temp != null) {
            this.ColumnConfig = temp.config;
            this.TempColumnConfig = JSON.parse(JSON.stringify(temp.config));
        }
    };
    TUStoresComponent.prototype.EditColumnConfig = function () {
        this._HelperService.OpenModal("EditCol");
    };
    TUStoresComponent.prototype.SaveColumnConfig = function () {
        this.ColumnConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
        this._HelperService.SaveStorage("StoreTable", {
            config: this.ColumnConfig,
        });
        this._HelperService.CloseModal("EditCol");
    };
    //#endregion
    TUStoresComponent.prototype.SetOtherFilters = function () {
        var _this = this;
        this.StoresList_Config.SearchBaseConditions = [];
        this.StoresList_Config.SearchBaseCondition = null;
        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex(function (filter) { return (filter.data[0].OtherType == _this._HelperService.AppConfig.OtherFilters.Stores.Owner); });
        if (CurrentIndex != -1) {
            this.StoresList_Filter_Owners_Selected = null;
            this.OwnersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    };
    TUStoresComponent.prototype.StoresList_Setup = function () {
        var SearchCondition = undefined;
        if (this._HelperService.AppConfig.ActiveReferenceId != 0 &&
            this._HelperService.AppConfig.ActiveReferenceKey != undefined &&
            this._HelperService.AppConfig.ActiveReferenceKey != null &&
            this._HelperService.AppConfig.ActiveReferenceKey != "") {
            SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, "OwnerId", this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, "=");
            SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, "OwnerKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, "=");
        }
        else {
            SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, "OwnerId", this._HelperService.AppConfig.DataType.Number, "0", ">");
        }
        if (SearchCondition != undefined) {
            this.StoresList_Config = {
                Id: null,
                Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
                Title: "Available Stores",
                StatusType: "default",
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: "CreateDate desc",
                // SearchBaseCondition: SearchCondition,
                TableFields: [
                    {
                        DisplayName: "Name",
                        SystemName: "DisplayName",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "MerchantDisplayName",
                        SystemName: "MerchantDisplayName",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Contact No",
                        SystemName: "ContactNumber",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Email Address",
                        SystemName: "EmailAddress",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Terminals",
                        SystemName: "Terminals",
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: "",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "R. %",
                        SystemName: "RewardPercentage",
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: "_600",
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Today Tr",
                        SystemName: "TodaysTransactionAmount",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Last Tr",
                        SystemName: "LastTransactionDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Last Login",
                        SystemName: "LastLoginDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: this._HelperService.AppConfig.CommonResource
                            .CreateDate,
                        SystemName: "CreateDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                ],
            };
        }
        else {
            this.StoresList_Config = {
                Id: null,
                Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
                Title: "Available Stores",
                StatusType: "default",
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: "CreateDate desc",
                TableFields: [
                    {
                        DisplayName: "Name",
                        SystemName: "DisplayName",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Contact No",
                        SystemName: "ContactNumber",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Email Address",
                        SystemName: "EmailAddress",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Merchant",
                        SystemName: "OwnerDisplayName",
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: "",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Terminals",
                        SystemName: "Terminals",
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: "",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "R. %",
                        SystemName: "RewardPercentage",
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: "_600",
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Last Tr",
                        SystemName: "LastTransactionDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                    {
                        DisplayName: "Last Login",
                        SystemName: "LastLoginDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: false,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                    {
                        DisplayName: this._HelperService.AppConfig.CommonResource
                            .CreateDate,
                        SystemName: "CreateDate",
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: "td-date",
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                    },
                ],
            };
        }
        this.StoresList_Config = this._DataHelperService.List_Initialize(this.StoresList_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.Stores, this.StoresList_Config);
        this.StoresList_GetData();
    };
    TUStoresComponent.prototype.StoresList_ToggleOption = function (event, Type) {
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.StoresList_Config);
        this.StoresList_Config = this._DataHelperService.List_Operations(this.StoresList_Config, event, Type);
        if ((this.StoresList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)) {
            this.StoresList_GetData();
        }
    };
    TUStoresComponent.prototype.StoresList_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
        this.StoresList_Config = TConfig;
    };
    TUStoresComponent.prototype.StoresList_RowSelected = function (ReferenceData) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveStore, {
            ReferenceKey: ReferenceData.ReferenceKey,
            ReferenceId: ReferenceData.ReferenceId,
            DisplayName: ReferenceData.DisplayName,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
        });
        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
                .Dashboard,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
        ]);
    };
    TUStoresComponent.prototype.StoresList_Filter_Owners_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.StoresList_Filter_Owners_Option = {
            placeholder: "Sort by Owner",
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUStoresComponent.prototype.StoresList_Filter_Owners_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.StoresList_Config, this._HelperService.AppConfig.OtherFilters.Stores.Owner);
        this.OwnersEventProcessing(event);
    };
    TUStoresComponent.prototype.OwnersEventProcessing = function (event) {
        if (event.value == this.StoresList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "MerchantId", this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, "=");
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.StoresList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.StoresList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "MerchantId", this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, "=");
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.StoresList_Filter_Owners_Selected = event.value;
            this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict("", "MerchantId", this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, "="));
        }
        this.StoresList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    //#endregion
    //#region filterOperations
    TUStoresComponent.prototype.Active_FilterValueChanged = function (event) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.StoresList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.StoresList_GetData();
    };
    TUStoresComponent.prototype.Save_NewFilter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._FilterHelperService._BuildFilterName_Store(result.value);
                _this._HelperService.Save_NewFilter(_this._HelperService.AppConfig.FilterTypeOption.Stores);
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    TUStoresComponent.prototype.Delete_Filter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._HelperService.Delete_Filter(_this._HelperService.AppConfig.FilterTypeOption.Stores);
                _this._FilterHelperService.SetStoreConfig(_this.StoresList_Config);
                _this.StoresList_GetData();
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    TUStoresComponent.prototype.ApplyFilters = function (event, Type) {
        this._HelperService.MakeFilterSnapPermanent();
        this.StoresList_GetData();
    };
    TUStoresComponent.prototype.ResetFilters = function (event, Type) {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.StoresList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.StoresList_GetData();
    };
    TUStoresComponent.prototype.RemoveFilterComponent = function (Type, index) {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.StoresList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.StoresList_GetData();
    };
    TUStoresComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-stores",
            template: __webpack_require__(/*! ./tustores.component.html */ "./src/app/modules/accounts/tustores/acquirer/tustores.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["DataHelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["FilterHelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], TUStoresComponent);
    return TUStoresComponent;
}());



/***/ }),

/***/ "./src/app/modules/accounts/tustores/acquirer/tustores.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/modules/accounts/tustores/acquirer/tustores.module.ts ***!
  \***********************************************************************/
/*! exports provided: TUStoresRoutingModule, TUStoresModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoresRoutingModule", function() { return TUStoresRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoresModule", function() { return TUStoresModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _service_main_pipe_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../service/main-pipe.module */ "./src/app/service/main-pipe.module.ts");
/* harmony import */ var _tustores_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./tustores.component */ "./src/app/modules/accounts/tustores/acquirer/tustores.component.ts");












var routes = [{ path: "", component: _tustores_component__WEBPACK_IMPORTED_MODULE_11__["TUStoresComponent"] }];
var TUStoresRoutingModule = /** @class */ (function () {
    function TUStoresRoutingModule() {
    }
    TUStoresRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUStoresRoutingModule);
    return TUStoresRoutingModule;
}());

var TUStoresModule = /** @class */ (function () {
    function TUStoresModule() {
    }
    TUStoresModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUStoresRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                _service_main_pipe_module__WEBPACK_IMPORTED_MODULE_10__["MainPipe"]
            ],
            declarations: [_tustores_component__WEBPACK_IMPORTED_MODULE_11__["TUStoresComponent"]]
        })
    ], TUStoresModule);
    return TUStoresModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-accounts-tustores-acquirer-tustores-module.js.map