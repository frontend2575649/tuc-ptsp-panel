(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-acquirer-campaigns-campaign-campaign-module"],{

/***/ "./src/app/panel/acquirer/campaigns/campaign/campaign.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaign/campaign.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-right mb-3\">\n\n    <div class=\"float-left\">\n        #{{_CampaignDetails.ReferenceId || \"--\"}}\n    </div>\n    <button type=\"button\" *ngIf=\"_CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Creating\"\n        class=\"btn btn-success btn-sm mr-2\"\n        (click)=\"UpdateCampaignStatus(_HelperService.AppConfig.Status.Campaign.Published)\">Publish </button>\n    <button type=\"button\"\n        *ngIf=\"_CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Paused || _CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.LowBalance\"\n        class=\"btn btn-success btn-sm mr-2\"\n        (click)=\"UpdateCampaignStatus(_HelperService.AppConfig.Status.Campaign.Published)\">Resume </button>\n    <button type=\"button\" *ngIf=\"_CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Published\"\n        class=\"btn btn-warning btn-sm mr-2\"\n        (click)=\"UpdateCampaignStatus(_HelperService.AppConfig.Status.Campaign.Paused)\">Pause</button>\n    <button type=\"button\" *ngIf=\"_CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Published  \n        || _CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Paused \n        ||  _CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.LowBalance \"\n        class=\"btn btn-danger btn-sm mr-2\"\n        (click)=\"UpdateCampaignStatus(_HelperService.AppConfig.Status.Campaign.Expired)\">Expire </button>\n    <button type=\"button\" class=\"btn btn-primary mr-2 btn-sm \" (click)=\"DuplicateCampaign()\"\n        *ngIf=\"_CampaignDetails.StatusCode != _HelperService.AppConfig.Status.Campaign.Creating\">\n        Duplicate\n    </button>\n    <button type=\"button\" class=\"btn btn-danger btn-sm \" (click)=\"DeleteCampaign()\">Delete\n    </button>\n\n</div>\n<div [className]=\"_HelperService._Assets.Box.root\">\n\n        <div class=\"card-header b-b\">\n            <h5>{{_CampaignDetails.Name || \"--\"}}\n    \n                <span class=\"float-right \">\n                    <span class=\"{{_CampaignDetails.StatusB}}\">{{ _CampaignDetails.StatusName || \"--\" }}</span>\n                </span>\n            </h5>\n        </div>\n \n  \n    <div class=\"box-body p-0\">\n        <div class=\"row no-gutters\">\n            <div class=\"col-lg-12 col-md-6 col-sm-6 col-xs-12 \">\n                <div class=\"row no-gutters\">\n                    <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\n                        <div class=\"p-3\">\n                            <div class=\"mb-2\" title=\"{{_CampaignDetails.SubTypeName}}\">\n                                <small class=\"text-muted\">Campaign Type</small>\n                                <div class=\"_500 text-ellipses\">{{ _CampaignDetails.TypeName || \"0\" }}\n                                </div>\n                            </div>\n                            <div title=\"{{_CampaignDetails.SubTypeValue}}\"\n                                *ngIf=\"_CampaignDetails.SubTypeCode=='fixedamount'\">\n                                <small class=\"text-muted\">Reward Amount</small>\n                                <div class=\"_700 text-warn text-ellipses\"> <span\n                                        [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>\n                                    {{ _CampaignDetails.SubTypeValue || \"0\" }}\n                                </div>\n                            </div>\n                            <div title=\"{{_CampaignDetails.SubTypeValue}}\"\n                                *ngIf=\"_CampaignDetails.SubTypeCode=='amountpercentage'\">\n                                <small class=\"text-muted \">Reward Percentage</small>\n                                <div class=\"_700 text-warn text-ellipses\">{{ _CampaignDetails.SubTypeValue || \"0\" }}%\n                                </div>\n                            </div>\n\n                        </div>\n                    </div>\n                    <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\n                        <div class=\"p-3\">\n                            <div class=\"mb-2\" title=\"{{_CampaignDetails.MinimumInvoiceAmount}}\">\n                                <small class=\"text-muted\">Minimum Invoice Amount</small>\n                                <div class=\"_500 text-ellipses\"> <span\n                                        [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{ _CampaignDetails.MinimumInvoiceAmount || \"0\" }}\n                                </div>\n                            </div>\n                            <div title=\"{{_CampaignDetails.MaximumRewardAmount}}\">\n                                <small class=\"text-muted\">Maximum Reward Amount</small>\n                                <div class=\"_500 text-ellipses\"> <span\n                                        [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>{{ _CampaignDetails.MaximumRewardAmount || \"0\" }}\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\n                        <div class=\"p-3\">\n                            <div class=\"mb-2\" title=\"{{ _CampaignDetails.StartDateS  }} -\n                            {{ _CampaignDetails.EndDateS \">\n                                <small class=\"text-muted\">Schedule</small>\n                                <div class=\"_500 text-ellipses\">{{ _CampaignDetails.StartDateS || \"--\" }} -\n                                    {{ _CampaignDetails.EndDateS || \"--\" }} </div>\n                            </div>\n                            <!-- <div title=\" {{ _HelperService._UserAccount.MobileNumber }}\">\n                                <small class=\"text-muted\">{{\"System.Users.ContactNumber\" | translate  }}</small>\n                                <div class=\"_500 text-ellipses\">{{ _HelperService._UserAccount.MobileNumber || \"--\" }}\n                                </div>\n                            </div> -->\n                        </div>\n                    </div>\n                    <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12  light lt  b-r\">\n                        <div class=\"p-3\">\n                            <div class=\"mb-2\" title=\"{{ _CampaignDetails.CreatedByDisplayName}}\">\n                                <small class=\"text-muted\">{{ \"Common.CreateDate\" | translate  }}\n                                    <!-- <span class=\"text-xxs\"> :\n                                        #{{ _CampaignDetails.ReferenceId || \"--\" }}</span> -->\n                                </small>\n                                <div class=\"text-sm _500 text-ellipses\">\n                                    {{ _CampaignDetails.CreateDateS || \"--\" }}\n                                    <span class=\"text-xxs \">{{ _CampaignDetails.CreatedByDisplayName || \"--\" }}</span>\n                                </div>\n                            </div>\n                            <div title=\"{{ _CampaignDetails.ModifyByDisplayName}}\">\n                                <small class=\"text-muted\">{{ \"Common.ModifyDate\" | translate  }} </small>\n                                <div class=\"text-sm _500 text-ellipses\">\n                                    {{ _CampaignDetails.ModifyDateS || \"--\" }}\n                                    <span class=\"text-xxs \">{{ _CampaignDetails.ModifyByDisplayName  }}</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"media-body mg-t-10\">\n    <ul class=\"nav nav-tabs\">\n        <li class=\"nav-item\"><a class=\"nav-link active\" href=\"#\" data-toggle=\"tab\"\n                data-target=\"#tabTransactions\">Rewards History</a>\n        </li>\n        <li class=\"nav-item\" *ngIf=\"_CampaignDetails.StatusCode  == _HelperService.AppConfig.Status.Campaign.Creating \">\n            <a class=\"nav-link\" href=\"#\" data-toggle=\"tab\" data-target=\"#tabManageCampaign\">Manage\n                Campaign</a></li>\n    </ul>\n</div>\n<div class=\"tab-content bg-white bd bd-gray-300 bd-t-0\">\n    <div class=\"tab-pane fade show active\" id=\"tabTransactions\">\n        <div class=\"card bd-0\"  *ngIf=\"TUTr_Config != undefined\">\n            <div [className]=\"_HelperService._Assets.Box.header\">\n\n         \n            <div [className]=\"_HelperService._Assets.Box.headerTitle\">\n               {{ TUTr_Config.Title }}\n            </div>\n            <div [className]=\"_HelperService._Assets.Box.headerTitleRight\">\n                <ul class=\"nav nav-xs\">\n                    <li title=\"{{ 'Common.FilterT' | translate }}\" class=\"nav-item\"\n                        (click)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.ToggleFilter)\"><a [className]=\"_HelperService._Assets.Box.headerTitleRightItem\"\n                            ><i class=\" {{ 'Common.FilterI' | translate }}\"></i></a></li>\n                    <li title=\"{{ 'Common.DownloadT' | translate }}\" class=\"nav-item\"\n                        (click)=\"TUTr_ToggleOption($event,_HelperService.AppConfig.ListToggleOption.Csv)\">\n                        <a [className]=\"_HelperService._Assets.Box.headerTitleRightItem\" ><i class=\"{{ 'Common.DownloadI' | translate }}\"></i></a>\n                    </li>\n                    <li title=\"{{ 'Common.RefreshT' | translate }}\" class=\"nav-item\"\n                        (click)=\"TUTr_ToggleOption($event,_HelperService.AppConfig.ListToggleOption.Refresh)\">\n                        <a  [className]=\"_HelperService._Assets.Box.headerTitleRightItem\" ><i class=\"{{ 'Common.RefreshI' | translate }}\"></i></a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n            <div [className]=\"_HelperService._Assets.Box.bodyFilter\">\n                <div class=\"row no-gutters\" >\n                    <div class=\"col-md-2\">\n                        <div class=\"form-group mr-2 mb-2\">\n                            <select2 class=\"full-width\" [options]=\"TUTr_Filter_UserAccount_Option\"\n                                (valueChanged)=\"TUTr_Filter_UserAccounts_Change($event)\">\n                            </select2>\n                        </div>\n                    </div>\n                    <div class=\"col-md-2 \">\n                        <div class=\"form-group  mr-2 mb-2\">\n                            <select2 class=\"full-width\" [options]=\"TUTr_Filter_TransactionType_Option\"\n                                (valueChanged)=\"TUTr_Filter_TransactionTypes_Change($event)\">\n                            </select2>\n                        </div>\n                    </div>\n\n                    <!-- <div class=\"col-md-2 \">\n                            <div class=\"input-group  mr-2\">\n                              <input type=\"text\" maxlength=\"100\" [(ngModel)]=\"TUTr_Config.SearchParameter\" class=\"form-control\" (keyup)=\"\n                                  TUTr_ToggleOption(\n                                    null,\n                                    _HelperService.AppConfig.ListToggleOption.Search\n                                  )\n                                \" placeholder=\"{{ 'Common.Search' | translate }}\" />\n                              <div class=\"input-group-append\">\n                                <button class=\"btn white\" type=\"button\" (click)=\"\n                                        TUTr_ToggleOption(\n                                          null,\n                                          _HelperService.AppConfig.ListToggleOption.Search\n                                        )\n                                      \">\n                                  {{ \"Common.Go\" | translate }}\n                                </button>\n                              </div>\n                            </div>\n                          </div> -->\n                    <div class=\"col-md-2\">\n                        <div class=\"form-group mr-2 mb-2\">\n                            <select2 class=\"full-width\" [options]=\"TUTr_Filter_Merchant_Option\"\n                                (valueChanged)=\"TUTr_Filter_Merchants_Change($event)\">\n                            </select2>\n                        </div>\n                    </div>\n                    <div class=\"col-md-2 \">\n                        <div class=\"form-group mr-2 mb-2\">\n                            <input type=\"text\" class=\"form-control full-width\" name=\"daterangeInput\" daterangepicker\n                                [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                                (selected)=\"TUTr_ToggleOption_Date($event,_HelperService.AppConfig.ListToggleOption.Date)\" />\n                        </div>\n                    </div>\n                    <div class=\"col-md-2 \">\n                        <div class=\"form-group mr-2 mb-2\">\n                            <select2 class=\"full-width\" [options]=\"_HelperService.S2_Sort_Option\"\n                                [data]=\"TUTr_Config.SortExpressionOption\" [value]=\"TUTr_Config.SortExpression\"\n                                (valueChanged)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Sort)\">\n                            </select2>\n                        </div>\n                    </div>\n                    <div class=\"col-md-2 \">\n                        <div class=\"form-group  mr-2 mb-2\">\n                            <select2 class=\"full-width\" [options]=\"_DataHelperService.S2_Status_Option\"\n                                [data]=\"TUTr_Config.StatusOptions\" [value]=\"TUTr_Config.Status\"\n                                (valueChanged)=\"TUTr_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                            </select2>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"mt-2\" *ngIf=\"TUTr_Config.ToggleFilter\">\n                    <div class=\"row no-gutters\">\n                        <div class=\"col-md-2\">\n                            <div class=\"form-group mr-2 mb-2\">\n                                <select2 class=\"full-width\" [options]=\"TUTr_Filter_CardBrand_Option\"\n                                    (valueChanged)=\"TUTr_Filter_CardBrands_Change($event)\">\n                                </select2>\n                            </div>\n                        </div>\n                        <div class=\"col-md-2\">\n                            <div class=\"form-group mr-2 mb-2\">\n                                <select2 class=\"full-width\" [options]=\"TUTr_Filter_CardBank_Option\"\n                                    (valueChanged)=\"TUTr_Filter_CardBanks_Change($event)\">\n                                </select2>\n                            </div>\n                        </div>\n                        <div class=\"col-md-2\">\n                            <div class=\"form-group mr-2 mb-2\" *ngIf=\"!TUTr_Filter_Store_Toggle\">\n                                <select2 class=\"full-width\" [options]=\"TUTr_Filter_Store_Option\"\n                                    (valueChanged)=\"TUTr_Filter_Stores_Change($event)\">\n                                </select2>\n                            </div>\n                        </div>\n\n                        <div class=\"col-md-2\">\n                            <div class=\"form-group mr-2 mb-2\">\n                                <select2 class=\"full-width\" [options]=\"TUTr_Filter_Provider_Option\"\n                                    (valueChanged)=\"TUTr_Filter_Providers_Change($event)\">\n                                </select2>\n                            </div>\n                        </div>\n                        <div class=\"col-md-2\">\n                            <div class=\"form-group mr-2 mb-2\" *ngIf=\"!_HelperService.ToggleField\">\n                                <select2 class=\"full-width\" [options]=\"TUTr_Filter_Issuer_Option\"\n                                    (valueChanged)=\"TUTr_Filter_Issuers_Change($event)\">\n                                </select2>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row mb-2\">\n                        <div class=\"col-md-6\">\n                            <div class=\"mb-2\">\n                                <ng5-slider [value]=\"TUTr_InvoiceRangeMinAmount\"\n                                    [highValue]=\"TUTr_InvoiceRangeMaxAmount\"\n                                    [options]=\"_HelperService.AppConfig.RangeInvoiceAmountOptions\"\n                                    (userChangeEnd)=\"TUTr_InvoiceRange_OnChange($event)\">\n                                </ng5-slider>\n                            </div>\n                        </div>\n                        <div class=\"col-md-6\">\n                            <div>\n                                <ng5-slider [value]=\"TUTr_RewardRangeMinAmount\" [highValue]=\"TUTr_RewardRangeMaxAmount\"\n                                    [options]=\"_HelperService.AppConfig.RangeRewardAmountOptions\"\n                                    (userChangeEnd)=\"TUTr_RewardRange_OnChange($event)\">\n                                </ng5-slider>\n                            </div>\n                        </div>\n                    </div>\n                    <!-- <div class=\"form-group\">\n                        <div class=\"radio float-left mr-3\">\n                            <label class=\"ui-check\"><input type=\"radio\" name=\"sourceoption\" checked [value]=\"0\"\n                                    (change)=\"TUTr_Filter_TransactionSources_Change(0)\">\n                                <i class=\"dark-white\"></i> All Transactions </label>\n                        </div>\n                        <div class=\"radio float-left mr-3\"><label class=\"ui-check\"><input type=\"radio\"\n                                    name=\"sourceoption\" [value]=\"167\"\n                                    (change)=\"TUTr_Filter_TransactionSources_Change(167)\">\n                                <i class=\"dark-white\"></i>\n                                <span class=\"ml-2 _600  text-deep-orange\"> ThankUCash </span>\n                            </label></div>\n                        <div class=\"radio  mr-3\"><label class=\"ui-check\"><input type=\"radio\" name=\"sourceoption\"\n                                    [value]=\"244\" (change)=\"TUTr_Filter_TransactionSources_Change(244)\">\n                                <i class=\"dark-white\"></i>\n                                <span class=\"ml-2 _600  text-deep-purple\"> ThankUCash+ </span>\n                            </label></div>\n                    </div> -->\n                </div>\n            </div>\n            <div class=\"box-body p-0\">\n                <table class=\"{{ _HelperService.AppConfig.TablesConfig.DefaultClass }}\">\n                    <thead>\n                        <tr>\n                            <th class=\"td-date\">Date</th>\n                            <th>\n                                User\n                            </th>\n                            <th>Type</th>\n                            <th class=\"td-number\">Bank</th>\n                            <th class=\"td-amount\">Sale</th>\n                            <th class=\"td-amount\">Reward</th>\n                            <th>Merchant</th>\n                            <th>Done By</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"\n                                let TItem of (TUTr_Config.Data\n                                  | paginate\n                                    : {\n                                        id: 'TUTr_Paginator',\n                                        itemsPerPage: TUTr_Config.PageRecordLimit,\n                                        currentPage: TUTr_Config.ActivePage,\n                                        totalItems: TUTr_Config.TotalRecords\n                                      })\n                              \">\n                            <td class=\"td-date\">\n                                <div>{{ TItem.TransactionDateD || \"--\" }}\n                                    <span class=\"text-xxs _600 w-60\">{{ TItem.TransactionDateT || \"--\" }}</span>\n                                </div>\n                                <div class=\"text-xs _500 \">\n                                    <span class=\"text-lowercase mr-1 \"\n                                        [ngClass]=\"TItem.StatusBadge\">{{ TItem.StatusName || \"--\" }}</span>\n                                    <!-- <span class=\"text-lowercase  badge deep-purple \"\n                                        *ngIf=\"TItem.SourceId == 244\">TUC+</span>\n                                    <span class=\"text-lowercase  badge deep-orange \"\n                                        *ngIf=\"TItem.SourceId == 167\">TUC</span> -->\n                                </div>\n                            </td>\n                            <td>\n                                <div class=\"text-warn _500\">\n                                    {{ TItem.UserDisplayName || \"--\" }}\n                                </div>\n                                <div class=\"text-xs _500 text-muted\">\n                                    {{ TItem.UserMobileNumber || \"--\" }}\n                                </div>\n                            </td>\n                            <td>\n                                <div class=\"text-xs _500 text-uppercase\" *ngIf=\"TItem.TypeName != 'Cash '\">\n                                    <span *ngIf=\"TItem.CardBrandName != undefined\">\n                                        <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'visa'\"\n                                            class=\"fab fa-cc-visa text-visa-card text-sm float-right\"></i>\n                                        <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'mastercard'\"\n                                            class=\"fab fa-cc-mastercard text-mastercard text-sm  float-right\"></i>\n                                        <i *ngIf=\"TItem.CardBrandName.toLowerCase() == 'verve'\"\n                                            class=\"fas fa-credit-card text-verve-card text-sm  float-right\"></i>\n                                    </span>\n                                    {{ TItem.CardBrandName }}\n                                </div>\n                                <div class=\"text-xs _500 text-uppercase\" *ngIf=\"TItem.TypeName == 'Cash '\">\n                                    <i class=\"fab fas fa-money-bill-alt text-cash   float-right\"></i>\n                                    Cash Payment\n                                </div>\n                                <div class=\"text-xs _500 text-muted\">\n                                    {{ TItem.TypeName  }}\n                                </div>\n                            </td>\n                            <td>\n                                <div class=\"text-ellipses\">{{ TItem.CardBankName || \"--\" }}</div>\n                                <div class=\"text-xs _500 text-muted\">\n                                    {{ TItem.AccountNumber  }}\n                                </div>\n                            </td>\n                            <td class=\"td-amount _600 text-warning\">\n                                <div>\n                                    <span [innerHtml]=\"_HelperService.AppConfig.CurrencySymbol\"></span>\n                                    {{ TItem.InvoiceAmount | number: \"1.2-2\" || \"--\" }}\n                                </div>\n                                <!-- <div class=\"text-xs _500 text-success\">\n                                    {{TItem.CommissionAmount | number:'1.2-2' || \"--\"}}\n                                    <span class=\"text-xxs \">COM</span>\n                                </div> -->\n                            </td>\n                            <td class=\"td-amount _600 \">\n                                <div>{{ TItem.RewardAmount | number: \"1.2-2\" || \"--\" }}</div>\n                                <div class=\"text-xs _500 text-success\"> {{TItem.UserAmount | number:'1.2-2' || \"--\"}}\n                                    <span class=\"text-xxs \">USER</span>\n                                </div>\n                            </td>\n\n                            <td>\n                                <div>{{ TItem.ParentDisplayName || \"--\" }}</div>\n                                <div class=\"text-xs _500 text-muted\">\n                                    {{ TItem.SubParentDisplayName || \"--\" }}\n                                </div>\n                            </td>\n\n                            <td>\n                                <div>{{ TItem.CreatedByDisplayName || \"--\" }}\n                                </div>\n                                <div class=\"text-xs _500 text-muted\">\n                                    {{ TItem.ProviderDisplayName || \"--\" }}\n                                </div>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n                <div class=\"  p-4\">\n                    <span class=\"mr-1\">\n                        {{ \"Common.Showing\" | translate }}\n                        <span class=\"bold\">{{ TUTr_Config.ShowingStart }}\n                            {{ \"Common.To\" | translate }}\n                            {{ TUTr_Config.ShowingEnd }}\n                        </span>\n                        {{ \"Common.Of\" | translate }} {{ TUTr_Config.TotalRecords }}\n                    </span>\n                    |\n                    <span class=\"ml-1\">\n                        {{ \"Common.Show\" | translate }}\n                        <select class=\" w-64 \" [(ngModel)]=\"TUTr_Config.PageRecordLimit\" (ngModelChange)=\"\n                                TUTr_ToggleOption(\n                                  $event,\n                                  _HelperService.AppConfig.ListToggleOption.Limit\n                                )\n                              \">\n                            <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n                                {{ TItem }}\n                            </option>\n                        </select>\n                        {{ \"Common.Entries\" | translate }}\n                    </span>\n                    <span class=\"float-right\">\n                        <pagination-controls id=\"TUTr_Paginator\" (pageChange)=\"\n                                TUTr_ToggleOption(\n                                  $event,\n                                  _HelperService.AppConfig.ListToggleOption.Page\n                                )\n                              \" nextLabel=\"{{ 'Common.Next' | translate }}\"\n                            previousLabel=\"{{ 'Common.Previous' | translate }}\">\n                        </pagination-controls>\n                    </span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"tab-pane fade\" id=\"tabManageCampaign\"\n        *ngIf=\"_CampaignDetails.StatusCode == _HelperService.AppConfig.Status.Campaign.Creating \">\n        <form [formGroup]=\"Form_AddUser\" (ngSubmit)=\"Form_AddUser_Process(Form_AddUser.value)\" role=\"form\">\n            <div class=\"card bd-0\">\n                <div class=\"pt-3 pb-3\">\n                <div class=\"card-header b-b b-2x\">\n                    <h6 class=\"text-primary\">Reward Campaign Information</h6>\n                    <small class=\"text-secondary\"> Campaign information setup </small>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"row\">\n                        <div class=\"col-lg-6 col-md-6 col-sm-8 col-xs-12\">\n                            <div class=\"row\">\n                                <div class=\"col-md-12\">\n                                    <div class=\"form-group\">\n                                        <label> Name <span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <input type=\"text\" name=\"Name\" maxlength=\"128\" class=\"form-control\"\n                                                [formControl]=\"Form_AddUser.controls['Name']\" [ngClass]=\"{\n                                                                        'is-invalid':\n                                                                          !Form_AddUser.controls['Name'].valid &&\n                                                                          Form_AddUser.controls['Name'].touched\n                                                                      }\" />\n                                        </div>\n                                        <label class=\"text-danger\" for=\"Name\"\n                                            *ngIf=\" Form_AddUser.controls['Name'].hasError('required') && Form_AddUser.controls['Name'].touched\">\n                                            enter campaign name</label>\n                                        <label class=\"text-danger\" for=\"Name\"\n                                            *ngIf=\"Form_AddUser.controls['Name'].hasError(  'minlength' ) || Form_AddUser.controls['Name'].hasError('maxlength')\">\n                                            campaign name must be between 4 to 128 characters\n                                        </label>\n                                    </div>\n                                </div>\n                                <!-- <div class=\"col-md-12\">\n                                        <div class=\"form-group\">\n                                            <label> Campaign Type <span class=\"text-danger\">*</span>\n                                            </label>\n                                            <div class=\"controls\">\n                                                <select class=\"form-control\" [formControl]=\"Form_AddUser.controls['TypeCode']\">\n                                                    <option *ngFor=\"let TItem of CampaignType\" value=\"{{TItem.TypeCode}}\"\n                                                        [value]=\"TItem.TypeCode\"> {{TItem.Name}}</option>\n                                                </select>\n                                            </div>\n                                            <label class=\"text-danger\" for=\"TypeCode\"\n                                                *ngIf=\" Form_AddUser.controls['TypeCode'].hasError('required') && Form_AddUser.controls['TypeCode'].touched\">\n                                                select campaign type</label>\n                                        </div>\n                                    </div> -->\n                                <div class=\"col-lg-12 mb-3\">\n                                    <div class=\"row\">\n                                        <div class=\"col-12\">\n                                            <label> Campaign Type <span class=\"text-danger\">*</span>\n                                            </label>\n                                        </div>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"b-a p-3 r-1x text-center cursor-pointer\"\n                                                [ngClass]=\"{'bg-success tx-white': Form_AddUser.value.TypeCode == 'owncardcustomers','bg-white tx-dark': Form_AddUser.value.TypeCode != 'owncardcustomers' }\"\n                                                (click)=\"SetCampaignType('owncardcustomers')\">\n                                                <div class=\"pb-1\">\n                                                    All <span\n                                                        class=\"tx-bold\">{{_HelperService.AppConfig.ActiveOwnerDisplayName}}\n                                                    </span> Bank Customers\n                                                </div>\n                                                <div  [ngClass]=\"{'bg-success tx-white ': Form_AddUser.value.TypeCode == 'owncardcustomers','bg-white tx-dark': Form_AddUser.value.TypeCode != 'owncardcustomers' }\"\n                                                >\n                                                    Reward users who use <span\n                                                        class=\"tx-bold\">{{_HelperService.AppConfig.ActiveOwnerDisplayName}}\n                                                    </span>\n                                                    bank card for puchase\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"b-a p-3 r-1x text-center cursor-pointer\"\n                                                [ngClass]=\"{'bg-success tx-white': Form_AddUser.value.TypeCode == 'othercardcustomers', 'bg-white tx-dark': Form_AddUser.value.TypeCode != 'othercardcustomers' }\"\n                                                (click)=\"SetCampaignType('othercardcustomers')\">\n                                                <div class=\"pb-1\">\n                                                    Non <span\n                                                        class=\"tx-bold\">{{_HelperService.AppConfig.ActiveOwnerDisplayName}}\n                                                    </span> Bank\n                                                    Customers\n                                                </div>\n                                                <div [ngClass]=\"{'bg-success tx-white tx-sm': Form_AddUser.value.TypeCode == 'othercardcustomers', 'bg-white tx-dark': Form_AddUser.value.TypeCode != 'othercardcustomers' }\"\n                                                >\n                                                    Reward other bank users who use <span\n                                                        class=\"tx-bold\">{{_HelperService.AppConfig.ActiveOwnerDisplayName}}\n                                                    </span>\n                                                    pos\n                                                    terminals\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-12\">\n                                    <div class=\"form-group\">\n                                        <label> Schedule <span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <input type=\"text\" class=\"form-control \" name=\"daterangeInput\"\n                                                daterangepicker readonly\n                                                [options]=\"_HelperService.AppConfig.DateRangeFutureOptions\"\n                                                [formControl]=\"Form_AddUser.controls['TStartDate']\"\n                                                (selected)=\"ScheduleDateRangeChange($event)\" />\n                                        </div>\n                                        <label class=\"text-danger\" for=\"TStartDate\"\n                                            *ngIf=\" Form_AddUser.controls['TStartDate'].hasError('required') && Form_AddUser.controls['TStartDate'].touched\">\n                                            select campaign Schedule</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"form-group\">\n                                        <label> Reward Type <span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <select class=\"form-control\"\n                                                [formControl]=\"Form_AddUser.controls['SubTypeCode']\"\n                                                (change)=\"RewardTypeChange()\">\n                                                <option *ngFor=\"let TItem of CampaignRewardType\"\n                                                    value=\"{{TItem.TypeCode}}\" [value]=\"TItem.TypeCode\"> {{TItem.Name}}\n                                                </option>\n                                            </select>\n                                        </div>\n                                        <label class=\"text-danger\" for=\"SubTypeCode\"\n                                            *ngIf=\" Form_AddUser.controls['SubTypeCode'].hasError('required') && Form_AddUser.controls['SubTypeCode'].touched\">\n                                            select campaign type</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"form-group\">\n                                        <label\n                                            *ngIf=\"Form_AddUser.value.SubTypeCode != 'fixedamount' && Form_AddUser.value.SubTypeCode != 'amountpercentage'\">Reward\n                                            Amount / Percentage<span class=\"text-danger\">*</span>\n                                        </label>\n                                        <label *ngIf=\"Form_AddUser.value.SubTypeCode == 'fixedamount'\">Reward\n                                            Amount<span class=\"text-danger\">*</span>\n                                        </label>\n                                        <label *ngIf=\"Form_AddUser.value.SubTypeCode == 'amountpercentage'\">Reward\n                                            Percentage<span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <input type=\"text\" name=\"SubTypeValue\" minlength=\"0\" maxlength=\"100000000\"\n                                                class=\"form-control\" (keypress)=\"_HelperService.PreventText($event)\"\n                                                [formControl]=\"Form_AddUser.controls['SubTypeValue']\" [ngClass]=\"{\n                                                                                        'is-invalid':\n                                                                                          !Form_AddUser.controls['SubTypeValue'].valid &&\n                                                                                          Form_AddUser.controls['SubTypeValue'].touched\n                                                                                      }\" />\n                                        </div>\n                                        <label class=\"text-danger\" for=\"SubTypeValue\" *ngIf=\"\n                                                                                      Form_AddUser.controls['SubTypeValue'].hasError(\n                                                                                        'required'\n                                                                                      ) && Form_AddUser.controls['SubTypeValue'].touched\n                                                                                    \"> Enter reward amount /\n                                            percentage</label>\n                                        <label class=\"text-danger\" for=\"SubTypeValue\" *ngIf=\"\n                                                                                      Form_AddUser.controls['SubTypeValue'].hasError(\n                                                                                        'minlength'\n                                                                                      ) ||\n                                                                                      Form_AddUser.controls['SubTypeValue'].hasError(\n                                                                                        'maxlength'\n                                                                                      )\n                                                                                    \">Enter valid reward amount /\n                                            percentage</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"form-group\">\n                                        <label>Minimum purchase amount<span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <input type=\"text\" name=\"MinimumInvoiceAmount\" minlength=\"0\"\n                                                maxlength=\"100000000\" class=\"form-control\"\n                                                (keypress)=\"_HelperService.PreventText($event)\"\n                                                [formControl]=\"Form_AddUser.controls['MinimumInvoiceAmount']\" [ngClass]=\"{\n                                                                                            'is-invalid':\n                                                                                              !Form_AddUser.controls['MinimumInvoiceAmount'].valid &&\n                                                                                              Form_AddUser.controls['MinimumInvoiceAmount'].touched\n                                                                                          }\" />\n\n                                        </div>\n                                        <label class=\"text-danger\" for=\"MinimumInvoiceAmount\" *ngIf=\"\n                                                                                          Form_AddUser.controls['MinimumInvoiceAmount'].hasError(\n                                                                                            'required'\n                                                                                          ) && Form_AddUser.controls['MinimumInvoiceAmount'].touched\n                                                                                        \"> Enter minimum invoice\n                                            amount</label>\n                                        <label class=\"text-danger\" for=\"MinimumInvoiceAmount\" *ngIf=\"\n                                                                                          Form_AddUser.controls['MinimumInvoiceAmount'].hasError(\n                                                                                            'minlength'\n                                                                                          ) ||\n                                                                                          Form_AddUser.controls['MinimumInvoiceAmount'].hasError(\n                                                                                            'maxlength'\n                                                                                          )\n                                                                                        \">Enter valid invoice\n                                            amount</label>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-md-6\" *ngIf=\"Form_AddUser.value.SubTypeCode != 'fixedamount'\">\n                                    <div class=\"form-group\">\n                                        <label>Maximum reward amount<span class=\"text-danger\">*</span>\n                                        </label>\n                                        <div class=\"controls\">\n                                            <input type=\"text\" name=\"MaximumRewardAmount\" minlength=\"0\"\n                                                maxlength=\"100000000\" class=\"form-control\"\n                                                (keypress)=\"_HelperService.PreventText($event)\"\n                                                [formControl]=\"Form_AddUser.controls['MaximumRewardAmount']\" [ngClass]=\"{\n                                                                                                'is-invalid':\n                                                                                                  !Form_AddUser.controls['MaximumRewardAmount'].valid &&\n                                                                                                  Form_AddUser.controls['MaximumRewardAmount'].touched\n                                                                                              }\" />\n                                        </div>\n                                        <label class=\"text-danger\" for=\"MaximumRewardAmount\" *ngIf=\"\n                                                                                              Form_AddUser.controls['MaximumRewardAmount'].hasError(\n                                                                                                'required'\n                                                                                              ) && Form_AddUser.controls['MaximumRewardAmount'].touched\n                                                                                            \"> Enter maximum reward\n                                            amount</label>\n                                        <label class=\"text-danger\" for=\"MaximumRewardAmount\" *ngIf=\"\n                                                                                              Form_AddUser.controls['MaximumRewardAmount'].hasError(\n                                                                                                'minlength'\n                                                                                              ) ||\n                                                                                              Form_AddUser.controls['MaximumRewardAmount'].hasError(\n                                                                                                'maxlength'\n                                                                                              )\n                                                                                            \">Enter valid maximum\n                                            reward\n                                            amount</label>\n                                    </div>\n                                </div>\n\n                                <!-- <div class=\"col-md-12\">\n                                                            <div class=\"form-group\">\n                                                                <label> About\n                                                                </label>\n                                                                <div class=\"controls\">\n                                                                    <input type=\"text\" name=\"Description\" maxlength=\"128\" class=\"form-control\"\n                                                                        [formControl]=\"Form_AddUser.controls['Description']\" [ngClass]=\"{\n                                                                            'is-invalid':\n                                                                              !Form_AddUser.controls['Description'].valid &&\n                                                                              Form_AddUser.controls['Description'].touched\n                                                                          }\" />\n                                                                </div>\n                                                            </div>\n                                                        </div> -->\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6 col-md-6 col-sm-4 col-xs-12 hidden-xs \">\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n                <div class=\"light p-3  b-b b-t\">\n                    <button type=\"submit\" class=\"btn btn-primary\"\n                        (click)=\"Form_AddUser.value.OperationType = 'new'\"\n                        [disabled]=\"!Form_AddUser.valid || _HelperService.IsFormProcessing\">\n                        <span *ngIf=\"!_HelperService.IsFormProcessing\">\n                            Update\n                        </span>\n                        <span *ngIf=\"_HelperService.IsFormProcessing\">\n                            <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                            {{ \"Common.PleaseWait\" | translate }}...\n                        </span>\n                    </button>\n                </div>\n            </div>\n\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/campaign/campaign.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaign/campaign.component.ts ***!
  \*************************************************************************/
/*! exports provided: TUCampaignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignComponent", function() { return TUCampaignComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");







var TUCampaignComponent = /** @class */ (function () {
    function TUCampaignComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this.CampaignKey = null;
        this.CampaignRewardType_FixedAmount = "fixedamount";
        this.CampaignRewardType_AmountPercentage = "amountpercentage";
        this._CampaignDetails = {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        };
        this.CampaignType = [
            {
                'TypeCode': 'owncardcustomers',
                'Name': 'Own Bank Card Customers'
            },
            {
                'TypeCode': 'othercardcustomers',
                'Name': 'Other Bank Card Customers'
            }
        ];
        this.CampaignRewardType = [
            {
                'TypeCode': 'fixedamount',
                'Name': 'Fixed Amount'
            },
            {
                'TypeCode': 'amountpercentage',
                'Name': 'Percentage'
            }
        ];
        this.TUTr_InvoiceRangeMinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
        this.TUTr_InvoiceRangeMaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
        this.TUTr_RewardRangeMinAmount = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
        this.TUTr_RewardRangeMaxAmount = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
        this.TUTr_Filter_UserAccount_Selected = 0;
        this.TUTr_Filter_Merchant_Selected = 0;
        this.TUTr_Filter_Store_Toggle = false;
        this.TUTr_Filter_Store_Selected = 0;
        this.TUTr_Filter_Provider_Selected = 0;
        this.TUTr_Filter_Issuer_Selected = 0;
        this.TUTr_Filter_TransactionSources_Selected = 0;
        this.TUTr_Filter_TransactionType_Selected = 0;
        this.TUTr_Filter_CardBrand_Selected = 0;
        this.TUTr_Filter_CardBank_Selected = 0;
    }
    TUCampaignComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null) {
                _this._Router.navigate([_this._HelperService.AppConfig.Pages.System.NotFound]);
            }
            else {
                _this.GetCampaignDetails();
            }
        });
        this.Form_AddUser_Load();
        // this.TUTr_Setup();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();
    };
    TUCampaignComponent.prototype.GetCampaignDetails = function () {
        var _this = this;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaign,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.IsFormProcessing = false;
                _this._CampaignDetails = _Response.Result;
                if (_this._DaterangePickerComponent != undefined) {
                    _this._DaterangePickerComponent.datePicker.setStartDate(moment(_this._CampaignDetails.StartDate).utc().format("YYYY-MM-DD"));
                    _this._DaterangePickerComponent.datePicker.setEndDate(moment(_this._CampaignDetails.EndDate).utc().format("YYYY-MM-DD"));
                }
                _this._CampaignDetails.StartDateS = _this._HelperService.GetDateS(_this._CampaignDetails.StartDate);
                _this._CampaignDetails.EndDateS = _this._HelperService.GetDateS(_this._CampaignDetails.EndDate);
                _this._CampaignDetails.CreateDateS = _this._HelperService.GetDateTimeS(_this._CampaignDetails.CreateDate);
                _this._CampaignDetails.ModifyDateS = _this._HelperService.GetDateTimeS(_this._CampaignDetails.ModifyDate);
                _this._CampaignDetails.StatusI = _this._HelperService.GetStatusIcon(_this._CampaignDetails.StatusCode);
                _this._CampaignDetails.StatusB = _this._HelperService.GetStatusBadge(_this._CampaignDetails.StatusCode);
                _this._CampaignDetails.StatusC = _this._HelperService.GetStatusColor(_this._CampaignDetails.StatusCode);
                //this.TUTr_Setup();
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],
                // : [null, Validators.compose([Validators.required])],
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
                // : 0,
                // : 0,
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(5)])],
                // StartDate: [null, Validators.compose([Validators.required])],
                // EndDate: [null, Validators.compose([Validators.required])],
                _this.Form_AddUser.patchValue({
                    Name: _this._CampaignDetails.Name,
                    Description: _this._CampaignDetails.Description,
                    TypeCode: _this._CampaignDetails.TypeCode,
                    SubTypeCode: _this._CampaignDetails.SubTypeCode,
                    SubTypeValue: _this._CampaignDetails.SubTypeValue,
                    MinimumInvoiceAmount: _this._CampaignDetails.MinimumInvoiceAmount,
                    MaximumInvoiceAmount: _this._CampaignDetails.MaximumInvoiceAmount,
                    MinimumRewardAmount: _this._CampaignDetails.MinimumRewardAmount,
                    MaximumRewardAmount: _this._CampaignDetails.MaximumRewardAmount,
                    TStartDate: moment(_this._CampaignDetails.StartDate).format('DD-MM-YYYY') + ' - ' + moment(_this._CampaignDetails.EndDate).format('DD-MM-YYYY'),
                    StartDate: _this._CampaignDetails.StartDate,
                    EndDate: _this._CampaignDetails.EndDate,
                });
            }
            else {
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.HandleException(_Error);
        });
    };
    TUCampaignComponent.prototype.RewardTypeChange = function () {
        this.Form_AddUser.patchValue({
            SubTypeValue: 0,
            MinimumInvoiceAmount: 0,
            MaximumInvoiceAmount: 0,
            MinimumRewardAmount: 0,
            MaximumRewardAmount: 0,
        });
    };
    TUCampaignComponent.prototype.SetCampaignType = function (TypeName) {
        this.Form_AddUser.patchValue({
            TypeCode: TypeName,
        });
    };
    TUCampaignComponent.prototype.ScheduleDateRangeChange = function (value) {
        this.Form_AddUser.patchValue({
            TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
        });
        this.Form_AddUser.patchValue({
            StartDate: value.start,
            EndDate: value.end,
        });
    };
    TUCampaignComponent.prototype.Form_AddUser_Load = function () {
        this.Form_AddUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateCampaign,
            ReferenceKey: this._CampaignDetails.ReferenceKey,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            Name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(128)])],
            Description: null,
            TypeCode: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            SubTypeCode: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            SubTypeValue: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)])],
            MinimumInvoiceAmount: 0,
            MaximumInvoiceAmount: 0,
            MinimumRewardAmount: 0,
            MaximumRewardAmount: 0,
            TStartDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            StartDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            EndDate: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
        });
    };
    TUCampaignComponent.prototype.Form_AddUser_Process = function (_FormValue) {
        var _this = this;
        if (_FormValue.StartDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.EndDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward amount");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward amount must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward percentage");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward percentage must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue > 100) {
            this._HelperService.NotifyError("Reward percentage must be between than 1 - 100");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.MaximumRewardAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.MinimumInvoiceAmount == undefined) {
            this._HelperService.NotifyError("Enter minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.MinimumInvoiceAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
                position: "top",
                title: "Update campaign ?",
                text: "Click on continue button to update campaign? ",
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
            }).then(function (result) {
                if (result.value) {
                    _FormValue.ReferenceKey = _this._HelperService.AppConfig.ActiveReferenceKey;
                    _FormValue.StartDate = new Date();
                    _FormValue.EndDate = new Date();
                    _this._HelperService.IsFormProcessing = true;
                    var _OResponse = void 0;
                    _OResponse = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
                    _OResponse.subscribe(function (_Response) {
                        _this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == _this._HelperService.StatusSuccess) {
                            _this.GetCampaignDetails();
                            _this._HelperService.NotifySuccess("Campaign updated successfully");
                        }
                        else {
                            _this._HelperService.NotifyError(_Response.Message);
                        }
                    }, function (_Error) {
                        console.log(_Error);
                        _this._HelperService.IsFormProcessing = false;
                        _this._HelperService.HandleException(_Error);
                    });
                }
            });
        }
    };
    TUCampaignComponent.prototype.UpdateCampaignStatus = function (status) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
            position: "top",
            title: "Update campaign ?",
            text: "Update campaign status? ",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                var pData = {
                    Task: "updatecampaign",
                    ReferenceKey: _this._CampaignDetails.ReferenceKey,
                    StatusCode: status,
                };
                _this._HelperService.IsFormProcessing = true;
                var _OResponse = void 0;
                _OResponse = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(function (_Response) {
                    _this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == _this._HelperService.StatusSuccess) {
                        _this._HelperService.NotifySuccess("Campaign deleted successfully");
                        // this.Form_UpdateUser_Close();
                        _this.GetCampaignDetails();
                    }
                    else {
                        _this._HelperService.NotifyError(_Response.Message);
                    }
                }, function (_Error) {
                    _this._HelperService.IsFormProcessing = false;
                    _this._HelperService.HandleException(_Error);
                });
            }
        });
    };
    TUCampaignComponent.prototype.DeleteCampaign = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
            position: "top",
            title: "Delete campaign ?",
            text: "Details cannot be recovered after deletion, do you want to continue ? ",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                var pData = {
                    Task: "deletecampaign",
                    ReferenceKey: _this._CampaignDetails.ReferenceKey,
                };
                _this._HelperService.IsFormProcessing = true;
                var _OResponse = void 0;
                _OResponse = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(function (_Response) {
                    _this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == _this._HelperService.StatusSuccess) {
                        _this._HelperService.NotifySuccess("Campaign status udpated successfully");
                        // this.Form_UpdateUser_Close();
                        _this._Router.navigate([_this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
                    }
                    else {
                        _this._HelperService.NotifyError(_Response.Message);
                    }
                }, function (_Error) {
                    _this._HelperService.IsFormProcessing = false;
                    _this._HelperService.HandleException(_Error);
                });
            }
        });
    };
    TUCampaignComponent.prototype.DuplicateCampaign = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default()({
            position: "top",
            title: "Duplicate campaign ?",
            text: "Click on continue to duplicate campaign",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                var pData = {
                    Task: "duplicatecampaign",
                    ReferenceKey: _this._CampaignDetails.ReferenceKey,
                };
                _this._HelperService.IsFormProcessing = true;
                var _OResponse = void 0;
                _OResponse = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(function (_Response) {
                    _this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == _this._HelperService.StatusSuccess) {
                        _this._HelperService.NotifySuccess("Campaign duplicated successfully");
                        // this.Form_UpdateUser_Close();
                        _this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result;
                        _this.GetCampaignDetails();
                    }
                    else {
                        _this._HelperService.NotifyError(_Response.Message);
                    }
                }, function (_Error) {
                    _this._HelperService.IsFormProcessing = false;
                    _this._HelperService.HandleException(_Error);
                });
            }
        });
    };
    TUCampaignComponent.prototype.TUTr_InvoiceRange_OnChange = function (changeContext) {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_RewardRange_OnChange = function (changeContext) {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'RewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_RewardRangeMinAmount = changeContext.value;
        this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'RewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
        if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Setup = function () {
        this.TUTr_Config =
            {
                Id: null,
                Sort: null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaignTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
                Title: 'Rewards History',
                StatusType: 'transaction',
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'CampaignId', this._HelperService.AppConfig.DataType.Number, this._CampaignDetails.ReferenceId, '='),
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: 'TransactionDate desc',
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Transaction Status',
                        SystemName: 'StatusName',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'User',
                        SystemName: 'UserDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Mobile Number',
                        SystemName: 'UserMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Invoice Amount',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Reward Amount',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Reward Amount',
                        SystemName: 'UserAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Commission Amount',
                        SystemName: 'CommissionAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Card Number',
                        SystemName: 'AccountNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Type',
                        SystemName: 'CardBrandName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Bank Provider',
                        SystemName: 'CardBankName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'TUC Card Number',
                        SystemName: 'TUCCardNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Merchant',
                        SystemName: 'ParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Acquirer / Bank',
                        SystemName: 'AcquirerDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Terminal Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Issuer (Done by)',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Reference',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },
                ]
            };
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        if (this._CampaignDetails.ReferenceId != undefined && this._CampaignDetails.ReferenceId != '') {
            this.TUTr_GetData();
        }
    };
    TUCampaignComponent.prototype.TUTr_ToggleOption_Date = function (event, Type) {
        this.TUTr_ToggleOption(event, Type);
    };
    TUCampaignComponent.prototype.TUTr_ToggleOption = function (event, Type) {
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (this.TUTr_Config.RefreshData == true) {
            this.TUTr_GetData();
        }
    };
    TUCampaignComponent.prototype.TUTr_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    };
    TUCampaignComponent.prototype.TUTr_RowSelected = function (ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    };
    TUCampaignComponent.prototype.TUTr_Filter_UserAccounts_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_UserAccounts_Change = function (event) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Filter_Merchants_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_Merchants_Change = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
            _this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    };
    TUCampaignComponent.prototype.TUTr_Filter_Stores_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };
        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_Stores_Change = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
        }, 500);
    };
    TUCampaignComponent.prototype.TUTr_Filter_Providers_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_Providers_Change = function (event) {
        var _this = this;
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(function () {
            _this._HelperService.ToggleField = false;
        }, 500);
    };
    TUCampaignComponent.prototype.TUTr_Filter_Issuers_Load = function () {
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text, [
            this._HelperService.AppConfig.AccountType.PosTerminal,
            this._HelperService.AppConfig.AccountType.PGAccount,
            this._HelperService.AppConfig.AccountType.Cashier,
        ], "=");
        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        if (this.TUTr_Filter_Store_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
        }
        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerOwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_Issuers_Change = function (event) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Filter_TransactionSources_Change = function (event) {
        var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '=');
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_Filter_TransactionSources_Selected = event;
        if (this.TUTr_Filter_TransactionSources_Selected != 0) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '=');
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Filter_TransactionTypes_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_TransactionTypes_Change = function (event) {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Filter_CardBrands_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_CardBrands_Change = function (event) {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    TUCampaignComponent.prototype.TUTr_Filter_CardBanks_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    TUCampaignComponent.prototype.TUTr_Filter_CardBanks_Change = function (event) {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_4__["DaterangePickerComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_4__["DaterangePickerComponent"])
    ], TUCampaignComponent.prototype, "_DaterangePickerComponent", void 0);
    TUCampaignComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-campaign",
            template: __webpack_require__(/*! ./campaign.component.html */ "./src/app/panel/acquirer/campaigns/campaign/campaign.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["DataHelperService"]])
    ], TUCampaignComponent);
    return TUCampaignComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/campaigns/campaign/campaign.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/panel/acquirer/campaigns/campaign/campaign.module.ts ***!
  \**********************************************************************/
/*! exports provided: TUCampaignRoutingModule, TUCampaignModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignRoutingModule", function() { return TUCampaignRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignModule", function() { return TUCampaignModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _campaign_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./campaign.component */ "./src/app/panel/acquirer/campaigns/campaign/campaign.component.ts");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");











var routes = [{ path: "", component: _campaign_component__WEBPACK_IMPORTED_MODULE_10__["TUCampaignComponent"] }];

var TUCampaignRoutingModule = /** @class */ (function () {
    function TUCampaignRoutingModule() {
    }
    TUCampaignRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUCampaignRoutingModule);
    return TUCampaignRoutingModule;
}());

var TUCampaignModule = /** @class */ (function () {
    function TUCampaignModule() {
    }
    TUCampaignModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUCampaignRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                ng5_slider__WEBPACK_IMPORTED_MODULE_11__["Ng5SliderModule"],
            ],
            declarations: [_campaign_component__WEBPACK_IMPORTED_MODULE_10__["TUCampaignComponent"]]
        })
    ], TUCampaignModule);
    return TUCampaignModule;
}());



/***/ })

}]);
//# sourceMappingURL=panel-acquirer-campaigns-campaign-campaign-module.js.map