(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-accountdetails-tustore-acquirer-tustore-module"],{

/***/ "./src/app/modules/accountdetails/tustore/acquirer/tustore.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tustore/acquirer/tustore.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--stores--details -->\n<div id=\"accordion\">\n    <div class=\"card shadow-none gredientBorder\">\n        <div class=\"card-header bd-b-0\" id=\"headingOne\">\n\n            <div class=\"row \">\n\n                <div class=\"col-sm-12 col-md-12 col-lg-1 d-flex\">\n                    <div class=\"media m-auto align-sm-items-center\">\n                        <div class=\"avatar avatar-xl    avatar-online\"><img\n                                src=\"{{ _HelperService.UserAccount.IconUrl }}\" class=\"rounded-circle \" alt=\"\"> </div>\n                    </div><!-- media -->\n                </div>\n\n                <div class=\"col-sm-12 col-md-12 col-lg-5 d-md-flex d-lg-flex Hm-AlignCenter Hm-media768\">\n                    <div class=\"media  Hm-textCenterMd\">\n                        <div class=\"media-body\">\n                            <h6 class=\"tx-20 tx-lg-24 tx-bold text-capitalize  mg-b-2\">\n                                {{_StoreDetails.DisplayName}}</h6>\n                            <div class=\"d-md-flex d-lg-flex Hm-mg-y Hm-AlignCenter align-items-baseline  mg-b-5\">\n                                <h2 class=\"tx-14 tx-lg-14 tx-color-03 tx-normal   lh-2 mg-b-0\">\n                                    <span class=\"mg-r-5 tx-teal\"> <i style=\"height: 18px; width: 18px;;\"\n                                        data-feather=\"users\"></i> </span>    {{ _StoreDetails.DisplayName}}</h2>\n                            </div>\n                            <div class=\"d-md-flex d-lg-flex Hm-AlignCenter align-items-baseline\">\n                                <h2 class=\"tx-14 tx-lg-14 tx-color-03 tx-normal  lh-2 mg-b-0 d-flex\">\n                                    <span class=\"text-danger mg-r-5\"> <i style=\"height: 18px; width: 18px;;\"\n                                            data-feather=\"map-pin\"></i> </span> <span>{{_StoreDetails.Address}}</span>\n                                </h2>\n                            </div>\n                        </div><!-- media-body -->\n                    </div><!-- media -->\n                </div>\n                <div class=\"col-lg-6 Hm-media768 \" id=\"StoresHide\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                            <div class=\"row flex-nowrap\">\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n\n                                    <div class=\" text-center stores\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\"tx-bold tx-40  tx-rubik tx-verve mg-b-5 mg-r-5 lh-1\">\n                                                {{_StoreDetails.RewardPercentage}}</h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0 \"> Rewards</p>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n                                    <div class=\" text-center Terminals\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\"tx-bold tx-40 tx-rubik tx-teal mg-b-5 mg-r-5 lh-1 \">\n                                                {{_StoreDetails.Terminals}}</h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0\"> Pos Terminal</p>\n\n                                    </div>\n\n                                </div>\n\n\n                            </div>\n                        </div>\n                        <div class=\"col-sm-12 col-md-6 col-lg-6 col-lg-6 d-flex Hm-media768 justify-content-end\">\n                            <div class=\"row\">\n                                <div class=\"manageButton mg-t-0 pd-t-0\">\n                                    <button type=\"button\" (click)=\"ShowOffCanvas()\"\n                                        class=\"btn btn-xs btn-transparent tx-color-03 bd bd-secondary rounded-pill\">Manage\n                                        Account</button>\n                                </div>\n\n                                <div class=\"collapseButton\">\n                                    <button [class.button-open]=\"!slideOpen\" [class.button-close]=\"slideOpen\"\n                                        (click)=\"changeSlide() ; HideStoreDetail()\" class=\"btn  btn-link\"\n                                        data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"false\"\n                                        aria-controls=\"collapseOne\">\n                                        <div class=\"tx-15 tx-lg-18 lh-0 tx-color-03\"><i class=\"fas fa-chevron-down\"\n                                                [class.clicked]=\"slideOpen\"></i>\n                                        </div>\n                                    </button>\n                                </div>\n\n                            </div>\n\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n\n        <div id=\"collapseOne\" class=\"collapse\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">\n            <div class=\"card-body pt-0\">\n                <div class=\"row\">\n\n                    <div class=\"col-lg-1\">\n\n                    </div>\n                    <div class=\"col-lg-11\">\n                        <div class=\"row\">\n\n\n                            <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                                <!-- <agm-map class=\"Hm-height ht-100p\" [zoom]=\"14\"\n                                    [latitude]=\"_HelperService._UserAccount.Latitude\"\n                                    [longitude]=\"_HelperService._UserAccount.Longitude\">\n                                    <agm-marker [latitude]=\"_HelperService._UserAccount.Latitude\"\n                                        [longitude]=\"_HelperService._UserAccount.Longitude\"></agm-marker>\n                                </agm-map> -->\n                                <div class=\"Hm-height ht-100p\" leaflet [leafletOptions]=\"_HelperService.lefletoptions\"\n                                    [leafletLayers]=\"_HelperService.layers\"\n                                    (leafletMapReady)=\"_HelperService.onMapReady($event,false)\">\n                                </div>\n                            </div>\n\n                            <div class=\"col-sm-12 col-md-6  col-lg-6\">\n                                <div class=\"row\">\n                                    <div class=\"col-sm-12 col-md-6  col-lg-6 Hm-media768\">\n                                        <label\n                                            class=\"tx-14 tx-bold text-capitalize Hm-HeaderColor  mg-b-15\">Contact\n                                            Person</label>\n                                        <ul class=\"list-unstyled profile-info-list\">\n\n                                            <li class=\"tx-color-03\"><i data-feather=\"smartphone\"></i> <a\n                                                    *ngIf=\"_StoreDetails.ContactPerson\">{{_StoreDetails.ContactNumber}}</a>\n                                            </li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"mail\"></i> <a\n                                                    *ngIf=\"_StoreDetails.ContactPerson\">\n                                                    {{_StoreDetails.ContactPerson.EmailAddress}}</a>\n                                            </li>\n                                        </ul>\n\n                                    </div>\n                                    <div class=\" col-sm-12 col-md-6  col-lg-6 Hm-media768\">\n                                        <label\n                                            class=\"tx-14 tx-bold text-capitalize Hm-HeaderColor  mg-b-15\">Relationship\n                                            Manager</label>\n                                        <ul class=\"list-unstyled profile-info-list\">\n                                            <li class=\"tx-color-03\"><i data-feather=\"user\"></i> <span\n                                                    *ngIf=\"_StoreDetails.Rm\"\n                                                    class=\"tx-color-03\">{{_StoreDetails.Rm.Name}}</span></li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"smartphone\"></i> <a\n                                                    *ngIf=\"_StoreDetails.Rm\"\n                                                    class=\"tx-color-03\">{{_StoreDetails.Rm.MobileNumber}}</a></li>\n                                            <li class=\"tx-color-03\"><i data-feather=\"mail\"></i> <a\n                                                    *ngIf=\"_StoreDetails.Rm\">{{_StoreDetails.Rm.EmailAddress}}</a>\n                                            </li>\n                                        </ul>\n\n                                    </div>\n                                    <div class=\"col-lg-12 Hm-media768\">\n                                        <div class=\"d-inline justify-content-between\"\n                                            *ngFor=\"let catagories of _StoreDetails['Categories']\">\n                                            <button type=\"button\"\n                                                class=\"btn btn-xs  tx-color-03 btn-transparent bd bd-primary rounded-pill mg-r-10\">{{catagories.Name}}</button>\n\n\n\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row Hm-media768 d-block d-sm-none\">\n                    <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                        <div class=\"row flex-nowrap\">\n\n                            <div class=\"col-sm-6 col-md-6 col-lg-6\">\n\n                                <div class=\" text-center stores\">\n                                    <div class=\"text-center\">\n                                        <h3 class=\"tx-bold tx-40 tx-rubik  tx-verve mg-b-5 mg-r-5 lh-1\">\n                                            {{_StoreDetails.RewardPercentage}}</h3>\n                                    </div>\n                                    <p class=\"tx-12 tx-color-03 mg-b-0 \"> Rewards</p>\n                                </div>\n                            </div>\n\n                            <div class=\"col-sm-6 col-md-6 col-lg-6\">\n                                <div class=\" text-center Terminals\">\n                                    <div class=\"text-center\">\n                                        <h3 class=\"tx-bold tx-40 tx-rubik  tx-teal mg-b-5 mg-r-5 lh-1 \">\n                                            {{_StoreDetails.Terminals}}</h3>\n                                    </div>\n                                    <p class=\"tx-12 tx-color-03 mg-b-0\"> Pos Terminal</p>\n\n                                </div>\n\n                            </div>\n\n\n                        </div>\n                    </div>\n                    <div class=\"col-sm-12 col-md-6 col-lg-6 col-lg-6 d-flex Hm-media768 justify-content-end\">\n                        <div class=\"row\">\n                            <div class=\"manageButton mg-t-0 pd-t-0\">\n                                <button type=\"button\" (click)=\"ShowOffCanvas()\"\n                                    class=\"btn btn-xs btn-transparent tx-color-03 bd bd-secondary rounded-pill\">Manage\n                                    Account</button>\n                            </div>\n\n                            <div class=\"collapseButton\">\n                                <button [class.button-open]=\"!slideOpen\" [class.button-close]=\"slideOpen\"\n                                    (click)=\"changeSlide() ; ShowStoreDetail()\" class=\"btn  btn-link\"\n                                    data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"false\"\n                                    aria-controls=\"collapseOne\">\n                                    <div class=\"tx-15 tx-lg-18 lh-0 tx-color-03\"><i class=\"fas fa-chevron-down\"\n                                            [class.clicked]=\"slideOpen\"></i>\n                                    </div>\n                                </button>\n                            </div>\n\n                        </div>\n\n                    </div>\n                </div>\n\n\n            </div>\n        </div>\n    </div>\n\n\n</div>\n\n<div class=\"card bd-b-0 pd-x-20 pd-b-20 pd-t-10 mt-3\">\n    <ul class=\"nav nav-line d-flex justify-content-between\" id=\"myTab5\" role=\"tablist\">\n        <div class=\"d-flex flex-wrap\">\n\n            <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n                <a class=\"nav-link\" id=\"home-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"\n                    routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                    role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Overview</a>\n            </li>\n            <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n                <a class=\"nav-link\" id=\"profile-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"\n                    routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.SalesHistory}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                    role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">Transactions</a>\n            </li>\n            <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n                <a class=\"nav-link\" id=\"contact-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"\n                    routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Terminals}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                    role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\">Terminals</a>\n            </li>\n\n        </div>\n        <!-- <li class=\"nav-item\">\n        <a class=\"nav-link\" id=\"contact-tab5\" data-toggle=\"tab\" href=\"#contact5\" role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\">Stores</a>\n      </li>      <li class=\"nav-item\">\n        <a class=\"nav-link\" id=\"contact-tab5\" data-toggle=\"tab\" routerLink=\"{{ _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.SalesHistory}}/{{ _HelperService.AppConfig.ActiveReferenceKey}}/{{ _HelperService.AppConfig.ActiveReferenceId}}\" role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\">History</a>\n      </li> -->\n        <div class=\"row  d-flex no-gutters pd-r-10\">\n            <div class=\" col-sm-12 col-md-6 d-flex mg-md-t-0 mg-t-10\" *ngIf=\"_HelperService.ShowDateRange\">\n                <div class=\" order-1\">\n                    <input type=\"text\" style=\"height: 30px;\" class=\"form-control tx-13 form-daterangepicker\"\n                        name=\"daterangeInput\" daterangepicker [options]=\"_HelperService.AppConfig.DatePickerOptions\"\n                        (selected)=\"_HelperService.SetDateRange($event)\" />\n                </div>\n            </div>\n\n\n            <div class=\"col-md-6 col-sm-12 btn-group mg-md-t-0 mg-t-10\" style=\"height: 30px;\" data-toggle=\"buttons\"\n                *ngIf=\"_HelperService.ShowDateRange\">\n                <label class=\"btn btn-xs pb-0 mb-0 tx-13 btn-white  active d-flex align-items-center\"\n                    (click)=\"_HelperService.ToogleRange('A')\">\n                    <input type=\"radio\" name=\"options\" style=\"display: none;\" checked>\n                    <span class=\"tx-13 mg-b-5\"> Month </span>\n                </label>\n                <label class=\"btn btn-xs  pb-0 mb-0 tx-13 btn-white d-flex align-items-center\"\n                    (click)=\"_HelperService.ToogleRange('B')\">\n                    <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n                    <span class=\"tx-13 mg-b-5\">Week</span>\n                </label>\n                <label class=\"btn pb-0 mb-0 btn-xs tx-13 btn-white  d-flex align-items-center\"\n                    (click)=\"_HelperService.ToogleRange('C')\">\n                    <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n                    <span class=\"tx-13 mg-b-5\"> Day </span>\n                </label>\n            </div>\n\n        </div>\n\n    </ul>\n</div>\n\n<router-outlet></router-outlet>\n<div class=\"off-canvas off-canvas-overlay off-canvas-right\" #offCanvas style=\"width: 480px !important;\">\n    <div class=\"off-canvas-content\">\n        <!-- <div class=\"off-canvas-header text-danger text-right\">\n            <a (click)=\"unclick()\"><i data-feather=\"x-circle\" class=\"wd-25 ht-25\"></i></a>\n        </div> -->\n        <div id=\"Form_EditUser_Content\" class=\"off-canvas-body\">\n            <form [formGroup]=\"Form_EditUser\" (ngSubmit)=\"Form_EditUser_Process(Form_EditUser.value)\" role=\"form\">\n                <div class=\"card\">\n                    <div class=\"card-header bd-0\">\n                        <h3 class=\"tx-roboto tx-bold tx-lg-24 tx-sm-8 pd-l-0\"> Manage Account </h3>\n                        <ul class=\"nav nav-line\" id=\"myTab\" role=\"tablist\">\n                            <li class=\"nav-item\">\n                                <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#Business\" role=\"tab\"\n                                    aria-controls=\"home\" aria-selected=\"true\">Business Info</a>\n                            </li>\n\n                            <li class=\"nav-item\">\n                                <a class=\"nav-link\" id=\"contact-tab\" data-toggle=\"tab\" href=\"#Address\" role=\"tab\"\n                                    aria-controls=\"contact\" aria-selected=\"false\">Address details</a>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"card-body  p-lg pd-l-15\">\n                        <div class=\"tab-content pd-x-20 bd-t-20 \" id=\"myTabContent\">\n                            <div class=\"tab-pane fade show active\" id=\"Business\" role=\"tabpanel\"\n                                aria-labelledby=\"home-tab\">\n                                <div class=\"row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"form-group\">\n                                            <label>Store Name<span class=\"text-danger\">*</span></label>\n                                            <div class=\"controls\">\n\n                                                <input type=\"text\" placeholder=\"{{'System.Users.Name' | translate }}\"\n                                                    name=\"Name\" maxlength=\"25\" class=\"form-control\"\n                                                    [(ngModel)]=\"_StoreDetails.Name\"\n                                                    [formControl]=\"Form_EditUser.controls['Name']\"\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['Name'].valid && Form_EditUser.controls['Name'].touched}\">\n                                            </div>\n                                            <div class=\"text-xs text-muted _500\"> </div>\n                                            <label class=\"text-danger\" for=\"Name\"\n                                                *ngIf=\"Form_EditUser.controls['Name'].hasError('required') && Form_EditUser.controls['Name'].touched\">\n                                                enter store name\n                                            </label>\n                                            <label class=\"text-danger\" for=\"Name\"\n                                                *ngIf=\"Form_EditUser.controls['Name'].hasError('minlength') || Form_EditUser.controls['Name'].hasError('maxlength')\">business\n                                                store name must be between 2 - 25 characters</label>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Store Nick Name<span class=\"text-danger\">*</span> </label>\n                                            <div class=\"controls\">\n                                                <input type=\"text\" name=\"DisplayName\" maxlength=\"256\"\n                                                    class=\"form-control\"\n                                                    [formControl]=\"Form_EditUser.controls['DisplayName']\"\n                                                    [(ngModel)]=\"_StoreDetails.DisplayName\"\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['DisplayName'].valid && Form_EditUser.controls['DisplayName'].touched}\">\n                                            </div>\n                                            <label class=\"text-danger\" for=\"DisplayName\"\n                                                *ngIf=\"Form_EditUser.controls['DisplayName'].hasError('required') && Form_EditUser.controls['DisplayName'].touched\">\n                                                enter store nick name\n                                            </label>\n                                            <label class=\"text-danger\" for=\"DisplayName\"\n                                                *ngIf=\"Form_EditUser.controls['DisplayName'].hasError('minlength') || Form_EditUser.controls['DisplayName'].hasError('maxlength')\">parent\n                                                enter store nick name must be between 4 - 256 characters</label>\n\n                                        </div>\n\n                                        <div class=\"form-group\">\n                                            <label>Phone Number<span class=\"text-danger\">*</span>\n                                            </label>\n                                            <div class=\"controls\">\n                                                <div class=\"input-group\">\n                                                    <div class=\"input-group-prepend\">\n                                                        <span class=\"input-group-text\">+234</span>\n                                                    </div>\n                                                    <input type=\"text\" name=\"ContactNumber\" minlength=\"10\"\n                                                        maxlength=\"18\" class=\"form-control contact-textbox-height\"\n                                                        (keypress)=\"_HelperService.PreventText($event)\"\n                                                        [(ngModel)]=\"_StoreDetails.ContactNumber\"\n                                                        [formControl]=\"Form_EditUser.controls['ContactNumber']\"\n                                                        [ngClass]=\"{'is-invalid':!Form_EditUser.controls['ContactNumber'].valid && Form_EditUser.controls['ContactNumber'].touched}\">\n                                                </div>\n                                            </div>\n                                            <label class=\"text-danger\" for=\"ContactNumber\"\n                                                *ngIf=\"Form_EditUser.controls['ContactNumber'].hasError('required') && Form_EditUser.controls['ContactNumber'].touched\">enter\n                                                enter phone number</label>\n                                            <label class=\"text-danger\" for=\"ContactNumber\"\n                                                *ngIf=\"Form_EditUser.controls['ContactNumber'].hasError('minlength') || Form_EditUser.controls['ContactNumber'].hasError('maxlength')\">\n                                                pnone number must be between 10 to 18 numbers\n                                            </label>\n\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Email ID<span class=\"text-danger\">*</span>\n                                            </label>\n                                            <div class=\"controls\">\n                                                <input type=\"text\" name=\"EmailAddress\" maxlength=\"256\"\n                                                    class=\"form-control\"\n                                                    [formControl]=\"Form_EditUser.controls['EmailAddress']\"\n                                                    [(ngModel)]=\"_StoreDetails.EmailAddress\"\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['EmailAddress'].valid && Form_EditUser.controls['EmailAddress'].touched}\">\n                                            </div>\n                                            <label class=\"text-danger\" for=\"EmailAddress\"\n                                                *ngIf=\"Form_EditUser.controls['EmailAddress'].hasError('required') && Form_EditUser.controls['EmailAddress'].touched\">\n                                                enter email address\n                                            </label>\n\n                                            <label class=\"text-danger\" for=\"EmailAddress\"\n                                                *ngIf=\"Form_EditUser.controls['EmailAddress'].hasError('email') && Form_EditUser.controls['EmailAddress'].touched\">\n                                                enter valid email address\n                                            </label>\n\n                                        </div>\n\n                                        <div class=\"form-group\">\n                                            <label>{{ 'System.Menu.Branch' | translate }}<span\n                                                    class=\"text-danger\">*</span>\n                                            </label>\n                                            <select2 name=\"S2GetBranches\" class=\"full-width\"\n                                                [options]=\"GetBranches_Option\"\n                                                (valueChanged)=\"GetBranches_ListChange($event)\"></select2>\n                                            <!-- <label class=\"text-danger\" for=\"RoleKey\"\n                                                *ngIf=\"Form_AddUser.controls['RoleKey'].hasError('required') && Form_AddUser.controls['RoleKey'].touched\">{{\n                                                    'System.Users.EmailAddress_ER_Required' | translate }}</label> -->\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>RM<span class=\"text-danger\">*</span>\n                                            </label>\n                                            <select2 name=\"S2GetManagers\" class=\"full-width\"\n                                                [options]=\"GetMangers_Option\"\n                                                (valueChanged)=\"GetMangers_ListChange($event)\"></select2>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane fade\" id=\"Address\" role=\"tabpanel\" aria-labelledby=\"contact-tab\">\n                                <div class=\"row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"form-group\">\n                                            <label class=\"tx-roboto tx-12 text-dark\">Locate Address<span\n                                                    class=\"text-danger\">*</span>\n                                            </label>\n                                            <div class=\"controls\">\n                                                <input type=\"text\" name=\"GAddress\" maxlength=\"256\" class=\"form-control\"\n                                                    ngx-google-places-autocomplete #placesStore=\"ngx-places\"\n                                                    (onAddressChange)=\"Form_EditUser_AddressChange($event)\">\n                                            </div>\n                                            <label class=\"text-warn\">You can click on map to mark exact location of\n                                                store </label>\n                                        </div>\n                                        <agm-map style=\"height:255px;\" [zoom]=\"12\" [latitude]=\"Form_EditUser_Latitude\"\n                                            [longitude]=\"Form_EditUser_Longitude\"\n                                            (mapClick)=\"Form_EditUser_PlaceMarkerClick($event)\"\n                                            [streetViewControl]=\"false\">\n                                            <agm-marker [latitude]=\"Form_EditUser_Latitude\"\n                                                [longitude]=\"Form_EditUser_Longitude\">\n                                            </agm-marker>\n                                        </agm-map>\n\n                                        <div class=\"form-group mt-3\">\n                                            <label class=\"tx-roboto tx-12 text-dark\">Detailed Address <span\n                                                    class=\"text-muted\"> (Street\n                                                    Name, Building Name, Floor Number Etc) </span> <span\n                                                    class=\"text-danger\">*</span>\n                                            </label>\n                                            <div class=\"controls\">\n                                                <textarea name=\"Address\" maxlength=\"256\" class=\"form-control\" rows=\"3\"\n                                                    [formControl]=\"Form_EditUser.controls['Address']\"\n                                                    [(ngModel)]=\"_StoreDetails.Address\"\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['Address'].valid && Form_EditUser.controls['Address'].touched}\"></textarea>\n                                            </div>\n                                            <label class=\"text-danger\" for=\"Address\"\n                                                *ngIf=\"Form_EditUser.controls['Address'].hasError('required') && Form_EditUser.controls['Address'].touched\">{{\n                                                                      'System.Users.Address_ER_Required' | translate }}</label>\n                                            <label class=\"text-danger\" for=\"Address\"\n                                                *ngIf=\"Form_EditUser.controls['Address'].hasError('minlength') || Form_EditUser.controls['Address'].hasError('maxlength')\">{{\n                                                                      'System.Users.Address_ER_Length' | translate }}</label>\n                                        </div>\n                                        <div class=\"form-group mt-3\">\n                                            <label class=\"tx-roboto tx-12 text-dark\">City<span\n                                                    class=\"text-danger\">*</span></label>\n                                            <div class=\"controls\">\n                                                <input type=\"text\" name=\"City\" maxlength=\"25\" class=\"form-control\"\n                                                    [formControl]=\"Form_EditUser.controls['CityName']\"\n                                                    [(ngModel)]='_CurrentAddress.administrative_area_level_2'\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['CityName'].valid && Form_EditUser.controls['CityName'].touched}\">\n                                            </div>\n                                            <div class=\"text-xs text-muted _500\"> </div>\n                                            <label class=\"text-danger\" for=\"CityName\"\n                                                *ngIf=\"Form_EditUser.controls['CityName'].hasError('required') && Form_EditUser.controls['CityName'].touched\">\n                                                enter city name\n                                            </label>\n                                            <label class=\"text-danger\" for=\"DisplayName\"\n                                                *ngIf=\"Form_EditUser.controls['CityName'].hasError('minlength') || Form_EditUser.controls['CityName'].hasError('maxlength')\">\n                                                {{'System.AddAcquirer.CityName' | translate }}</label>\n                                        </div>\n\n                                        <div class=\"form-group mt-3\">\n                                            <label class=\"tx-roboto tx-12 text-dark\">State<span\n                                                    class=\"text-danger\">*</span></label>\n                                            <div class=\"controls\">\n\n                                                <input type=\"text\" name=\"City\" maxlength=\"25\" class=\"form-control\"\n                                                    [formControl]=\"Form_EditUser.controls['StateName']\"\n                                                    [(ngModel)]='_CurrentAddress.administrative_area_level_1'\n                                                    [(ngModel)]='_HelperService.selectedLocation.address.Region'\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['StateName'].valid && Form_EditUser.controls['StateName'].touched}\">\n                                            </div>\n                                            <div class=\"text-xs text-muted _500\"> </div>\n                                            <label class=\"text-danger\" for=\"StateName\"\n                                                *ngIf=\"Form_EditUser.controls['StateName'].hasError('required') && Form_EditUser.controls['StateName'].touched\">\n                                                enter state name\n                                            </label>\n                                            <label class=\"text-danger\" for=\"DisplayName\"\n                                                *ngIf=\"Form_EditUser.controls['StateName'].hasError('minlength') || Form_EditUser.controls['StateName'].hasError('maxlength')\">\n                                                {{'System.AddAcquirer.StateName' | translate }}</label>\n                                        </div>\n\n                                        <div class=\"form-group mt-3\">\n                                            <label class=\"tx-roboto tx-12 text-dark\">Country<span\n                                                    class=\"text-danger\">*</span></label>\n                                            <div class=\"controls\">\n\n                                                <input type=\"text\" name=\"City\" maxlength=\"25\" class=\"form-control\"\n                                                    [formControl]=\"Form_EditUser.controls['CountryName']\"\n                                                    [(ngModel)]='_CurrentAddress.country'\n                                                    [(ngModel)]='_HelperService.selectedLocation.address.Region'\n                                                    [ngClass]=\"{'is-invalid':!Form_EditUser.controls['CountryName'].valid && Form_EditUser.controls['CountryName'].touched}\">\n                                            </div>\n                                            <div class=\"text-xs text-muted _500\"> </div>\n                                            <label class=\"text-danger\" for=\"CountryName\"\n                                                *ngIf=\"Form_EditUser.controls['CountryName'].hasError('required') && Form_EditUser.controls['CountryName'].touched\">\n                                                enter state name\n                                            </label>\n                                            <label class=\"text-danger\" for=\"DisplayName\"\n                                                *ngIf=\"Form_EditUser.controls['CountryName'].hasError('minlength') || Form_EditUser.controls['CountryName'].hasError('maxlength')\">\n                                                {{'System.AddAcquirer.CountryName' | translate }}</label>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-footer d-flex justify-content-between pd-l-35\">\n                        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\"\n                            (click)=\"Form_EditUser_Block()\">\n                            {{ \"Common.Block\" | translate }}\n                        </button>\n                        <button type=\"submit\" class=\"btn btn-primary\"\n                            (click)=\"Form_EditUser.value.OperationType = 'new'\"\n                            [disabled]=\"!Form_EditUser.valid || _HelperService.IsFormProcessing\">\n                            <span [hidden]=\"_HelperService.IsFormProcessing\">\n                                {{ \"Common.Save\" | translate }}\n                            </span>\n                            <span [hidden]=\"!_HelperService.IsFormProcessing\">\n                                <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                {{ \"Common.PleaseWait\" | translate }}...\n                            </span>\n                        </button>\n                    </div>\n                </div>\n            </form>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/accountdetails/tustore/acquirer/tustore.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tustore/acquirer/tustore.component.ts ***!
  \******************************************************************************/
/*! exports provided: TUStoreComponent, OAccountOverview */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoreComponent", function() { return TUStoreComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccountOverview", function() { return OAccountOverview; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");







var TUStoreComponent = /** @class */ (function () {
    //#endregion
    function TUStoreComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _ChangeDetectorRef) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._ChangeDetectorRef = _ChangeDetectorRef;
        //#region MapCorrection 
        this.slideOpen = false;
        //#region EditUser_Address 
        this._CurrentAddress = {};
        this.Form_EditUser_Address = null;
        this.Form_EditUser_Latitude = 0;
        this.Form_EditUser_Longitude = 0;
        //#endregion
        //#endregion
        //#region StoreDetails 
        this._StoreDetails = {
            ManagerName: null,
            BranchName: null,
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        };
    }
    TUStoreComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
        this.ReloadSubscription.unsubscribe();
    };
    TUStoreComponent.prototype.changeSlide = function () {
        this.slideOpen = !this.slideOpen;
        if (this.slideOpen) {
            this._HelperService._MapCorrection();
        }
    };
    //#endregion
    //#region ToogleDetailView 
    TUStoreComponent.prototype.HideStoreDetail = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-HideDiv");
        element.classList.remove("Hm-ShowStoreDetail");
    };
    TUStoreComponent.prototype.ShowStoreDetail = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-ShowStoreDetail");
        element.classList.remove("Hm-HideDiv");
    };
    //#endregion
    TUStoreComponent.prototype.BackDropInit = function () {
        var _this = this;
        var backdrop = document.getElementById("backdrop");
        backdrop.onclick = function () {
            $(_this.divView.nativeElement).removeClass('show');
            backdrop.classList.remove("show");
        };
    };
    TUStoreComponent.prototype.ngOnInit = function () {
        //#region UIInit 
        var _this = this;
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this._HelperService.ContainerHeight = window.innerHeight;
        this._HelperService.AppConfig.ShowHeader = true;
        this.BackDropInit();
        this._HelperService._InitMap();
        //#endregion
        //#region Subscriptions 
        this.subscription = this._HelperService.isprocessingtoogle
            .subscribe(function (item) {
            _this._ChangeDetectorRef.detectChanges();
        });
        this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe(function (number) {
        });
        //#endregion
        var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveStore);
        if (StorageDetails != null) {
            this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
            this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
            this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
            this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
        }
        //#region DropdownInit 
        this.GetSoresDetails();
        this.GetBranches_List();
        this.GetMangers_List();
        //#endregion
        this.Form_EditUser_Load();
    };
    TUStoreComponent.prototype.Form_EditUser_PlaceMarkerClick = function (event) {
        this.Form_EditUser_Latitude = event.coords.lat;
        this.Form_EditUser_Longitude = event.coords.lng;
    };
    TUStoreComponent.prototype.Form_EditUser_AddressChange = function (address) {
        this.Form_EditUser_Latitude = address.geometry.location.lat();
        this.Form_EditUser_Longitude = address.geometry.location.lng();
        this.Form_EditUser_Address = address.formatted_address;
        this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
        this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    };
    //#endregion
    TUStoreComponent.prototype.Form_EditUser_Show = function () {
        this._HelperService.OpenModal("Form_EditUser_Content");
    };
    TUStoreComponent.prototype.Form_EditUser_Close = function () {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.OpenModal("Form_EditUser_Content");
    };
    TUStoreComponent.prototype.Form_EditUser_Load = function () {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;
        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;
        this.Form_EditUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateStore,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            AccountId: this._HelperService.UserAccount.AccountId,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            BranchId: [null],
            BranchKey: [null],
            RmId: [null],
            RmKey: [null],
            DisplayName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])],
            Name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)])],
            ContactNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(18)])],
            EmailAddress: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2)])],
            Address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)])],
            CityName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])],
            StateName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])],
            CountryName: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])],
            Latitude: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            Longitude: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
        });
    };
    TUStoreComponent.prototype.Form_EditUser_Clear = function () {
        //this.Form_EditUser.reset();
        //this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    };
    TUStoreComponent.prototype.Form_EditUser_Process = function (_FormValue) {
        var _this = this;
        // _FormValue.DisplayName = _FormValue.FirstName;
        // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
        _FormValue.Latitude = this.Form_EditUser_Latitude;
        _FormValue.Longitude = this.Form_EditUser_Longitude;
        _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
        this._HelperService.IsFormProcessing = true;
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, _FormValue);
        _OResponse.subscribe(function (_Response) {
            _this._HelperService.IsFormProcessing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.NotifySuccess("Account created successfully");
                _this._HelperService.CloseModal('off-canvas');
                _this.Form_EditUser_Clear();
                _this.RemoveOffCanvas();
                _this.GetSoresDetails();
                // if (_FormValue.OperationType == "close") {
                //   this.Form_EditUser_Close();
                // }
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.IsFormProcessing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    TUStoreComponent.prototype.ShowOffCanvas = function () {
        $(this.divView.nativeElement).addClass('show');
        var backdrop = document.getElementById("backdrop");
        backdrop.classList.add("show");
    };
    TUStoreComponent.prototype.RemoveOffCanvas = function () {
        $(this.divView.nativeElement).removeClass('show');
        var backdrop = document.getElementById("backdrop");
        backdrop.classList.remove("show");
    };
    TUStoreComponent.prototype.GetBranches_List = function () {
        var PlaceHolder = "Select Branch";
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        };
        this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select);
        this.GetBranches_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetBranches_Transport,
            multiple: false,
        };
    };
    TUStoreComponent.prototype.GetBranches_ListChange = function (event) {
        // alert(event);\
        this.Form_EditUser.patchValue({
            BranchKey: event.data[0].ReferenceKey,
            BranchId: event.data[0].ReferenceId
        });
    };
    TUStoreComponent.prototype.GetMangers_List = function () {
        var PlaceHolder = "Select Manager";
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select);
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    };
    TUStoreComponent.prototype.GetMangers_ListChange = function (event) {
        // alert(event);
        this.Form_EditUser.patchValue({
            RmKey: event.data[0].ReferenceKey,
            RmId: event.data[0].ReferenceId
        });
    };
    TUStoreComponent.prototype.GetSoresDetails = function () {
        var _this = this;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStore,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.IsFormProcessing = false;
                _this._StoreDetails = _Response.Result;
                //#region ResponseInit 
                _this.GetBranches_Option.placeholder = _this._StoreDetails.BranchName;
                _this.GetMangers_Option.placeholder = _this._StoreDetails.ManagerName;
                _this._StoreDetails.EndDateS = _this._HelperService.GetDateS(_this._StoreDetails.EndDate);
                _this._StoreDetails.CreateDateS = _this._HelperService.GetDateTimeS(_this._StoreDetails.CreateDate);
                _this._StoreDetails.ModifyDateS = _this._HelperService.GetDateTimeS(_this._StoreDetails.ModifyDate);
                _this._StoreDetails.StatusI = _this._HelperService.GetStatusIcon(_this._StoreDetails.StatusCode);
                _this._StoreDetails.StatusB = _this._HelperService.GetStatusBadge(_this._StoreDetails.StatusCode);
                _this._StoreDetails.StatusC = _this._HelperService.GetStatusColor(_this._StoreDetails.StatusCode);
                //#endregion
                //#region InitLocationParams 
                if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
                    _this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
                    _this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
                    _this.Form_EditUser_Latitude = _Response.Result.Latitude;
                    _this.Form_EditUser_Longitude = _Response.Result.Longitude;
                    _this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
                    _this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);
                }
                else {
                    _this._HelperService._UserAccount.Longitude = _this._HelperService._UserAccount.Latitude;
                    _this._HelperService._UserAccount.Longitude = _this._HelperService._UserAccount.Longitude;
                }
                _this._HelperService._ReLocate();
                //#endregion
            }
            else {
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.HandleException(_Error);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('placesStore'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__["GooglePlaceDirective"])
    ], TUStoreComponent.prototype, "placesStore", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("offCanvas"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], TUStoreComponent.prototype, "divView", void 0);
    TUStoreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-store",
            template: __webpack_require__(/*! ./tustore.component.html */ "./src/app/modules/accountdetails/tustore/acquirer/tustore.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["DataHelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], TUStoreComponent);
    return TUStoreComponent;
}());

var OAccountOverview = /** @class */ (function () {
    function OAccountOverview() {
    }
    return OAccountOverview;
}());



/***/ }),

/***/ "./src/app/modules/accountdetails/tustore/acquirer/tustore.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tustore/acquirer/tustore.module.ts ***!
  \***************************************************************************/
/*! exports provided: TUStoreRoutingModule, TUStoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoreRoutingModule", function() { return TUStoreRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUStoreModule", function() { return TUStoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _tustore_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./tustore.component */ "./src/app/modules/accountdetails/tustore/acquirer/tustore.component.ts");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");














var routes = [
    {
        path: "",
        component: _tustore_component__WEBPACK_IMPORTED_MODULE_12__["TUStoreComponent"],
        children: [
            // { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/store/acquirer/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/store/acquirer/dashboard.module#TUDashboardModule" },
            { path: 'saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/transactions/tusale/store/tusale.module#TUSaleModule' },
            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Store' }, loadChildren: '../../../../modules/accounts/tuterminals/store/tuterminals.module#TUTerminalsModule' },
        ]
    }
];
var TUStoreRoutingModule = /** @class */ (function () {
    function TUStoreRoutingModule() {
    }
    TUStoreRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUStoreRoutingModule);
    return TUStoreRoutingModule;
}());

var TUStoreModule = /** @class */ (function () {
    function TUStoreModule() {
    }
    TUStoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                TUStoreRoutingModule,
                ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_11__["GooglePlaceModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_13__["LeafletModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_10__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
                }),
            ],
            declarations: [_tustore_component__WEBPACK_IMPORTED_MODULE_12__["TUStoreComponent"]]
        })
    ], TUStoreModule);
    return TUStoreModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-accountdetails-tustore-acquirer-tustore-module.js.map