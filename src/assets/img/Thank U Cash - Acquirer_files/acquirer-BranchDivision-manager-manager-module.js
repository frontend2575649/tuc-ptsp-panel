(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["acquirer-BranchDivision-manager-manager-module"],{

/***/ "./src/app/panel/acquirer/BranchDivision/manager/manager.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/panel/acquirer/BranchDivision/manager/manager.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--branches--detail--team -->\n<div class=\"card bd-t-0\">\n  <div class=\"box-header px-3 pb-3 d-flex align-items-center justify-content-between\">\n    <div class=\"form-inline \">\n      <div class=\"input-group input-group-sm mr-3\">\n        <input type=\"text\" class=\"form-control\" [value]=\"StoresList_Config.SearchParameter\" maxlength=\"100\"\n          class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n          (keyup)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n        <div class=\"input-group-append\">\n          <button class=\"btn btn-outline-light\" type=\"button\"\n            (click)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n              data-feather=\"search\"></i> </button>\n        </div>\n      </div>\n\n      <span class=\"badge badge-pill badge-primary\"\n        *ngFor=\"let TItem of StoresList_Config.Filters\">{{TItem.Title}}</span>\n    </div>\n  \n    <nav class=\"nav nav-with-icon tx-20\">\n      <div class=\"dropdown dropleft\">\n        <a href=\"\" class=\"nav-link mr-2\" id=\"StoresList_sdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n          aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n        <div class=\"dropdown-menu tx-13\">\n          <form class=\"wd-250 pd-15\">\n            <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n              <div class=\"float-right tx-20\" *ngIf=\"StoresList_Config.Sort.SortOrder == 'desc'\"\n                (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                <i class=\"fas fa-sort-alpha-down-alt\"></i>\n              </div>\n              <div class=\"float-right tx-20\" *ngIf=\"StoresList_Config.Sort.SortOrder == 'asc'\"\n                (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                <i class=\"fas fa-sort-alpha-up-alt\"></i>\n              </div>\n            </h6>\n            <a class=\"dropdown-item\" *ngFor=\"let TItem of StoresList_Config.Sort.SortOptions\"\n              (click)=\"StoresList_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n              {{TItem.Name}}\n            </a>\n\n          </form>\n        </div>\n      </div>\n      <div class=\"dropdown dropleft\">\n        <a href=\"\" class=\"nav-link\" id=\"StoresList_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n          aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n        <div class=\"dropdown-menu tx-13\">\n          <form class=\"wd-250 pd-15\">\n            <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Filters\n            </h6>\n            <div>\n              <div class=\"form-group\">\n                <label>Date</label>\n                <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                  [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                  (selected)=\"StoresList_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n              </div>\n            </div>\n            <div>\n              <div class=\"form-group\">\n                <label>Status</label>\n                <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                  [data]=\"StoresList_Config.StatusOptions\" [value]=\"StoresList_Config.Status\"\n                  (valueChanged)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                </select2>\n              </div>\n            </div>\n            <!-- <div>\n              <div class=\"form-group\">\n                <label>Owner</label>\n               <select2 [options]=\"UserAccounts_Filter_Owners_Option\" class=\"wd-100p\"\n                                    (valueChanged)=\"UserAccounts_Filter_Owners_Change($event)\">\n                                </select2>\n              </div>\n            </div> -->\n            <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n              (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n              Reset</button>\n            <button type=\"button\" class=\"btn btn-primary btn-sm \"\n              (click)=\"StoresList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n\n          </form>\n        </div>\n      </div>\n      <a class=\"nav-link ml-2 RefreshColor\"\n        (click)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n          data-feather=\"refresh-cw\"></i> </a>\n    </nav>\n  </div>\n  <div class=\"card-body p-0\">\n    <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n      <thead>\n        <tr>\n          <th class=\"text-center\">\n          </th>\n          <th>\n            Name\n          </th>\n\n          <th>\n            Contact\n          </th>\n          <th class=\"text-center\">\n            Designation\n          </th>\n          <th>\n            Reporting To\n          </th>\n          <th>\n            POS\n          </th>\n          <th>\n            Stores\n          </th>\n          <th>\n            Added on\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr class=\"cursor-pointer\" (click)=\"StoresList_RowSelected(TItem)\" *ngFor=\"let TItem of StoresList_Config.Data\n                                                | paginate: { id: 'StoresList_Paginator',itemsPerPage: StoresList_Config.PageRecordLimit, currentPage: StoresList_Config.ActivePage,\n                                                totalItems: StoresList_Config.TotalRecords }\">\n          <td class=\"td-status text-center pl-0 ml-0\">\n            <img src=\"{{TItem.IconUrl}}\" class=\"avatar-sm rounded-circle bd bd-2\" [style.border-color]=\"TItem.StatusC\">\n          </td>\n          <td>\n            <div class=\"tx-semibold tx-14\">\n              {{TItem.Name}}\n            </div>\n          </td>\n\n          <td>\n            <div class=\"tx-verve\">{{TItem.MobileNumber  || \"--\"}}</div>\n            <div class=\"text-muted tx-12\">{{TItem.EmailAddress  || \"--\"}}</div>\n          </td>\n\n          <td class=\"text-center\">\n\n            <div class=\"\">\n              {{ TItem.RoleName || \"--\"}}\n            </div>\n          </td>\n\n          <td>\n            <div class=\"\">\n\n              {{TItem.OwnerDisplayName || \"--\" }}\n            </div>\n          </td>\n\n          <td>\n            <div class=\"\">\n\n              {{TItem.Terminals}}\n            </div>\n          </td>\n\n          <td>\n            <div class=\"text-primary\">\n\n              {{TItem.Stores}}\n            </div>\n          </td>\n          <td>\n            <div class=\"text-muted text-uppercase\">\n              {{TItem.CreateDate }}\n            </div>\n          </td>\n\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  <div class=\"card-footer pd-20\">\n    <span class=\"mr-3\">\n      {{ 'Common.Showing' | translate }}\n      <span class=\"bold\">{{StoresList_Config.ShowingStart}} {{ 'Common.To' | translate }}\n        {{StoresList_Config.ShowingEnd}}\n      </span> {{ 'Common.Of' | translate }} {{StoresList_Config.TotalRecords}}\n    </span>\n    <span>\n      {{ 'Common.Show' | translate }}\n      <select class=\"wd-40 \" [(ngModel)]=\"StoresList_Config.PageRecordLimit\"\n        (ngModelChange)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n        <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n          {{TItem}}\n        </option>\n      </select>\n    </span>\n    <span class=\"float-right\">\n      <pagination-controls id=\"StoresList_Paginator\" class=\"pagination pagination-space\"\n        (pageChange)=\"StoresList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n        nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n      </pagination-controls>\n    </span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/panel/acquirer/BranchDivision/manager/manager.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/panel/acquirer/BranchDivision/manager/manager.component.ts ***!
  \****************************************************************************/
/*! exports provided: ManagerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerComponent", function() { return ManagerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_5__);






var ManagerComponent = /** @class */ (function () {
    function ManagerComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this.UserAccounts_Filter_Owners_Selected = 0;
        this._HelperService.ShowDateRange = false;
    }
    ManagerComponent.prototype.ngOnInit = function () {
        var _this = this;
        feather_icons__WEBPACK_IMPORTED_MODULE_5__["replace"]();
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            _this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (_this._HelperService.AppConfig.ActiveReferenceKey == null ||
                _this._HelperService.AppConfig.ActiveReferenceId == null) {
                // this.StoresList_Filter_Owners_Load();
                _this.UserAccounts_Filter_Owners_Load();
                _this.StoresList_Setup();
            }
            else {
                // this._HelperService.Get_UserAccountDetails(true);
                // this.StoresList_Filter_Owners_Load();
                _this.StoresList_Setup();
                _this.UserAccounts_Filter_Owners_Load();
            }
        });
    };
    ManagerComponent.prototype.StoresList_Setup = function () {
        this.StoresList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Title: "Available Stores",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            // DefaultSortExpression: 'CreateDate desc',
            TableFields: [
                {
                    DisplayName: "Name",
                    SystemName: "Name",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "Owner",
                    SystemName: "OwnerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "MobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: false,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: "Email Address",
                    SystemName: "EmailAddress",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: false,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: "Terminals",
                    SystemName: "Terminals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: "R. %",
                    SystemName: "RewardPercentage",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "_600",
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: "Last Tr",
                    SystemName: "LastTransactionDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: "Last Login",
                    SystemName: "LastLoginDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: false,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ],
        };
        this.StoresList_Config = this._DataHelperService.List_Initialize_ForStores(this.StoresList_Config);
        this.StoresList_GetData();
    };
    ManagerComponent.prototype.StoresList_ToggleOption = function (event, Type) {
        this.StoresList_Config = this._DataHelperService.List_Operations(this.StoresList_Config, event, Type);
        if (this.StoresList_Config.RefreshData == true) {
            this.StoresList_GetData();
        }
    };
    ManagerComponent.prototype.StoresList_GetData = function () {
        var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
        this.StoresList_Config = TConfig;
    };
    ManagerComponent.prototype.StoresList_RowSelected = function (ReferenceData) {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    };
    ManagerComponent.prototype.UserAccounts_Filter_Owners_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
        //     [
        //         this._HelperService.AppConfig.AccountType.Merchant,
        //         this._HelperService.AppConfig.AccountType.Acquirer,
        //         this._HelperService.AppConfig.AccountType.PGAccount,
        //         this._HelperService.AppConfig.AccountType.PosAccount
        //     ]
        //     , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.UserAccounts_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    ManagerComponent.prototype.UserAccounts_Filter_Owners_Change = function (event) {
        this._HelperService.Update_CurrentFilterSnap(event, this._HelperService.AppConfig.ListToggleOption.Other, this.StoresList_Config, this._HelperService.AppConfig.OtherFilters.Manager.Owner);
        this.OwnerEventProcessing(event);
    };
    ManagerComponent.prototype.OwnerEventProcessing = function (event) {
        if (event.value == this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = event.value;
            this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '='));
        }
    };
    ManagerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-manager",
            template: __webpack_require__(/*! ./manager.component.html */ "./src/app/panel/acquirer/BranchDivision/manager/manager.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_4__["DataHelperService"]])
    ], ManagerComponent);
    return ManagerComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/BranchDivision/manager/manager.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/panel/acquirer/BranchDivision/manager/manager.module.ts ***!
  \*************************************************************************/
/*! exports provided: TUManagerRoutingModule, ManagerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUManagerRoutingModule", function() { return TUManagerRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagerModule", function() { return ManagerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _manager_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./manager.component */ "./src/app/panel/acquirer/BranchDivision/manager/manager.component.ts");











var routes = [{ path: "", component: _manager_component__WEBPACK_IMPORTED_MODULE_10__["ManagerComponent"] }];
var TUManagerRoutingModule = /** @class */ (function () {
    function TUManagerRoutingModule() {
    }
    TUManagerRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUManagerRoutingModule);
    return TUManagerRoutingModule;
}());

var ManagerModule = /** @class */ (function () {
    function ManagerModule() {
    }
    ManagerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_manager_component__WEBPACK_IMPORTED_MODULE_10__["ManagerComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUManagerRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"]
            ]
        })
    ], ManagerModule);
    return ManagerModule;
}());



/***/ })

}]);
//# sourceMappingURL=acquirer-BranchDivision-manager-manager-module.js.map