(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../../../../modules/accounts/tustores/branch/tustores.module": [
		"./src/app/modules/accounts/tustores/branch/tustores.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tustores-branch-tustores-module"
	],
	"../../../../modules/accounts/tustores/manager/tustores.module": [
		"./src/app/modules/accounts/tustores/manager/tustores.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tustores-manager-tustores-module"
	],
	"../../../../modules/accounts/tustores/merchant/tustores.module": [
		"./src/app/modules/accounts/tustores/merchant/tustores.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tustores-merchant-tustores-module"
	],
	"../../../../modules/accounts/tuteams/manager/tuteams.module": [
		"./src/app/modules/accounts/tuteams/manager/tuteams.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuteams-manager-tuteams-module"
	],
	"../../../../modules/accounts/tuterminals/branch/tuterminals.module": [
		"./src/app/modules/accounts/tuterminals/branch/tuterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuterminals-branch-tuterminals-module"
	],
	"../../../../modules/accounts/tuterminals/manager/tuterminals.module": [
		"./src/app/modules/accounts/tuterminals/manager/tuterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuterminals-manager-tuterminals-module"
	],
	"../../../../modules/accounts/tuterminals/merchant/tuterminals.module": [
		"./src/app/modules/accounts/tuterminals/merchant/tuterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuterminals-merchant-tuterminals-module"
	],
	"../../../../modules/accounts/tuterminals/store/tuterminals.module": [
		"./src/app/modules/accounts/tuterminals/store/tuterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuterminals-store-tuterminals-module"
	],
	"../../../../modules/transactions/tusale/branch/acquirer/tusale.module": [
		"./src/app/modules/transactions/tusale/branch/acquirer/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-transactions-tusale-branch-acquirer-tusale-module"
	],
	"../../../../modules/transactions/tusale/merchant/acquirer/tusale.module": [
		"./src/app/modules/transactions/tusale/merchant/acquirer/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-transactions-tusale-merchant-acquirer-tusale-module"
	],
	"../../../../modules/transactions/tusale/store/tusale.module": [
		"./src/app/modules/transactions/tusale/store/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-transactions-tusale-store-tusale-module"
	],
	"../../../../modules/transactions/tusale/terminal/acquirer/tusale.module": [
		"./src/app/modules/transactions/tusale/terminal/acquirer/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-transactions-tusale-terminal-acquirer-tusale-module"
	],
	"../../../../panel/acquirer/BranchDivision/dashboard/dashboard.module": [
		"./src/app/panel/acquirer/BranchDivision/dashboard/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-BranchDivision-dashboard-dashboard-module"
	],
	"../../../../panel/acquirer/campaigns/sales/tusale.module": [
		"./src/app/panel/acquirer/campaigns/sales/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panel-acquirer-campaigns-sales-tusale-module"
	],
	"../../../acquirer/BranchDivision/manager/manager.module": [
		"./src/app/panel/acquirer/BranchDivision/manager/manager.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"acquirer-BranchDivision-manager-manager-module"
	],
	"../../../dashboards/campaign/acquirer/dashboard.module": [
		"./src/app/modules/dashboards/campaign/acquirer/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"dashboards-campaign-acquirer-dashboard-module"
	],
	"../../../dashboards/manager/acquirer/dashboard.module": [
		"./src/app/modules/dashboards/manager/acquirer/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"dashboards-manager-acquirer-dashboard-module"
	],
	"../../../dashboards/merchant/acquirer/dashboard.module": [
		"./src/app/modules/dashboards/merchant/acquirer/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"dashboards-merchant-acquirer-dashboard-module"
	],
	"../../../dashboards/store/acquirer/dashboard.module": [
		"./src/app/modules/dashboards/store/acquirer/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"dashboards-store-acquirer-dashboard-module"
	],
	"../../../dashboards/terminal/acquirer/dashboard.module": [
		"./src/app/modules/dashboards/terminal/acquirer/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"dashboards-terminal-acquirer-dashboard-module"
	],
	"../../../modules/transactions/turedeem/terminal/turedeem.module": [
		"./src/app/modules/transactions/turedeem/terminal/turedeem.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-transactions-turedeem-terminal-turedeem-module"
	],
	"../../../modules/transactions/tureward/terminal/tureward.module": [
		"./src/app/modules/transactions/tureward/terminal/tureward.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-transactions-tureward-terminal-tureward-module"
	],
	"../../../modules/transactions/turewardclaim/terminal/turewardclaim.module": [
		"./src/app/modules/transactions/turewardclaim/terminal/turewardclaim.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-transactions-turewardclaim-terminal-turewardclaim-module"
	],
	"../../../modules/transactions/tusale/terminal/tusale.module": [
		"./src/app/modules/transactions/tusale/terminal/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-transactions-tusale-terminal-tusale-module"
	],
	"../../panel/acquirer/BranchDivision/EditBranch/EditBranch.module": [
		"./src/app/panel/acquirer/BranchDivision/EditBranch/EditBranch.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-BranchDivision-EditBranch-EditBranch-module"
	],
	"../../panel/acquirer/accountdetails/BranchDetails/branchdetails.module": [
		"./src/app/panel/acquirer/accountdetails/BranchDetails/branchdetails.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"panel-acquirer-accountdetails-BranchDetails-branchdetails-module"
	],
	"../../panel/acquirer/accounts/branch/branch.module": [
		"./src/app/panel/acquirer/accounts/branch/branch.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panel-acquirer-accounts-branch-branch-module"
	],
	"../tuterminal/dashboard/tudashboard.module": [
		"./src/app/modules/accounts/tuterminal/dashboard/tudashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"tuterminal-dashboard-tudashboard-module"
	],
	"./../../modules/accountdetails/tucampaign/acquirer/tucampaign.module": [
		"./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accountdetails-tucampaign-acquirer-tucampaign-module"
	],
	"./../../modules/accountdetails/tumanager/acquirer/tumanager.module": [
		"./src/app/modules/accountdetails/tumanager/acquirer/tumanager.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"modules-accountdetails-tumanager-acquirer-tumanager-module"
	],
	"./../../modules/accountdetails/tumerchant/acquirer/tumerchant.module": [
		"./src/app/modules/accountdetails/tumerchant/acquirer/tumerchant.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"default~modules-accountdetails-tumerchant-acquirer-tumerchant-module~modules-dashboards-tulivetermin~96d9a3c3",
		"common",
		"modules-accountdetails-tumerchant-acquirer-tumerchant-module"
	],
	"./../../modules/accountdetails/tustore/acquirer/tustore.module": [
		"./src/app/modules/accountdetails/tustore/acquirer/tustore.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"modules-accountdetails-tustore-acquirer-tustore-module"
	],
	"./../../modules/accountdetails/tuterminal/acquirer/tuterminal.module": [
		"./src/app/modules/accountdetails/tuterminal/acquirer/tuterminal.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"modules-accountdetails-tuterminal-acquirer-tuterminal-module"
	],
	"./../../modules/accounts/temp/hclist.module": [
		"./src/app/modules/accounts/temp/hclist.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-temp-hclist-module"
	],
	"./../../modules/accounts/tumerchants/acquirer/tumerchants.module": [
		"./src/app/modules/accounts/tumerchants/acquirer/tumerchants.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tumerchants-acquirer-tumerchants-module"
	],
	"./../../modules/accounts/tustores/acquirer/tustores.module": [
		"./src/app/modules/accounts/tustores/acquirer/tustores.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tustores-acquirer-tustores-module"
	],
	"./../../modules/accounts/tusubaccounts/acquirer/tusubaccounts.module": [
		"./src/app/modules/accounts/tusubaccounts/acquirer/tusubaccounts.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tusubaccounts-acquirer-tusubaccounts-module"
	],
	"./../../modules/accounts/tuterminal/tuterminal.module": [
		"./src/app/modules/accounts/tuterminal/tuterminal.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-accounts-tuterminal-tuterminal-module"
	],
	"./../../modules/accounts/tuterminals/acquirer/tuterminals.module": [
		"./src/app/modules/accounts/tuterminals/acquirer/tuterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-accounts-tuterminals-acquirer-tuterminals-module"
	],
	"./../../modules/dashboards/tuactivity/acquirer/tuactivity.module": [
		"./src/app/modules/dashboards/tuactivity/acquirer/tuactivity.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-dashboards-tuactivity-acquirer-tuactivity-module"
	],
	"./../../modules/dashboards/tulivemerchants/acquirer/tulivemerchants.module": [
		"./src/app/modules/dashboards/tulivemerchants/acquirer/tulivemerchants.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-dashboards-tulivemerchants-acquirer-tulivemerchants-module"
	],
	"./../../modules/dashboards/tuliveterminals/acquirer/tuliveterminals.module": [
		"./src/app/modules/dashboards/tuliveterminals/acquirer/tuliveterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"default~modules-accountdetails-tumerchant-acquirer-tumerchant-module~modules-dashboards-tulivetermin~96d9a3c3",
		"common",
		"modules-dashboards-tuliveterminals-acquirer-tuliveterminals-module"
	],
	"./../../modules/dashboards/turmreport/acquirer/turmreport.module": [
		"./src/app/modules/dashboards/turmreport/acquirer/turmreport.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-dashboards-turmreport-acquirer-turmreport-module"
	],
	"./../../modules/dashboards/tusalesreport/acquirer/tusalesreport.module": [
		"./src/app/modules/dashboards/tusalesreport/acquirer/tusalesreport.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"modules-dashboards-tusalesreport-acquirer-tusalesreport-module"
	],
	"./../../modules/transactions/tusale/acquirer/tusale.module": [
		"./src/app/modules/transactions/tusale/acquirer/tusale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"modules-transactions-tusale-acquirer-tusale-module"
	],
	"./../../pages/accounts/tuacquirermerchants/tuacquirermerchants.module": [
		"./src/app/pages/accounts/tuacquirermerchants/tuacquirermerchants.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-accounts-tuacquirermerchants-tuacquirermerchants-module"
	],
	"./../../pages/accounts/tuappusers/tuappusers.module": [
		"./src/app/pages/accounts/tuappusers/tuappusers.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-accounts-tuappusers-tuappusers-module"
	],
	"./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module": [
		"./src/app/pages/dashboard/tumerchantterminallive/tumerchantterminallive.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-dashboard-tumerchantterminallive-tumerchantterminallive-module"
	],
	"./../../pages/hcprofile/hcprofile.module": [
		"./src/app/pages/hcprofile/hcprofile.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-hcprofile-hcprofile-module"
	],
	"./../../pages/transactions/tuterminalssale/tuterminalssale.module": [
		"./src/app/pages/transactions/tuterminalssale/tuterminalssale.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-transactions-tuterminalssale-tuterminalssale-module"
	],
	"./../../pages/useronboarding/customeronboarding/customeronboarding.module": [
		"./src/app/pages/useronboarding/customeronboarding/customeronboarding.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-useronboarding-customeronboarding-customeronboarding-module"
	],
	"./../../pages/useronboarding/customerupload/customerupload.module": [
		"./src/app/pages/useronboarding/customerupload/customerupload.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~pages-useronboarding-customerupload-customerupload-module~pages-useronboarding-merchantuploa~50ff6ecd",
		"common",
		"pages-useronboarding-customerupload-customerupload-module"
	],
	"./../../pages/useronboarding/merchantmanager/merchantmanager.module": [
		"./src/app/pages/useronboarding/merchantmanager/merchantmanager.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-useronboarding-merchantmanager-merchantmanager-module"
	],
	"./../../pages/useronboarding/merchantonboarding/merchantonboarding.module": [
		"./src/app/pages/useronboarding/merchantonboarding/merchantonboarding.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"pages-useronboarding-merchantonboarding-merchantonboarding-module"
	],
	"./../../pages/useronboarding/merchantupload/merchantupload.module": [
		"./src/app/pages/useronboarding/merchantupload/merchantupload.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~pages-useronboarding-customerupload-customerupload-module~pages-useronboarding-merchantuploa~50ff6ecd",
		"common",
		"pages-useronboarding-merchantupload-merchantupload-module"
	],
	"./../../pages/useronboarding/tuaddposterminal/tuaddposterminal.module": [
		"./src/app/pages/useronboarding/tuaddposterminal/tuaddposterminal.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"pages-useronboarding-tuaddposterminal-tuaddposterminal-module"
	],
	"./../../panel/acquirer/accountcreation/addbranch/addbranch.module": [
		"./src/app/panel/acquirer/accountcreation/addbranch/addbranch.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"panel-acquirer-accountcreation-addbranch-addbranch-module"
	],
	"./../../panel/acquirer/accountcreation/rm/tuaddrm.module": [
		"./src/app/panel/acquirer/accountcreation/rm/tuaddrm.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-accountcreation-rm-tuaddrm-module"
	],
	"./../../panel/acquirer/accountdetails/rm/rm.module": [
		"./src/app/panel/acquirer/accountdetails/rm/rm.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-accountdetails-rm-rm-module"
	],
	"./../../panel/acquirer/accountdetails/subadmindetails/subadmindetails.module": [
		"./src/app/panel/acquirer/accountdetails/subadmindetails/subadmindetails.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"default~modules-accountdetails-tumanager-acquirer-tumanager-module~modules-accountdetails-tumerchant~5e72725d",
		"common",
		"panel-acquirer-accountdetails-subadmindetails-subadmindetails-module"
	],
	"./../../panel/acquirer/accounts/relationshipmanagers/relationshipmanagers.module": [
		"./src/app/panel/acquirer/accounts/relationshipmanagers/relationshipmanagers.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panel-acquirer-accounts-relationshipmanagers-relationshipmanagers-module"
	],
	"./../../panel/acquirer/accounts/subadmins/subadmins.module": [
		"./src/app/panel/acquirer/accounts/subadmins/subadmins.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panel-acquirer-accounts-subadmins-subadmins-module"
	],
	"./../../panel/acquirer/analytics/salesanalytics/salesanalytics.module": [
		"./src/app/panel/acquirer/analytics/salesanalytics/salesanalytics.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-analytics-salesanalytics-salesanalytics-module"
	],
	"./../../panel/acquirer/campaigns/addcampaign/addcampaign.module": [
		"./src/app/panel/acquirer/campaigns/addcampaign/addcampaign.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-campaigns-addcampaign-addcampaign-module"
	],
	"./../../panel/acquirer/campaigns/campaign/campaign.module": [
		"./src/app/panel/acquirer/campaigns/campaign/campaign.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-campaigns-campaign-campaign-module"
	],
	"./../../panel/acquirer/campaigns/campaigns/campaigns.module": [
		"./src/app/panel/acquirer/campaigns/campaigns/campaigns.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panel-acquirer-campaigns-campaigns-campaigns-module"
	],
	"./../../panel/acquirer/dashboards/root/dashboard.module": [
		"./src/app/panel/acquirer/dashboards/root/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-acquirer-dashboards-root-dashboard-module"
	],
	"./../../panel/rm/accounts/stores/stores.module": [
		"./src/app/panel/rm/accounts/stores/stores.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-rm-accounts-stores-stores-module"
	],
	"./../../panel/rm/accounts/terminals/terminals.module": [
		"./src/app/panel/rm/accounts/terminals/terminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-rm-accounts-terminals-terminals-module"
	],
	"./../../panel/rm/dashboards/root/dashboard.module": [
		"./src/app/panel/rm/dashboards/root/dashboard.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-rm-dashboards-root-dashboard-module"
	],
	"./../../panel/rm/live/liveterminals/liveterminals.module": [
		"./src/app/panel/rm/live/liveterminals/liveterminals.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~dashboards-campaign-acquirer-dashboard-module~32d071e5",
		"common",
		"panel-rm-live-liveterminals-liveterminals-module"
	],
	"./panels/acquirer/acquirer.module": [
		"./src/app/panels/acquirer/acquirer.module.ts",
		"default~acquirer-BranchDivision-manager-manager-module~modules-accountdetails-tucampaign-acquirer-tu~2cb5de8e",
		"common",
		"panels-acquirer-acquirer-module"
	],
	"./panels/rm/rm.module": [
		"./src/app/panels/rm/rm.module.ts",
		"panels-rm-rm-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/service */ "./src/app/service/service.ts");
/* harmony import */ var _auth_access_access_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/access/access.component */ "./src/app/auth/access/access.component.ts");
/* harmony import */ var _auth_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/forgotpassword/forgotpassword.component */ "./src/app/auth/forgotpassword/forgotpassword.component.ts");
/* harmony import */ var _auth_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/resetpassword/resetpassword.component */ "./src/app/auth/resetpassword/resetpassword.component.ts");
/* harmony import */ var _auth_onboarding_register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/onboarding/register/register.component */ "./src/app/auth/onboarding/register/register.component.ts");
/* harmony import */ var _auth_onboarding_registerstore_registerstore_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth/onboarding/registerstore/registerstore.component */ "./src/app/auth/onboarding/registerstore/registerstore.component.ts");
/* harmony import */ var _auth_onboarding_registerreward_registerreward_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./auth/onboarding/registerreward/registerreward.component */ "./src/app/auth/onboarding/registerreward/registerreward.component.ts");
/* harmony import */ var _auth_onboarding_registercomplete_registercomplete_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./auth/onboarding/registercomplete/registercomplete.component */ "./src/app/auth/onboarding/registercomplete/registercomplete.component.ts");












var _Routes = [
    { path: '', redirectTo: 'account/login', pathMatch: 'full' },
    { path: 'login', redirectTo: 'account/login', pathMatch: 'full' },
    { path: 'forgotpassword', redirectTo: 'account/forgotpassword', pathMatch: 'full' },
    { path: 'resetpassword/:referencekey', redirectTo: 'account/resetpassword', pathMatch: 'full' },
    { path: 'access/:referencekey/:username', component: _auth_access_access_component__WEBPACK_IMPORTED_MODULE_5__["AccessComponent"] },
    { path: 'login', component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'account/login', component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'account/register', component: _auth_onboarding_register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"] },
    { path: 'account/setup', component: _auth_onboarding_registerstore_registerstore_component__WEBPACK_IMPORTED_MODULE_9__["RegisterStoreComponent"] },
    { path: 'account/setupcomplete', component: _auth_onboarding_registercomplete_registercomplete_component__WEBPACK_IMPORTED_MODULE_11__["RegisterCompleteComponent"] },
    { path: 'account/reward', component: _auth_onboarding_registerreward_registerreward_component__WEBPACK_IMPORTED_MODULE_10__["RegisterRewardComponent"] },
    { path: 'account/forgotpassword', component: _auth_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordComponent"] },
    { path: 'account/resetpassword/:referencekey', component: _auth_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_7__["ResetPasswordComponent"] },
    { path: 'resetpassword/:referencekey', component: _auth_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_7__["ResetPasswordComponent"] },
    { canActivate: [_service_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"]], path: '', loadChildren: './panels/acquirer/acquirer.module#TUAcquirerModule' },
    { canActivate: [_service_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"]], path: '', loadChildren: './panels/rm/rm.module#TURmModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(_Routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<!-- <ngx-loading-bar color=\"#CD67BA\" height=\"8px\"></ngx-loading-bar> -->\n<ng-progress [spinnerPosition]=\"'right'\" [color]=\"'#00cccc'\" thick='true'></ng-progress>\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(_TranslateService) {
        this._TranslateService = _TranslateService;
        this.idleState = 'Not started.';
        this.timedOut = false;
        this.lastPing = null;
        _TranslateService.addLangs(['en']);
        _TranslateService.setDefaultLang('en');
        var browserLang = _TranslateService.getBrowserLang();
        _TranslateService.use(browserLang.match(/en/) ? browserLang : 'en');
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _auth_onboarding_register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/onboarding/register/register.component */ "./src/app/auth/onboarding/register/register.component.ts");
/* harmony import */ var _auth_onboarding_registerstore_registerstore_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/onboarding/registerstore/registerstore.component */ "./src/app/auth/onboarding/registerstore/registerstore.component.ts");
/* harmony import */ var _auth_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/forgotpassword/forgotpassword.component */ "./src/app/auth/forgotpassword/forgotpassword.component.ts");
/* harmony import */ var _auth_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth/resetpassword/resetpassword.component */ "./src/app/auth/resetpassword/resetpassword.component.ts");
/* harmony import */ var _auth_access_access_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./auth/access/access.component */ "./src/app/auth/access/access.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./service/service */ "./src/app/service/service.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _ngx_progressbar_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ngx-progressbar/core */ "./node_modules/@ngx-progressbar/core/fesm5/ngx-progressbar-core.js");
/* harmony import */ var _ngx_progressbar_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ngx-progressbar/http */ "./node_modules/@ngx-progressbar/http/fesm5/ngx-progressbar-http.js");
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var agm_overlays__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! agm-overlays */ "./node_modules/agm-overlays/index.js");
/* harmony import */ var agm_overlays__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(agm_overlays__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var ngx_image_cropper__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-image-cropper */ "./node_modules/ngx-image-cropper/fesm5/ngx-image-cropper.js");
/* harmony import */ var angular_archwizard__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular-archwizard */ "./node_modules/angular-archwizard/fesm5/angular-archwizard.js");
/* harmony import */ var angular4_paystack__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! angular4-paystack */ "./node_modules/angular4-paystack/fesm5/angular4-paystack.js");
/* harmony import */ var ngx_json_viewer__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-json-viewer */ "./node_modules/ngx-json-viewer/ngx-json-viewer.es5.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var _auth_onboarding_registerreward_registerreward_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./auth/onboarding/registerreward/registerreward.component */ "./src/app/auth/onboarding/registerreward/registerreward.component.ts");
/* harmony import */ var _auth_onboarding_registercomplete_registercomplete_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./auth/onboarding/registercomplete/registercomplete.component */ "./src/app/auth/onboarding/registercomplete/registercomplete.component.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @ngx-loading-bar/http-client */ "./node_modules/@ngx-loading-bar/http-client/fesm5/ngx-loading-bar-http-client.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/fesm5/ngx-loading-bar-core.js");






























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _auth_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _auth_onboarding_register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"],
                _auth_onboarding_registerstore_registerstore_component__WEBPACK_IMPORTED_MODULE_7__["RegisterStoreComponent"],
                _auth_onboarding_registerreward_registerreward_component__WEBPACK_IMPORTED_MODULE_25__["RegisterRewardComponent"],
                _auth_onboarding_registercomplete_registercomplete_component__WEBPACK_IMPORTED_MODULE_26__["RegisterCompleteComponent"],
                _auth_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_8__["ForgotPasswordComponent"],
                _auth_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_9__["ResetPasswordComponent"],
                _auth_access_access_component__WEBPACK_IMPORTED_MODULE_10__["AccessComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                _service_service__WEBPACK_IMPORTED_MODULE_13__["TranslaterModule"],
                ng5_slider__WEBPACK_IMPORTED_MODULE_19__["Ng5SliderModule"],
                angular_archwizard__WEBPACK_IMPORTED_MODULE_21__["ArchwizardModule"],
                ngx_image_cropper__WEBPACK_IMPORTED_MODULE_20__["ImageCropperModule"],
                agm_overlays__WEBPACK_IMPORTED_MODULE_18__["AgmOverlays"],
                angular4_paystack__WEBPACK_IMPORTED_MODULE_22__["Angular4PaystackModule"],
                ngx_json_viewer__WEBPACK_IMPORTED_MODULE_23__["NgxJsonViewerModule"],
                ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_24__["GooglePlaceModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_27__["ChartsModule"],
                _ngx_progressbar_core__WEBPACK_IMPORTED_MODULE_15__["NgProgressModule"],
                _ngx_progressbar_http__WEBPACK_IMPORTED_MODULE_16__["NgProgressHttpModule"],
                _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_29__["LoadingBarModule"],
                _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_28__["LoadingBarHttpClientModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
                }),
                ng2_file_input__WEBPACK_IMPORTED_MODULE_17__["Ng2FileInputModule"].forRoot(),
            ],
            providers: [
                _service_service__WEBPACK_IMPORTED_MODULE_13__["HelperService"],
                _service_service__WEBPACK_IMPORTED_MODULE_13__["DataHelperService"],
                _service_service__WEBPACK_IMPORTED_MODULE_13__["FilterHelperService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/access/access.component.html":
/*!***************************************************!*\
  !*** ./src/app/auth/access/access.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"dark h-v d-flex flex align-items-center\">\n    <div class=\"mx-auto w-xl w-auto-xs animate fadeIn text-center\">\n        <div class=\"mb-3\"><img src=\"../../../assets/img/logo.png\" class=\"w-md \">\n            <div class=\"mt-3 font-bold\"> Login To Panel</div>\n        </div>\n        <form [formGroup]=\"_Form_Login\" (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\">\n            <div class=\"md-form-group\" [ngClass]=\"{'has-error':!_Form_Login.controls['username'].valid && _Form_Login.controls['username'].touched}\">\n                <input type=\"text\" name=\"username\" maxlength=\"256\" class=\"md-input text-center\" [formControl]=\"_Form_Login.controls['username']\">\n                <label class=\"block w-100\">{{ 'Login.UserName' | translate }}</label>\n                <div>\n                    <label class=\"block w-100 text-danger\" for=\"username\" *ngIf=\"_Form_Login.controls['username'].hasError('required') && _Form_Login.controls['username'].touched\">{{\n                        'Login.UserName_ER_Required' | translate }}</label>\n                </div>\n            </div>\n            <div class=\"md-form-group\" [ngClass]=\"{'has-error':!_Form_Login.controls['password'].valid && _Form_Login.controls['password'].touched}\">\n                <input type=\"password\" name=\"password\" maxlength=\"50\" class=\"md-input text-center\" [formControl]=\"_Form_Login.controls['password']\">\n                <label class=\"block w-100\">{{ 'Login.Password' | translate }}</label>\n                <div>\n                    <label class=\"text-danger\" for=\"password\" *ngIf=\"_Form_Login.controls['password'].hasError('required') && _Form_Login.controls['password'].touched\">{{\n                        'Login.Password_ER_Required' | translate }}</label>\n                </div>\n            </div>\n            <button type=\"submit\" class=\"btn btn-rounded warn\" [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                <span *ngIf=\"!_Form_Login_Processing\">\n                    {{ 'Login.LoginBtn' | translate }}\n                </span>\n                <span *ngIf=\"_Form_Login_Processing\">\n                    <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i> {{ 'Common.PleaseWait' | translate }}...\n                </span>\n            </button>\n        </form>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/auth/access/access.component.ts":
/*!*************************************************!*\
  !*** ./src/app/auth/access/access.component.ts ***!
  \*************************************************/
/*! exports provided: AccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessComponent", function() { return AccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/service */ "./src/app/service/service.ts");






var AccessComponent = /** @class */ (function () {
    function AccessComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _TranslateService) {
        var _this = this;
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._TranslateService = _TranslateService;
        this._Form_Login_Processing = false;
        this._Form_Login = _FormBuilder.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
        this._ActivatedRoute.params.subscribe(function (params) {
            var AccountKey = params['referencekey'];
            var UserName = params['username'];
            if (AccountKey != undefined && UserName != undefined) {
                _this.ProcessAccount(UserName, AccountKey);
            }
        });
    }
    AccessComponent.prototype.ngOnInit = function () {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    };
    AccessComponent.prototype.ProcessAccount = function (username, password) {
        var _this = this;
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'login',
            UserName: username,
            AccountKey: password,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(function (_Response) {
            _this._Form_Login_Processing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.Account, _Response.Result);
                var _StorageReqH = _this._HelperService.GetStorage(_this._HelperService.AppConfig.Storage.OReqH);
                _StorageReqH.hcuak = _Response.Result['AccessKey'];
                _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                _this._HelperService.NotifySuccess(_Response.Message);
                _this._Form_Login.reset();
                if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.PosAccount) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.PTSP;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Merchant) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
                }
                else {
                    _this._HelperService.NotifyError('Invalid account. Please contact support');
                }
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._Form_Login_Processing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    AccessComponent.prototype._Form_Login_Process = function (value) {
        var _this = this;
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'login',
            UserName: value.username,
            Password: value.password,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(function (_Response) {
            _this._Form_Login_Processing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.Account, _Response.Result);
                var _StorageReqH = _this._HelperService.GetStorage(_this._HelperService.AppConfig.Storage.OReqH);
                _StorageReqH.hcuak = _Response.Result['AccessKey'];
                _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                _this._HelperService.NotifySuccess(_Response.Message);
                _this._Form_Login.reset();
                if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.PosAccount) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.PTSP;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Merchant) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
                }
                else {
                    _this._HelperService.NotifyError('Invalid account. Please contact support');
                }
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._Form_Login_Processing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    AccessComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'access',
            template: __webpack_require__(/*! ./access.component.html */ "./src/app/auth/access/access.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]])
    ], AccessComponent);
    return AccessComponent;
}());



/***/ }),

/***/ "./src/app/auth/forgotpassword/forgotpassword.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/auth/forgotpassword/forgotpassword.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content bg-white\">\n    <div class=\"container d-flex justify-content-center ht-100p\">\n        <div class=\"mx-wd-300 wd-sm-450 ht-100p d-flex flex-column align-items-center justify-content-center\">\n          <div class=\"wd-80p wd-sm-300 mg-b-15\" style=\"transform: scale(-1,1);\"><img src=\"../../../assets/img/Merchant.svg\" class=\"img-fluid\" alt=\"\"></div>\n          <h4 class=\"tx-20 tx-sm-24\">Forget Password?</h4>\n          <p class=\"tx-color-03 mg-b-30 tx-center\">Enter your username or email address and we will send you a link to reset your password.</p>\n          <div class=\"wd-100p d-flex align-items-center justify-content-center lex-column flex-sm-row mg-b-40\">\n            <form [formGroup]=\"_Form_Login\" (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\"> \n              <div class=\"form-group\" \n              [ngClass]=\"{'has-error':!_Form_Login.controls['username'].valid && _Form_Login.controls['username'].touched}\">\n                    <div class=\"d-flex \">\n                        <input type=\"text\" class=\"form-control wd-sm-250 flex-fill \" \n                        [formControl]=\"_Form_Login.controls['username']\"\n                        placeholder=\"Enter username or email address\">\n                        <button class=\"btn btn-brand-02 mg-sm-l-10 mg-t-10 mg-sm-t-0\"\n                        [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                        <span *ngIf=\"!_Form_Login_Processing\">Send Link</span>\n                        <span *ngIf=\"_Form_Login_Processing\">\n                            <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                            {{ 'Common.PleaseWait' | translate }}...\n                        </span>  \n                        </button> \n                    </div>    \n              </div>    \n            </form>  \n          </div>\n    \n            <div class=\" text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-5 text-muted\">\n                © Copyright <a target=\"_blank\" class=\"text-primary\"\n                    href=\"http://connectedanalytics.xyz\">Connected\n                    Analytics Inc.</a> 2020 |\n                All Right\n                Reserved\n            </div>\n        </div>\n    </div>\n    <!-- <div class=\"mx-auto w-xl w-auto-xs animate fadeIn text-center\">\n        <div class=\"mb-3\"><img src=\"../../../assets/img/thankulogo.svg\" class=\"w-md \">\n            <div class=\"mt-3 font-bold text-white\"> Forgot Password</div>\n        </div>\n        <form [formGroup]=\"_Form_Login\" (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\">\n            <div class=\"md-form-group\"\n                [ngClass]=\"{'has-error':!_Form_Login.controls['username'].valid && _Form_Login.controls['username'].touched}\">\n                <input type=\"text\" name=\"username\" maxlength=\"256\" class=\"md-input text-center\"\n                    [formControl]=\"_Form_Login.controls['username']\">\n                <label class=\"block w-100\">{{ 'Login.UserName' | translate }}</label>\n                <div>\n                    <label class=\"block w-100 text-danger\" for=\"username\"\n                        *ngIf=\"_Form_Login.controls['username'].hasError('required') && _Form_Login.controls['username'].touched\">{{\n                        'Login.UserName_ER_Required' | translate }}</label>\n                </div>\n            </div>\n            <button type=\"submit\" class=\"btn btn-rounded warn\"\n                [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                <span *ngIf=\"!_Form_Login_Processing\">\n                    Send Password Reset Link\n                </span>\n                <span *ngIf=\"_Form_Login_Processing\">\n                    <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i> {{ 'Common.PleaseWait' | translate }}...\n                </span>\n            </button>\n        </form>\n    </div> -->\n\n</div>"

/***/ }),

/***/ "./src/app/auth/forgotpassword/forgotpassword.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/auth/forgotpassword/forgotpassword.component.ts ***!
  \*****************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/service */ "./src/app/service/service.ts");






var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(_FormBuilder, _TranslateService, _Router, _HelperService) {
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this._Form_Login_Processing = false;
        this._Form_Login = _FormBuilder.group({
            'username': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    };
    ForgotPasswordComponent.prototype._Form_Login_Process = function (value) {
        var _this = this;
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'forgotpasswordrequest',
            UserName: value.username,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(function (_Response) {
            _this._Form_Login_Processing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.NotifySuccess(' Password reset link is sent on your registered email address. Please click on link to reset password');
                _this._Form_Login.reset();
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._Form_Login_Processing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    ForgotPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'forgotpassword',
            template: __webpack_require__(/*! ./forgotpassword.component.html */ "./src/app/auth/forgotpassword/forgotpassword.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content content-fixed content-auth\">\n    <div class=\"container\">\n        <div class=\"media align-items-stretch justify-content-center ht-100p pos-relative\">\n            <div class=\"media-body align-items-center d-none d-lg-flex\">\n                <div class=\"mx-wd-600\" style=\"transform: scale(-1,1);\">\n                    <img src=\"../../../assets/img/Merchant.svg\" class=\"img-fluid\" alt=\"\">\n                </div>\n            </div>\n            <div class=\"sign-wrapper mg-lg-l-50 mg-xl-l-60\">\n                <div class=\"wd-100p\">\n                    <img src=\"../../../assets/img/logoinverse.png\" class=\"wd-200 mb-4\">\n                    <h3 class=\"tx-color-01 mg-b-5\">Sign In</h3>\n                    <p class=\"tx-color-03 tx-16 mg-b-40\">Welcome back! Please signin to continue.</p>\n                    <form [formGroup]=\"_Form_Login\" (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\">\n                        <div class=\"form-group\"\n                            [ngClass]=\"{'text-danger':!_Form_Login.controls['username'].valid && _Form_Login.controls['username'].touched}\">\n                            <label>{{ 'Login.UserName' | translate }}</label>\n                            <input type=\"text\" name=\"username\" maxlength=\"256\" class=\"form-control border-radius: 0.25rem\"\n                                [formControl]=\"_Form_Login.controls['username']\">\n                            <!-- <label class=\"block text-xs _600 text-danger\" for=\"username\"\n                                *ngIf=\"_Form_Login.controls['username'].hasError('required') && _Form_Login.controls['username'].touched\">{{\n                                        'Login.UserName_ER_Required' | translate }}</label> -->\n                        </div>\n                        <div class=\"form-group\"\n                            [ngClass]=\"{'text-danger':!_Form_Login.controls['password'].valid && _Form_Login.controls['password'].touched}\">\n                            <div class=\"d-flex justify-content-between mg-b-5\">\n                                <label class=\"mg-b-0-f\">{{ 'Login.Password' | translate }}</label>\n                                <a class=\"tx-13\" routerLink=\"{{_HelperService.AppConfig.Pages.System.ForgotPassword}}\">\n                                    Forgot Password?\n                                </a>\n                            </div>\n                            <input type=\"password\" name=\"password\" maxlength=\"50\" class=\"form-control   \"\n                                [formControl]=\"_Form_Login.controls['password']\">\n                        </div>\n                        \n                        <div>\n                            <!-- <label class=\"text-danger text-xs _600\" for=\"password\"\n                                *ngIf=\"_Form_Login.controls['password'].hasError('required') && _Form_Login.controls['password'].touched\">{{\n                                    'Login.Password_ER_Required' | translate }}</label> -->\n                        </div>\n                        <button type=\"submit\" class=\"btn btn-brand-02 btn-block\"\n                            [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                            <span *ngIf=\"!_Form_Login_Processing\">\n                                {{ 'Login.LoginBtn' | translate }}\n                            </span>\n                            <span *ngIf=\"_Form_Login_Processing\">\n                                <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                {{ 'Common.PleaseWait' | translate }}...\n                            </span>\n                        </button>\n                        <div class=\"row\">\n                            <div class=\" text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-5 text-muted\">\n                                © Copyright <a target=\"_blank\" class=\"text-primary\"\n                                    href=\"http://connectedanalytics.xyz\">Connected\n                                    Analytics Inc.</a> 2020 |\n                                All Right\n                                Reserved\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/service */ "./src/app/service/service.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(_FormBuilder, _TranslateService, _Router, _HelperService) {
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this._Form_Login_Processing = false;
        this._Form_Login = _FormBuilder.group({
            'username': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            'password': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    };
    LoginComponent.prototype._Form_Login_Process = function (value) {
        var _this = this;
        this._Form_Login_Processing = true;
        var pData = {
            Task: 'login',
            UserName: value.username,
            Password: value.password,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(function (_Response) {
            _this._Form_Login_Processing = false;
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.Account, _Response.Result);
                var _StorageReqH = _this._HelperService.GetStorage(_this._HelperService.AppConfig.Storage.OReqH);
                _StorageReqH.hcuak = _Response.Result['AccessKey'];
                _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                _this._HelperService.NotifySuccess(_Response.Message);
                _this._Form_Login.reset();
                if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.PosAccount) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.PTSP;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Merchant) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.MerchantSubAccount) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Acquirer) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Acquirer;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.RelationshipManager) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Rm;
                }
                else if (_Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Store) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Store;
                }
                else if ((_this._HelperService.AppConfig.Host == "console.thankucash.com" || _this._HelperService.AppConfig.Host == "testconsole.thankucash.com" || _this._HelperService.AppConfig.Host == "betaconsole.thankucash.com") && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Admin) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else if ((_this._HelperService.AppConfig.Host == "console.thankucash.com" || _this._HelperService.AppConfig.Host == "testconsole.thankucash.com" || _this._HelperService.AppConfig.Host == "betaconsole.thankucash.com") && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Controller) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else if (_this._HelperService.AppConfig.Host == "localhost:4200" && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Controller) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else if (_this._HelperService.AppConfig.Host == "localhost:4200" && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Admin) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else if (_this._HelperService.AppConfig.Host == "localhost:4201" && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Controller) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else if (_this._HelperService.AppConfig.Host == "localhost:4201" && _Response.Result.UserAccount.AccountTypeCode == _this._HelperService.AppConfig.AccountType.Admin) {
                    window.location.href = _this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Console;
                }
                else {
                    _this._HelperService.NotifyError('Invalid account. Please contact support');
                }
            }
            else {
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._Form_Login_Processing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/onboarding/register/register.component.html":
/*!******************************************************************!*\
  !*** ./src/app/auth/onboarding/register/register.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" h-v d-flex flex align-items-center white \" style=\"  display: block; \nposition: relative;\noverflow: hidden;\">\n    <!-- <div class=\"auroral-agrabah\"></div>\n    <div class=\"auroral-stars\"></div> -->\n    <div class=\"mx-auto w-xl w-auto-xs animate fadeIn\" style=\"width: 70%;\">\n        <div class=\"row\">\n            <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12\">\n                <div class=\"mb-3\" *ngIf=\"_HelperService.AppConfig.Host == 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/interswitch.png\" class=\"w-xmd\">\n                </div>\n                <div class=\"mb-3\" *ngIf=\"_HelperService.AppConfig.Host != 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/logoinverse.png\" class=\"w-xmd\">\n                </div>\n                <div class=\"mb-4\">\n                    <h4 class=\"_400\">Register for your Account</h4>\n                    <div>Already have an account? <span class=\"text-uppercase _600 text-primary\">\n                            <a routerLink=\"{{_HelperService.AppConfig.Pages.System.Login}}\">\n                                LOGIN\n                            </a>\n                        </span></div>\n                </div>\n                <form [formGroup]=\"Form_AddUser\" (ngSubmit)=\"Form_AddUser_Process(Form_AddUser.value)\" role=\"form\">\n                    <div class=\"md-form-group\"\n                        [ngClass]=\"{'has-error':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                        <input type=\"text\" required name=\"Name\" maxlength=\"256\" class=\"md-input\"\n                            [formControl]=\"Form_AddUser.controls['Name']\"\n                            [ngClass]=\"{'is-invalid':!Form_AddUser.controls['Name'].valid && Form_AddUser.controls['Name'].touched}\">\n                        <label class=\"block w-100\">Enter your business name</label>\n                        <div>\n                            <label class=\"text-danger text-xs _600\" for=\"Name\"\n                                *ngIf=\"Form_AddUser.controls['Name'].hasError('required') && Form_AddUser.controls['Name'].touched\">Enter\n                                your business name</label>\n                            <label class=\"text-danger text-xs _600\" for=\"DisplayName\"\n                                *ngIf=\"Form_AddUser.controls['Name'].hasError('minlength') || Form_AddUser.controls['Name'].hasError('maxlength')\">Enter\n                                your valid business name</label>\n                        </div>\n                    </div>\n                    <div class=\"md-form-group\"\n                        [ngClass]=\"{'has-error':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                        <input type=\"text\" required name=\"EmailAddress\" maxlength=\"256\" class=\"md-input\"\n                            [formControl]=\"Form_AddUser.controls['EmailAddress']\"\n                            [ngClass]=\"{'is-invalid':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                        <label class=\"block w-100\">Enter email address</label>\n                        <div>\n                            <label class=\"text-danger text-xs _600\" for=\"EmailAddress\"\n                                *ngIf=\"Form_AddUser.controls['EmailAddress'].hasError('required') && Form_AddUser.controls['EmailAddress'].touched\">{{ 'System.AddMerchant.EmailAddress_ER_Required' | translate }}</label>\n\n                            <label class=\"text-danger text-xs _600\" for=\"EmailAddress\"\n                                *ngIf=\"Form_AddUser.controls['EmailAddress'].hasError('email') && Form_AddUser.controls['EmailAddress'].touched\">{{ 'System.AddMerchant.EmailAddress_ER_Invalid' | translate }}</label>\n                        </div>\n\n                    </div>\n                    <div class=\"md-form-group\"\n                        [ngClass]=\"{'has-error':!Form_AddUser.controls['MobileNumber'].valid && Form_AddUser.controls['MobileNumber'].touched}\">\n                        <input type=\"text\" name=\"MobileNumber\" minlength=\"10\" maxlength=\"14\" class=\"md-input\"\n                            (keypress)=\"_HelperService.PreventText($event)\" value=\"234\"\n                            [formControl]=\"Form_AddUser.controls['MobileNumber']\"\n                            [ngClass]=\"{'is-invalid':!Form_AddUser.controls['MobileNumber'].valid && Form_AddUser.controls['MobileNumber'].touched}\">\n                        <label class=\"block w-100\">Enter mobile number</label>\n                        <div>\n                            <label class=\"text-danger text-xs _600\" for=\"MobileNumber\"\n                                *ngIf=\"Form_AddUser.controls['MobileNumber'].hasError('required') && Form_AddUser.controls['MobileNumber'].touched\">{{ 'System.AddMerchant.MobileNumber_ER_Required' | translate }}</label>\n                            <label class=\"text-danger text-xs _600\" for=\"MobileNumber\"\n                                *ngIf=\"Form_AddUser.controls['MobileNumber'].hasError('minlength') || Form_AddUser.controls['MobileNumber'].hasError('maxlength')\">{{  'System.AddMerchant.MobileNumber_ER_Length' | translate }}</label>\n                        </div>\n                    </div>\n                    <div class=\"md-form-group\"\n                        [ngClass]=\"{'has-error':!Form_AddUser.controls['Password'].valid && Form_AddUser.controls['Password'].touched}\">\n                        <input type=\"password\" name=\"Password\" maxlength=\"20\" class=\"md-input\"\n                            [formControl]=\"Form_AddUser.controls['Password']\"\n                            [ngClass]=\"{ 'is-invalid': !Form_AddUser.controls['Password'].valid && Form_AddUser.controls['Password'].touched}\" />\n                        <label class=\"block w-100\">Create your password</label>\n                        <div>\n                            <label class=\"text-danger text-xs _600\" for=\"Password\"\n                                *ngIf=\" Form_AddUser.controls['Password'].hasError('required') &&  Form_AddUser.controls['Password'].touched\">{{ \"System.Users.Password_ER_Required\" | translate }}</label>\n                            <label class=\"text-danger text-xs _600\" for=\"Password\"\n                                *ngIf=\"  Form_AddUser.controls['Password'].hasError('pattern') ||   Form_AddUser.controls['Password'].hasError('minlength') ||  Form_AddUser.controls['Password'].hasError('maxlength') \">{{ \"System.Users.Password_ER_Length\" | translate }}</label>\n                        </div>\n                    </div>\n                    <div class=\"mb-3\">\n                        <button type=\"submit\" class=\"btn btn-block purple\"\n                            [disabled]=\"!Form_AddUser.valid || Form_AddUser_Processing\">\n                            <span *ngIf=\"!Form_AddUser_Processing\">\n                                Register\n                            </span>\n                            <span *ngIf=\"Form_AddUser_Processing\">\n                                <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                I'm creating your account...\n                            </span>\n                        </button>\n                        <div class=\"mt-2 text-xs text-muted\">\n                            By clicking the “Register” button, you agree to ThankUCash’s terms of acceptable use\n                        </div>\n                    </div>\n                </form>\n            </div>\n            <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 hidden-xs pt-5 pl-5\">\n                <img src=\"../../../../assets/img/onboarding_merchant.png\" class=\"img-fluid\">\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/onboarding/register/register.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/auth/onboarding/register/register.component.ts ***!
  \****************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/service */ "./src/app/service/service.ts");






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_FormBuilder, _TranslateService, _Router, _HelperService) {
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this.Form_AddUser_Processing = false;
        this.Form_AddUser = _FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.Register,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            SubOwnerKey: null,
            BankKey: null,
            Password: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this._HelperService.AppConfig.ValidatorRegex.Password)
                ])
            ],
            // DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)])],
            // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            EmailAddress: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2)])],
            MobileNumber: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(14)])],
            // ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            // SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            // WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            // Description: null,
            // Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            CountValue: 0,
            AverageValue: 0,
            ApplicationStatusCode: null,
            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,
            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,
            BusinessCategories: null,
            AccountLevelCode: 'accountlevel.ca',
            SubscriptionKey: 'merchantbasic',
            AccountPercentage: 0,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Owners: [],
            Configuration: [],
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    };
    RegisterComponent.prototype.Form_AddUser_Process = function (_FormValue) {
        var _this = this;
        _FormValue.DisplayName = _FormValue.Name;
        this.Form_AddUser_Processing = true;
        _FormValue.Configurations = [
            {
                SystemName: 'rewardpercentage',
                Value: 3,
            },
            {
                SystemName: 'rewardmaxinvoiceamount',
                Value: '0',
            },
            {
                SystemName: 'rewarddeductiontype',
                Value: 'Prepay',
                HelperCode: 'rewarddeductiontype.prepay',
            },
            {
                SystemName: 'maximumrewarddeductionamount',
                Value: '0',
            },
            {
                SystemName: 'thankucashplus',
                Value: '0',
            },
            {
                SystemName: 'thankucashplusrewardcriteria',
                HelperCode: 'rewardcriteriatype.mininvoice',
                Value: '2000',
            },
            {
                SystemName: 'thankucashplusmintransferamount',
                Value: '2000',
            },
            {
                SystemName: 'settlementScheduletype',
                HelperCode: 'settlementSchedule.daily',
                Value: 'Daily',
            },
            {
                SystemName: 'minimumsettlementamount',
                Value: '1',
            },
        ];
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(function (_ResponseI) {
            _this.Form_AddUser_Processing = false;
            if (_ResponseI.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.NotifySuccess(_ResponseI.Message);
                var pData = {
                    Task: 'login',
                    UserName: _FormValue.EmailAddress,
                    Password: _FormValue.Password,
                };
                var _OResponse_1;
                _OResponse_1 = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse_1.subscribe(function (_Response) {
                    if (_Response.Status == _this._HelperService.StatusSuccess) {
                        _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.Account, _Response.Result);
                        var _StorageReqH = _this._HelperService.GetStorage(_this._HelperService.AppConfig.Storage.OReqH);
                        _StorageReqH.hcuak = _Response.Result['AccessKey'];
                        _StorageReqH.hcupk = btoa(_Response.Result['PublicKey']);
                        _this._HelperService.SaveStorage(_this._HelperService.AppConfig.Storage.OReqH, _StorageReqH);
                        _this._HelperService.RefreshHelper();
                        var PostData = {
                            Task: "creditmerchantwallet",
                            AccountId: _Response.Result.UserAccount.AccountId,
                            InvoiceAmount: 10000,
                            ReferenceNumber: "ONBOARDINGCREDIT",
                        };
                        var _OResponse_2;
                        _OResponse_2 = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V2.ThankU, PostData);
                        _OResponse_2.subscribe(function (_Response) {
                            debugger;
                            _this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == _this._HelperService.StatusSuccess) {
                                _this.Form_AddUser.reset();
                                window.location.href = _this._HelperService.AppConfig.Pages.System.MerchantSetup;
                            }
                            else {
                                _this._HelperService.NotifySuccess(_Response.Message);
                            }
                        }, function (_Error) {
                            debugger;
                            _this._HelperService.IsFormProcessing = false;
                            _this._HelperService.HandleException(_Error);
                        });
                    }
                    else {
                        _this._HelperService.NotifyError(_Response.Message);
                    }
                }, function (_Error) {
                    _this._HelperService.HandleException(_Error);
                });
            }
            else {
                _this._HelperService.NotifyError(_ResponseI.Message);
            }
        }, function (_Error) {
            _this.Form_AddUser_Processing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/auth/onboarding/register/register.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/auth/onboarding/registercomplete/registercomplete.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/auth/onboarding/registercomplete/registercomplete.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" h-v d-flex flex align-items-center white \" style=\"  display: block; \nposition: relative;\noverflow: hidden;\">\n    <!-- <div class=\"auroral-agrabah\"></div>\n    <div class=\"auroral-stars\"></div> -->\n    <div class=\"mx-auto w-xl w-auto-xs animate fadeIn pb-5\" style=\"width: 70%;\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 \">\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host == 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/interswitch.png\" class=\"w-xmd\">\n                </div>\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host != 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/logoinverse.png\" class=\"w-xmd\">\n                </div>\n            </div>\n            <div class=\"col-lg-12\">\n                <ul class=\"nav nav-tabs process-model more-icon-preocess\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"discover\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>About Business</p>\n                        </a></li>\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"strategy\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Business Location</p>\n                        </a></li>\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"optimization\" role=\"tab\"\n                            data-toggle=\"tab\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Credit First Rewards</p>\n                        </a></li>\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"reporting\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>All Set</p>\n                        </a></li>\n                </ul>\n            </div>\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 b-a r-2x pb-3\">\n                <div class=\"row\">\n                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center mt-4  mb-4\">\n                        <h4 class=\"_400\">Welcome , {{_HelperService.UserAccount.DisplayName}}</h4>\n                        <div class=\"text-muted\">\n                            Your account is all ready, Our customer representative will reach you soon to help you\n                        </div>\n                    </div>\n                    <div class=\"text-center full-width mb-3\">\n                        <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/nZeC4TZigZ8\" frameborder=\"0\"\n                            allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\n                            allowfullscreen style=\"border-radius: 10px;border: 2px solid #9c26b0;\"></iframe>\n                    </div>\n                    <div class=\"col-12 text-center\">\n                        <button type=\"button\" (click)=\"NavigateToDashboard()\" class=\"btn btn-lg purple text-lg\"\n                            [disabled]=\"Form_AddUser_Processing\" style=\"width: 50%;\">\n                            Access Dashboard\n                        </button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/onboarding/registercomplete/registercomplete.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/auth/onboarding/registercomplete/registercomplete.component.ts ***!
  \********************************************************************************/
/*! exports provided: RegisterCompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterCompleteComponent", function() { return RegisterCompleteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");








var RegisterCompleteComponent = /** @class */ (function () {
    function RegisterCompleteComponent(_sanitizer, _Http, _FormBuilder, _TranslateService, _Router, _HelperService) {
        this._sanitizer = _sanitizer;
        this._Http = _Http;
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this.safeURL = null;
        this.IsMobileNumber1 = true;
        this.MobileNumber1 = null;
        this.MobileNumber1Message = null;
        this.MobileNumber2 = null;
        this.MobileNumber3 = null;
        this.Form_AddUser_Processing = false;
        // this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl();
    }
    RegisterCompleteComponent.prototype.ngOnInit = function () {
    };
    RegisterCompleteComponent.prototype.NavigateToDashboard = function () {
        window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;
    };
    RegisterCompleteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'registercomplete',
            template: __webpack_require__(/*! ./registercomplete.component.html */ "./src/app/auth/onboarding/registercomplete/registercomplete.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["DomSanitizer"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], RegisterCompleteComponent);
    return RegisterCompleteComponent;
}());



/***/ }),

/***/ "./src/app/auth/onboarding/registerreward/registerreward.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/auth/onboarding/registerreward/registerreward.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" h-v d-flex flex align-items-center white \" style=\"  display: block; \nposition: relative;\noverflow: hidden;\">\n    <!-- <div class=\"auroral-agrabah\"></div>\n    <div class=\"auroral-stars\"></div> -->\n    <div class=\"mx-auto w-xl w-auto-xs animate fadeIn pb-5\" style=\"width: 70%;\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 \">\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host == 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/interswitch.png\" class=\"w-xmd\">\n                </div>\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host != 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/logoinverse.png\" class=\"w-xmd\">\n                </div>\n            </div>\n            <div class=\"col-lg-12\">\n                <ul class=\"nav nav-tabs process-model more-icon-preocess\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"discover\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>About Business</p>\n                        </a></li>\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"strategy\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Business Location</p>\n                        </a></li>\n                    <li role=\"presentation\"><a aria-controls=\"optimization\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Credit First Rewards</p>\n                        </a></li>\n                    <li role=\"presentation\"><a aria-controls=\"reporting\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>All Set</p>\n                        </a></li>\n                </ul>\n            </div>\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 b-a r-2x pb-3\">\n                <div class=\"row\">\n                    <!-- <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center mt-4 \">\n                       \n                    </div> -->\n                    <div class=\"col-12\">\n                        <div class=\"m-4\">\n                            <div class=\"row\">\n                                <div class=\"col-lg-5 col-md-5 col-sm-12 col-xs-12\">\n                                    <h4 class=\"_400 text-center\">Let's reward your Customers</h4>\n                                    <div class=\"text-muted text-center\">\n                                        We will reward your any 3 customers to show you how it works\n                                    </div>\n                                    <div class=\" pt-2 pb-4 text-purple _600  text-center\">\n                                        {{Attempts}}/3 rewards remaining\n                                    </div>\n                                    <div class=\"md-form-group\">\n                                        <input type=\"text\" name=\"MobileNumber\" minlength=\"10\" maxlength=\"14\"\n                                            class=\"md-input\" (keypress)=\"_HelperService.PreventText($event)\"\n                                            [(ngModel)]=\"MobileNumber1\">\n                                        <label class=\"block w-100\">Enter mobile number to credit reward</label>\n                                        <div class=\"text-purple _500 mt-2\"> {{MobileNumber1Message}}</div>\n                                    </div>\n                                    <div class=\"mb-3\">\n                                        <button type=\"button\" (click)=\"RewardCustomer1()\"\n                                            class=\"btn btn-block purple mb-3\" [disabled]=\"Form_AddUser_Processing\">\n                                            <span *ngIf=\"!Form_AddUser_Processing\">\n                                                Reward Customer\n                                            </span>\n                                            <span *ngIf=\"Form_AddUser_Processing\">\n                                                <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                                Crediting reward...\n                                            </span>\n                                        </button>\n                                        <button type=\"button\" (click)=\"Skip()\"\n                                            class=\"btn btn-block btn-outline  b-success\">\n                                            Skip\n                                        </button>\n                                    </div>\n                                </div>\n                                <div class=\"col-lg-7 col-md-7 col-sm-12 col-xs-12 hidden-xs pt-5 pl-5\">\n                                    <img src=\"../../../../assets/img/onboarding_merchant.png\" class=\"img-fluid\">\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/onboarding/registerreward/registerreward.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/auth/onboarding/registerreward/registerreward.component.ts ***!
  \****************************************************************************/
/*! exports provided: RegisterRewardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterRewardComponent", function() { return RegisterRewardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");










var RegisterRewardComponent = /** @class */ (function () {
    function RegisterRewardComponent(_Http, _FormBuilder, _TranslateService, _Router, _HelperService) {
        this._Http = _Http;
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this.IsMobileNumber1 = true;
        this.MobileNumber1 = null;
        this.MobileNumber1Message = null;
        this.MobileNumber2 = null;
        this.MobileNumber3 = null;
        this.Form_AddUser_Processing = false;
        this.Attempts = 0;
    }
    RegisterRewardComponent.prototype.ngOnInit = function () {
        var smsC = this._HelperService.GetStorage(this._HelperService.UserAccount.AccountCode);
        if (smsC != null) {
            this.Attempts = smsC;
        }
    };
    RegisterRewardComponent.prototype.RewardCustomer1 = function () {
        var _this = this;
        var smsC = this._HelperService.GetStorage(this._HelperService.UserAccount.AccountCode);
        if (smsC != null) {
            this.Attempts = smsC;
        }
        if (this.Attempts < 3) {
            if (this.MobileNumber1 == undefined && this.MobileNumber1 == null && this.MobileNumber1 == '') {
                this._HelperService.NotifyError("Please enter mobile number 1");
            }
            else {
                this.Form_AddUser_Processing = true;
                this.MobileNumber1Message = "Sending request...";
                this._HelperService.IsFormProcessing = true;
                var pData1 = {
                    MerchantId: this._HelperService.UserAccount.AccountCode,
                };
                var _OResponseConnect = void 0;
                _OResponseConnect = this.ConnectMerchant(pData1);
                _OResponseConnect.subscribe(function (_ResponseConnect) {
                    _this._HelperService.IsFormProcessing = false;
                    console.log(_ResponseConnect);
                    if (_ResponseConnect.Status == _this._HelperService.StatusSuccess) {
                        _this.MobileNumber1Message = "Validating mobile number...";
                        var pDataGetBalance = {
                            MerchantId: _this._HelperService.UserAccount.AccountCode,
                            ReferenceNumber: _this.MobileNumber1 + " " + _this._HelperService.UserAccount.AccountCode,
                            MobileNumber: _this.MobileNumber1,
                        };
                        var _OResponseGetBalance = void 0;
                        _OResponseGetBalance = _this.GetBalance(pDataGetBalance);
                        _OResponseGetBalance.subscribe(function (_ResponseGetBalance) {
                            if (_ResponseGetBalance.Status == _this._HelperService.StatusSuccess) {
                                console.log(_ResponseGetBalance);
                                if (_ResponseGetBalance.Result != undefined && _ResponseGetBalance.Result != null && _ResponseGetBalance.Result.Balance != undefined) {
                                    _this.Form_AddUser_Processing = false;
                                    _this.MobileNumber1Message = "Mobile number already registered with ThankUCash. Please use other number ";
                                    _this._HelperService.NotifyError("Mobile number already registered with ThankUCash. Please use other number");
                                }
                                else {
                                    _this.MobileNumber1Message = "Rewarding mobile number ...";
                                    var pData1 = {
                                        MerchantId: _this._HelperService.UserAccount.AccountCode,
                                        ReferenceNumber: _this.MobileNumber1 + "_" + _this._HelperService.UserAccount.AccountCode,
                                        MobileNumber: _this.MobileNumber1,
                                        InvoiceAmount: 100000,
                                        TransactionMode: "cash",
                                    };
                                    var _OResponse = void 0;
                                    _OResponse = _this.PostReward(pData1);
                                    _OResponse.subscribe(function (_Response) {
                                        if (_Response.Status == _this._HelperService.StatusSuccess) {
                                            console.log(_Response);
                                            var smsC = _this._HelperService.GetStorage(_this._HelperService.UserAccount.AccountCode);
                                            if (smsC != null) {
                                                _this.Attempts = smsC + 1;
                                                _this._HelperService.SaveStorage(_this._HelperService.UserAccount.AccountCode, smsC + 1);
                                                if (_this.Attempts == 3) {
                                                    window.location.href = _this._HelperService.AppConfig.Pages.System.MerchantSetupComplete;
                                                }
                                            }
                                            else {
                                                _this.Attempts = 1;
                                                _this._HelperService.SaveStorage(_this._HelperService.UserAccount.AccountCode, 1);
                                            }
                                            _this.MobileNumber1 = "";
                                            _this.Form_AddUser_Processing = false;
                                            _this.MobileNumber1Message = "Customer rewarded with N" + _Response.Result.RewardAmount / 100 + " for the purchase of N" + _Response.Result.InvoiceAmount / 100 + " successfully";
                                            _this._HelperService.NotifySuccess("Customer rewarded with N" + _Response.Result.RewardAmount / 100 + " for the purchase of N" + _Response.Result.InvoiceAmount / 100 + " successfully");
                                        }
                                        else {
                                            _this.Form_AddUser_Processing = false;
                                            _this.MobileNumber1Message = _Response.Message;
                                            _this._HelperService.NotifyError(_Response.Message);
                                        }
                                    }, function (_Error) {
                                        _this.Form_AddUser_Processing = false;
                                        console.log(_Error);
                                        //this._HelperService.HandleException(_Error);
                                    });
                                }
                            }
                            else {
                                _this.Form_AddUser_Processing = false;
                                _this._HelperService.NotifyError(_ResponseGetBalance.Message);
                            }
                        }, function (_Error) {
                            _this.Form_AddUser_Processing = false;
                            console.log(_Error);
                        });
                    }
                    else {
                        _this.Form_AddUser_Processing = false;
                        _this.MobileNumber1Message = "Unable to validate your account";
                        _this._HelperService.NotifyError(_ResponseConnect.Message);
                    }
                }, function (_ErrorConnect) {
                    console.log(_ErrorConnect);
                    _this.Form_AddUser_Processing = false;
                    //this._HelperService.HandleException(_Error);
                });
            }
        }
        else {
            this._HelperService.NotifyError("You have used all test rewards.");
        }
    };
    RegisterRewardComponent.prototype.Skip = function () {
        window.location.href = this._HelperService.AppConfig.Pages.System.MerchantSetupComplete;
    };
    RegisterRewardComponent.prototype.ConnectMerchant = function (Data) {
        var Url = "https://testapi.thankucash.com/api/v1/thankuconnect/connectaccount";
        var Headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "NTBlYWE2ZDY3YzUzMjUzYWM0ZjZjNzNkM2QzOGQ2Yjc2ZTM2N2Q2ZQ=="
        };
        var _Headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"](Headers);
        return this._Http
            .post(Url, Data, {
            headers: _Headers
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (_Response) { return JSON.parse(_Response); }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(function (error) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["throwError"])(error); }));
    };
    RegisterRewardComponent.prototype.GetBalance = function (Data) {
        var Url = "https://testapi.thankucash.com/api/v1/thankuconnect/getbalance";
        var Headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "NTBlYWE2ZDY3YzUzMjUzYWM0ZjZjNzNkM2QzOGQ2Yjc2ZTM2N2Q2ZQ=="
        };
        var _Headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"](Headers);
        return this._Http
            .post(Url, Data, {
            headers: _Headers
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (_Response) { return JSON.parse(_Response); }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(function (error) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["throwError"])(error); }));
    };
    RegisterRewardComponent.prototype.PostReward = function (Data) {
        var Url = "https://testapi.thankucash.com/api/v1/thankuconnect/reward";
        var Headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "NTBlYWE2ZDY3YzUzMjUzYWM0ZjZjNzNkM2QzOGQ2Yjc2ZTM2N2Q2ZQ=="
        };
        var _Headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"](Headers);
        return this._Http
            .post(Url, Data, {
            headers: _Headers
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (_Response) { return JSON.parse(_Response); }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["catchError"])(function (error) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["throwError"])(error); }));
    };
    RegisterRewardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'registerreward',
            template: __webpack_require__(/*! ./registerreward.component.html */ "./src/app/auth/onboarding/registerreward/registerreward.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], RegisterRewardComponent);
    return RegisterRewardComponent;
}());



/***/ }),

/***/ "./src/app/auth/onboarding/registerstore/registerstore.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/auth/onboarding/registerstore/registerstore.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" h-v d-flex flex align-items-center white \" style=\"  display: block; \nposition: relative;\noverflow: hidden;\">\n    <!-- <div class=\"auroral-agrabah\"></div>\n    <div class=\"auroral-stars\"></div> -->\n    <div class=\"mx-auto w-xl w-auto-xs animate fadeIn pb-5\" style=\"width: 70%;\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12 \">\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host == 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/interswitch.png\" class=\"w-xmd\">\n                </div>\n                <div class=\"mb-3 mt-5\" *ngIf=\"_HelperService.AppConfig.Host != 'interswitch.thankucash.com'\"><img\n                        src=\"../../../assets/img/logoinverse.png\" class=\"w-xmd\">\n                </div>\n            </div>\n            <div class=\"col-lg-12\">\n                <ul class=\"nav nav-tabs process-model more-icon-preocess\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\"><a aria-controls=\"discover\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>About Business</p>\n                        </a></li>\n                    <li role=\"presentation\"><a aria-controls=\"strategy\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Business Location</p>\n                        </a></li>\n                    <li role=\"presentation\"><a aria-controls=\"optimization\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>Credit First Rewards</p>\n                        </a></li>\n                    <li role=\"presentation\"><a aria-controls=\"reporting\" role=\"tab\" data-toggle=\"tab\"><i\n                                class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                            <p>All Set</p>\n                        </a></li>\n                </ul>\n            </div>\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 b-a r-2x pb-3\">\n                <div class=\"row\">\n                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center mt-4 \">\n                        <h4 class=\"_400\">Let's add your Business Location</h4>\n                        <div class=\"text-muted\">\n                            You can add and manage locations from account panel\n                        </div>\n                    </div>\n                    <div class=\"col-12\">\n                        <form [formGroup]=\"Form_AddUser\" (ngSubmit)=\"Form_AddUser_Process(Form_AddUser.value)\"\n                            role=\"form\">\n                            <div class=\"m-4\">\n                                <div class=\"row\">\n                                    <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\n                                        <div class=\"md-form-group\"\n                                            [ngClass]=\"{'has-error':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                                            <input type=\"text\" required name=\"Name\" maxlength=\"256\" class=\"md-input\"\n                                                [formControl]=\"Form_AddUser.controls['Name']\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['Name'].valid && Form_AddUser.controls['Name'].touched}\">\n                                            <label class=\"block w-100\">Enter business name</label>\n                                            <div>\n                                                <label class=\"text-danger text-xs _600\" for=\"Name\"\n                                                    *ngIf=\"Form_AddUser.controls['Name'].hasError('required') && Form_AddUser.controls['Name'].touched\">Enter\n                                                    your business name</label>\n                                                <label class=\"text-danger text-xs _600\" for=\"DisplayName\"\n                                                    *ngIf=\"Form_AddUser.controls['Name'].hasError('minlength') || Form_AddUser.controls['Name'].hasError('maxlength')\">Enter\n                                                    your valid business name</label>\n                                            </div>\n                                        </div>\n                                        <div class=\"md-form-group\"\n                                            [ngClass]=\"{'has-error':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                                            <input type=\"text\" required name=\"EmailAddress\" maxlength=\"256\"\n                                                class=\"md-input\" [formControl]=\"Form_AddUser.controls['EmailAddress']\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                                            <label class=\"block w-100\">Enter business email address</label>\n                                            <div>\n                                                <label class=\"text-danger text-xs _600\" for=\"EmailAddress\"\n                                                    *ngIf=\"Form_AddUser.controls['EmailAddress'].hasError('required') && Form_AddUser.controls['EmailAddress'].touched\">{{ 'System.AddMerchant.EmailAddress_ER_Required' | translate }}</label>\n\n                                                <label class=\"text-danger text-xs _600\" for=\"EmailAddress\"\n                                                    *ngIf=\"Form_AddUser.controls['EmailAddress'].hasError('email') && Form_AddUser.controls['EmailAddress'].touched\">{{ 'System.AddMerchant.EmailAddress_ER_Invalid' | translate }}</label>\n                                            </div>\n\n                                        </div>\n                                        <div class=\"md-form-group\"\n                                            [ngClass]=\"{'has-error':!Form_AddUser.controls['MobileNumber'].valid && Form_AddUser.controls['MobileNumber'].touched}\">\n                                            <input type=\"text\" name=\"MobileNumber\" minlength=\"10\" maxlength=\"14\"\n                                                class=\"md-input\" (keypress)=\"_HelperService.PreventText($event)\"\n                                                value=\"234\" [formControl]=\"Form_AddUser.controls['MobileNumber']\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['MobileNumber'].valid && Form_AddUser.controls['MobileNumber'].touched}\">\n                                            <label class=\"block w-100\">Enter business mobile number</label>\n                                            <div>\n                                                <label class=\"text-danger text-xs _600\" for=\"MobileNumber\"\n                                                    *ngIf=\"Form_AddUser.controls['MobileNumber'].hasError('required') && Form_AddUser.controls['MobileNumber'].touched\">{{ 'System.AddMerchant.MobileNumber_ER_Required' | translate }}</label>\n                                                <label class=\"text-danger text-xs _600\" for=\"MobileNumber\"\n                                                    *ngIf=\"Form_AddUser.controls['MobileNumber'].hasError('minlength') || Form_AddUser.controls['MobileNumber'].hasError('maxlength')\">{{  'System.AddMerchant.MobileNumber_ER_Length' | translate }}</label>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"md-form-group\">\n                                            <input type=\"text\" name=\"WebsiteUrl\" maxlength=\"1024\" class=\"md-input\"\n                                                placeholder=\"(optional)\"\n                                                [formControl]=\"Form_AddUser.controls['WebsiteUrl']\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['WebsiteUrl'].valid && Form_AddUser.controls['WebsiteUrl'].touched}\">\n                                            <label class=\"block w-100\">Website link of your business</label>\n                                            <div>\n                                                <label class=\"text-danger\" for=\"WebsiteUrl\"\n                                                    *ngIf=\"Form_AddUser.controls['WebsiteUrl'].hasError('required') && Form_AddUser.controls['WebsiteUrl'].touched\">{{ 'System.AddMerchant.WebsiteUrl_ER_Required' | translate }}</label>\n                                                <label class=\"text-danger\" for=\"WebsiteUrl\"\n                                                    *ngIf=\"Form_AddUser.controls['WebsiteUrl'].hasError('pattern') && Form_AddUser.controls['WebsiteUrl'].touched\">{{ 'System.AddMerchant.WebsiteUrl_ER_Invalid' | translate }}</label>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <div> <label class=\"text-muted text-xs\"> Your business type is\n                                                </label>\n                                            </div>\n                                            <div class=\"radio float-left mr-3\">\n                                                <label class=\"ui-check\"><input type=\"radio\"\n                                                        name=\"accountoperationtypecode\"\n                                                        [value]=\"_HelperService.AppConfig.AccountOperationType.Offline\"\n                                                        [formControl]=\"Form_AddUser.controls['AccountOperationTypeCode']\">\n                                                    <i class=\"dark-white\"></i>\n                                                    Offline (store)</label>\n                                            </div>\n                                            <div class=\"radio  float-left  mr-3\"><label class=\"ui-check\"><input\n                                                        type=\"radio\" name=\"accountoperationtypecode\"\n                                                        [value]=\"_HelperService.AppConfig.AccountOperationType.Online\"\n                                                        [formControl]=\"Form_AddUser.controls['AccountOperationTypeCode']\">\n                                                    <i class=\"dark-white\"></i>\n                                                    Online (Website , Portal) </label></div>\n\n                                            <div class=\"radio    mr-3\"><label class=\"ui-check\"><input type=\"radio\"\n                                                        name=\"accountoperationtypecode\"\n                                                        [value]=\"_HelperService.AppConfig.AccountOperationType.OnlineAndOffline\"\n                                                        [formControl]=\"Form_AddUser.controls['AccountOperationTypeCode']\">\n                                                    <i class=\"dark-white\"></i>\n                                                    Both</label></div>\n                                        </div>\n                                        <div class=\"md-form-group\"\n                                            [ngClass]=\"{'has-error':!Form_AddUser.controls['EmailAddress'].valid && Form_AddUser.controls['EmailAddress'].touched}\">\n                                            <textarea required name=\"Description\" maxlength=\"256\" rows=\"3\"\n                                                class=\"md-input\" [formControl]=\"Form_AddUser.controls['Description']\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['Description'].valid && Form_AddUser.controls['Description'].touched}\"></textarea>\n                                            <label class=\"block w-100\">Tell us about your business</label>\n                                            <div>\n                                                <label class=\"text-danger text-xs _600\" for=\"Description\"\n                                                    *ngIf=\"Form_AddUser.controls['Description'].hasError('required') && Form_AddUser.controls['Description'].touched\">Enter\n                                                    your business information</label>\n                                                <label class=\"text-danger text-xs _600\" for=\"Description\"\n                                                    *ngIf=\"Form_AddUser.controls['Description'].hasError('minlength') || Form_AddUser.controls['Description'].hasError('maxlength')\">Enter\n                                                    valid business information</label>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\n                                        <div class=\"md-form-group\"\n                                            [ngClass]=\"{'has-error':!Form_AddUser.controls['Address'].valid && Form_AddUser.controls['Address'].touched}\">\n                                            <textarea required name=\"Address\" maxlength=\"256\" class=\"md-input\"\n                                                [formControl]=\"Form_AddUser.controls['Address']\" rows=\"2\"\n                                                [ngClass]=\"{'is-invalid':!Form_AddUser.controls['Address'].valid && Form_AddUser.controls['Address'].touched}\"></textarea>\n                                            <label class=\"block w-100\">Enter business address</label>\n                                            <div>\n                                                <label class=\"text-danger text-xs _600\" for=\"Address\"\n                                                    *ngIf=\"Form_AddUser.controls['Address'].hasError('required') && Form_AddUser.controls['Address'].touched\">Enter\n                                                    your business address</label>\n                                                <label class=\"text-danger text-xs _600\" for=\"Address\"\n                                                    *ngIf=\"Form_AddUser.controls['Address'].hasError('minlength') || Form_AddUser.controls['Address'].hasError('maxlength')\">Enter\n                                                    your valid business address</label>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"md-form-group\">\n                                            <input type=\"text\" name=\"GAddress\" maxlength=\"256\" class=\"md-input\"\n                                                ngx-google-places-autocomplete #places=\"ngx-places\"\n                                                (onAddressChange)=\"Form_AddUser_AddressChange($event)\">\n                                            <label class=\"block w-100\">Locate business address</label>\n                                            <div>\n                                                <label class=\"text-warn\">You can click on map to mark exact location\n                                                    of store </label>\n                                            </div>\n                                        </div>\n                                        <div class=\"mb-3\">\n                                            <label class=\"block w-100 text-muted text-xs\">Click on map to mark exact\n                                                location\n                                                of store</label>\n                                            <agm-map style=\"height:160px;\" [zoom]=\"12\"\n                                                [latitude]=\"Form_AddUser_Latitude\" [longitude]=\"Form_AddUser_Longitude\"\n                                                (mapClick)=\"Form_AddUser_PlaceMarkerClick($event)\">\n                                                <agm-marker [latitude]=\"Form_AddUser_Latitude\"\n                                                    [longitude]=\"Form_AddUser_Longitude\">\n                                                </agm-marker>\n                                            </agm-map>\n                                        </div>\n                                        <div class=\"mb-3\">\n                                            <button type=\"submit\" class=\"btn btn-block purple\"\n                                                [disabled]=\"!Form_AddUser.valid || Form_AddUser_Processing\">\n                                                <span *ngIf=\"!Form_AddUser_Processing\">\n                                                    Add Details\n                                                </span>\n                                                <span *ngIf=\"Form_AddUser_Processing\">\n                                                    <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                                    {{ 'Common.PleaseWait' | translate }}...\n                                                </span>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/onboarding/registerstore/registerstore.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/auth/onboarding/registerstore/registerstore.component.ts ***!
  \**************************************************************************/
/*! exports provided: RegisterStoreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterStoreComponent", function() { return RegisterStoreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/service */ "./src/app/service/service.ts");






var RegisterStoreComponent = /** @class */ (function () {
    function RegisterStoreComponent(_FormBuilder, _TranslateService, _Router, _HelperService) {
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this.Form_AddUser_Processing = false;
        this.Form_AddUser_Address = null;
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser = _FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.Register,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: this._HelperService.UserAccount.AccountKey,
            SubOwnerKey: null,
            BankKey: null,
            // Password: [
            //     null,
            //     Validators.compose([
            //         Validators.required,
            //         Validators.minLength(8),
            //         Validators.maxLength(20),
            //         Validators.pattern(
            //             this._HelperService.AppConfig.ValidatorRegex.Password
            //         )
            //     ])
            // ],
            // DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [this._HelperService.UserAccount.DisplayName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)])],
            // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            // LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            EmailAddress: [this._HelperService.User.EmailAddress, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2)])],
            MobileNumber: [this._HelperService.User.MobileNumber, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(14)])],
            // ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            // SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            Address: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(256)])],
            CountValue: 0,
            AverageValue: 0,
            ApplicationStatusCode: null,
            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,
            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,
            BusinessCategories: null,
            AccountLevelCode: 'accountlevel.ca',
            SubscriptionKey: 'merchantbasic',
            AccountPercentage: 0,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Owners: [],
            Configuration: [],
        });
    }
    RegisterStoreComponent.prototype.ngOnInit = function () {
    };
    RegisterStoreComponent.prototype.Form_AddUser_Process = function (_FormValue) {
        var _this = this;
        if (this.Form_AddUser_Latitude == 0 || this.Form_AddUser_Longitude == 0) {
            this._HelperService.NotifyError('Mark your business location on map to continue');
        }
        else {
            _FormValue.DisplayName = _FormValue.Name;
            _FormValue.UserName = this._HelperService.GeneratePassoword();
            _FormValue.Password = this._HelperService.GeneratePassoword();
            this.Form_AddUser_Processing = true;
            var _OResponse = void 0;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
            _OResponse.subscribe(function (_Response) {
                _this.Form_AddUser_Processing = false;
                if (_Response.Status == _this._HelperService.StatusSuccess) {
                    window.location.href = _this._HelperService.AppConfig.Pages.System.MerchantRewardSetup;
                }
                else {
                    _this._HelperService.NotifyError(_Response.Message);
                }
            }, function (_Error) {
                _this.Form_AddUser_Processing = false;
                _this._HelperService.HandleException(_Error);
            });
        }
    };
    RegisterStoreComponent.prototype.Form_AddUser_PlaceMarkerClick = function (event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    };
    RegisterStoreComponent.prototype.Form_AddUser_AddressChange = function (address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
    };
    RegisterStoreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'registerstore',
            template: __webpack_require__(/*! ./registerstore.component.html */ "./src/app/auth/onboarding/registerstore/registerstore.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], RegisterStoreComponent);
    return RegisterStoreComponent;
}());



/***/ }),

/***/ "./src/app/auth/resetpassword/resetpassword.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/auth/resetpassword/resetpassword.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content mg-t-50  content-auth\">\n    <div class=\"container\">\n        <div class=\"media align-items-stretch justify-content-center ht-100p pos-relative\">\n            <div class=\"media-body align-items-center d-none d-lg-flex\">\n                <div class=\"mx-wd-600\" style=\"transform: scale(-1,1);\">\n                    <img src=\"../../../assets/img/Merchant.svg\" class=\"img-fluid\" alt=\"\">\n                </div>\n            </div>\n            <div class=\"sign-wrapper mg-lg-l-50 mg-xl-l-60\">\n                <div class=\"wd-100p\">\n                    <img src=\"../../../assets/img/logoinverse.png\" class=\"wd-200 mg-t-15 mb-4\">\n                    <div class=\"\">\n                        <h3 class=\"tx-color-01 mg-b-5\">Reset Password</h3>\n                        <p class=\"tx-color-03 tx-16 mg-b-40\">You can change your default password now</p>\n                        <form [formGroup]=\"_Form_Login\" (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\">\n                            <div class=\"form-group\"\n                                [ngClass]=\"{'text-danger':!_Form_Login.controls['password'].valid && _Form_Login.controls['password'].touched}\">\n                                <div class=\"d-flex justify-content-between \">\n                                    <label class=\"mg-b-0-f\">{{ 'Login.Password' | translate }}</label>\n                                    \n                                </div>\n                                <input type=\"password\" name=\"password\" maxlength=\"50\" class=\"form-control\"\n                                    [formControl]=\"_Form_Login.controls['password']\">\n                            </div>\n                            <div>\n                                <!-- <label class=\"text-danger\" for=\"password\"\n                                    *ngIf=\"_Form_Login.controls['password'].hasError('required') && _Form_Login.controls['password'].touched\">{{\n                                        'Login.Password_ER_Required' | translate }}</label> -->\n                            </div>\n                            \n                            <div class=\"form-group mg-t-15\"\n                                [ngClass]=\"{'text-danger':!_Form_Login.controls['newpassword'].valid && _Form_Login.controls['newpassword'].touched}\">\n                                <div class=\"d-flex justify-content-between\">\n                                    <label class=\"mg-b-0-f\">Re-enter Password</label>\n                                    \n                                </div>\n                                <input type=\"password\" name=\"newpassword\"  required\n                                class=\"form-control\" [formControl]=\"_Form_Login.controls['newpassword']\">\n                                \n                                <div>\n                                    <!-- <label class=\"text-danger\" for=\"newpassword\"\n                                        *ngIf=\"_Form_Login.controls['newpassword'].hasError('required') && _Form_Login.controls['newpassword'].touched\">\n                                        Enter above similarly\n                                    </label> -->\n                                </div>\n                                <div>\n                                    <!-- <label class=\"text-danger\" for=\"newpassword\"\n                                        *ngIf=\"_Form_Login.controls['newpassword'].hasError('password') && _Form_Login.controls['newpassword'].touched\">\n                                        Password must match with above password\n                                    </label> -->\n                                </div>\n                            </div>\n                            <button type=\"submit\" class=\"btn btn-block btn-brand-02\"\n                                [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                                <span *ngIf=\"!_Form_Login_Processing\">\n                                    Reset Password\n                                </span>\n                                <span *ngIf=\"_Form_Login_Processing\">\n                                    <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                    {{ 'Common.PleaseWait' | translate }}...\n                                </span>\n                            </button>\n                           \n                        </form>\n                            <!-- <form [formGroup]=\"_Form_Login\"  (ngSubmit)=\"_Form_Login_Process(_Form_Login.value)\" role=\"form\">\n                            \n                                    <div class=\"form-group\"\n                                        [ngClass]=\"{'text-danger':!_Form_Login.controls['username'].valid && _Form_Login.controls['username'].touched}\">\n                                        <label>{{ 'Login.UserName' | translate }}</label>\n                                        <input type=\"text\" name=\"username\" maxlength=\"256\" class=\"form-control\"\n                                            [formControl]=\"_Form_Login.controls['username']\">\n                                        <label class=\"block text-xs _600 text-danger\" for=\"username\"\n                                            *ngIf=\"_Form_Login.controls['username'].hasError('required') && _Form_Login.controls['username'].touched\">{{\n                                                    'Login.UserName_ER_Required' | translate }}</label>\n                                    </div>\n                                    <div class=\"form-group\"\n                                        [ngClass]=\"{'text-danger':!_Form_Login.controls['password'].valid && _Form_Login.controls['password'].touched}\">\n                                        <div class=\"d-flex justify-content-between mg-b-5\">\n                                            <label class=\"mg-b-0-f\">{{ 'Login.Password' | translate }}</label>\n                                            <a class=\"tx-13\" routerLink=\"{{_HelperService.AppConfig.Pages.System.ForgotPassword}}\">\n                                                Forgot Password?\n                                            </a>\n                                        </div>\n                                        <input type=\"password\" name=\"password\" maxlength=\"50\" class=\"form-control   \"\n                                            [formControl]=\"_Form_Login.controls['password']\">\n                                    </div>\n                                    <div>\n                                        <label class=\"text-danger text-xs _600\" for=\"password\"\n                                            *ngIf=\"_Form_Login.controls['password'].hasError('required') && _Form_Login.controls['password'].touched\">{{\n                                                'Login.Password_ER_Required' | translate }}</label>\n                                    </div>\n                                    <button type=\"submit\" class=\"btn btn-brand-02 btn-block\"\n                                        [disabled]=\"!_Form_Login.valid || _Form_Login_Processing\">\n                                        <span *ngIf=\"!_Form_Login_Processing\">\n                                            {{ 'Login.LoginBtn' | translate }}\n                                        </span>\n                                        <span *ngIf=\"_Form_Login_Processing\">\n                                            <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                                            {{ 'Common.PleaseWait' | translate }}...\n                                        </span>\n                                    </button>\n                            </form> -->\n                    </div>    \n                    <div class=\"row \">\n                        <div class=\" text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 mg-t-100 text-muted\">\n                            © Copyright <a target=\"_blank\" class=\"text-primary\"\n                                href=\"http://connectedanalytics.xyz\">Connected\n                                Analytics Inc.</a> 2020 |\n                            All Right\n                            Reserved\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/auth/resetpassword/resetpassword.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/auth/resetpassword/resetpassword.component.ts ***!
  \***************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/service */ "./src/app/service/service.ts");






var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(_ActivatedRoute, _FormBuilder, _TranslateService, _Router, _HelperService) {
        var _this = this;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._TranslateService = _TranslateService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this._Form_Login_Processing = false;
        this._Form_Login = _FormBuilder.group({
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
            'newpassword': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
        });
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
        this._ActivatedRoute.params.subscribe(function (params) {
            _this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            if (_this._HelperService.AppConfig.ActiveReferenceKey != undefined) {
            }
            else {
                window.location.href = "/account/login";
            }
        });
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Device);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.SetNetwork();
    };
    ResetPasswordComponent.prototype._Form_Login_Process = function (value) {
        var _this = this;
        if (value.password != value.newpassword) {
            this._HelperService.NotifyError('Password does not match. Both passwords must be same to reset your password');
        }
        else {
            this._Form_Login_Processing = true;
            var pData = {
                Task: 'forgotpasswordverify',
                RequestToken: this._HelperService.AppConfig.ActiveReferenceKey,
                Password: value.password,
            };
            var _OResponse = void 0;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
            _OResponse.subscribe(function (_Response) {
                _this._Form_Login_Processing = false;
                if (_Response.Status == _this._HelperService.StatusSuccess) {
                    _this._HelperService.NotifySuccess('Passsword changed successfully');
                    setTimeout(function () {
                        window.location.href = _this._HelperService.AppConfig.Pages.System.Login;
                    }, 1500);
                }
                else {
                    _this._HelperService.NotifyError(_Response.Message);
                }
            }, function (_Error) {
                _this._Form_Login_Processing = false;
                _this._HelperService.HandleException(_Error);
            });
        }
    };
    ResetPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'resetpassword',
            template: __webpack_require__(/*! ./resetpassword.component.html */ "./src/app/auth/resetpassword/resetpassword.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/service/datahelper.service.ts":
/*!***********************************************!*\
  !*** ./src/app/service/datahelper.service.ts ***!
  \***********************************************/
/*! exports provided: DataHelperService, OSearchFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataHelperService", function() { return DataHelperService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSearchFilter", function() { return OSearchFilter; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _helper_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper.service */ "./src/app/service/helper.service.ts");




var DataHelperService = /** @class */ (function () {
    function DataHelperService(_TranslateService, _HelperService) {
        this._TranslateService = _TranslateService;
        this._HelperService = _HelperService;
    }
    DataHelperService.prototype.S2_BuildList = function (S2_Options) {
        var _this = this;
        var Id = [];
        var Text = [];
        var Transport = {
            delay: 200,
            data: function (params) {
                return {
                    search: params.term,
                    page: params.page || 1
                };
            },
            transport: function (params, success, failure) {
                var SearchContent = "";
                if (params.data.search != undefined) {
                    SearchContent = params.data.search;
                }
                Id = [];
                Text = [];
                var SearchData = '';
                S2_Options.Fields.forEach(function (Item) {
                    if (Item.Id == true) {
                        Id.push(Item.SystemName);
                    }
                    if (Item.Text == true) {
                        Text.push(Item.SystemName);
                    }
                    if (Item.SearchCondition != undefined && Item.SearchValue != undefined) {
                        SearchData = _this._HelperService.GetSearchConditionStrict(SearchData, Item.SystemName, Item.Type, Item.SearchValue, Item.SearchCondition);
                    }
                    else {
                        SearchData = _this._HelperService.GetSearchCondition(SearchData, Item.SystemName, Item.Type, SearchContent);
                    }
                });
                if (S2_Options.SearchCondition != undefined && S2_Options.SearchCondition != null && S2_Options.SearchCondition != '') {
                    if (SearchData != undefined && SearchData != null && SearchData != "") {
                        SearchData = "(" + S2_Options.SearchCondition + ") AND " + "(" + SearchData + ")";
                    }
                    else {
                        SearchData = "(" + S2_Options.SearchCondition + ")";
                    }
                }
                var SortCondition = null;
                if (S2_Options.SortCondition != null) {
                    SortCondition = _this._HelperService.GetSortCondition(S2_Options.SortCondition);
                }
                else {
                    SortCondition = _this._HelperService.GetSortCondition([Text[0] + ' asc']);
                }
                var S2Response = _this.S2_GetResults(params.data.page, SearchData, S2_Options.Location, S2_Options.Task, success, failure, SortCondition, S2_Options.ReferenceId, S2_Options.ReferenceKey);
            },
            processResults: function (data, params) {
                return _this.S2_ProcessData(data, params, Text, Id);
            }
        };
        return Transport;
    };
    DataHelperService.prototype.S2_ProcessData = function (data, params, NameParameters, ValueParameters) {
        var MoreResults = false;
        var Offset = 0;
        var FData;
        if (data != undefined && data != null) {
            if (params.page == undefined) {
                Offset = data.Limit;
            }
            else {
                Offset = params.page * data.Limit;
            }
            MoreResults = false;
            var FData = $.map(data.Data, function (obj) {
                if (NameParameters.length == 1) {
                    obj.text = obj[NameParameters[0]];
                }
                else {
                    var NameContent = '';
                    for (var index = 0; index < NameParameters.length; index++) {
                        var element = NameParameters[index];
                        if (NameContent == '') {
                            if (obj[element] != undefined) {
                                NameContent += obj[element];
                            }
                        }
                        else {
                            if (obj[element] != undefined) {
                                NameContent += ' - ' + obj[element];
                            }
                        }
                    }
                    obj.text = NameContent;
                }
                if (ValueParameters.length == 1) {
                    obj.id = obj[ValueParameters[0]];
                }
                else {
                    var IdContent = '';
                    for (var index = 0; index < ValueParameters.length; index++) {
                        var element = ValueParameters[index];
                        if (IdContent == '') {
                            IdContent += obj[element];
                        }
                        else {
                            IdContent += ' - ' + obj[element];
                        }
                    }
                    obj.text = NameContent;
                }
                return obj;
            });
            params.page = params.page || 1;
            return {
                results: FData,
                pagination: {
                    more: MoreResults,
                }
            };
        }
        else {
            Offset = 1;
            FData = [];
            return {
                results: FData,
                pagination: {
                    more: 0,
                }
            };
        }
    };
    DataHelperService.prototype.S2_GetResults = function (Offset, SearchContent, NetworkApiLocation, NetworkApi, success, failure, SortCondition, ReferenceId, ReferenceKey) {
        var _this = this;
        var PData = {
            Task: NetworkApi,
            Offset: (Offset - 1) * this._HelperService.AppConfig.DropDownListLimit,
            Limit: this._HelperService.AppConfig.DropDownListLimit,
            RefreshCount: false,
            SearchCondition: SearchContent,
            SortExpression: SortCondition,
            ReferenceId: ReferenceId,
            ReferenceKey: ReferenceKey,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(NetworkApiLocation, PData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                var _ResponseData = _Response.Result;
                success(_ResponseData);
            }
            else {
                success(_ResponseData);
            }
        }, function (_Error) {
            failure();
        });
    };
    DataHelperService.prototype.List_Initialize = function (ListOptions) {
        var _this = this;
        if (ListOptions.Task == undefined) {
            ListOptions.Task = null;
        }
        if (ListOptions.ReferenceKey == undefined) {
            ListOptions.ReferenceKey = null;
        }
        if (ListOptions.Type == undefined) {
            ListOptions.Type = null;
        }
        if (ListOptions.Location == undefined) {
            ListOptions.Location = null;
        }
        if (ListOptions.Title == undefined) {
            ListOptions.Title = 'List';
        }
        if (ListOptions.TableFields == undefined) {
            ListOptions.TableFields = [];
        }
        if (ListOptions.StatusOptions == undefined) {
            ListOptions.StatusOptions = [];
        }
        if (ListOptions.TitleResourceId == undefined) {
            ListOptions.TitleResourceId = null;
        }
        if (ListOptions.RefreshData == undefined) {
            ListOptions.RefreshData = false;
        }
        if (ListOptions.IsDownload == undefined) {
            ListOptions.IsDownload = false;
        }
        if (ListOptions.ActivePage == undefined) {
            ListOptions.ActivePage = 1;
        }
        if (ListOptions.PageRecordLimit == undefined) {
            ListOptions.PageRecordLimit = this._HelperService.AppConfig.ListRecordLimit[0];
        }
        if (ListOptions.TotalRecords == undefined) {
            ListOptions.TotalRecords = 0;
        }
        if (ListOptions.ShowingStart == undefined) {
            ListOptions.ShowingStart = 0;
        }
        if (ListOptions.ShowingEnd == undefined) {
            ListOptions.ShowingEnd = 0;
        }
        if (ListOptions.StartTime == undefined) {
            ListOptions.StartTime = null;
        }
        if (ListOptions.EndTime == undefined) {
            ListOptions.EndTime = null;
        }
        if (ListOptions.ReferenceId == undefined) {
            ListOptions.ReferenceId = 0;
        }
        if (ListOptions.SubReferenceId == undefined) {
            ListOptions.SubReferenceId = 0;
        }
        if (ListOptions.SearchParameter == undefined) {
            ListOptions.SearchParameter = null;
        }
        if (ListOptions.SearchCondition == undefined) {
            ListOptions.SearchCondition = null;
        }
        if (ListOptions.SearchBaseCondition == undefined) {
            ListOptions.SearchBaseCondition = null;
        }
        if (ListOptions.SearchBaseConditions == undefined) {
            ListOptions.SearchBaseConditions = [];
        }
        if (ListOptions.Data == undefined) {
            ListOptions.Data = [];
        }
        if (ListOptions.StatusType == undefined) {
            ListOptions.StatusType = 'default';
        }
        if (ListOptions.Status == undefined) {
            ListOptions.Status = 0;
        }
        if (ListOptions.VisibleHeaders == undefined) {
            ListOptions.VisibleHeaders = [];
        }
        if (ListOptions.Sort == undefined) {
            ListOptions.Sort =
                {
                    SortDefaultName: null,
                    SortDefaultColumn: 'CreateDate',
                    SortName: null,
                    SortColumn: null,
                    SortOrder: 'desc',
                    SortOptions: [],
                };
        }
        ListOptions.Sort =
            {
                SortDefaultName: ListOptions.Sort.SortDefaultName,
                SortDefaultColumn: ListOptions.Sort.SortDefaultColumn,
                SortDefaultOrder: ListOptions.Sort.SortDefaultOrder,
                SortName: ListOptions.Sort.SortName,
                SortColumn: ListOptions.Sort.SortColumn,
                SortOrder: ListOptions.Sort.SortOrder,
                SortOptions: [],
            };
        if (ListOptions.Sort.SortDefaultOrder == undefined || ListOptions.Sort.SortDefaultOrder == null) {
            ListOptions.Sort.SortDefaultOrder = 'desc';
        }
        if (ListOptions.Sort.SortName == undefined || ListOptions.Sort.SortName == null || ListOptions.Sort.SortName == '') {
            ListOptions.Sort.SortName = ListOptions.Sort.SortDefaultName;
        }
        if (ListOptions.Sort.SortColumn == undefined || ListOptions.Sort.SortColumn == null || ListOptions.Sort.SortColumn == '') {
            ListOptions.Sort.SortColumn = ListOptions.Sort.SortDefaultColumn;
        }
        if (ListOptions.Sort.SortOrder == undefined || ListOptions.Sort.SortOrder == null || ListOptions.Sort.SortOrder == '') {
            ListOptions.Sort.SortOrder = ListOptions.Sort.SortDefaultOrder;
        }
        ListOptions.StatusOptions = this._HelperService.AppConfig.StatusList[ListOptions.StatusType];
        this.S2_Status_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_SavedFilters_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_NewSavedFilters_Option = {
            multiple: false,
            placeholder: "default"
        };
        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort results"
        };
        if (ListOptions.TitleResourceId != undefined && ListOptions.TitleResourceId != null && ListOptions.TitleResourceId != '') {
            this._TranslateService.get(ListOptions.TitleResourceId).subscribe(function (_Message) {
                ListOptions.Title = _Message;
            });
        }
        ListOptions.TableFields.forEach(function (element) {
            if (element.ResourceId != undefined && element.ResourceId != null && element.ResourceId != '') {
                _this._TranslateService.get(element.ResourceId).subscribe(function (_Message) {
                    element.DisplayName = _Message;
                });
            }
            ListOptions.TableFields.push(element);
            if (element.Sort == true) {
                var FilterItem = {
                    Name: element.DisplayName,
                    SystemName: element.SystemName,
                };
                ListOptions.Sort.SortOptions.push(FilterItem);
            }
            if (element.Show == true) {
                ListOptions.VisibleHeaders.push(element);
                var Option = {
                    id: element.SystemName,
                    text: element.DisplayName,
                };
            }
        });
        return ListOptions;
    };
    DataHelperService.prototype.List_Initialize_ForStores = function (ListOptions) {
        var _this = this;
        if (ListOptions.Task == undefined) {
            ListOptions.Task = null;
        }
        if (ListOptions.ReferenceKey == undefined) {
            ListOptions.ReferenceKey = null;
        }
        if (ListOptions.Type == undefined) {
            ListOptions.Type = null;
        }
        if (ListOptions.Location == undefined) {
            ListOptions.Location = null;
        }
        if (ListOptions.Title == undefined) {
            ListOptions.Title = 'List';
        }
        if (ListOptions.TableFields == undefined) {
            ListOptions.TableFields = [];
        }
        if (ListOptions.StatusOptions == undefined) {
            ListOptions.StatusOptions = [];
        }
        if (ListOptions.TitleResourceId == undefined) {
            ListOptions.TitleResourceId = null;
        }
        if (ListOptions.RefreshData == undefined) {
            ListOptions.RefreshData = false;
        }
        if (ListOptions.IsDownload == undefined) {
            ListOptions.IsDownload = false;
        }
        if (ListOptions.ActivePage == undefined) {
            ListOptions.ActivePage = 1;
        }
        if (ListOptions.PageRecordLimit == undefined) {
            ListOptions.PageRecordLimit = this._HelperService.AppConfig.ListRecordLimit[0];
        }
        if (ListOptions.TotalRecords == undefined) {
            ListOptions.TotalRecords = 0;
        }
        if (ListOptions.ShowingStart == undefined) {
            ListOptions.ShowingStart = 0;
        }
        if (ListOptions.ShowingEnd == undefined) {
            ListOptions.ShowingEnd = 0;
        }
        if (ListOptions.StartTime == undefined) {
            ListOptions.StartTime = null;
        }
        if (ListOptions.EndTime == undefined) {
            ListOptions.EndTime = null;
        }
        if (ListOptions.ReferenceId == undefined) {
            ListOptions.ReferenceId = 0;
        }
        if (ListOptions.SubReferenceId == undefined) {
            ListOptions.SubReferenceId = 0;
        }
        if (ListOptions.SubReferenceKey == undefined) {
            ListOptions.SubReferenceKey = null;
        }
        if (ListOptions.SearchParameter == undefined) {
            ListOptions.SearchParameter = null;
        }
        if (ListOptions.SearchCondition == undefined) {
            ListOptions.SearchCondition = null;
        }
        if (ListOptions.SearchBaseCondition == undefined) {
            ListOptions.SearchBaseCondition = null;
        }
        if (ListOptions.SearchBaseConditions == undefined) {
            ListOptions.SearchBaseConditions = [];
        }
        if (ListOptions.Data == undefined) {
            ListOptions.Data = [];
        }
        if (ListOptions.StatusType == undefined) {
            ListOptions.StatusType = 'default';
        }
        if (ListOptions.Status == undefined) {
            ListOptions.Status = 0;
        }
        if (ListOptions.VisibleHeaders == undefined) {
            ListOptions.VisibleHeaders = [];
        }
        if (ListOptions.Sort == undefined) {
            ListOptions.Sort =
                {
                    SortDefaultName: null,
                    SortDefaultColumn: 'CreateDate',
                    SortName: null,
                    SortColumn: null,
                    SortOrder: 'desc',
                    SortOptions: [],
                };
        }
        ListOptions.Sort =
            {
                SortDefaultName: ListOptions.Sort.SortDefaultName,
                SortDefaultColumn: ListOptions.Sort.SortDefaultColumn,
                SortDefaultOrder: ListOptions.Sort.SortDefaultOrder,
                SortName: ListOptions.Sort.SortName,
                SortColumn: ListOptions.Sort.SortColumn,
                SortOrder: ListOptions.Sort.SortOrder,
                SortOptions: [],
            };
        if (ListOptions.Sort.SortDefaultOrder == undefined || ListOptions.Sort.SortDefaultOrder == null) {
            ListOptions.Sort.SortDefaultOrder = 'desc';
        }
        if (ListOptions.Sort.SortName == undefined || ListOptions.Sort.SortName == null || ListOptions.Sort.SortName == '') {
            ListOptions.Sort.SortName = ListOptions.Sort.SortDefaultName;
        }
        if (ListOptions.Sort.SortColumn == undefined || ListOptions.Sort.SortColumn == null || ListOptions.Sort.SortColumn == '') {
            ListOptions.Sort.SortColumn = ListOptions.Sort.SortDefaultColumn;
        }
        if (ListOptions.Sort.SortOrder == undefined || ListOptions.Sort.SortOrder == null || ListOptions.Sort.SortOrder == '') {
            ListOptions.Sort.SortOrder = ListOptions.Sort.SortDefaultOrder;
        }
        ListOptions.StatusOptions = this._HelperService.AppConfig.StatusList[ListOptions.StatusType];
        this.S2_Status_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_SavedFilters_Option = {
            multiple: false,
            placeholder: "Filter by status"
        };
        this.S2_NewSavedFilters_Option = {
            multiple: false,
            placeholder: "default"
        };
        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort results"
        };
        if (ListOptions.TitleResourceId != undefined && ListOptions.TitleResourceId != null && ListOptions.TitleResourceId != '') {
            this._TranslateService.get(ListOptions.TitleResourceId).subscribe(function (_Message) {
                ListOptions.Title = _Message;
            });
        }
        ListOptions.TableFields.forEach(function (element) {
            if (element.ResourceId != undefined && element.ResourceId != null && element.ResourceId != '') {
                _this._TranslateService.get(element.ResourceId).subscribe(function (_Message) {
                    element.DisplayName = _Message;
                });
            }
            ListOptions.TableFields.push(element);
            if (element.Sort == true) {
                var FilterItem = {
                    Name: element.DisplayName,
                    SystemName: element.SystemName,
                };
                ListOptions.Sort.SortOptions.push(FilterItem);
            }
            if (element.Show == true) {
                ListOptions.VisibleHeaders.push(element);
                var Option = {
                    id: element.SystemName,
                    text: element.DisplayName,
                };
            }
        });
        // console.log(ListOptions);
        return ListOptions;
    };
    DataHelperService.prototype.List_Operations = function (ListOptions, event, Type) {
        var ResetOffset = false;
        ListOptions.RefreshData = false;
        ListOptions.RefreshCount = false;
        if (Type == this._HelperService.AppConfig.ListToggleOption.Limit) {
            ListOptions.PageRecordLimit = event;
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Search) {
            if (event != null) {
                ListOptions.SearchParameter = event.target.value;
            }
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Date) {
            if (ListOptions.StartDate == undefined && ListOptions.EndDate == undefined) {
                ListOptions.StartTime = event.start;
                ListOptions.EndTime = event.end;
            }
            ResetOffset = true;
            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Status) {
            if (event.data[0].code != undefined
                && event.data[0].code.includes('default')) {
                ListOptions.Status = event.data[0].code;
            }
            else {
                ListOptions.Status = event.value;
            }
            ListOptions.RefreshData = false;
            ListOptions.RefreshCount = false;
            ResetOffset = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Sort) {
            ListOptions.Sort.SortColumn = event.SystemName;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortOrder) {
            if (ListOptions.Sort.SortOrder == 'asc') {
                ListOptions.Sort.SortOrder = 'desc';
            }
            else {
                ListOptions.Sort.SortOrder = 'asc';
            }
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortApply) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.SortReset) {
            ListOptions.Sort.SortOrder = 'desc';
            ListOptions.Sort.SortColumn = null;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
            $('#' + ListOptions.Id + "_sdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Page) {
            ListOptions.ActivePage = event;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = false;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.MultiSelect) {
            ListOptions.ActivePage = event;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Refresh) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Refresh) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.ResetOffset) {
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.Csv) {
            this.List_DownloadCsv(ListOptions);
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.ApplyFilter) {
            ResetOffset = true;
            ListOptions.RefreshData = true;
            ListOptions.RefreshCount = true;
            $('#' + ListOptions.Id + "_fdropdown").dropdown('toggle');
        }
        else if (Type == this._HelperService.AppConfig.ListToggleOption.CancelFilter) {
            $('#' + ListOptions.Id + "_fdropdown").dropdown('toggle');
        }
        if (ResetOffset == true) {
            ListOptions.ActivePage = 1;
            ListOptions.TotalRecords = 0;
            ListOptions.ShowingStart = 0;
            ListOptions.ShowingEnd = 0;
        }
        return ListOptions;
    };
    DataHelperService.prototype.List_GetSearchCondition = function (ListOptions) {
        var _this = this;
        var DateSearchFieldName = null;
        ListOptions.SearchCondition = '';
        if (ListOptions.TableFields != undefined) {
            ListOptions.TableFields.forEach(function (element) {
                if (element.IsDateSearchField == true) {
                    DateSearchFieldName = element.SystemName;
                }
                if (element.Search == true) {
                    ListOptions.SearchCondition = _this._HelperService.GetSearchCondition(ListOptions.SearchCondition, element.SystemName, element.DataType, ListOptions.SearchParameter);
                }
            });
        }
        if (ListOptions.Status != 0) {
            ListOptions.SearchCondition = this._HelperService.GetSearchConditionStrict(ListOptions.SearchCondition, 'StatusCode', 'text', ListOptions.Status, "=");
        }
        if (DateSearchFieldName != null) {
            ListOptions.SearchCondition = this._HelperService.GetDateCondition(ListOptions.SearchCondition, DateSearchFieldName, ListOptions.StartTime, ListOptions.EndTime);
        }
        if (ListOptions.SearchBaseCondition != undefined && ListOptions.SearchBaseCondition != null && ListOptions.SearchBaseCondition != '') {
            if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + ListOptions.SearchBaseCondition + ')';
            }
            else {
                ListOptions.SearchCondition = ListOptions.SearchBaseCondition;
            }
        }
        if (ListOptions.SearchBaseConditions != undefined && ListOptions.SearchBaseConditions.length > 0) {
            ListOptions.SearchBaseConditions.forEach(function (element) {
                if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                    ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + element + ')';
                }
                else {
                    ListOptions.SearchCondition = element;
                }
            });
        }
        return ListOptions;
    };
    DataHelperService.prototype.List_GetSearchConditionTur = function (ListOptions) {
        var _this = this;
        var DateSearchFieldName = null;
        ListOptions.SearchCondition = '';
        if (ListOptions.TableFields != undefined) {
            ListOptions.TableFields.forEach(function (element) {
                if (element.IsDateSearchField == true) {
                    DateSearchFieldName = element.SystemName;
                }
                if (element.Search == true) {
                    ListOptions.SearchCondition = _this._HelperService.GetSearchCondition(ListOptions.SearchCondition, element.SystemName, element.DataType, ListOptions.SearchParameter);
                }
            });
        }
        if (ListOptions.Status != 0) {
            ListOptions.SearchCondition = this._HelperService.GetSearchConditionStrict(ListOptions.SearchCondition, 'StatusId', this._HelperService.AppConfig.DataType.Number, ListOptions.Status, "=");
        }
        if (DateSearchFieldName != null) {
            ListOptions.SearchCondition = this._HelperService.GetDateCondition(ListOptions.SearchCondition, DateSearchFieldName, ListOptions.StartTime, ListOptions.EndTime);
        }
        if (ListOptions.SearchBaseCondition != undefined && ListOptions.SearchBaseCondition != null && ListOptions.SearchBaseCondition != '') {
            if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + ListOptions.SearchBaseCondition + ')';
            }
            else {
                ListOptions.SearchCondition = ListOptions.SearchBaseCondition;
            }
        }
        if (ListOptions.SearchBaseConditions != undefined && ListOptions.SearchBaseConditions.length > 0) {
            ListOptions.SearchBaseConditions.forEach(function (element) {
                if (ListOptions.SearchCondition != undefined && ListOptions.SearchCondition != null && ListOptions.SearchCondition != '') {
                    ListOptions.SearchCondition = '(' + ListOptions.SearchCondition + ') AND (' + element + ')';
                }
                else {
                    ListOptions.SearchCondition = element;
                }
            });
        }
        return ListOptions;
    };
    DataHelperService.prototype.List_FormatResponse = function (ListOptions, ResponseData, Show) {
        ListOptions.Data = [];
        ListOptions.ShowingStart = ((ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit) + 1;
        ListOptions.ShowingEnd = (ListOptions.ShowingStart + ListOptions.PageRecordLimit) - 1;
        ListOptions.TotalRecords = ResponseData.TotalRecords;
        var Result = ResponseData.Data;
        if (Show != undefined && Show != null) {
            if (Result.length == 0) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            }
            else {
                Show.ListShow = 'block';
                Show.IconShow = 'none';
            }
        }
        for (var index = 0; index < Result.length; index++) {
            var element = Result[index];
            if (Result[index].ProcessingTime != undefined) {
                Result[index].ProcessingTime = (Result[index].ProcessingTime / 1000);
            }
            Result[index].StartDate = this._HelperService.GetDateS(element.StartDate);
            Result[index].EndDate = this._HelperService.GetDateS(element.EndDate);
            Result[index].LastUseDate = this._HelperService.GetDateTimeS(element.LastUseDate);
            Result[index].ActivityDate = this._HelperService.GetDateTimeS(element.ActivityDate);
            Result[index].LastTransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.LastTransactionDate, moment());
            Result[index].TransactionDateDiff = this._HelperService.GetTimeDifferenceS(element.TransactionDate, moment());
            Result[index].TransactionDateD = this._HelperService.GetDateS(element.TransactionDate);
            Result[index].TransactionDateT = this._HelperService.GetTimeS(element.TransactionDate);
            Result[index].TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
            Result[index].VerifyDate = this._HelperService.GetDateTimeS(element.VerifyDate);
            Result[index].InvoiceDate = this._HelperService.GetDateS(element.InvoiceDate);
            Result[index].RequestTime = this._HelperService.GetDateTimeS(element.RequestTime);
            Result[index].ResponseTime = this._HelperService.GetDateTimeS(element.ResponseTime);
            Result[index].CreateDateD = this._HelperService.GetDateS(element.CreateDate);
            Result[index].CreateDateT = this._HelperService.GetTimeS(element.CreateDate);
            Result[index].CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
            Result[index].Date = this._HelperService.GetDateS(element.Date);
            Result[index].ModifyDate = this._HelperService.GetDateTimeS(element.ModifyDate);
            Result[index].LoginDate = this._HelperService.GetDateTimeS(element.LoginDate);
            if (Result[index].LastLoginDate != "0001-01-01T00:00:00") {
                Result[index].LastLoginDate = this._HelperService.GetDateTimeS(element.LastLoginDate);
            }
            else {
                Result[index].LastLoginDate = null;
            }
            if (Result[index].LastTransactionDate != "0001-01-01T00:00:00") {
                Result[index].LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
            }
            else {
                Result[index].LastTransactionDate = "";
            }
            Result[index].LogoutDate = this._HelperService.GetDateTimeS(element.LogoutDate);
            if (element.StatusId != undefined && element.StatusId != null) {
                Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusId);
                Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusId);
                Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusId);
            }
            else {
                Result[index].StatusI = this._HelperService.GetStatusIcon(element.StatusCode);
                Result[index].StatusBadge = this._HelperService.GetStatusBadge(element.StatusCode);
                Result[index].StatusC = this._HelperService.GetStatusColor(element.StatusCode);
            }
            if (element.ApplicationStatusId != undefined) {
                Result[index].ApplStatusBadge = this._HelperService.GetStatusBadge(element.ApplicationStatusId);
            }
        }
        ListOptions.Data = Result;
        return ListOptions;
    };
    DataHelperService.prototype.List_GetData = function (ListOptions, Show) {
        var _this = this;
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }
        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;
        ;
        // ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }
        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                if (Show != undefined && Show != null) {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result, Show);
                }
                else {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result);
                }
                return ListOptions;
            }
            else {
                ListOptions.Data = [];
                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }
                _this._HelperService.NotifyError(_Response.Message);
                return ListOptions;
            }
        }, function (_Error) {
            if (Show != undefined && Show != null) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            }
            _this._HelperService.HandleException(_Error);
            return ListOptions;
        });
        return ListOptions;
    };
    DataHelperService.prototype.List_GetDataTur = function (ListOptions, Show) {
        var _this = this;
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }
        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchConditionTur(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;
        ;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }
        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                if (Show != undefined && Show != null) {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result, Show);
                }
                else {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result);
                }
                return ListOptions;
            }
            else {
                ListOptions.Data = [];
                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }
                _this._HelperService.NotifyError(_Response.Message);
                return ListOptions;
            }
        }, function (_Error) {
            if (Show != undefined && Show != null) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            }
            _this._HelperService.HandleException(_Error);
            return ListOptions;
        });
        return ListOptions;
    };
    DataHelperService.prototype.List_GetDataTerm = function (ListOptions, Show) {
        var _this = this;
        if (Show != undefined && Show != null) {
            Show.ListShow = 'none';
            Show.IconShow = 'block';
        }
        ListOptions.SearchCondition = '';
        ListOptions = this.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;
        ;
        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }
        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }
        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: SortExpression,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            SubReferenceKey: ListOptions.SubReferenceKey,
            ListType: ListOptions.ListType,
            IsDownload: false,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                if (Show != undefined && Show != null) {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result, Show);
                }
                else {
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result);
                }
                _this._HelperService.SortTerminals(ListOptions.Data);
                return ListOptions;
            }
            else {
                ListOptions.Data = [];
                if (Show != undefined && Show != null) {
                    Show.ListShow = 'none';
                    Show.IconShow = 'block';
                }
                _this._HelperService.NotifyError(_Response.Message);
                return ListOptions;
            }
        }, function (_Error) {
            if (Show != undefined && Show != null) {
                Show.ListShow = 'none';
                Show.IconShow = 'block';
            }
            _this._HelperService.HandleException(_Error);
            return ListOptions;
        });
        return ListOptions;
    };
    DataHelperService.prototype.List_DownloadCsv = function (ListOptions) {
        var _this = this;
        var pData = {
            Task: ListOptions.Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: 0,
            Limit: ListOptions.TotalRecords,
            RefreshCount: false,
            SearchCondition: ListOptions.SearchCondition,
            Type: ListOptions.Type,
            ReferenceKey: ListOptions.ReferenceKey,
            StartDate: ListOptions.StartDate,
            EndDate: ListOptions.EndDate,
            ReferenceId: ListOptions.ReferenceId,
            SubReferenceId: ListOptions.SubReferenceId,
            ListType: ListOptions.ListType,
            IsDownload: ListOptions.IsDownload,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(ListOptions.Location, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                if (ListOptions.IsDownload) {
                    _this._HelperService.NotifySuccess('Your download will be available shortly in downloads section');
                }
                else {
                    var _ResponseData = _Response.Result;
                    var Result = _ResponseData.Data;
                    ListOptions = _this.List_FormatResponse(ListOptions, _Response.Result);
                    var CsvData = [];
                    var CsvLabels = [];
                    var CsvSystemNames = [];
                    ListOptions.VisibleHeaders.forEach(function (element) {
                        if (element.Show == true) {
                            CsvLabels.push(element.DisplayName);
                            CsvSystemNames.push(element.SystemName);
                        }
                    });
                    for (var index = 0; index < Result.length; index++) {
                        var Item = Result[index];
                        var Content = {};
                        CsvSystemNames.forEach(function (element) {
                            var item = Item[element];
                            if (item != undefined && item != null) {
                                Content[element] = Item[element];
                            }
                            else {
                                Content[element] = "n/a";
                            }
                        });
                        CsvData.push(Content);
                    }
                    var CsvOptions = {
                        showLabels: true,
                        useBom: false,
                        showTitle: true,
                        title: ListOptions.Title,
                        headers: CsvLabels,
                    };
                    _this._HelperService.DownloadCsv(CsvData, ListOptions.Title, CsvOptions);
                }
            }
        }, function (_Error) {
            _this._HelperService.HandleException(_Error);
        });
    };
    DataHelperService.prototype.S2_Status_List = function (Value) {
        var PlaceHolder = 'Sort by status';
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            ReferenceId: 0,
            ReferenceKey: null,
            Fields: [
                {
                    SystemName: 'ReferenceId',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ParentCode', this._HelperService.AppConfig.DataType.Text, Value, '=');
        this.S2_Status_Transport = this.S2_BuildList(_Select);
        this.S2_Status_Option = {
            placeholder: PlaceHolder,
            ajax: this.S2_Status_Transport,
            multiple: false,
            allowClear: true,
        };
    };
    DataHelperService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
            _helper_service__WEBPACK_IMPORTED_MODULE_3__["HelperService"]])
    ], DataHelperService);
    return DataHelperService;
}());

// export class OList {
//     public Id: string;
//     public ListType?: number = 0;
//     public Task: string;
//     public Location: string;
//     public Title: string = "List";
//     public TableFields: OListField[];
//     public VisibleHeaders?: any[];
//     public ActivePage?: number = 1;
//     public PageRecordLimit?: number = 10;
//     public TotalRecords?: number = 0;
//     public ShowingStart?: number = 0;
//     public ShowingEnd?: number = 0;
//     public DefaultSortExpression?: string;
//     public SearchBaseCondition?: string;
//     public SearchBaseConditions?: any[];
//     public SearchParameter?: string;
//     public SearchCondition?: string;
//     public Filters?: OSearchFilter[];
//     public Data?: any[];
//     public StatusType?: string = "default";
//     public Status?: number = 0;
//     public StatusOptions?: any[];
//     public Sort: OListSort;
//     public RefreshData?: boolean = false;
//     public IsDownload?: boolean = false;
//     public ReferenceId?: number = null;
//     public SubReferenceId?: number = null;
//     public ReferenceKey?: string = null;
//     public Type?: string = null;
//     public RefreshCount?: boolean = true;
//     public TitleResourceId?: string = null;
//     public StartTime?: any = null;
//     public EndTime?: any = null;
//     public StartDate?: any = null;
//     public EndDate?: any = null;
// }
var OSearchFilter = /** @class */ (function () {
    function OSearchFilter() {
    }
    return OSearchFilter;
}());

// export class OListSort {
//     public SortDefaultName: string;
//     public SortDefaultColumn: string;
//     public SortDefaultOrder?: string = "desc";
//     public SortName?: string;
//     public SortOrder?: string = "desc";
//     public SortColumn?: string;
//     public SortOptions?: any[];
// }
// export class OListField {
//     public DefaultValue?: string = "--";
//     public DisplayName: string;
//     public DownloadDisplayName?: string;
//     public SystemName: string;
//     public Content?: string;
//     public DataType: string = "text";
//     public Class?: string = "";
//     public ResourceId?: string = "";
//     public Sort?: boolean = true;
//     public Show?: boolean = true;
//     public Search?: boolean = true;
//     public NavigateLink?: string = "";
//     public NavigateField?: string = "";
//     public IsDateSearchField?: boolean = false;
// }


/***/ }),

/***/ "./src/app/service/filterhelper.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/filterhelper.service.ts ***!
  \*************************************************/
/*! exports provided: FilterHelperService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterHelperService", function() { return FilterHelperService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/js/systemhelper.js */ "./src/assets/js/systemhelper.js");
/* harmony import */ var _assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _helper_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helper.service */ "./src/app/service/helper.service.ts");





var FilterHelperService = /** @class */ (function () {
    function FilterHelperService(_HelperService) {
        this._HelperService = _HelperService;
    }
    //#region MerchantFilterConfig 
    FilterHelperService.prototype._BuildFilterName_Merchant = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion    
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetMerchantConfig = function (MerchantsList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) || Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            MerchantsList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            MerchantsList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) || Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            MerchantsList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) || Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            MerchantsList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            MerchantsList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        else {
            MerchantsList_Config.SearchCondition = null;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            MerchantsList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
        else {
            MerchantsList_Config.SearchBaseConditions = [];
        }
    };
    FilterHelperService.prototype._RemoveFilter_Merchant = function (Type, index) {
        //#region RemoveFromFilterSnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnap.OtherFilters.splice(index, 1);
        }
        //#endregion
        //#region RemoveFromTemprarySnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnapTemprary.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.StartTime = null;
            }
            {
                this._HelperService.FilterSnapTemprary.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnapTemprary.StatusType = "default";
            }
            {
                this._HelperService.FilterSnapTemprary.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnapTemprary.Sort.SortName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortColumn = null;
                this._HelperService.FilterSnapTemprary.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnapTemprary.OtherFilters.splice(index, 1);
        }
        //#endregion
    };
    //#endregion
    //#region POSFilterConfig
    FilterHelperService.prototype._BuildFilterName_POS = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetPOSConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            POSList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
    };
    FilterHelperService.prototype._RemoveFilter_POS = function (Type, index) {
        //#region RemoveFromFilterSnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnap.OtherFilters.splice(index, 1);
        }
        //#endregion
        //#region RemoveFromTemprarySnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnapTemprary.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.StartTime = null;
            }
            {
                this._HelperService.FilterSnapTemprary.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnapTemprary.StatusType = "default";
            }
            {
                this._HelperService.FilterSnapTemprary.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnapTemprary.Sort.SortName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortColumn = null;
                this._HelperService.FilterSnapTemprary.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnapTemprary.OtherFilters.splice(index, 1);
        }
        //#endregion
    };
    //#endregion
    //#region StoreFilterConfig
    FilterHelperService.prototype._BuildFilterName_Store = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetStoreConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
    };
    FilterHelperService.prototype._RemoveFilter_Store = function (Type, index) {
        //#region RemoveFromFilterSnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnap.OtherFilters.splice(index, 1);
        }
        //#endregion
        //#region RemoveFromTemprarySnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnapTemprary.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.StartTime = null;
            }
            {
                this._HelperService.FilterSnapTemprary.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnapTemprary.StatusType = "default";
            }
            {
                this._HelperService.FilterSnapTemprary.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnapTemprary.Sort.SortName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortColumn = null;
                this._HelperService.FilterSnapTemprary.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnapTemprary.OtherFilters.splice(index, 1);
        }
        //#endregion
    };
    //#endregion
    //#region BranchFilterConfig
    FilterHelperService.prototype._BuildFilterName_Branch = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetBranchConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        else {
            POSList_Config.SearchCondition = null;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            POSList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
        else {
            POSList_Config.SearchBaseConditions = [];
        }
    };
    FilterHelperService.prototype._RemoveFilter_Branch = function (Type) {
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
    };
    //#endregion
    //#region ManagerFilterConfig
    FilterHelperService.prototype._BuildFilterName_Manager = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetManagerConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        else {
            POSList_Config.SearchCondition = null;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            POSList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
        else {
            POSList_Config.SearchBaseConditions = [];
        }
    };
    FilterHelperService.prototype._RemoveFilter_Manager = function (Type) {
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
    };
    //#endregion
    //#region CampaignFilterConfig
    FilterHelperService.prototype._BuildFilterName_Campaign = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetCampaignConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        else {
            POSList_Config.SearchCondition = null;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            POSList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
        else {
            POSList_Config.SearchBaseConditions = [];
        }
    };
    FilterHelperService.prototype._RemoveFilter_Campaign = function (Type) {
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
    };
    //#endregion
    //#region MerchantSalesFilterConfig
    FilterHelperService.prototype._BuildFilterName_MerchantSales = function (name) {
        //#region StorePreviousState 
        this._HelperService.FilterSnapPrev.id = this._HelperService.FilterSnap.id;
        this._HelperService.FilterSnapPrev.text = this._HelperService.FilterSnap.text;
        //#endregion
        this._HelperService.FilterSnap.text = name;
    };
    FilterHelperService.prototype.SetMerchantSalesConfig = function (POSList_Config) {
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Status) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Status))) {
            POSList_Config.Status = this._HelperService.FilterSnap.Status;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.StartTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.StartTime))) {
            POSList_Config.StartTime = this._HelperService.FilterSnap.StartTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.EndTime) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.EndTime))) {
            POSList_Config.EndTime = this._HelperService.FilterSnap.EndTime;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.Sort) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.Sort))) {
            POSList_Config.Sort = this._HelperService.FilterSnap.Sort;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchCondition) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchCondition))) {
            POSList_Config.SearchCondition = this._HelperService.FilterSnap.SearchCondition;
        }
        else {
            POSList_Config.SearchCondition = null;
        }
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_2__["isNull"])(this._HelperService.FilterSnap.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(this._HelperService.FilterSnap.SearchBaseConditions))) {
            POSList_Config.SearchBaseConditions = this._HelperService.FilterSnap.SearchBaseConditions;
        }
        else {
            POSList_Config.SearchBaseConditions = [];
        }
    };
    FilterHelperService.prototype._RemoveFilter_MerchantSales = function (Type, index) {
        //#region RemoveFromFilterSnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnap.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnap.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnap.StartTime = null;
            }
            {
                this._HelperService.FilterSnap.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnap.StatusType = "default";
            }
            {
                this._HelperService.FilterSnap.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnap.Sort.SortDefaultName = null;
                this._HelperService.FilterSnap.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnap.Sort.SortName = null;
                this._HelperService.FilterSnap.Sort.SortColumn = null;
                this._HelperService.FilterSnap.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnap.OtherFilters.splice(index, 1);
        }
        //#endregion
        //#region RemoveFromTemprarySnap 
        if (Type == "Time") {
            {
                this._HelperService.FilterSnapTemprary.ShowingStart = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.ShowingEnd = 0;
            }
            {
                this._HelperService.FilterSnapTemprary.StartTime = null;
            }
            {
                this._HelperService.FilterSnapTemprary.EndTime = null;
            }
        }
        if (Type == "Status") {
            {
                this._HelperService.FilterSnapTemprary.StatusType = "default";
            }
            {
                this._HelperService.FilterSnapTemprary.Status = 0;
            }
        }
        if (Type == "Sort") {
            {
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortDefaultColumn = "CreateDate";
                this._HelperService.FilterSnapTemprary.Sort.SortName = null;
                this._HelperService.FilterSnapTemprary.Sort.SortColumn = null;
                this._HelperService.FilterSnapTemprary.Sort.SortOrder = "desc";
            }
        }
        if (Type == "Other") {
            this._HelperService.FilterSnapTemprary.OtherFilters.splice(index, 1);
        }
        //#endregion
    };
    FilterHelperService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_helper_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"]])
    ], FilterHelperService);
    return FilterHelperService;
}());



/***/ }),

/***/ "./src/app/service/helper.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/helper.service.ts ***!
  \*******************************************/
/*! exports provided: HelperService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelperService", function() { return HelperService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! chartjs-plugin-datalabels */ "./node_modules/chartjs-plugin-datalabels/dist/chartjs-plugin-datalabels.js");
/* harmony import */ var chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var esri_leaflet_geocoder__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! esri-leaflet-geocoder */ "./node_modules/esri-leaflet-geocoder/dist/esri-leaflet-geocoder-debug.js");
/* harmony import */ var esri_leaflet_geocoder__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(esri_leaflet_geocoder__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm5/ng5-slider.js");
/* harmony import */ var notie__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! notie */ "./node_modules/notie/dist/notie.min.js");
/* harmony import */ var notie__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(notie__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../assets/js/systemhelper.js */ "./src/assets/js/systemhelper.js");
/* harmony import */ var _assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_assets_js_systemhelper_js__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! lodash/cloneDeep */ "./node_modules/lodash/cloneDeep.js");
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_19__);





















var HelperService = /** @class */ (function () {
    function HelperService(_Location, _Router, _TranslateService, _Ng2FileInputService, _Http) {
        var _this = this;
        this._Location = _Location;
        this._Router = _Router;
        this._TranslateService = _TranslateService;
        this._Ng2FileInputService = _Ng2FileInputService;
        this._Http = _Http;
        //#region leafletOperations
        this.lefletoptions = {};
        //#region LeafletGeocoder
        this.navchange = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        this.selectedLocation = {
            address: {},
        };
        this.geocoder = esri_leaflet_geocoder__WEBPACK_IMPORTED_MODULE_8__["geosearch"]();
        this.revgeocoder = esri_leaflet_geocoder__WEBPACK_IMPORTED_MODULE_8__["reverseGeocode"]();
        //#endregion
        this.terminalsavecount = 0;
        this.FullContainer = false;
        this.ShowAsidePanel = true;
        this.RandomNumber = null;
        this.RandomPassword = null;
        this.IsFormProcessing = false;
        this.ToggleField = false;
        this.UserRolePermissions = []; //Array<OUserAccountRolePermission>;
        this.Center = "center";
        this.StatusSuccess = "Success";
        this.StatusError = "Error";
        this.AppConfig = {
            PayStackKey: "pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12",
            TabId: 0,
            ShowMenu: true,
            DefaultStartTimeAll: new Date(2017, 0, 1, 0, 0, 0, 0),
            DefaultEndTimeToday: moment().endOf("day"),
            DefaultStartTime: moment().startOf("day"),
            DefaultEndTime: moment().endOf("day"),
            CurrencySymbol: "&#8358;",
            RangeInvoiceAmountMinimumLimit: 0,
            RangeInvoiceAmountMaximumLimit: 100000,
            RangeInvoiceAmountOptions: {
                floor: 0,
                ceil: 100000,
                draggableRange: true,
                noSwitching: true,
                translate: function (value, label) {
                    switch (label) {
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].Low:
                            return ("min purchase:" + _this.AppConfig.CurrencySymbol + " " + value);
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].High:
                            return ("max purchase:</b>" + _this.AppConfig.CurrencySymbol + " " + value);
                        default:
                            return _this.AppConfig.CurrencySymbol + " " + value;
                    }
                },
            },
            RangeRewardAmountMinimumLimit: 0,
            RangeRewardAmountMaximumLimit: 100000,
            RangeRewardAmountOptions: {
                floor: 0,
                ceil: 100000,
                draggableRange: true,
                noSwitching: true,
                translate: function (value, label) {
                    switch (label) {
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].Low:
                            return "min reward:" + _this.AppConfig.CurrencySymbol + " " + value;
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].High:
                            return ("max reward:</b>" + _this.AppConfig.CurrencySymbol + " " + value);
                        default:
                            return _this.AppConfig.CurrencySymbol + " " + value;
                    }
                },
            },
            RangeRedeemAmountMinimumLimit: 0,
            RangeRedeemAmountMaximumLimit: 100000,
            RangeRedeemAmountOptions: {
                floor: 0,
                ceil: 100000,
                draggableRange: true,
                noSwitching: true,
                translate: function (value, label) {
                    switch (label) {
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].Low:
                            return "min redeem:" + _this.AppConfig.CurrencySymbol + " " + value;
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].High:
                            return ("max redeem:</b>" + _this.AppConfig.CurrencySymbol + " " + value);
                        default:
                            return _this.AppConfig.CurrencySymbol + " " + value;
                    }
                },
            },
            RangeRewardClaimAmountMinimumLimit: 0,
            RangeRewardClaimAmountMaximumLimit: 100000,
            RangeRewardClaimAmountOptions: {
                floor: 0,
                ceil: 100000,
                draggableRange: true,
                noSwitching: true,
                translate: function (value, label) {
                    switch (label) {
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].Low:
                            return "min claim:" + _this.AppConfig.CurrencySymbol + " " + value;
                        case ng5_slider__WEBPACK_IMPORTED_MODULE_11__["LabelType"].High:
                            return ("max claim:</b>" + _this.AppConfig.CurrencySymbol + " " + value);
                        default:
                            return _this.AppConfig.CurrencySymbol + " " + value;
                    }
                },
            },
            ActiveOwnerId: null,
            ActiveOwnerAccountCode: null,
            ActiveOwnerAccountTypeCode: null,
            ActiveOwnerAccountTypeId: null,
            ActiveOwnerKey: null,
            ActiveOwnerDisplayName: null,
            ActiveOwnerIsTucPlusEnabled: null,
            ActiveReferenceAccountTypeCode: null,
            ActiveReferenceAccountTypeId: null,
            ActiveReferenceId: null,
            ActiveReferenceKey: null,
            ActiveReferenceListType: null,
            ActiveReferenceDisplayName: null,
            FlagStatusCode: {
                Green: "accountflag.green",
                Orange: "accountflag.orange",
                Red: "accountflag.red",
                Yellow: "accountflag.yellow",
                Black: "accountflag.black",
                Purple: "accountflag.purple",
            },
            ActiveSubReferenceKey: null,
            ShowHeader: true,
            ActivePageName: "",
            Client: null,
            ClientHeader: {
                "Content-Type": "application/json; charset=utf-8",
                hcak: null,
                hcavk: null,
                hctk: null,
                hcudlt: null,
                hcudln: null,
                hcuak: null,
                hcupk: null,
                hcudk: null,
                hcuata: "",
                hcuatp: "",
            },
            Host: "",
            HostConnect: "",
            DefaultPassword: "Welcome@ThankU",
            WebsiteUrl: "",
            AppName: "",
            AppVersion: "",
            TimeZone: "Africa/Lagos",
            TimeFormat: "h:mm a",
            DateDayFormat: "DD-MM",
            DateMonthFormat: "MM-YY",
            DateYearFormat: "YYYY",
            DateFormat: "DD-MM-YYYY",
            DateTimeFormat: "DD-MM-YYYY HH:mm",
            DateTimeFormatForChart: "YYYY-MM-DD HH:mm:ss",
            DateTimeFormatForChartX: "DD MMM",
            //DateTimeFormat: "DD MMM YYYY h:mm a",
            //DateTimeFormatShort: "DD-MM-YYYY h:mm a",
            AppKey: "33f51da41c95415fba82ff8a9f4981a5",
            AppVersionKey: "18da648a59024f2ba93e2e07319b1764",
            DropDownListLimit: 7,
            ListRecordLimit: [10, 25, 30, 50, 90, 100],
            Center: "center",
            Alert_Position: this.Center,
            Alert_Animation: "",
            Alert_AllowAnimation: false,
            Alert_AllowOutsideClick: false,
            Alert_AllowEscapeKey: false,
            Color_Green: "#22b66e",
            Color_Red: "#f14d4d",
            Color_Grey: "#d1dade",
            TablesConfig: {
                DefaultClass: "table  table-hover mb-0",
                InfoClass: "fa fa-info-circle text-warn",
            },
            HostDomain: {
                App: "app.thankucash.com",
                Console: "console.thankucash.com",
                Merchant: "merchant.thankucash.com",
                Store: "store.thankucash.com",
                Cashier: "cashier.thankucash.com",
            },
            Icon_Cropper_Options: {
                MaintainAspectRatio: "true",
                AspectRatio: "1 / 1",
                MinimumWidth: 128,
                MinimumHeight: 128,
                ResizeToWidth: 128,
                Format: "png",
            },
            Poster_Cropper_Options: {
                MaintainAspectRatio: "true",
                AspectRatio: "19 / 9",
                MinimumWidth: 800,
                MinimumHeight: 450,
                ResizeToWidth: 800,
                Format: "png",
            },
            DateRangeOptionsMonth: {
                locale: { format: "YYYY-MM-DD" },
                alwaysShowCalendars: false,
                startDate: moment().startOf("month"),
                endDate: moment().endOf("month"),
                dateLimit: {
                    month: 1,
                },
                // minDate: moment().subtract(30, "days"),
                // maxDate: moment().add(30, "days"),
                // startDate: moment().startOf("month"),
                // endDate: moment().endOf("month"),
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "This Week": [moment().startOf("week"), moment().endOf("week")],
                    "Last Week": [
                        moment().subtract(1, "weeks").startOf("isoWeek"),
                        moment().subtract(1, "weeks").endOf("isoWeek"),
                    ],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 15 Days": [moment().subtract(14, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [
                        moment().startOf("month").subtract(1, "month"),
                        moment().startOf("month").subtract(1, "days"),
                    ],
                },
            },
            DateRangeOptions: {
                locale: { format: "YYYY-MM-DD" },
                alwaysShowCalendars: false,
                startDate: moment().startOf("day"),
                endDate: moment().endOf("day"),
                dateLimit: {
                    month: 1,
                },
                // minDate: moment().subtract(30, "days"),
                // maxDate: moment().add(30, "days"),
                // startDate: moment().startOf("month"),
                // endDate: moment().endOf("month"),
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "This Week": [moment().startOf("week"), moment().endOf("week")],
                    "Last Week": [
                        moment().subtract(1, "weeks").startOf("isoWeek"),
                        moment().subtract(1, "weeks").endOf("isoWeek"),
                    ],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 15 Days": [moment().subtract(14, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [
                        moment().startOf("month").subtract(1, "month"),
                        moment().startOf("month").subtract(1, "days"),
                    ],
                    All: [new Date(2017, 0, 1, 0, 0, 0, 0), moment()],
                },
            },
            DateRangeMonthOptions: {
                locale: { format: "YYYY-MM-DD" },
                alwaysShowCalendars: false,
                startDate: moment().startOf("month").subtract(6, "month"),
                endDate: moment().endOf("month"),
                dateLimit: {
                    month: 12,
                },
                // minDate: moment().subtract(30, "days"),
                // maxDate: moment().add(30, "days"),
                // startDate: moment().startOf("month"),
                // endDate: moment().endOf("month"),
                ranges: {
                    "This Week": [moment().startOf("week"), moment().endOf("week")],
                    "Last Week": [
                        moment().subtract(1, "weeks").startOf("isoWeek"),
                        moment().subtract(1, "weeks").endOf("isoWeek"),
                    ],
                    // "Last 2 Weeks": [moment().subtract(14, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "Last 3 Months": [
                        moment().startOf("month").subtract(3, "month"),
                        moment().endOf("month"),
                    ],
                    "Last 6 Months": [
                        moment().startOf("month").subtract(6, "month"),
                        moment().endOf("month"),
                    ],
                    "This Year": [moment().startOf("year"), moment().endOf("year")],
                    "Last Year": [
                        moment().startOf("year").subtract(1, "year"),
                        moment().endOf("year").subtract(1, "year"),
                    ],
                    "Last 3 Year": [
                        moment().startOf("year").subtract(3, "year"),
                        moment().endOf("year"),
                    ],
                    "Last 6 Year": [
                        moment().startOf("year").subtract(6, "year"),
                        moment().endOf("year"),
                    ],
                    All: [new Date(2017, 0, 1, 0, 0, 0, 0), moment()],
                },
            },
            DateRangeFutureOptions: {
                locale: { format: "DD-MM-YYYY" },
                alwaysShowCalendars: false,
                autoUpdateInput: false,
                startDate: moment(),
                endDate: moment().endOf("month"),
                minDate: moment(),
                ranges: {
                    "This Week": [moment().startOf("week"), moment().endOf("week")],
                    "Next Week": [
                        moment().add(1, "weeks").startOf("isoWeek"),
                        moment().add(1, "weeks").endOf("isoWeek"),
                    ],
                    "Next 15 Days": [moment(), moment().add(14, "days")],
                    "Next 30 Days": [moment(), moment().add(29, "days")],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Next Month": [
                        moment().endOf("month").add(1, "days"),
                        moment().startOf("month").add(1, "month"),
                    ],
                },
            },
            DatePickerOptions: {
                singleDatePicker: true,
                locale: { format: "DD-MM-YYYY" },
                alwaysShowCalendars: false,
                showDropdowns: true,
                startDate: moment().startOf("day").subtract(1, "days"),
                endDate: moment().startOf("day").subtract(1, "days"),
                minYear: 1901,
                maxYear: parseInt(moment().format("YYYY"), 10),
                maxDate: moment().subtract(1, "days"),
            },
            ListToggleOption: {
                SortApply: "sortapply",
                SortOrder: "sortorder",
                CancelFilter: "cancelfilter",
                ApplyFilter: "applyfilter",
                SortReset: "sortreset",
                ToggleFilter: "togglefilter",
                Visiblity: "visiblity",
                Limit: "limit",
                Refresh: "refresh",
                ResetOffset: "resetoffset",
                Reset: "reset",
                Sort: "sort",
                Status: "status",
                Page: "page",
                Search: "search",
                Date: "date",
                Csv: "csv",
                SearchColumnChange: "searchcolumnchange",
                ColumnSearch: "columnsearch",
                MultiSelect: "multiselect",
                DateRange: "daterange",
                Other: "other"
            },
            FilterTypeOption: {
                Merchant: "bankMerchant",
                Terminal: "bankTerminal",
                Stores: "bankStores",
                Branch: "bankBranch",
                Manager: "bankManager",
                Campaign: "bankCampaign",
                MerchantSales: "bankMerchantSales",
            },
            OtherFilters: {
                Merchant: {
                    Owner: "owner"
                },
                Terminal: {
                    Merchant: "merchant",
                    Store: "store",
                    Provider: "provider"
                },
                Stores: {
                    Owner: "owner"
                },
                Branch: {},
                Manager: {
                    Branch: "branch",
                    Owner: "owner"
                },
                Campaign: {},
                MerchantSales: {
                    Merchant: "merchant",
                    Store: "store",
                    CardBank: "cardbank",
                    TransactionType: "transactiontype",
                    CardBrand: "cardbrand",
                    Provider: "provider"
                }
            },
            ValidatorRegex: {
                Password: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
                WebsiteUrl: "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?",
            },
            Storage: {
                OReqH: "hcscoreqh",
                Device: "hcscd",
                Location: "hcsl",
                Account: "hca",
                RegInfo: "reginfo",
                Permissions: "hcap",
                ImageStorage: "hcimgstorage",
                ImageStoragePoster: "hcimgstorageposter",
                ActiveAppUser: "hcactiveappuser",
                ActiveMerchant: "hcactivemerchant",
                ActiveCampaign: "hcactivecampaign",
                ActiveStore: "hcactivestore",
                ActiveBranch: "hcactivebranch",
                ActiveManager: "hcactivemanager",
                ActiveTerminal: "hcactiveterminal",
                ActivePgAccount: "hcactivepgaccount",
                ActivePosAccount: "hcactiveposaccount",
                ActiveAcquirer: "hcactiveacquirer",
            },
            HelperTypes: {
                Os: "hcore.os",
                Gender: "hcore.gender",
                Configuration: "hcore.configuration",
                ConfigurationValue: "hcore.configurationvalue",
                ResponseCode: "hcore.responsecodes",
                AccountType: "hcore.accounttype",
                AccountOperationType: "hcore.accountoperationtype",
                RegistrationSource: "hcore.regsource",
                App: "hcore.app",
                AppVersion: "hcore.appversion",
                Api: "hcore.api",
                Feature: "hcore.feature",
                FeatureOption: "hcore.featureoption",
                FeatureApi: "hcore.featureapi",
                Role: "hcore.role",
                RoleFeature: "hcore.rolefeatures",
                RoleAccess: "hcore.roleaccess",
                TransactionType: "hcore.transactiontype",
                TransactionTypeReward: "reward",
                TransactionTypeRedeem: "redeem",
                CardBrand: "cardbrand",
                CardSubBrand: "cardsubbrand",
                CardType: "cardtype",
                CardBank: "cardbank",
                MerchantCategories: "merchantcategories",
                SliderImage: "sliderimage",
                InvoiceType: "hcore.invoicetype",
            },
            DataType: {
                Text: "text",
                Number: "number",
                Date: "date",
                Decimal: "decimal",
            },
            SystemDataType: {
                Text: "datatype.text",
                Number: "datatype.number",
                Date: "datatype.date",
                Boolean: "datatype.boolean",
            },
            Gender: {
                Male: "gender.male",
                Female: "gender.female",
            },
            AccountBookmark: {
                NonBookmarkId: 407,
                NonBookmark: "accountlevel.nonbookmark",
                BookmarkId: 406,
                Bookmark: "accountlevel.bookmark",
            },
            AccountFlag: {
                GreenId: 400,
                Green: "accountflag.green",
                OrangeId: 401,
                Orange: "accountflag.orange",
                RedId: 402,
                Red: "accountflag.red",
                YellowId: 403,
                Yellow: "accountflag.yellow",
                BlackId: 404,
                Black: "accountflag.black",
                PurpleId: 405,
                Purple: "accountflag.purple",
            },
            AccountType: {
                Controller: "controller",
                ControllerId: 106,
                Admin: "admin",
                AppUser: "appuser",
                AppUserId: 109,
                CardUser: "carduser",
                Merchant: "thankumerchant",
                MerchantId: 108,
                Store: "merchantstore",
                StoreId: 111,
                Manager: "manager",
                Branch: "branch",
                Campaign: "campaign",
                StoreSubAccount: "merchantstoresubaccount",
                Cashier: "merchantcashier",
                PGAccount: "pgaccount",
                PosAccount: "posaccount",
                PosSubAccount: "possubaccount",
                PosTerminal: "posterminal",
                PosTerminalId: 115,
                Acquirer: "acquirer",
                AcquirerId: 116,
                AcquirerSubAccount: "acquirersubaccount",
                MerchantSubAccount: "merchantsubaccount",
                RelationshipManager: "relationshipmanager",
            },
            ListType: {
                All: "all",
                Owner: "owner",
                Branch: "branch",
                SubOwner: "subowner",
                Bank: "bank",
                CreatedBy: "createdby",
                CreatedByOwner: "createdbyowner",
                RM: "rm",
                Manager: "manager",
                Controller: "controller",
                Admin: "admin",
                AppUser: "appuser",
                Merchant: "thankumerchant",
                Store: "merchantstore",
                StoreSubAccount: "merchantstoresubaccount",
                Cashier: "merchantcashier",
                Provider: "provider",
                PosSubAccount: "possubaccount",
                PosTerminal: "posterminal",
                Acquirer: "acquirer",
                AcquirerSubAccount: "acquirersubaccount",
                MerchantSubAccount: "merchantsubaccount",
                RelationshipManager: "relationshipmanager",
            },
            AccountOperationType: {
                Online: "accountoperationtype.online",
                Offline: "accountoperationtype.offline",
                OnlineAndOffline: "accountoperationtype.onlineandoffline",
            },
            RegistrationSource: {
                System: "regsource.system",
            },
            Status: {
                Active: "default.active",
                Inactive: "default.inactive",
                Suspended: "default.suspended",
                Blocked: "default.blocked",
                Transaction: {
                    Success: "transaction.success",
                },
                Campaign: {
                    Creating: "campaign.creating",
                    UnderReview: "campaign.underreview",
                    Approved: "campaign.approved",
                    Rejected: "campaign.rejected",
                    Published: "campaign.published",
                    Paused: "campaign.paused",
                    LowBalance: "campaign.lowbalance",
                    Expired: "campaign.expired",
                    Archived: "campaign.archived",
                },
                WorkHorse: {
                    New: "workhorsestatus.new",
                    Acknowledge: "workhorsestatus.acknowledge",
                    Rejected: "workhorsestatus.rejected",
                    Approved: "workhorsestatus.approved",
                    Inprogress: "workhorsestatus.inprogress",
                    Finalizing: "workhorsestatus.finalizing",
                    BetaTesting: "workhorsestatus.betatesting",
                    ReadyToRelease: "workhosestatus.readytorelease",
                    Released: "workhorsestatus.released",
                    Cancelled: "workhorsestatus.cancelled",
                    Duplicate: "workhorsestatus.duplicate",
                },
            },
            WorkHorseType: {
                SystemUpdates: "workhorsetype.systemupdates",
                NewFeatureRequest: "workhorsetype.newfeaturerequest",
                UpdateFeatureRequest: "workhorsetype.updatefeaturerequest",
                SystemErrorRequest: "workhorsetype.systemerrorrequest",
                FeatureDocumentation: "workhorsetype.documentation.feature",
                ComponentDocument: "workhorsetype.documentation.component",
            },
            WorkHorseSubType: {
                workhorseitem: "workhorseitem",
                workhorsesubitem: "workhorsesubitem",
                workhorseactivity: "workhorseactivity",
            },
            WorkHorsePriority: {
                Low: "workhorsepriority.low",
                Medium: "workhorsepriority.medium",
                High: "workhorsepriority.high",
                RedAlert: "workhorseprioriy.redalert",
            },
            TransactionMode: {
                Credit: "transaction.mode.credit",
                Debit: "transaction.mode.debit",
            },
            TransactionSource: {
                Merchant: "transaction.source.merchant",
                Settlement: "transaction.source.settlement",
                GiftCard: "transaction.source.giftcards",
            },
            StatusList: {
                defaultitem: 0,
                default: [
                    {
                        id: 0,
                        text: "All Status",
                        code: "",
                    },
                    {
                        id: 1,
                        text: "Inactive",
                        code: "default.inactive",
                    },
                    {
                        id: 2,
                        text: "Active",
                        code: "default.active",
                    },
                    {
                        id: 3,
                        text: "Blocked",
                        code: "default.blocked",
                    },
                    {
                        id: 4,
                        text: "Suspended",
                        code: "default.suspended",
                    },
                ],
                transactiondefaultitem: 28,
                transaction: [
                    {
                        id: 0,
                        text: "All Status",
                        code: "",
                    },
                    {
                        id: 27,
                        text: "Processing",
                        code: "transaction.processing",
                    },
                    {
                        id: 28,
                        text: "Success",
                        code: "transaction.success",
                    },
                    {
                        id: 29,
                        text: "Pending",
                        code: "transaction.pending",
                    },
                    {
                        id: 30,
                        text: "Failed",
                        code: "transaction.failed",
                    },
                    {
                        id: 31,
                        text: "Cancelled",
                        code: "transaction.cancelled",
                    },
                    {
                        id: 32,
                        text: "Error",
                        code: "transaction.error",
                    },
                ],
                invoice: [
                    {
                        id: 0,
                        text: "All Status",
                        code: "",
                    },
                    {
                        id: 38,
                        text: "Pending",
                        code: "invoice.pending",
                    },
                    {
                        id: 39,
                        text: "Paid",
                        code: "invoice.paid",
                    },
                    {
                        id: 40,
                        text: "Cancelled",
                        code: "invoice.cancelled",
                    },
                ],
                workhorsestatus: [
                    {
                        id: 0,
                        text: "All Status",
                        code: "",
                    },
                    {
                        id: 415,
                        text: "New",
                        code: "workhorsestatus.new",
                    },
                    {
                        id: 416,
                        text: "Acknowledged",
                        code: "workhorsestatus.acknowledged",
                    },
                    {
                        id: 417,
                        text: "Rejected",
                        code: "workhorsestatus.rejected",
                    },
                    {
                        id: 418,
                        text: "Approved",
                        code: "workhorsestatus.approved",
                    },
                    {
                        id: 419,
                        text: "In progress",
                        code: "workhorsestatus.inprogress",
                    },
                    {
                        id: 420,
                        text: "Finalizing",
                        code: "workhorsestatus.finalizing",
                    },
                    {
                        id: 421,
                        text: "Beta Testing",
                        code: "workhorsestatus.betatesting",
                    },
                    {
                        id: 422,
                        text: "Ready To Release",
                        code: "workhorsestatus.readytorelease",
                    },
                    {
                        id: 423,
                        text: "Released",
                        code: "workhorsestatus.released",
                    },
                    {
                        id: 424,
                        text: "Cancelled",
                        code: "workhorsestatus.cancelled",
                    },
                    {
                        id: 437,
                        text: "Duplicate",
                        code: "workhorsestatus.duplicate",
                    },
                ],
                productcodestatus: [
                    {
                        id: 0,
                        text: "All Status",
                        code: "",
                    },
                    {
                        id: 461,
                        text: "Unused",
                        code: "productcodestatus.unused",
                    },
                    {
                        id: 463,
                        text: "Used",
                        code: "productcodestatus.used",
                    },
                    {
                        id: 462,
                        text: "Blocked",
                        code: "productcodestatus.blocked",
                    },
                    {
                        id: 464,
                        text: "Expired",
                        code: "productcodestatus.expired",
                    },
                ]
            },
            NetworkLocation: {
                V1: {
                    System: "api/v1/system/",
                    ThankU: "api/v1/thanku/",
                    acquirer: "api/v1/acquirer/",
                },
                V2: {
                    System: "api/v2/system/",
                    ThankU: "api/v2/thankucash/",
                    TUCTransCore: "api/v2/tuctranscore/",
                    TUCAccCore: "api/v2/tucacccore/",
                    TUCRmManager: "api/v2/rmmanager/",
                    ThankUCashLead: "api/v2/tucashlead/",
                    TUCCampaign: "api/v2/tuccampaign/",
                    TUCAnalytics: "api/v2/tucanalytics/",
                    TUCGiftPoints: "api/v2/tucgiftpoints/",
                    CAProduct: "api/v2/caproduct/",
                    HCWorkHorse: "api/v2/workhorse/",
                },
                V3: {
                    Account: "api/v3/acquirer/accounts/",
                    Branch: "api/v3/acquirer/branch/",
                    SubAccounts: "api/v3/acquirer/subaccounts/",
                    Analytics: "api/v3/acquirer/analytics/",
                },
            },
            Api: {
                Login: "login",
                Logout: "logout",
                ThankUCash: {
                    Analytics: {
                        getaccountrewardsummary: "getaccountrewardsummary",
                        getaccountrewardhistory: "getaccountrewardhistory",
                        getaccountsaleshistory: "getaccountsaleshistory",
                        getaccountsalessummary: "getaccountsalessummary",
                    },
                    GetAppUserVisits: "getappuservisits",
                    GetAppUsers: "getappusers",
                    GetAppUsersLite: "getappuserslite",
                    GetMerchantAppUsers: "getmerchantappusers",
                    GetMerchant: "getmerchant",
                    GetMerchants: "getmerchants",
                    GetTerminal: "getterminal",
                    GetStore: "getstore",
                    GetRelationshipManagers: "getrelationshipmanagers",
                    GetAcquirerMerchants: "getacquirermerchants",
                    GetStores: "getstores",
                    GetStoresLite: "getstoreslite",
                    GetStoreBankCollection: "getstorebankcollection",
                    GetCashiers: "getcashiers",
                    GetAcquirers: "getacquirers",
                    GetPgAccounts: "getpgaccounts",
                    GetPosAccounts: "getposaccounts",
                    GetPosTerminals: "posterminals",
                    GetTerminals: "getterminals",
                    getrewardprductlist: "getrewardprductlist",
                    GetSaleTransactions: "getsaletransactions",
                    GetTransactions: "gettransactions",
                    GetTUCRewardTransctions: "gettucrewardtransactions",
                    GetRewardTransctions: "getrewardtransactions",
                    GetRewardClaimTransctions: "getrewardclaimtransactions",
                    GetCampaignTransactions: "getcampaigntransactions",
                    GetTUCPlusRewardTransctions: "gettucplusrewardtransactions",
                    GetTUCPlusRewardTransferTransctions: "gettucplusrewardtransfertransactions",
                    GetRedeemTransactions: "getredeemtransactions",
                    GetGiftTransactions: "getgifttransactions",
                    GetChangeTransactions: "getchangetransactions",
                    GetPaymentTransactions: "getpaymenttransactions",
                    GetComissionTransactions: "getcommissiontransactions",
                    GetSalesTransactions: "getsalestransactions",
                    GetInvoices: "getsettlementinvoices",
                    SaveCampaign: "savecampaign",
                    UpdateCampaign: "updatecampaign",
                    DeleteCampaign: "deletecampaign",
                    GetCampaign: "getcampaign",
                    GetCampaigns: "getcampaigns",
                },
                Core: {
                    getcitybyregionarea: "getcitybyregionarea",
                    getcityareabycity: "getcityareabycity",
                    getregionsbycountry: "getregionsbycountry",
                    getsubadmins: "getsubadmins",
                    getsubadmindetails: "getsubadmindetails",
                    addsubadmin: "addsubadmin",
                    updatesubadmin: "updatesubadmin",
                    deletesubadmin: "deletesubadmin",
                    Register: "register",
                    SaveCoreCommon: "savecorecommon",
                    SaveCoreHelper: "savecorehelper",
                    SaveCoreParameter: "savecoreparameter",
                    SaveUserAccount: "saveuseraccount",
                    SaveUserParameter: "saveuserparameter",
                    UpdateCoreCommon: "updatecorecommon",
                    UpdateCoreHelper: "updatecorehelper",
                    UpdateCoreParameter: "updatecoreparameter",
                    UpdateUserAccount: "updateuseraccount",
                    UpdateUserAccountPassword: "updateuserpassword",
                    UpdateUserAccountAccessPin: "updateuseraccesspin",
                    UpdateUserParameter: "updateuserparameter",
                    DeleteCoreCommon: "deletecorecommon",
                    DeleteCoreHelper: "deletecorehelper",
                    DeleteCoreParameter: "deletecoreparameter",
                    DeleteUserAccount: "deleteuseraccount",
                    DeleteUserParameter: "deleteuserparameter",
                    UpdateUserSession: "updateusersession",
                    UpdateUserDevice: "updateuserdevice",
                    GetCoreHelper: "getcorehelper",
                    GetCoreHelpers: "getcorehelpers",
                    GetCoreHelpersLite: "getcorehelperslite",
                    GetCoreCommon: "getcorecommon",
                    GetCoreCommons: "getcorecommons",
                    GetCoreCommonsLite: "getcorecommonslite",
                    GetCoreParameter: "getcoreparameter",
                    GetCoreParameters: "getcoreparameters",
                    GetCoreLog: "getcorelog",
                    GetCoreLogs: "getcorelogs",
                    GetCoreUsage: "getcoreusage",
                    GetCoreUsages: "getcoreusages",
                    GetStorage: "getstorage",
                    GetStorages: "getstorages",
                    GetCoreVerification: "getcoreverification",
                    GetCoreVerifications: "getcoreverifications",
                    GetUserAccount: "getuseraccount",
                    GetUserAccounts: "getuseraccounts",
                    GetRelationshipManagers: "getrelationshipmanagers",
                    GetUserAccountsLite: "getuseraccountslite",
                    GetUserAccountOwner: "getuseraccountowner",
                    GetUserAccountOwners: "getuseraccountowners",
                    GetTransaction: "gettransaction",
                    GetTransactions: "gettransactions",
                    GetUserActivity: "getuseractivity",
                    GetUserActivities: "getuseractivities",
                    GetUserDevice: "getuserdevice",
                    GetUserDevices: "getuserdevices",
                    GetUserSession: "getusersession",
                    GetUserSessions: "getusersessions",
                    GetUserParameter: "getuserparameter",
                    GetUserParameters: "getuserparameters",
                    GetUserLocation: "getuserlocation",
                    GetUserLocations: "getuserlocations",
                    GetUserInvoice: "getuserinvoice",
                    GetUserInvoices: "getuserinvoices",
                    GetRMStores: "getrmstores",
                    UpdateStore: "updatestore",
                    GetRMTerminals: "getrmterminals",
                    GetManagers: "getmanagers",
                    SaveManager: "savemanager",
                    GetManger: "getmanager",
                    UpdateManager: "updatemanager",
                    SaveBranch: "savebranch",
                    GetBranch: "getbranch",
                    UpdateBranch: "updatebranch",
                    GetBranches: "getbranches",
                    SaveTerminal: "saveterminal",
                    GetSubAccounts: "getsubaccounts",
                    SaveSubAccount: "savesubaccount",
                    GetSubAccount: "getsubaccount",
                    UpdateSubAccount: "updatesubaccount",
                    SaveStore: "savestore",
                    SaveMerchant: "savemerchant",
                    ResetUserAccountPin: "resetuseraccesspin",
                },
                WorkHorse: {
                    saveworkhorse: "saveworkhorse",
                    updateworkhorse: "updateworkhorse",
                    getworkhorse: "getworkhorse",
                    getworkhorseitems: "getworkhorseitems",
                    getworkhorsesubitems: "getworkhorsesubitems",
                    getworkhorseactivities: "getworkhorseactivities",
                },
                Product: {
                    updateproductrewardpercentage: "updateproductrewardpercentage",
                    getproductbalance: "getproductbalance",
                    creditproductaccount: "creditproductaccount",
                    saveproduct: "saveproduct",
                    createquickgiftcard: "createquickgiftcard",
                    getproductcodes: "getproductcodes",
                    getproducts: "getproducts",
                    creategiftcard: "creategiftcard",
                    creategiftcardcode: "creategiftcardcode",
                },
            },
            Pages: {
                ThankUCash: {
                    Dashboard: {
                        PTSP: "/provider/dashboard",
                        Merchant: "/m/dashboard",
                        MerchantSales: "/m/salesdashboard",
                        MerchantRewards: "/m/rewardsdashboard",
                        Acquirer: "/acquirer/dashboard",
                        Rm: "/rm/dashboard",
                        Store: "/store/dashboard",
                        Console: "/system/dashboard",
                    },
                    PTSP: {
                        Terminals: "/provider/terminals",
                        Merchants: "/provider/merchants",
                        RewardHistory: "/provider/rewardhistory",
                        RedeemHistory: "/provider/redeemhistory",
                        CommissionHistory: "/provider/commissionhistory",
                        RequestHistory: "/provider/requesthistory",
                        Profile: "/provider/profile",
                    },
                    PanelConsole: {
                        Profile: "/system/profile",
                        LiveTerminals: "/system/liveterminals",
                        Merchants: "/system/accounts/merchants",
                        Stores: "/system/accounts/stores",
                        Terminals: "/system/accounts/terminals",
                        Acquirers: "/system/accounts/acquirers",
                        Customers: "/system/accounts/customers",
                        SuspendedCustomers: "/system/accounts/suspendedcustomers",
                        BlockedCustomers: "/system/accounts/blockedcustomers",
                        PgAccounts: "/system/accounts/gatewayproviders",
                        PosAccounts: "/system/accounts/terminalprovider",
                        Cashier: "/system/account/cashier",
                        Transactions: "/system/transactions/all",
                        Reward: "/system/transactions/rewardhistory",
                        RewardClaim: "/system/transactions/rewardclaimhistory",
                        Redeem: "/system/transactions/redeemhistory",
                        Sale: "/system/sales/salehistory",
                        Downloads: "/system/downloads",
                        AppUser: {
                            Dashboard: "/system/account/customer/dashboard",
                            Reward: "/system/account/customer/transactions/rewardhistory",
                            RewardClaim: "/system/account/customer/transactions/rewardclaimhistory",
                            Redeem: "/system/account/customer/transactions/redeemhistory",
                            Sale: "/system/account/customer/sales/salehistory",
                            ManageAccount: "/system/account/customer/manage",
                        },
                        Merchant: {
                            Dashboard: "/system/account/merchant/dashboard",
                            Dashboards: {
                                TodaysOverview: "/system/account/merchant/todaysoverview",
                                Sales: "/system/account/merchant/salesdashboard",
                                Rewards: "/system/account/merchant/rewardsdashboard",
                            },
                            // ManageAccount: "/system/account/merchant/manage",
                            ManageAccount: {
                                Overview: "/system/account/merchantmanager/overview",
                                BusinessProfile: "/system/account/merchantmanager/businessprofile",
                                Account: "/system/account/merchantmanager/account",
                                Terminals: "/system/account/merchantmanager/terminals",
                                Stores: "/system/account/merchantmanager/stores",
                                SubAccounts: "/system/account/merchantmanager/managers",
                                Cashiers: "/system/account/merchantmanager/cashiers",
                                Configurations: "/system/account/merchantmanager/configurations",
                                Products: "/system/account/merchantmanager/products",
                            },
                            SubAccounts: {
                                Customers: "/system/account/merchant/customers",
                                Stores: "/system/account/merchant/stores",
                                Terminals: "/system/account/merchant/terminals",
                                Managers: "/system/account/merchant/managers",
                                Cashiers: "/system/account/merchant/cashiers",
                            },
                            GiftPoints: "/system/account/merchant/transactions/giftpoints",
                            Product: {
                                GiftCards: {
                                    QuickGiftCards: "/system/account/merchant/product/guickgiftcards",
                                    AddBulkGiftCards: "/system/account/merchant/product/creategiftcard",
                                    Wallet: "/system/account/merchant/product/giftcards/wallet",
                                    Manage: "/system/account/merchant/product/giftcards/manage",
                                    Codes: "/system/account/merchant/product/giftcards/saleshistory",
                                    GiftCards: "/system/account/merchant/product/giftcards",
                                },
                            },
                            Reward: "/system/account/merchant/transactions/rewardhistory",
                            RewardClaim: "/system/account/merchant/transactions/rewardclaimhistory",
                            Redeem: "/system/account/merchant/transactions/redeemhistory",
                            Sale: "/system/account/merchant/sales/salehistory",
                        },
                        Store: {
                            Dashboard: "/system/account/store/dashboard",
                            ManageAccount: "/system/account/store/manage",
                            Reward: "/system/account/store/transactions/rewardhistory",
                            RewardClaim: "/system/account/store/transactions/rewardclaimhistory",
                            Redeem: "/system/account/store/transactions/redeemhistory",
                            Sale: "/system/account/store/sales/salehistory",
                            SubAccounts: {
                                Customers: "/system/account/store/customers",
                                Terminals: "/system/account/store/terminals",
                            },
                        },
                        Acquirer: {
                            Dashboard: "/system/account/acquirer/dashboard",
                            ManageAccount: "/system/account/acquirer/manage",
                            Reward: "/system/account/acquirer/transactions/rewardhistory",
                            RewardClaim: "/system/account/acquirer/transactions/rewardclaimhistory",
                            Redeem: "/system/account/acquirer/transactions/redeemhistory",
                            Sale: "/system/account/acquirer/sales/salehistory",
                            SubAccounts: {
                                Terminals: "/system/account/acquirer/terminals",
                            },
                        },
                        PgAccount: {
                            Dashboard: "/system/account/gatewayprovider/dashboard",
                            ManageAccount: "/system/account/gatewayprovider/manage",
                            Reward: "/system/account/gatewayprovider/transactions/rewardhistory",
                            RewardClaim: "/system/account/gatewayprovider/transactions/rewardclaimhistory",
                            Redeem: "/system/account/gatewayprovider/transactions/redeemhistory",
                            Sale: "/system/account/gatewayprovider/sales/salehistory",
                        },
                        PosAccount: {
                            Dashboard: "/system/account/terminalprovider/dashboard",
                            ManageAccount: "/system/account/terminalprovider/manage",
                            Reward: "/system/account/terminalprovider/transactions/rewardhistory",
                            RewardClaim: "/system/account/terminalprovider/transactions/rewardclaimhistory",
                            Redeem: "/system/account/terminalprovider/transactions/redeemhistory",
                            Sale: "/system/account/terminalprovider/sales/salehistory",
                            SubAccounts: {
                                Terminals: "/system/account/terminalprovider/terminals",
                            },
                        },
                        Terminal: {
                            Dashboard: "/system/account/terminal/dashboard",
                            ManageAccount: "/system/account/terminal/manage",
                            Reward: "/system/account/terminal/transactions/rewardhistory",
                            RewardClaim: "/system/account/terminal/transactions/rewardclaimhistory",
                            Redeem: "/system/account/terminal/transactions/redeemhistory",
                            Sale: "/system/account/terminal/sales/salehistory",
                            SubAccounts: {
                                Customers: "/system/account/terminal/customers",
                            },
                        },
                        UserOnboarding: {
                            Merchant: "/system/onboarding/merchant",
                            PendingMerchant: "/system/onboarding/pendingmerchantonboarding",
                            PendingMerchantManager: "/system/onboarding/pendingmerchantmanager",
                            Customer: "/system/onboarding/customer",
                            Acquirer: "/system/onboarding/acquirer",
                            PgAccount: "/system/onboarding/gatewayprovider",
                            PosAccount: "/system/onboarding/terminalprovider",
                        },
                        Invoices: {
                            All: "/system/invoices",
                            Details: "/system/invoice",
                        },
                        Analytics: {
                            Home: "/system/analytics/home",
                            TodaysOverview: "/system/analytics/todaysoverview",
                            SystemOverview: "/system/analytics/systemoverview",
                            GrowthMap: "/system/analytics/growthmap",
                            CustomerAnalytics: "/system/analytics/customeranalytics",
                            Rewards: {
                                DailyRewards: {
                                    Merchants: "/system/analytics/dailyrewards/merchants",
                                    Stores: "/system/analytics/dailyrewards/stores",
                                    Terminals: "/system/analytics/dailyrewards/terminals",
                                },
                                RewardsSummary: {
                                    Merchants: "/system/analytics/rewardssummary/merchants",
                                    Stores: "/system/analytics/rewardssummary/stores",
                                    Terminals: "/system/analytics/rewardssummary/terminals",
                                },
                            },
                            Sales: {
                                // DailySales: "/system/analytics/dailysales",
                                DailySales: {
                                    Merchants: "/system/analytics/dailysales/merchants",
                                    Stores: "/system/analytics/dailysales/stores",
                                    Terminals: "/system/analytics/dailysales/terminals",
                                },
                                SalesSummary: {
                                    Merchants: "/system/analytics/salessummary/merchants",
                                    Stores: "/system/analytics/salessummary/stores",
                                    Terminals: "/system/analytics/salessummary/terminals",
                                },
                            },
                        },
                        Administration: {
                            AppSlider: "/system/control/appslider",
                            AdministrationHome: "/system/control/administration",
                            ApiRequestHistory: "/system/control/apirequesthistory",
                            Categories: "/system/control/categories",
                            Roles: "/system/control/roles",
                            AdminFeatures: "/system/control/features",
                            ResponseCodes: "/system/control/responsecodes",
                            SystemLog: "/system/control/systemlog",
                            Apps: "/system/control/apps",
                            Configurations: "/system/control/configurations",
                            ConfigurationManager: "/system/control/configurationmanager",
                            Verifications: "/system/control/verifications",
                            UserSessions: "/system/control/usersessions",
                            AdminUsers: "/system/control/adminusers",
                            AddAdminUsers: "/system/control/addadminuser",
                            AdminUser: {
                                Dashboard: "/system/control/adminuser/dashboard",
                                Sessions: "/system/control/adminuser/loginhistory",
                            },
                            WorkHorse: {
                                SystemUpdates: "/system/control/workhorse/systemupdates",
                                UpdateFeatureRequests: "/system/control/workhorse/updatefeaturerequests",
                            },
                        },
                    },
                    PanelAcquirer: {
                        SalesHistory: "/acquirer/saleshistory",
                        Activity: "/acquirer/activity",
                        SalesReport: "/acquirer/salesreport",
                        RMReport: "/acquirer/rmreports",
                        SubAccounts: "/acquirer/subaccounts",
                        Terminals: "/acquirer/terminals",
                        Campaigns: {
                            Dashboard: "/acquirer/campaign/dashboard",
                            Campaign: "/acquirer/campaign",
                            Campaigns: "/acquirer/campaigns",
                            AddCampaign: "/acquirer/addcampaign",
                            Sales: "/acquirer/campaign/sales/saleshistory",
                        },
                        Accounts: {
                            Branch: "/acquirer/branch",
                            Rms: "/acquirer/rms",
                            Subadmins: "/acquirer/subadmins",
                        },
                        AddAccount: {
                            AddRm: "/acquirer/addrm",
                            AddBranch: "/acquirer/addbranch",
                        },
                        AccountDetails: {
                            BranchDetails: "/acquirer/branchdetails",
                            Rm: "/acquirer/rm",
                            SubAdminDetails: "/acquirer/subadmindetails",
                            Branch: "/acquirer/branchs",
                        },
                        Branches: {
                            BranchUpdate: "/acquirer/branchdetails/branchupdate",
                            Manager: "/acquirer/branchdetails/manager",
                            EditBranch: "/acquirer/branchdetails/editbranch",
                            BranchTransactions: "/acquirer/branchdetails/sales",
                            BranchTerminals: "/acquirer/branchdetails/terminals"
                        },
                        Analytics: {
                            Sales: "/acquirer/salesanalytics",
                        },
                        Live: {
                            Terminals: "/acquirer/live/terminals",
                            Merchants: "/acquirer/live/merchants",
                        },
                        Manager: {
                            Dashboard: "/acquirer/manager/dashboard",
                            // RewardHistory: "/acquirer/terminal/rewardhistory",
                            // RewardClaimHistory: "/acquirer/terminal/rewardclaimhistory",
                            // RedeemHistory: "/acquirer/terminal/redeemhistory",
                            Terminals: "/acquirer/manager/terminals",
                            Stores: "/acquirer/branchdetails/stores",
                            SalesHistory: "/acquirer/branchdetails/sales",
                        },
                        ManagerDetail: {
                            Overview: "/acquirer/manager/dashboard",
                            Terminals: "/acquirer/manager/terminals",
                            Stores: "/acquirer/manager/stores",
                            Teams: "/acquirer/manager/teams",
                        },
                        Terminal: {
                            Dashboard: "/acquirer/terminal/dashboard",
                            // RewardHistory: "/acquirer/terminal/rewardhistory",
                            // RewardClaimHistory: "/acquirer/terminal/rewardclaimhistory",
                            // RedeemHistory: "/acquirer/terminal/redeemhistory",
                            SalesHistory: "/acquirer/terminal/sales/saleshistory",
                        },
                        // LiveTerminals: "/acquirer/liveterminals",
                        // Live: "/acquirer/live",
                        Customers: "/acquirer/customers",
                        Customer: "/acquirer/customer/dashboard",
                        ReferredMerchants: "/acquirer/referredmerchants",
                        AddTerminal: "/acquirer/addterminal",
                        TerminalsSale: "/acquirer/terminalssale",
                        Store: {
                            Dashboard: "/acquirer/store/dashboard",
                            SalesHistory: "/acquirer/store/saleshistory",
                            Terminals: "/acquirer/store/terminals",
                        },
                        Merchant: {
                            Dashboard: "/acquirer/merchant/dashboard",
                            // SalesHistory: "/acquirer/merchant/saleshistory",
                            Stores: "/acquirer/merchant/stores",
                            Terminals: "/acquirer/merchant/terminals",
                            SalesHistory: "/acquirer/merchant/sales/saleshistory",
                        },
                        Merchants: "/acquirer/merchants",
                        CustomerUpload: "/acquirer/customerupload",
                        CustomerOnboarding: "/acquirer/customeronboarding",
                        MerchantOnboarding: "/acquirer/merchantonboarding",
                        MerchantUpload: "/acquirer/merchantupload",
                        MerchantManager: "/acquirer/merchantmanager",
                        CustomerRewardHistory: "/acquirer/customer/rewardhistory",
                        CustomerRedeemHistory: "/acquirer/customer/redeemhistory",
                        Stores: "/acquirer/stores",
                        Temp: "/acquirer/temp",
                        AppUser: {
                            Dashboard: "/acquirer/customer/dashboard",
                            UpdateStore: "/acquirer/customer/updatestore",
                            RewardHistory: "/acquirer/customer/rewardhistory",
                            TUCPlusRewardHistory: "/acquirer/customer/tucplusrewardhistory",
                            TUCPlusRewardClaimHistory: "/acquirer/customer/tucplusrewardclaimhistory",
                            RedeemHistory: "/acquirer/customer/redeemhistory",
                        },
                        TerminalRewardHistory: "/acquirer/terminal/rewardhistory",
                        TerminalRedeemHistory: "/acquirer/terminal/redeemhistory",
                        Pssp: "/acquirer/gateways",
                        // PaymentsHistory: "/acquirer/paymentshistory",
                        TUCPlusRewardHistory: "/acquirer/tucplusrewardhistory",
                        TUCPlusRewardClaimHistory: "/acquirer/tucplusrewardclaimhistory",
                        RewardHistory: "/acquirer/rewardhistory",
                        RedeemHistory: "/acquirer/redeemhistory",
                        CommissionHistory: "/acquirer/commissionhistory",
                        DailyCommissionHistory: "/acquirer/dailycommissionhistory",
                        StoreSale: "/acquirer/storesale",
                        Profile: "/acquirer/profile",
                        RewardInvoices: "/acquirer/rewardinvoices",
                        RedeemInvoices: "/acquirer/redeeminvoices",
                    },
                    PanelRm: {
                        Live: {
                            Terminals: "/rm/live/terminals",
                        },
                        Accounts: {
                            Terminals: "/rm/terminals",
                            Stores: "/rm/stores",
                        },
                    },
                    PanelMerchant: {
                        Invoices: "/m/invoices",
                        Invoice: "/m/invoice",
                        Cashiers: "/m/cashiers",
                        Cashier: "/m/cashier",
                        Managers: "/m/managers",
                        Stores: "/m/stores",
                        Terminals: "/m/terminals",
                        Customers: "/m/customers",
                        GiftPoints: "/m/giftpoints",
                        GiftCards: "/m/giftcards",
                        Products: "/m/products",
                        Downloads: "/m/downloads",
                        Profile: "/m/profile",
                        Analytics: {
                            StoresTucSummary: "/m/analytics/storestucsummary",
                            Live: "/m/live",
                            TerminalLive: "/m/analytics/terminalstracker",
                            DailySales: "/m/analytics/dailysales",
                            StoreBankCollection: "/m/analytics/bankcollections",
                        },
                        Transactions: {
                            Sales: "/m/transactions/saleshistory",
                            Rewards: "/m/transactions/rewards",
                            RewardClaims: "/m/transactions/rewardclaims",
                            Redeem: "/m/transactions/redeem",
                        },
                        Store: {
                            Dashboard: "/m/store/dashboard",
                            Terminals: "/m/store/terminals",
                            RewardHistory: "/m/store/transactions/rewardhistory",
                            RewardClaimHistory: "/m/store/transactions/rewardclaimhistory",
                            RedeemHistory: "/m/store/transactions/redeemhistory",
                            SalesHistory: "/m/store/sales/salehistory",
                        },
                        Terminal: {
                            Dashboard: "/m/terminal/dashboard",
                            RewardHistory: "/m/terminal/transactions/rewardhistory",
                            RewardClaimHistory: "/m/terminal/transactions/rewardclaimhistory",
                            RedeemHistory: "/m/terminal/transactions/redeemhistory",
                            SalesHistory: "/m/terminal/sales/salehistory",
                        },
                        Customer: {
                            Dashboard: "/m/customer/dashboard",
                            RewardHistory: "/m/customer/transactions/rewardhistory",
                            RewardClaimHistory: "/m/customer/transactions/rewardclaimhistory",
                            RedeemHistory: "/m/customer/transactions/redeemhistory",
                            SalesHistory: "/m/customer/sales/salehistory",
                        },
                        // Customer: "/merchant/customer/dashboard",
                        SubAccounts: "/merchant/subaccounts",
                        RewardHistory: "/merchant/rewardhistory",
                        RewardClaimHistory: "/merchant/rewardclaimhistory",
                        RedeemHistory: "/merchant/redeemhistory",
                        SalesHistory: "/merchant/saleshistory",
                        AppUser: {
                            Dashboard: "/merchant/customer/dashboard",
                            RewardHistory: "/merchant/customer/rewardhistory",
                            RewardClaimHistory: "/merchant/customer/rewardclaimhistory",
                            RedeemHistory: "/merchant/customer/redeemhistory",
                            SalesHistory: "/merchant/customer/saleshistory",
                        },
                        Pssp: "/merchant/gateways",
                        CommissionHistory: "/merchant/commissionhistory",
                        StoreSale: "/merchant/storesale",
                        // Profile: "/merchant/profile",
                        RewardInvoices: "/merchant/rewardinvoices",
                        RedeemInvoices: "/merchant/redeeminvoices",
                    },
                    PanelMerchantRewards: {
                        Invoices: "/mr/invoices",
                        Invoice: "/mr/invoice",
                        Cashiers: "/mr/cashiers",
                        Cashier: "/mr/cashier",
                        Managers: "/mr/managers",
                        Transactions: {
                            Sales: "/mr/transactions/saleshistory",
                        },
                        Stores: "/mr/stores",
                        Store: {
                            Dashboard: "/merchant/store/dashboard",
                            Terminals: "/merchant/store/terminals",
                            RewardHistory: "/merchant/store/rewardhistory",
                            RewardClaimHistory: "/merchant/store/rewardclaimhistory",
                            RedeemHistory: "/merchant/store/redeemhistory",
                            SalesHistory: "/merchant/store/saleshistory",
                        },
                        Terminals: "/m/terminals",
                        Terminal: {
                            Dashboard: "/merchant/terminal/dashboard",
                            RewardHistory: "/merchant/terminal/rewardhistory",
                            RewardClaimHistory: "/merchant/terminal/rewardclaimhistory",
                            RedeemHistory: "/merchant/terminal/redeemhistory",
                            SalesHistory: "/merchant/terminal/saleshistory",
                        },
                        Live: "/merchant/live",
                        TerminalLive: "/merchant/terminallive",
                        Customers: "/merchant/customers",
                        Customer: "/merchant/customer/dashboard",
                        SubAccounts: "/merchant/subaccounts",
                        RewardHistory: "/merchant/rewardhistory",
                        RewardClaimHistory: "/merchant/rewardclaimhistory",
                        RedeemHistory: "/merchant/redeemhistory",
                        SalesHistory: "/merchant/saleshistory",
                        StoreBankCollection: "/merchant/storebankcollection",
                        AppUser: {
                            Dashboard: "/merchant/customer/dashboard",
                            RewardHistory: "/merchant/customer/rewardhistory",
                            RewardClaimHistory: "/merchant/customer/rewardclaimhistory",
                            RedeemHistory: "/merchant/customer/redeemhistory",
                            SalesHistory: "/merchant/customer/saleshistory",
                        },
                        Pssp: "/merchant/gateways",
                        CommissionHistory: "/merchant/commissionhistory",
                        StoreSale: "/merchant/storesale",
                        Profile: "/merchant/profile",
                        RewardInvoices: "/merchant/rewardinvoices",
                        RedeemInvoices: "/merchant/redeeminvoices",
                    },
                    Customers: "/system/customers",
                    AppUsers: "/system/appusers",
                    AppUser: {
                        Dashboard: "/system/appuser/dashboard",
                    },
                    AddMerchant: "/system/addmerchant",
                    Merchants: "/system/merchants",
                    Merchant: {
                        Dashboard: "/system/merchant/dashboard",
                    },
                    Stores: "/system/stores",
                    AddStore: "/system/addstore",
                    Store: {
                        Dashboard: "/system/store/dashboard",
                        UpdateStore: "/system/store/updatestore",
                    },
                    AddCashier: "/system/addcashier",
                    Cashiers: "/system/cashiers",
                    Cashier: {
                        Dashboard: "/system/cashier/dashboard",
                    },
                    AddAcquirer: "/system/addacquirer",
                    Acquirers: "/system/acquirers",
                    Acquirer: {
                        Dashboard: "/system/acquirer/dashboard",
                    },
                    AddPgAccount: "/system/addpgaccount",
                    PgAccounts: "/system/pgaccounts",
                    PgAccount: {
                        Dashboard: "/system/pgaccount/dashboard",
                    },
                    AddPosAccount: "/system/addposaccount",
                    PosAccounts: "/system/posaccounts",
                    PosAccount: {
                        Dashboard: "/provider/terminals",
                    },
                    AddPosTerminal: "/system/addposterminal",
                    PosTerminals: "/system/posterminals",
                    PosTerminal: {
                        Dashboard: "/system/posterminal/dashboard",
                    },
                    Transactions: {
                        Reward: "/system/transactions/reward",
                        Redeem: "/system/transactions/redeem",
                        Change: "/system/transactions/change",
                        Gift: "/system/transactions/gift",
                        Payments: "/system/transactions/payment",
                    },
                },
                System: {
                    NotFound: "/system/notfound",
                    Login: "/account/login",
                    MerchantSetup: "/account/setup",
                    MerchantSetupComplete: "/account/setupcomplete",
                    MerchantRewardSetup: "/account/reward",
                    ForgotPassword: "/account/forgotpassword",
                    Dashboard: "/system/dashboard",
                    Roles: "/system/roles",
                    Features: "/system/features",
                    Categories: "/system/categories",
                    ResponseCodes: "/system/responsecodes",
                    Log: "/system/log",
                    Apps: "/system/apps",
                    CoreUsage: "/system/usage",
                    Configurations: "/system/configurations",
                    Profile: "/system/profile",
                    CoreTransactions: "/system/coretransactions",
                    CoreUsers: "/system/coreusers",
                    EditCoreUser: "/system/editcoreuser",
                    AddCoreHelpers: "/system/addcorehelpers",
                    EditCoreHelpers: "/system/editcorehelpers",
                    CoreHelpers: "/system/corehelpers",
                    CoreCommons: "/system/corecommons",
                    CoreParameters: "/system/coreparameters",
                },
            },
            CommonResource: {
                SaveFilterTitle: "Save This Filter View",
                SaveFilterHelp: "Enter Filter Name",
                LogoutTitle: "Are You Sure You want to Logout",
                ReferenceId: "Reference Id",
                ReferenceKey: " Reference Key",
                ReferenceCode: " Reference Code",
                SystemName: " Reference Code",
                Name: "Name",
                Select: "Select",
                Cancel: "Cancel",
                Continue: "Continue",
                Status: "Status",
                CreateDate: "Added On",
                CreatedBy: "Added By",
                ModifyDate: "Updated On",
                ModifyBy: "Updated By",
                AccessPin: "4 digit pin",
                FilterName: "filter name",
                UpdateTitle: "Update details ?",
                UpdateHelp: "Enter 4 digit pin to update details",
                DeleteTitle: "Delete details ?",
                DeleteHelp: "Details can not be recovered once deleted. Do you want to continue ?",
                InvalidImage: "Invalid image dimension. Please select valid image",
            },
        };
        this._Assets = {
            Box: {
                root: "card shadow-none",
                header: "card-header d-flex pd-y-15  align-items-center justify-content-between",
                headerTitle: "h6  m-0 mt-1 mb-1",
                filterIcons: "tx-12",
                headerTitleRight: "d-flex",
                headerTitleRightItem: "link-03 lh-0 mg-l-10",
                bodyFilter: "card-body pd-y-10",
                bodyContent: "card-body p-0",
                ShowIcon: "wd-35 ht-35 align-items-center justify-content-center op-6 d-none d-sm-flex",
                TextCenter: "text-center",
            },
        };
        this._FileSelect_Poster = this.AppConfig.Storage.ImageStoragePoster;
        this._FileSelect_Poster_Data = {
            Name: null,
            Content: null,
            Extension: null,
            TypeCode: null,
            Height: 400,
            Width: 800,
        };
        this._FileSelect_Poster_Processing = false;
        this._Icon_Cropper_ChangedEvent = "";
        this._Icon_Cropper_Image = "";
        this._Icon_Cropper_Data = {
            Name: null,
            Content: null,
            Extension: null,
            TypeCode: null,
            Height: 400,
            Width: 800,
        };
        this._Poster_Cropper_ChangedEvent = "";
        this._Poster_Cropper_Image = "";
        this._Poster_Cropper_Data = {
            Name: null,
            Content: null,
            Extension: null,
            TypeCode: null,
            Height: 450,
            Width: 800,
        };
        //#region DateRange
        this.RangeAltered = new rxjs__WEBPACK_IMPORTED_MODULE_13__["BehaviorSubject"](false);
        this.FilterSnapPrev = {};
        this._RefreshUI = true;
        //#endregion
        this._FileSelect_Icon_Data = {
            Name: null,
            Content: null,
            Extension: null,
            TypeCode: null,
            Height: 128,
            Width: 128,
        };
        this._FileSelect_Icon = this.AppConfig.Storage.ImageStorage;
        this._FileSelect_Icon_Processing = false;
        this._OSalesHistory = {
            Labels: [],
            SaleColors: [],
            SaleDataSet: [],
            SaleCustomersColors: [],
            SaleCustomersDataSet: [],
            TransactionStatusDataSetColors: [],
            TransactionStatusDataSet: [],
            TransactionTypeDataSetColors: [],
            TransactionTypeDataSet: [],
            TransactionTypeCustomersDataSetColors: [],
            TransactionTypeCustomersDataSet: [],
        };
        this.IconData = {
            url: "assets/img/map-store-marker.png",
            scaledSize: {
                height: 26,
                width: 26,
            },
        };
        this.ChartConfig = {
            SalesTypeColors: [
                {
                    backgroundColor: ["#FFC20A", "#10b759"],
                },
            ],
            SalesTypeCustomersColors: [
                {
                    backgroundColor: ["#00aff0", "#990099"],
                },
            ],
            TransactionStatusDataSetColors: [
                {
                    backgroundColor: ["#61c03c"],
                },
                {
                    backgroundColor: ["#dc3912"],
                },
            ],
            TransactionStatusColors: [
                {
                    backgroundColor: ["#00cccc", "#f10075"],
                },
            ],
            CardTypeColors: [
                {
                    backgroundColor: ["#ffc107", "#00cccc", "#f10075", "#0168fa"],
                },
            ],
            Plugins: [chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_7__],
            DoughnetChart: {
                Type: "doughnut",
                Options: {
                    responsive: true,
                    plugins: {
                        datalabels: {
                            backgroundColor: "#fff",
                            color: "#000000bf",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "center",
                            align: "center",
                            padding: 2,
                            opacity: 0.9,
                            display: true,
                            font: {
                                size: 10,
                                weight: 500,
                            },
                            formatter: function (value, ctx) {
                                var sum = 0;
                                var dataArr = ctx.chart.data.datasets[0].data;
                                dataArr.map(function (data) {
                                    sum += data;
                                });
                                var percentage = ((value * 100) / sum).toFixed(2) + "%";
                                // return percentage;
                                var label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return label + "\n" + percentage;
                                    // return label;
                                }
                                else {
                                    return value;
                                }
                            },
                        },
                    },
                    legend: {
                        display: false,
                        position: "right",
                    },
                    legendCallback: function (chart) {
                        // Return the HTML string here.
                    },
                },
            },
            LineChart: {
                layout: {
                    padding: {
                        top: 16,
                    },
                },
                Type: "line",
                Options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: "bottom",
                    },
                    ticks: {
                        autoSkip: false,
                    },
                    scales: {
                        xAxes: [
                            {
                                // categoryPercentage: 0.98,
                                // barPercentage: 0.98,
                                maxBarThickness: 50,
                                categoryPercentage: 0.4,
                                barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false,
                                },
                                ticks: {
                                    autoSkip: false,
                                    fontSize: 11,
                                },
                            },
                        ],
                        yAxes: [
                            {
                                maxBarThickness: 50,
                                categoryPercentage: 0.4,
                                barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false,
                                },
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 11,
                                },
                            },
                        ],
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: "#ffffff47",
                            color: "#798086",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "end",
                            align: "end",
                            padding: 2,
                            font: {
                                size: 9,
                                weight: 400,
                            },
                            formatter: function (value, ctx) {
                                var label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                                else {
                                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            },
                        },
                    },
                },
            },
            BarChart: {
                layout: {
                    padding: {
                        top: 16,
                    },
                },
                Type: "bar",
                Options: {
                    cornerRadius: 20,
                    responsive: true,
                    legend: {
                        display: false,
                        position: "right",
                    },
                    ticks: {
                        autoSkip: false,
                    },
                    scales: {
                        xAxes: [
                            {
                                // stacked: true,
                                // categoryPercentage: 0.98,
                                // barPercentage: 0.98,
                                // maxBarThickness: 30,
                                // categoryPercentage: 0.4,
                                // barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false,
                                },
                                ticks: {
                                    autoSkip: false,
                                    fontSize: 11,
                                },
                            },
                        ],
                        yAxes: [
                            {
                                // maxBarThickness: 30,
                                // categoryPercentage: 0.4,
                                // barPercentage: 0.4,
                                // stacked: true,
                                gridLines: {
                                    stacked: true,
                                    display: false,
                                },
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 11,
                                },
                            },
                        ],
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: "#ffffff47",
                            color: "#798086",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "end",
                            align: "end",
                            padding: 2,
                            font: {
                                size: 10,
                                weight: 500,
                            },
                            formatter: function (value, ctx) {
                                var label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return value;
                                }
                                else {
                                    return value;
                                }
                            },
                        },
                    },
                },
            },
        };
        this._UserOverviewPlot = {
            PieChartPlugins: [chartjs_plugin_datalabels__WEBPACK_IMPORTED_MODULE_7__],
            StartTime: null,
            EndTime: null,
            GuageLight: {
                Type: "arch",
                Append: "%",
                Cap: "round",
                Thick: 6,
                Size: 55,
                Color: "#ffffff",
            },
            PlotChartHeight: 95,
            ChartTypeDoughnet: "doughnut",
            ChartTypeBar: "bar",
            ChartTypeBarHorizontal: "horizontalBar",
            ChartTypeBarHOptions: {
                layout: {
                    padding: {
                        right: 16,
                    },
                },
                responsive: true,
                legend: {
                    display: false,
                },
                ticks: {
                    autoSkip: false,
                },
                scales: {
                    xAxes: [
                        {
                            categoryPercentage: 0.1,
                            barPercentage: 0.1,
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                            ticks: {
                                autoSkip: false,
                                fontSize: 11,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                        },
                    ],
                },
                plugins: {
                    datalabels: {
                        backgroundColor: "transparent",
                        color: "#000000bf",
                        borderRadius: "2",
                        borderWidth: "1",
                        borderColor: "transparent",
                        anchor: "end",
                        align: "end",
                        padding: 2,
                        font: {
                            size: 10,
                            weight: 500,
                        },
                        formatter: function (value, ctx) {
                            var label = ctx.chart.data.labels[ctx.dataIndex];
                            if (label != undefined) {
                                return value;
                            }
                            else {
                                return value;
                            }
                        },
                    },
                },
            },
            ChartTypeBarOptions: {
                layout: {
                    padding: {
                        top: 12,
                    },
                },
                responsive: true,
                legend: {
                    display: false,
                },
                ticks: {
                    autoSkip: false,
                },
                scales: {
                    xAxes: [
                        {
                            categoryPercentage: 0.98,
                            barPercentage: 0.98,
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                            ticks: {
                                autoSkip: false,
                                fontSize: 11,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                        },
                    ],
                },
                plugins: {
                    datalabels: {
                        backgroundColor: "transparent",
                        color: "#000000bf",
                        borderRadius: "2",
                        borderWidth: "1",
                        borderColor: "transparent",
                        anchor: "end",
                        align: "end",
                        padding: 2,
                        font: {
                            size: 10,
                            weight: 500,
                        },
                        formatter: function (value, ctx) {
                            var label = ctx.chart.data.labels[ctx.dataIndex];
                            if (label != undefined) {
                                return value;
                            }
                            else {
                                return value;
                            }
                        },
                    },
                },
            },
            ChartTypePieOptions: {
                responsive: true,
                plugins: {
                    datalabels: {
                        backgroundColor: "transparent",
                        color: "#000000bf",
                        borderRadius: "2",
                        borderWidth: "1",
                        borderColor: "transparent",
                        anchor: "end",
                        align: "end",
                        padding: 2,
                        display: true,
                        font: {
                            size: 10,
                            weight: 500,
                        },
                        formatter: function (value, ctx) {
                            var sum = 0;
                            var dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(function (data) {
                                sum += data;
                            });
                            var percentage = ((value * 100) / sum).toFixed(2) + "%";
                            // return percentage;
                            var label = ctx.chart.data.labels[ctx.dataIndex];
                            if (label != undefined) {
                                return label + "\n" + percentage;
                                // return label;
                            }
                            else {
                                return value;
                            }
                        },
                    },
                },
                legend: {
                    display: true,
                    position: "bottom",
                },
                legendCallback: function (chart) {
                    // Return the HTML string here.
                },
            },
            Colors: [
                {
                    backgroundColor: "#907eec",
                },
                {
                    backgroundColor: "rgb(0, 194, 146)",
                },
                {
                    backgroundColor: "rgb(171, 140, 228)",
                },
                {
                    backgroundColor: "rgb(255, 180, 99)",
                },
                {
                    backgroundColor: "rgb(3, 169, 243)",
                },
                {
                    backgroundColor: "rgb(251, 150, 120)",
                },
            ],
            PieColors: [
                {
                    backgroundColor: [
                        "rgb(0, 194, 146)",
                        "rgb(171, 140, 228)",
                        "rgb(255, 180, 99)",
                        "rgb(3, 169, 243)",
                        "rgb(251, 150, 120)",
                    ],
                },
            ],
            ChartTypePlotOptions: {
                fill: false,
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                stacked: true,
                                display: false,
                            },
                        },
                    ],
                },
                legend: {
                    display: true,
                    position: "bottom",
                },
                plugins: {
                    datalabels: {
                        backgroundColor: "transparent",
                        color: "#000000bf",
                        borderRadius: "2",
                        borderWidth: "1",
                        borderColor: "transparent",
                        anchor: "end",
                        align: "end",
                        padding: 2,
                        font: {
                            size: 11,
                            weight: 500,
                        },
                        formatter: function (value, ctx) {
                            var label = ctx.chart.data.labels[ctx.dataIndex];
                            if (label != undefined) {
                                return value;
                            }
                            else {
                                return value;
                            }
                        },
                    },
                },
            },
            RewardTypeLabel: [],
            RewardTypeData: [],
            GenderLabel: [],
            GenderData: [],
            RegSourceLabel: [],
            RegSourceData: [],
            VisitorLabel: [],
            VisitorData: [],
            RewardLabel: [],
            RewardData: [],
            RedeemData: [],
            RedeemLabel: [],
            PaymentData: [],
            PaymentLabel: [],
            RewardPlotLabel: [],
            RewardPlotDataSet: [],
            RedeemPlotLabel: [],
            RedeemPlotDataSet: [],
            VisitorsPlotLabel: [],
            VisitorsPlotDataSet: [],
            SalePlotLabel: [],
            SalePlotDataSet: [],
            CardUserPlotLabel: [],
            CardUserPlotDataSet: [],
            PaymentsPlotLabel: [],
            PaymentsPlotDataSet: [],
            HourlyVisitLabel: [],
            HourlyVisitData: [],
            HourlySalesData: [],
            HourlyVisitNextLabel: [],
            HourlyVisitNextData: [],
        };
        this._CoreHelper = {
            ParentCode: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Description: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentName: null,
            PosterUrl: null,
            Reference: null,
            ReferenceKey: null,
            Sequence: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubParentCode: null,
            SubParentName: null,
            SystemName: null,
            TypeName: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,
        };
        this._CoreCommon = {
            Count: 0,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Data: null,
            Description: null,
            HelperCode: null,
            HelperName: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentCode: null,
            ParentKey: null,
            ParentName: null,
            PosterUrl: null,
            Reference: null,
            ReferenceKey: null,
            Sequence: null,
            SubItemsCount: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubParentCode: null,
            SubParentKey: null,
            SubParentName: null,
            SubValue: null,
            SystemName: null,
            TypeCode: null,
            TypeName: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountKey: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,
        };
        this._SubCoreCommon = {
            Count: 0,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Data: null,
            Description: null,
            HelperCode: null,
            HelperName: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentCode: null,
            ParentKey: null,
            SubItemsCount: null,
            ParentName: null,
            PosterUrl: null,
            Reference: null,
            ReferenceKey: null,
            Sequence: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubParentCode: null,
            SubParentKey: null,
            SubParentName: null,
            SubValue: null,
            SystemName: null,
            TypeCode: null,
            TypeName: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountKey: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,
        };
        this._CoreParameter = {
            ReferenceId: 0,
            CommonParentCode: null,
            CommonParentKey: null,
            CommonParentName: null,
            Count: 0,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Data: null,
            Description: null,
            HelperCode: null,
            HelperName: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            SubItemsCount: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentCode: null,
            ParentKey: null,
            ParentName: null,
            PosterUrl: null,
            Reference: null,
            ReferenceKey: null,
            Sequence: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubCommonParentCode: null,
            SubCommonParentKey: null,
            SubCommonParentName: null,
            SubParentCode: null,
            SubParentKey: null,
            SubParentName: null,
            SubValue: null,
            SystemName: null,
            TypeCode: null,
            TypeName: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountKey: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,
        };
        this._SubCoreParameter = {
            CommonParentCode: null,
            CommonParentKey: null,
            CommonParentName: null,
            Count: 0,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Data: null,
            Description: null,
            HelperCode: null,
            HelperName: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            Name: null,
            ParentCode: null,
            ParentKey: null,
            ParentName: null,
            PosterUrl: null,
            SubItemsCount: null,
            Reference: null,
            ReferenceKey: null,
            Sequence: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubCommonParentCode: null,
            SubCommonParentKey: null,
            SubCommonParentName: null,
            SubParentCode: null,
            SubParentKey: null,
            SubParentName: null,
            SubValue: null,
            SystemName: null,
            TypeCode: null,
            TypeName: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountKey: null,
            Value: null,
            StatusI: null,
            CreateDateS: null,
            ModifyDateS: null,
        };
        this._UserAccount = {
            MerchantDisplayName: null,
            SecondaryEmailAddress: null,
            BankDisplayName: null,
            BankKey: null,
            SubOwnerAddress: null,
            SubOwnerLatitude: null,
            SubOwnerDisplayName: null,
            SubOwnerKey: null,
            SubOwnerLongitude: null,
            AccessPin: null,
            LastLoginDateS: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            CreateDate: null,
            CreateDateS: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            CreatedByKey: null,
            Description: null,
            IconUrl: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            PosterUrl: null,
            ReferenceKey: null,
            StatusCode: null,
            StatusI: null,
            StatusId: null,
            StatusName: null,
            AccountCode: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            Address: null,
            AppVersionName: null,
            ApplicationStatusCode: null,
            ApplicationStatusName: null,
            AverageValue: null,
            CityAreaKey: null,
            CityAreaName: null,
            CityKey: null,
            CityName: null,
            ContactNumber: null,
            CountValue: null,
            CountryKey: null,
            CountryName: null,
            DateOfBirth: null,
            DisplayName: null,
            EmailAddress: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            FirstName: null,
            GenderCode: null,
            GenderName: null,
            LastLoginDate: null,
            LastName: null,
            Latitude: null,
            Longitude: null,
            MobileNumber: null,
            Name: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            OwnerDisplayName: null,
            OwnerKey: null,
            Password: null,
            Reference: null,
            ReferralCode: null,
            ReferralUrl: null,
            RegionAreaKey: null,
            RegionAreaName: null,
            RegionKey: null,
            RegionName: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            RequestKey: null,
            RoleKey: null,
            RoleName: null,
            SecondaryPassword: null,
            SystemPassword: null,
            UserName: null,
            WebsiteUrl: null,
        };
        this._RewardOverview = {
            RewardAmount: 0,
            RewardChargeAmount: 0,
            RewardPurchaseAmount: 0,
            RewardTransactions: 0,
            RewardUserAmount: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
        };
        this._RewardOverviewNext = {
            RewardAmount: 0,
            RewardChargeAmount: 0,
            RewardPurchaseAmount: 0,
            RewardTransactions: 0,
            RewardUserAmount: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
        };
        this._RewardTypeOverview = {
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            Transactions: 0,
            RewardTypeData: [],
            RewardTypeLabels: [],
            CardRewardTransactionsPerc: 0,
            CashRewardTransactionsPerc: 0,
            AppUsersBank: [],
            AppUsersBankLabel: [],
            AppUsersBankDataPurchase: [],
            AppUsersBankDataTransactions: [],
            AppUsersBankDataUsers: [],
            BankCompare: [],
            BankCompareLabel: [],
            BankCompareDataUsers: [],
            BankCompareDataTransactions: [],
            BankCompareDataPurchase: [],
            AppUsersCardType: [],
            AppUsersCardTypeLabel: [],
            AppUsersCardTypeDataPurchase: [],
            AppUsersCardTypeDataTransactions: [],
            AppUsersCardTypeDataUsers: [],
        };
        this._RewardTypeOverviewNext = {
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            RewardTypeData: [],
            RewardTypeLabels: [],
            Transactions: 0,
            CardRewardTransactionsPerc: 0,
            CashRewardTransactionsPerc: 0,
            AppUsersBank: [],
            AppUsersBankLabel: [],
            AppUsersBankDataPurchase: [],
            AppUsersBankDataTransactions: [],
            AppUsersBankDataUsers: [],
            BankCompare: [],
            BankCompareLabel: [],
            BankCompareDataUsers: [],
            BankCompareDataTransactions: [],
            BankCompareDataPurchase: [],
            AppUsersCardType: [],
            AppUsersCardTypeLabel: [],
            AppUsersCardTypeDataPurchase: [],
            AppUsersCardTypeDataTransactions: [],
            AppUsersCardTypeDataUsers: [],
        };
        this._RedeemOverview = {
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemTransactions: 0,
        };
        this._RedeemOverviewNext = {
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemTransactions: 0,
        };
        this._AppUsersOverview = {
            AppUsers: 0,
            AppUsersByAgeGroup: [],
            AppUsersCountByAgeGroup: [],
            AppUsersLabelsByAgeGroup: [],
            AppUsersPurchaseByAgeGroup: [],
            AppUsersVisitByAgeGroup: [],
            AppUsersDataByGender: [],
            AppUsersLabelsByGender: [],
            AppUsersFemale: 0,
            AppUsersMale: 0,
            AppUsersOther: 0,
            OwnAppUsers: 0,
            ReferralAppUsers: 0,
            RepeatingAppUsers: 0,
            UniqueAppUsers: 0,
        };
        this._AppUsersOverviewNext = {
            AppUsers: 0,
            AppUsersByAgeGroup: [],
            AppUsersCountByAgeGroup: [],
            AppUsersLabelsByAgeGroup: [],
            AppUsersPurchaseByAgeGroup: [],
            AppUsersVisitByAgeGroup: [],
            AppUsersDataByGender: [],
            AppUsersLabelsByGender: [],
            AppUsersFemale: 0,
            AppUsersMale: 0,
            AppUsersOther: 0,
            OwnAppUsers: 0,
            ReferralAppUsers: 0,
            RepeatingAppUsers: 0,
            UniqueAppUsers: 0,
        };
        this._UserCounts = {
            RewardPercentage: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            TotalCustomerNew: 0,
            TotalCustomerRepeating: 0,
            TotalCustomerUnique: 0,
            TotalAcquirer: 0,
            TotalCashier: 0,
            TotalCustomer: 0,
            TotalManager: 0,
            TotalMerchant: 0,
            TotalPssp: 0,
            TotalPtsp: 0,
            TotalRm: 0,
            TotalStore: 0,
            TotalTerminal: 0,
        };
        this._CardTypeSalesSummary = {
            CardTypeLabels: [],
            CardTypeData: [],
            CardTypes: [],
        };
        this._TerminalStatusCount = {
            Total: 0,
            Unused: 0,
            Active: 0,
            Idle: 0,
            Dead: 0,
        };
        this._SalesOverview = {
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,
            TotalSuccessfulTransaction: 0,
            TotalSuccessfulTransactionCustomer: 0,
            TotalSuccessfulTransactionInvoiceAmount: 0,
            TotalFailedTransaction: 0,
            TotalFailedTransactionCustomer: 0,
            TotalFailedTransactionInvoiceAmount: 0,
            TotalCardTransaction: 0,
            TotalCardTransactionInvoiceAmount: 0,
            TotalCashTransaction: 0,
            TotalCashTransactionCustomer: 0,
            TotalCashTransactionInvoiceAmount: 0,
            FirstTransactionDate: null,
            LastTransactionDate: null,
            LastTransactionDateD: "",
            TotalCardTransactionCustomer: 0,
            TransactionTypeData: [],
            TransactionTypeLabels: [],
        };
        this._SalesSummary = {
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,
            TotalSuccessfulTransaction: 0,
            TotalSuccessfulTransactionCustomer: 0,
            TotalSuccessfulTransactionInvoiceAmount: 0,
            TotalFailedTransaction: 0,
            TotalFailedTransactionCustomer: 0,
            TotalFailedTransactionInvoiceAmount: 0,
            TotalCardTransaction: 0,
            TotalCardTransactionInvoiceAmount: 0,
            TotalCashTransaction: 0,
            TotalCashTransactionCustomer: 0,
            TotalCashTransactionInvoiceAmount: 0,
            FirstTransactionDate: null,
            LastTransactionDate: null,
            LastTransactionDateD: "",
            TotalCardTransactionCustomer: 0,
            TransactionTypeData: [],
            TransactionTypeLabels: [],
        };
        this._RewardsSummary = {
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,
            TotalRewardTransaction: 0,
            TotalRewardTransactionCustomer: 0,
            TotalRewardTransactionAmount: 0,
            TotalRewardTransactionInvoiceAmount: 0,
            TotalTucRewardTransaction: 0,
            TotalTucRewardTransactionCustomer: 0,
            TotalTucRewardTransactionAmount: 0,
            TotalTucRewardTransactionInvoiceAmount: 0,
            TotalTucPlusRewardTransaction: 0,
            TotalTucPlusRewardTransactionCustomer: 0,
            TotalTucPlusRewardTransactionAmount: 0,
            TotalTucPlusRewardTransactionInvoiceAmount: 0,
            TotalTucPlusRewardClaimTransaction: 0,
            TotalTucPlusRewardClaimTransactionCustomer: 0,
            TotalTucPlusRewardClaimTransactionAmount: 0,
            TotalTucPlusRewardClaimTransactionInvoiceAmount: 0,
            TotalRedeemTransaction: 0,
            TotalRedeemTransactionCustomer: 0,
            TotalRedeemTransactionAmount: 0,
            TotalRedeemTransactionInvoiceAmount: 0,
            FirstTransactionDate: null,
            FirstTransactionDateD: "",
            LastTransactionDate: null,
            LastTransactionDateD: "",
            TucBalance: 0,
            TucPlusBalance: 0,
            TucBalanceCredit: 0,
            TucBalanceDebit: 0,
            TucBalanceTransaction: 0,
            TucPlusBalanceCredit: 0,
            TucPlusBalanceDebit: 0,
            TucPlusBalanceTransaction: 0,
            UserBalance: [],
        };
        this._SubAccountOverview = {
            TransactionFailed: 0,
            TransactionSuccess: 0,
            UnusedTerminals: 0,
            ReferredAppUsers: 0,
            ReferredAppUsersPurchase: 0,
            ReferredAppUsersVisit: 0,
            ReferredMerchantSale: 0,
            ReferredMerchantVisits: 0,
            ReferredMerchants: 0,
            ReferredReferredStores: 0,
            TerminalsOverview: [],
            StoresOverview: [],
            Merchants: 0,
            Stores: 0,
            Acquirers: 0,
            Terminals: 0,
            Ptsp: 0,
            Pssp: 0,
            Cashiers: 0,
            RewardCards: 0,
            RewardCardsUsed: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusForMerchant: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            RewardPercentage: 0,
            CommissionPercentage: 0,
            Balance: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            RewardTransactions: 0,
            RewardAmount: 0,
            RewardPurchaseAmount: 0,
            RewardLastTransaction: null,
            RedeemTransactions: 0,
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemLastTransaction: null,
            Transactions: 0,
            TransactionsPercentage: 0,
            NewTransactions: 0,
            NewTransactionsPercentage: 0,
            RepeatingTransactions: 0,
            RepeatingTransactionsPercentage: 0,
            ReferralTransactions: 0,
            ReferralTransactionsPercentage: 0,
            PurchaseAmount: 0,
            PurchaseAmountPercentage: 0,
            NewPurchaseAmount: 0,
            NewPurchaseAmountPercentage: 0,
            RepeatingPurchaseAmount: 0,
            RepeatingPurchaseAmountPercentage: 0,
            ReferralPurchaseAmount: 0,
            ReferralPurchaseAmountPercentage: 0,
            Commission: 0,
            LastCommissionDate: null,
            IssuerCommissionAmount: 0,
            LastIssuerCommissionDate: null,
            CommissionAmount: 0,
            SettlementCredit: 0,
            SettlementDebit: 0,
            AppUsers: 0,
            AppUsersPercentage: 0,
            OwnAppUsers: 0,
            OwnAppUsersPercentage: 0,
            RepeatingAppUsers: 0,
            RepeatingAppUsersPercentage: 0,
            ReferralAppUsers: 0,
            ReferralAppUsersPercentage: 0,
            AppUsersMale: 0,
            AppUsersFemale: 0,
            LastAppUserDate: null,
            LastTransaction: {
                Amount: 0,
                InvoiceAmount: 0,
                MerchantName: "",
                ReferenceId: 0,
                RewardAmount: 0,
                TransactionDate: null,
                TypeName: null,
            },
            GenderLabel: [],
            GenderData: [],
            RewardTypeLabel: [],
            RewardTypeData: [],
            VisitTrendLabel: [],
            VisitTrendData: [],
            AcquirerAmountDistribution: [],
            Charge: 0,
            ClaimedReward: 0,
            ActiveTerminals: 0,
            AppUsersByAge: [],
            AppUsersOther: 0,
            AppUsersPurchaseByAge: [],
            CardRewardPurchaseAmountOther: 0,
            CardRewardTransactionsOther: 0,
            ClaimedRewardTransations: 0,
            Credit: 0,
            DeadTerminals: 0,
            Debit: 0,
            FrequentBuyers: [],
            IdleTerminals: 0,
            LastTransactionDate: null,
            MerchantOverview: [],
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            PosOverview: [],
            RewardChargeAmount: 0,
            RewardUserAmount: 0,
            SettlementPending: 0,
            TUCPlusBalance: 0,
            TUCPlusPurchaseAmount: 0,
            TUCPlusReward: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
            ThankUAmount: 0,
            TransactionIssuerAmountCredit: 0,
            TransactionIssuerAmountDebit: 0,
            TransactionIssuerChargeCredit: 0,
            TransactionIssuerChargeDebit: 0,
            TransactionIssuerTotalCreditAmount: 0,
            TransactionIssuerTotalDebitAmount: 0,
            UniqueAppUsers: 0,
            UserAmount: 0,
        };
        this._AccountOverview = {
            TransactionFailed: 0,
            TransactionSuccess: 0,
            UnusedTerminals: 0,
            ReferredAppUsers: 0,
            ReferredAppUsersPurchase: 0,
            ReferredAppUsersVisit: 0,
            ReferredMerchantSale: 0,
            ReferredMerchantVisits: 0,
            ReferredMerchants: 0,
            ReferredReferredStores: 0,
            TerminalsOverview: [],
            StoresOverview: [],
            Merchants: 0,
            Stores: 0,
            Acquirers: 0,
            Terminals: 0,
            Ptsp: 0,
            Pssp: 0,
            Cashiers: 0,
            RewardCards: 0,
            RewardCardsUsed: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusForMerchant: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            RewardPercentage: 0,
            CommissionPercentage: 0,
            Balance: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            RewardTransactions: 0,
            RewardAmount: 0,
            RewardPurchaseAmount: 0,
            RewardLastTransaction: null,
            RedeemTransactions: 0,
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemLastTransaction: null,
            Transactions: 0,
            TransactionsPercentage: 0,
            NewTransactions: 0,
            NewTransactionsPercentage: 0,
            RepeatingTransactions: 0,
            RepeatingTransactionsPercentage: 0,
            ReferralTransactions: 0,
            ReferralTransactionsPercentage: 0,
            PurchaseAmount: 0,
            PurchaseAmountPercentage: 0,
            NewPurchaseAmount: 0,
            NewPurchaseAmountPercentage: 0,
            RepeatingPurchaseAmount: 0,
            RepeatingPurchaseAmountPercentage: 0,
            ReferralPurchaseAmount: 0,
            ReferralPurchaseAmountPercentage: 0,
            Commission: 0,
            LastCommissionDate: null,
            IssuerCommissionAmount: 0,
            LastIssuerCommissionDate: null,
            CommissionAmount: 0,
            SettlementCredit: 0,
            SettlementDebit: 0,
            AppUsers: 0,
            AppUsersPercentage: 0,
            OwnAppUsers: 0,
            OwnAppUsersPercentage: 0,
            RepeatingAppUsers: 0,
            RepeatingAppUsersPercentage: 0,
            ReferralAppUsers: 0,
            ReferralAppUsersPercentage: 0,
            AppUsersMale: 0,
            AppUsersFemale: 0,
            LastAppUserDate: null,
            LastTransaction: {
                Amount: 0,
                InvoiceAmount: 0,
                MerchantName: "",
                ReferenceId: 0,
                RewardAmount: 0,
                TransactionDate: null,
                TypeName: null,
            },
            GenderLabel: [],
            GenderData: [],
            RewardTypeLabel: [],
            RewardTypeData: [],
            VisitTrendLabel: [],
            VisitTrendData: [],
            AcquirerAmountDistribution: [],
            Charge: 0,
            ClaimedReward: 0,
            ActiveTerminals: 0,
            AppUsersByAge: [],
            AppUsersOther: 0,
            AppUsersPurchaseByAge: [],
            CardRewardPurchaseAmountOther: 0,
            CardRewardTransactionsOther: 0,
            ClaimedRewardTransations: 0,
            Credit: 0,
            DeadTerminals: 0,
            Debit: 0,
            FrequentBuyers: [],
            IdleTerminals: 0,
            LastTransactionDate: null,
            MerchantOverview: [],
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            PosOverview: [],
            RewardChargeAmount: 0,
            RewardUserAmount: 0,
            SettlementPending: 0,
            TUCPlusBalance: 0,
            TUCPlusPurchaseAmount: 0,
            TUCPlusReward: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
            ThankUAmount: 0,
            TransactionIssuerAmountCredit: 0,
            TransactionIssuerAmountDebit: 0,
            TransactionIssuerChargeCredit: 0,
            TransactionIssuerChargeDebit: 0,
            TransactionIssuerTotalCreditAmount: 0,
            TransactionIssuerTotalDebitAmount: 0,
            UniqueAppUsers: 0,
            UserAmount: 0,
        };
        this._AccountOverviewNext = {
            ReferredAppUsers: 0,
            ReferredAppUsersPurchase: 0,
            ReferredAppUsersVisit: 0,
            ReferredMerchantSale: 0,
            ReferredMerchantVisits: 0,
            ReferredMerchants: 0,
            ReferredReferredStores: 0,
            TerminalsOverview: [],
            StoresOverview: [],
            Merchants: 0,
            Stores: 0,
            Acquirers: 0,
            Terminals: 0,
            Ptsp: 0,
            Pssp: 0,
            Cashiers: 0,
            RewardCards: 0,
            RewardCardsUsed: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusForMerchant: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            RewardPercentage: 0,
            CommissionPercentage: 0,
            Balance: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            RewardTransactions: 0,
            RewardAmount: 0,
            RewardPurchaseAmount: 0,
            RewardLastTransaction: null,
            RedeemTransactions: 0,
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemLastTransaction: null,
            Transactions: 0,
            TransactionsPercentage: 0,
            NewTransactions: 0,
            NewTransactionsPercentage: 0,
            RepeatingTransactions: 0,
            RepeatingTransactionsPercentage: 0,
            ReferralTransactions: 0,
            ReferralTransactionsPercentage: 0,
            PurchaseAmount: 0,
            PurchaseAmountPercentage: 0,
            NewPurchaseAmount: 0,
            NewPurchaseAmountPercentage: 0,
            RepeatingPurchaseAmount: 0,
            RepeatingPurchaseAmountPercentage: 0,
            ReferralPurchaseAmount: 0,
            ReferralPurchaseAmountPercentage: 0,
            Commission: 0,
            LastCommissionDate: null,
            IssuerCommissionAmount: 0,
            LastIssuerCommissionDate: null,
            CommissionAmount: 0,
            SettlementCredit: 0,
            SettlementDebit: 0,
            AppUsers: 0,
            AppUsersPercentage: 0,
            OwnAppUsers: 0,
            OwnAppUsersPercentage: 0,
            RepeatingAppUsers: 0,
            RepeatingAppUsersPercentage: 0,
            ReferralAppUsers: 0,
            ReferralAppUsersPercentage: 0,
            AppUsersMale: 0,
            AppUsersFemale: 0,
            LastAppUserDate: null,
            LastTransaction: {
                Amount: 0,
                InvoiceAmount: 0,
                MerchantName: "",
                ReferenceId: 0,
                RewardAmount: 0,
                TransactionDate: null,
                TypeName: null,
            },
            GenderLabel: [],
            GenderData: [],
            RewardTypeLabel: [],
            RewardTypeData: [],
            VisitTrendLabel: [],
            VisitTrendData: [],
            AcquirerAmountDistribution: [],
            Charge: 0,
            ClaimedReward: 0,
            ActiveTerminals: 0,
            AppUsersByAge: [],
            AppUsersOther: 0,
            AppUsersPurchaseByAge: [],
            CardRewardPurchaseAmountOther: 0,
            CardRewardTransactionsOther: 0,
            ClaimedRewardTransations: 0,
            Credit: 0,
            DeadTerminals: 0,
            Debit: 0,
            FrequentBuyers: [],
            IdleTerminals: 0,
            LastTransactionDate: null,
            MerchantOverview: [],
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            PosOverview: [],
            RewardChargeAmount: 0,
            RewardUserAmount: 0,
            SettlementPending: 0,
            TUCPlusBalance: 0,
            TUCPlusPurchaseAmount: 0,
            TUCPlusReward: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
            ThankUAmount: 0,
            TransactionIssuerAmountCredit: 0,
            TransactionIssuerAmountDebit: 0,
            TransactionIssuerChargeCredit: 0,
            TransactionIssuerChargeDebit: 0,
            TransactionIssuerTotalCreditAmount: 0,
            TransactionIssuerTotalDebitAmount: 0,
            UniqueAppUsers: 0,
            UserAmount: 0,
        };
        this.JData = [
            {
                featureType: "administrative.locality",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "administrative.locality",
                elementType: "geometry.fill",
                stylers: [
                    {
                        saturation: "100",
                    },
                    {
                        lightness: "-53",
                    },
                ],
            },
            {
                featureType: "landscape.man_made",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        color: "#efefe9",
                    },
                ],
            },
            {
                featureType: "landscape.natural",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        color: "#ebe6e1",
                    },
                    {
                        lightness: "0",
                    },
                ],
            },
            {
                featureType: "landscape.natural.landcover",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "landscape.natural.terrain",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "poi",
                elementType: "geometry.fill",
                stylers: [
                    {
                        color: "#cdec9d",
                    },
                ],
            },
            {
                featureType: "poi.attraction",
                elementType: "geometry",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "poi.attraction",
                elementType: "geometry.fill",
                stylers: [
                    {
                        color: "#ff0000",
                    },
                ],
            },
            {
                featureType: "poi.business",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "poi.business",
                elementType: "geometry.fill",
                stylers: [
                    {
                        color: "#ee9334",
                    },
                ],
            },
            {
                featureType: "poi.government",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        lightness: "-22",
                    },
                    {
                        saturation: "15",
                    },
                    {
                        color: "#b8a1cd",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "geometry.fill",
                stylers: [
                    {
                        color: "#ffffff",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        saturation: "0",
                    },
                    {
                        lightness: "-25",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "labels.text.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        lightness: "-82",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "labels.text.stroke",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        color: "#ffffff",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "all",
                stylers: [
                    {
                        saturation: "0",
                    },
                    {
                        lightness: "0",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [
                    {
                        saturation: "1",
                    },
                    {
                        lightness: "11",
                    },
                    {
                        gamma: "2.60",
                    },
                    {
                        color: "#aa85e5",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [
                    {
                        gamma: "6.13",
                    },
                ],
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        saturation: "75",
                    },
                    {
                        color: "#818fe6",
                    },
                    {
                        lightness: "14",
                    },
                    {
                        gamma: "1.61",
                    },
                ],
            },
        ];
        //#region TerminalSorting
        this.SortedTerminals = {
            All: [],
            Active: [],
            Idle: [],
            Dead: [],
            UnUsed: [],
        };
        //#endregion
        //#region dispatchevent
        this.isprocessingtoogle = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        //#endregion
        //#region ReloadEventEmitF
        this.ReloadEventEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
        this.FullContainer = false;
        this.ContainerHeight = window.innerHeight - 56;
        var UserTimeZone = SystemHelper.GetUserTimeZone();
        if (UserTimeZone != undefined && UserTimeZone != null) {
            this.AppConfig.TimeZone = UserTimeZone;
        }
        else {
            this.AppConfig.TimeZone = "Asia/Calcutta";
        }
        this._TranslateService
            .get("Common.ReferenceId")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ReferenceId = _Message;
        });
        this._TranslateService
            .get("Common.InvalidImage")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ReferenceId = _Message;
        });
        this._TranslateService
            .get("Common.ReferenceCode")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ReferenceCode = _Message;
        });
        this._TranslateService
            .get("Common.ReferenceKey")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ReferenceKey = _Message;
        });
        this._TranslateService.get("Common.Name").subscribe(function (_Message) {
            _this.AppConfig.CommonResource.Name = _Message;
        });
        this._TranslateService
            .get("Common.Select")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.Select = _Message;
        });
        this._TranslateService
            .get("Common.Cancel")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.Cancel = _Message;
        });
        this._TranslateService
            .get("Common.Continue")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.Continue = _Message;
        });
        this._TranslateService
            .get("Common.Status")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.Status = _Message;
        });
        this._TranslateService
            .get("Common.CreateDate")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.CreateDate = _Message;
        });
        this._TranslateService
            .get("Common.CreatedBy")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.CreatedBy = _Message;
        });
        this._TranslateService
            .get("Common.ModifyDate")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ModifyDate = _Message;
        });
        this._TranslateService
            .get("Common.ModifyBy")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.ModifyBy = _Message;
        });
        this._TranslateService
            .get("Common.DeleteTitle")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.DeleteTitle = _Message;
        });
        this._TranslateService
            .get("Common.DeleteHelp")
            .subscribe(function (_Message) {
            _this.AppConfig.CommonResource.DeleteHelp = _Message;
        });
        this.RefreshHelper();
    }
    HelperService.prototype.onResize = function (event) {
        this.ContainerHeight = window.innerHeight - 56;
    };
    HelperService.prototype.onMapReady = function (map, showSearchControl) {
        var _this = this;
        this.map = map;
        if (showSearchControl) {
            this.geocoder.addTo(this.map);
            this.map.on("click", function (e) {
                if (!Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(e) && !Object(util__WEBPACK_IMPORTED_MODULE_16__["isNull"])(e)) {
                    _this.revgeocoder
                        .latlng([e.latlng.lat, e.latlng.lng])
                        .run(function (error, result, response) {
                        _this.selectedLocation = result;
                        _this.navchange.emit(0);
                    });
                }
            });
        }
        else {
            this.map.removeControl(this.geocoder);
        }
        //    this.layers.push(this.searchControl);
    };
    //#endregion
    HelperService.prototype._InitMap = function () {
        //#region markerIconInit
        var iconRetinaUrl = "assets/marker-icon-2x.png";
        var iconUrl = "assets/marker-icon.png";
        var shadowUrl = "assets/marker-shadow.png";
        var iconDefault = leaflet__WEBPACK_IMPORTED_MODULE_9__["icon"]({
            iconRetinaUrl: iconRetinaUrl,
            iconUrl: iconUrl,
            shadowUrl: shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41],
        });
        leaflet__WEBPACK_IMPORTED_MODULE_9__["Marker"].prototype.options.icon = iconDefault;
        //#endregion
        //#region Marker
        try {
            this.marker = leaflet__WEBPACK_IMPORTED_MODULE_9__["marker"](leaflet__WEBPACK_IMPORTED_MODULE_9__["latLng"](this._UserAccount.Latitude.valueOf(), this._UserAccount.Longitude.valueOf()));
        }
        catch (error) {
            this.marker = leaflet__WEBPACK_IMPORTED_MODULE_9__["marker"](leaflet__WEBPACK_IMPORTED_MODULE_9__["latLng"](18.5204, 73.8567));
        }
        //#endregion
        //#region layers
        this.layers = [
            this.marker,
            Object(leaflet__WEBPACK_IMPORTED_MODULE_9__["tileLayer"])("http://{s}.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png", {
                minZoom: 7,
                maxZoom: 18,
                noWrap: true,
            }),
        ];
        //#endregion
        //#region Options
        this.lefletoptions.zoom = 14;
        try {
            this.lefletoptions.center = Object(leaflet__WEBPACK_IMPORTED_MODULE_9__["latLng"])(this._UserAccount.Latitude.valueOf(), this._UserAccount.Longitude.valueOf());
        }
        catch (error) {
            this.lefletoptions.center = Object(leaflet__WEBPACK_IMPORTED_MODULE_9__["latLng"])(18.5204, 73.8567);
        }
        //#endregion
    };
    HelperService.prototype._ReLocate = function () {
        try {
            this.marker.setLatLng([
                this._UserAccount.Latitude,
                this._UserAccount.Longitude,
            ]);
            this.map.panTo(new leaflet__WEBPACK_IMPORTED_MODULE_9__["LatLng"](this._UserAccount.Latitude, this._UserAccount.Longitude));
        }
        catch (error) {
            this.marker.setLatLng([18.5204, 73.8567]);
            this.map.panTo(new leaflet__WEBPACK_IMPORTED_MODULE_9__["LatLng"](18.5204, 73.8567));
        }
        this._MapCorrection();
    };
    HelperService.prototype._MapCorrection = function () {
        var _this = this;
        setTimeout(function () {
            _this.map.invalidateSize(false);
        }, 200);
    };
    HelperService.prototype.RefreshHelper = function () {
        var _this = this;
        this.AppConfig.Client = this.GetStorage(this.AppConfig.Storage.OReqH);
        this.AppConfig.Host = window.location.host;
        if (this.AppConfig.Client != null) {
            this.AppConfig.ClientHeader = {
                "Content-Type": "application/json; charset=utf-8",
                hcak: this.AppConfig.Client.hcak,
                hcavk: this.AppConfig.Client.hcavk,
                hctk: null,
                hcudlt: this.AppConfig.Client.hcudlt,
                hcudln: this.AppConfig.Client.hcudln,
                hcuak: this.AppConfig.Client.hcuak,
                hcupk: this.AppConfig.Client.hcupk,
                hcudk: this.AppConfig.Client.hcudk,
                hcuata: "",
                hcuatp: "",
            };
        }
        if (this.AppConfig.Host == "interswitch.thankucash.com" ||
            this.AppConfig.Host == "app.thankucash.com" ||
            this.AppConfig.Host == "console.thankucash.com" ||
            this.AppConfig.Host == "betaconsole.thankucash.com" ||
            this.AppConfig.Host == "acquirer.thankucash.com" ||
            this.AppConfig.Host == "merchant.thankucash.com" ||
            this.AppConfig.Host == "provider.thankucash.com" ||
            this.AppConfig.Host == "ptsp.thankucash.com" ||
            this.AppConfig.Host == "pssp.thankucash.com") {
            this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
        }
        else if (this.AppConfig.Host == "playapp.thankucash.com" ||
            this.AppConfig.Host == "playacquirer.thankucash.com" ||
            this.AppConfig.Host == "playmerchant.thankucash.com" ||
            this.AppConfig.Host == "playprovider.thankucash.com" ||
            this.AppConfig.Host == "playptsp.thankucash.com" ||
            this.AppConfig.Host == "playpssp.thankucash.com") {
            this.AppConfig.HostConnect = "https://playwebconnect.thankucash.com/";
        }
        else if (this.AppConfig.Host == "testapp.thankucash.com" ||
            this.AppConfig.Host == "testacquirer.thankucash.com" ||
            this.AppConfig.Host == "testmerchant.thankucash.com" ||
            this.AppConfig.Host == "testprovider.thankucash.com" ||
            this.AppConfig.Host == "testconsole.thankucash.com" ||
            this.AppConfig.Host == "testptsp.thankucash.com" ||
            this.AppConfig.Host == "testpssp.thankucash.com") {
            this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
        }
        else if (this.AppConfig.Host == "localhost:4201") {
            this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://localhost:5001/";
            // this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
        }
        else if (this.AppConfig.Host == "localhost:4200" ||
            this.AppConfig.Host == "68.183.82.221:4000" ||
            this.AppConfig.Host == "dev.thankucash.com:4000") {
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            //  this.AppConfig.HostConnect = "https://localhost:5001/";
            this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
        }
        else if (this.AppConfig.Host == "http://127.0.0.1:5501") {
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            //  this.AppConfig.HostConnect = "https://localhost:5001/";
            this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://webconnect.thankucash.com/";
            // this.AppConfig.HostConnect = "https://testwebconnect.thankucash.com/";
        }
        else {
            this.AppConfig.HostConnect = "";
        }
        this.Select2Options_Multiple = {
            multiple: true,
            placeholder: "Select",
        };
        this.S2_Sort_Option = {
            multiple: false,
            placeholder: "Sort Results",
        };
        this.Select2Options_Single = {
            multiple: false,
            placeholder: "Sort Results",
        };
        this.Select2Options_SearchColumn = {
            multiple: true,
            placeholder: "Search On Fields",
        };
        var UserAccountInformation = this.GetStorage(this.AppConfig.Storage.Account);
        if (UserAccountInformation != null) {
            this.AccessKey = UserAccountInformation.AccessKey;
            this.PublicKey = UserAccountInformation.PublicKey;
            this.LoginTime = UserAccountInformation.LoginTime;
            this.User = UserAccountInformation.User;
            this.UserAccount = UserAccountInformation.UserAccount;
            this.UserOwner = UserAccountInformation.UserOwner;
            this.UserCountry = UserAccountInformation.UserCountry;
            this.UserRoles = UserAccountInformation.UserRoles;
            if (this.UserAccount.AccountTypeCode == this.AppConfig.AccountType.Store) {
                this.AppConfig.ActiveOwnerKey = this.UserAccount.AccountKey;
                this.AppConfig.ActiveOwnerId = this.UserAccount.AccountId;
                this.AppConfig.ActiveOwnerDisplayName = this.UserAccount.DisplayName;
                this.AppConfig.ActiveOwnerAccountCode = this.UserAccount.AccountCode;
                this.AppConfig.ActiveOwnerAccountTypeCode = this.UserAccount.AccountTypeCode;
                this.AppConfig.ActiveOwnerIsTucPlusEnabled = this.UserOwner.IsTucPlusEnabled;
            }
            else if (this.UserAccount.AccountTypeCode ==
                this.AppConfig.AccountType.MerchantSubAccount) {
                this.AppConfig.ActiveOwnerKey = this.UserOwner.AccountKey;
                this.AppConfig.ActiveOwnerId = this.UserOwner.AccountId;
                this.AppConfig.ActiveOwnerDisplayName = this.UserOwner.DisplayName;
                this.AppConfig.ActiveOwnerAccountCode = this.UserOwner.AccountCode;
                this.AppConfig.ActiveOwnerAccountTypeCode = this.UserOwner.AccountTypeCode;
                this.AppConfig.ActiveOwnerIsTucPlusEnabled = this.UserOwner.IsTucPlusEnabled;
            }
            else if (this.UserAccount.AccountTypeCode ==
                this.AppConfig.AccountType.AcquirerSubAccount) {
                this.AppConfig.ActiveOwnerKey = this.UserOwner.AccountKey;
                this.AppConfig.ActiveOwnerId = this.UserOwner.AccountId;
                this.AppConfig.ActiveOwnerDisplayName = this.UserOwner.DisplayName;
                this.AppConfig.ActiveOwnerAccountCode = this.UserOwner.AccountCode;
                this.AppConfig.ActiveOwnerAccountTypeCode = this.UserOwner.AccountTypeCode;
                this.AppConfig.ActiveOwnerIsTucPlusEnabled = this.UserOwner.IsTucPlusEnabled;
            }
            else {
                this.AppConfig.ActiveOwnerKey = this.UserAccount.AccountKey;
                this.AppConfig.ActiveOwnerId = this.UserAccount.AccountId;
                this.AppConfig.ActiveOwnerDisplayName = this.UserAccount.DisplayName;
                this.AppConfig.ActiveOwnerAccountCode = this.UserAccount.AccountCode;
                this.AppConfig.ActiveOwnerAccountTypeCode = this.UserAccount.AccountTypeCode;
                this.AppConfig.ActiveOwnerIsTucPlusEnabled = this.UserAccount.IsTucPlusEnabled;
            }
            if (this.UserRoles != undefined) {
                this.UserRoles.forEach(function (_UserRole) {
                    if (_UserRole.RolePermissions.length > 0) {
                        _UserRole.RolePermissions.forEach(function (_Permission) {
                            _this.UserRolePermissions.push(_Permission);
                        });
                    }
                });
            }
        }
        var UserLocationInformation = this.GetStorage(this.AppConfig.Storage.Location);
        if (UserLocationInformation != null) {
            this.UserLocation = UserLocationInformation;
        }
        var UserDeviceInformation = this.GetStorage(this.AppConfig.Storage.Device);
        if (UserDeviceInformation != null) {
            this.UserDevice = UserDeviceInformation;
        }
    };
    HelperService.prototype.NavigateBack = function () {
        this._Location.back();
    };
    HelperService.prototype.ToggleAsidePanel = function () {
        if (this.ShowAsidePanel == false) {
            this.ShowAsidePanel = true;
        }
        else {
            this.ShowAsidePanel = false;
        }
    };
    // public _FileSelect_Poster_onAdded(event: any) {
    //   var _FileName: string = event.file.name;
    //   var _File: File = event.file;
    //   var _FileReader: FileReader = new FileReader();
    //   _FileReader.onloadend = e => {
    //     var _FileContent = _FileReader.result;
    //     this._FileSelect_Poster_Processing = true;
    //     var _Image = new Image();
    //     _Image.src = _FileContent.toString();
    //     _Image.onload = () => {
    //       this._FileSelect_Poster_Data.Width = _Image.width;
    //       this._FileSelect_Poster_Data.Height = _Image.height;
    //       if (
    //         _Image.width == this._FileSelect_Poster_Data.Width &&
    //         _Image.height == this._FileSelect_Poster_Data.Height
    //       ) {
    //         var FileSelect_Icon_Data = this.GetImageDetails(
    //           _FileContent
    //         ) as OStorageContent;
    //         this._FileSelect_Poster_Data.Content = FileSelect_Icon_Data.Content;
    //         this._FileSelect_Poster_Data.Extension =
    //           FileSelect_Icon_Data.Extension;
    //         this._FileSelect_Poster_Data.Name = _FileName;
    //         this._FileSelect_Poster_Data.Width = _Image.width;
    //         this._FileSelect_Poster_Data.Height = _Image.height;
    //       } else {
    //         this.NotifyWarning(this.AppConfig.CommonResource.InvalidImage);
    //         this._FileSelect_Poster_Reset();
    //       }
    //     };
    //   };
    //   _FileReader.readAsDataURL(_File);
    // }
    HelperService.prototype._FileSelect_Poster_Reset = function () {
        this._FileSelect_Poster_Data.Name = null;
        this._FileSelect_Poster_Data.Content = null;
        this._FileSelect_Poster_Data.Extension = null;
        // this._FileSelect_Poster_Data.Height = 0;
        // this._FileSelect_Poster_Data.Width = 0;
        if (this._FileSelect_Poster_Data.Content != null) {
            this._Ng2FileInputService.reset(this._FileSelect_Poster);
        }
    };
    HelperService.prototype.Icon_Change = function (event) {
        this._Icon_Cropper_ChangedEvent = event;
    };
    HelperService.prototype.Icon_Cropped = function (event) {
        this._Icon_Cropper_Image = event.base64;
        var ImageDetails = this.GetImageDetails(this._Icon_Cropper_Image);
        this._Icon_Cropper_Data.Content = ImageDetails.Content;
        this._Icon_Cropper_Data.Extension = ImageDetails.Extension;
    };
    HelperService.prototype.Icon_Crop_Failed = function () {
        this.NotifyWarning("Unable to load image. Please make sure valid image selected or refresh page and try again");
    };
    HelperService.prototype.Icon_Crop_Loaded = function () {
        this.OpenModal("_Icon_Cropper_Modal");
    };
    HelperService.prototype.Icon_Crop_Clear = function () {
        this._Icon_Cropper_ChangedEvent = "";
        this._Icon_Cropper_Image = "";
        this._Icon_Cropper_Data.Name = null;
        this._Icon_Cropper_Data.Content = null;
        this._Icon_Cropper_Data.Extension = null;
        this._Icon_Cropper_Data.TypeCode = null;
        this.CloseModal("_Icon_Cropper_Modal");
    };
    HelperService.prototype.Poster_Change = function (event) {
        this._Poster_Cropper_ChangedEvent = event;
    };
    HelperService.prototype.Poster_Cropped = function (event) {
        this._Poster_Cropper_Image = event.base64;
        var ImageDetails = this.GetImageDetails(this._Poster_Cropper_Image);
        this._Poster_Cropper_Data.Content = ImageDetails.Content;
        this._Poster_Cropper_Data.Extension = ImageDetails.Extension;
    };
    HelperService.prototype.Poster_Crop_Failed = function () {
        this.NotifyWarning("Unable to load image. Please make sure valid image selected or refresh page and try again");
    };
    HelperService.prototype.Poster_Crop_Loaded = function () {
        this.OpenModal("_Poster_Cropper_Modal");
    };
    HelperService.prototype.Poster_Crop_Clear = function () {
        this._Icon_Cropper_ChangedEvent;
        this._Poster_Cropper_ChangedEvent = "";
        this._Poster_Cropper_Image = "";
        this._Poster_Cropper_Data.Name = null;
        this._Poster_Cropper_Data.Content = null;
        this._Poster_Cropper_Data.Extension = null;
        this._Poster_Cropper_Data.TypeCode = null;
        this.CloseModal("_Poster_Cropper_Modal");
    };
    HelperService.prototype.ToogleRange = function (val) {
        this.DateFilterRange = val;
        var isStartNotExist = this.DateRangeStart == undefined && this.DateRangeStart == null;
        var isEndNotExist = this.DateRangeEnd == undefined && this.DateRangeEnd == null;
        if (isStartNotExist || isEndNotExist) {
            this.DateRangeStart = moment().startOf("day");
            this.DateRangeEnd = moment().endOf("day");
            this.DateRangeStartO = moment().startOf("day");
            this.DateRangeEndO = moment().endOf("day");
        }
        switch (this.DateFilterRange) {
            case "A":
                {
                    this.DateRangeStart = moment(this.DateRangeStartO)
                        .startOf("month")
                        .startOf("day");
                    this.DateRangeEnd = moment(this.DateRangeEndO)
                        .endOf("month")
                        .endOf("day");
                }
                break;
            case "B":
                {
                    this.DateRangeStart = moment(this.DateRangeStartO)
                        .startOf("week")
                        .startOf("day");
                    this.DateRangeEnd = moment(this.DateRangeEndO)
                        .endOf("week")
                        .endOf("day");
                }
                break;
            case "C":
                {
                    this.DateRangeStart = moment(this.DateRangeStartO);
                    this.DateRangeEnd = moment(this.DateRangeEndO);
                }
                break;
            default:
                {
                    this.DateRangeStart = moment(this.DateRangeStartO);
                    this.DateRangeEnd = moment(this.DateRangeEndO);
                }
                break;
        }
        this.RangeAltered.next(true);
    };
    HelperService.prototype.ResetDateRange = function () {
        this.DateFilterRange = "C";
        this.DateRangeStart = moment().startOf("day");
        this.DateRangeEnd = moment().endOf("day");
        this.DateRangeStartO = moment().startOf("day");
        this.DateRangeEndO = moment().endOf("day");
    };
    HelperService.prototype.SetDateRange = function (event) {
        var StartTime = moment(event.start).startOf("day");
        var EndTime = moment(event.end).endOf("day");
        switch (this.DateFilterRange) {
            case "A":
                {
                    StartTime = StartTime.startOf("month");
                    EndTime = EndTime.endOf("month");
                }
                break;
            case "B":
                {
                    StartTime = StartTime.startOf("week");
                    EndTime = EndTime.endOf("week");
                }
                break;
            case "C":
                {
                    StartTime = StartTime;
                    EndTime = EndTime;
                }
                break;
            default:
                {
                    StartTime = StartTime;
                    EndTime = EndTime;
                }
                break;
        }
        this.DateRangeStart = StartTime;
        this.DateRangeEnd = EndTime;
        this.DateRangeStartO = moment(StartTime).startOf("day");
        this.DateRangeEndO = moment(EndTime).endOf("day");
        this.RangeAltered.next(true);
    };
    HelperService.prototype.Active_FilterInit = function (filterType, InitializeConfig) {
        this.CurrentType = filterType;
        this.CleanConfig = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(InitializeConfig);
        this.Active_FilterOptions = [];
        //#region Create Default Option 
        var temp = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(InitializeConfig);
        temp.id = -1;
        temp.text = "default";
        temp.Sort.SortDefaultName = temp.Sort.SortDefaultName + " desc";
        temp.OtherFilters = [];
        this.Active_FilterOptions.push(temp);
        //#endregion
        //#region Initiate FilterSnap With Default 
        this.FilterSnap = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(temp);
        this.FilterSnap.OtherFilters = [];
        this.FilterSnap.Badges = [];
        this.FilterSnapTemprary = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap);
        //#endregion
        //#region Get Stored Values 
        var StorageValue = this.GetStorage(filterType);
        if (StorageValue != null) {
            this.Active_FilterOptions = this.Active_FilterOptions.concat(StorageValue);
        }
        //#endregion
    };
    HelperService.prototype.Save_NewFilter = function (filterType) {
        //#region checkforduplicate
        var IsNameDuplicate = this.IsNameDuplicate();
        if (IsNameDuplicate) {
            this.NotifyError("Duplicate Name. Please Enter New Name");
            return;
        }
        var IsStructureDuplicate = this.IsStructureDuplicate();
        if (IsStructureDuplicate) {
            return;
        }
        //#endregion
        this.FilterSnap.id = this.Active_FilterOptions.length - 1;
        this.Active_FilterOptions.push(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap));
        this.SaveStorage(filterType, this.Active_FilterOptions.slice(1, this.Active_FilterOptions.length));
        //#region ReStorePreviousState 
        this.FilterSnap.id = this.FilterSnapPrev.id;
        this.FilterSnap.text = this.FilterSnapPrev.text;
        //#endregion
        this.FilterSnapTemprary = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap);
    };
    HelperService.prototype.Delete_Filter = function (filterType) {
        var deleteIndex = 0;
        for (var index = 0; index < this.Active_FilterOptions.length; index++) {
            var element = this.Active_FilterOptions[index];
            if (element.text == this.FilterSnap.text) {
                deleteIndex = index;
                break;
            }
        }
        //#region delete existing
        if (deleteIndex == 0) {
            this.NotifyError("default Filter cannot be deleted");
            return;
        }
        else {
            //#region set current snap to default
            this.FilterSnap = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.Active_FilterOptions[0]);
            this.FilterSnap.element = undefined;
            this.FilterSnapTemprary = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap);
            //#endregion
        }
        this.Active_FilterOptions.splice(deleteIndex, 1);
        this.SaveStorage(filterType, this.Active_FilterOptions.slice(1, this.Active_FilterOptions.length));
        //#endregion
    };
    HelperService.prototype.IsNameDuplicate = function () {
        for (var index = 0; index < this.Active_FilterOptions.length; index++) {
            var element = this.Active_FilterOptions[index];
            if (this.FilterSnap.text == element.text) {
                return true;
            }
        }
        return false;
    };
    HelperService.prototype.IsStructureDuplicate = function () {
        //#region clone FilterSnap 
        var FilterSnapClone = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap);
        FilterSnapClone.text = null;
        FilterSnapClone.id = null;
        //#endregion
        for (var index = 0; index < this.Active_FilterOptions.length; index++) {
            var element = this.Active_FilterOptions[index];
            var elementName = element.text;
            //#region clone ArrayElement 
            var ElementClone = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(element);
            ElementClone.text = null;
            ElementClone.id = null;
            //#endregion
            var areTwoEqual = this.AreTwoFilterSnapEqual(FilterSnapClone, ElementClone);
            if (areTwoEqual) {
                this.NotifyError("Duplicate Filter." + elementName + " " + "is similar Filter.");
                return true;
            }
        }
        return false;
    };
    HelperService.prototype.Active_FilterValueChanged = function (event) {
        this.FilterSnap = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(event.data[0]);
        this.FilterSnap.element = undefined;
        this.FilterSnapTemprary = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnap);
    };
    HelperService.prototype.Update_CurrentFilterSnap = function (event, Type, List_Config, SubType) {
        var ResetOffset = false;
        this.FilterSnapTemprary.RefreshData = false;
        this.FilterSnapTemprary.RefreshCount = false;
        if (!(Object(util__WEBPACK_IMPORTED_MODULE_16__["isNull"])(List_Config.SearchBaseConditions) ||
            Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(List_Config.SearchBaseConditions))) {
            this.FilterSnapTemprary.SearchBaseConditions = List_Config.SearchBaseConditions;
        }
        if (Type == this.AppConfig.ListToggleOption.Limit) {
            this.FilterSnapTemprary.PageRecordLimit = event;
            ResetOffset = true;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.Search) {
            if (event != null) {
                this.FilterSnapTemprary.SearchParameter = event.target.value;
            }
            ResetOffset = true;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.Date) {
            if (this.FilterSnapTemprary.StartDate == undefined &&
                this.FilterSnapTemprary.EndDate == undefined) {
                this.FilterSnapTemprary.StartTime = event.start;
                this.FilterSnapTemprary.EndTime = event.end;
            }
            ResetOffset = true;
            this.FilterSnapTemprary.RefreshData = false;
            this.FilterSnapTemprary.RefreshCount = false;
        }
        else if (Type == this.AppConfig.ListToggleOption.Status) {
            this.FilterSnapTemprary.StatusName = event.data[0].text;
            if (this.CurrentType == this.AppConfig.FilterTypeOption.MerchantSales) {
                this.FilterSnapTemprary.Status = event.data[0].id;
            }
            else {
                this.FilterSnapTemprary.Status = event.data[0].code;
            }
            this.FilterSnapTemprary.RefreshData = false;
            this.FilterSnapTemprary.RefreshCount = false;
            ResetOffset = false;
        }
        else if (Type == this.AppConfig.ListToggleOption.Sort) {
            if (Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(this.FilterSnapTemprary.Sort)) {
                this.FilterSnapTemprary.Sort = {};
            }
            this.FilterSnapTemprary.Sort.SortColumn = event.SystemName;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.SortOrder) {
            if (Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(this.FilterSnapTemprary.Sort)) {
                this.FilterSnapTemprary.Sort = {};
            }
            if (this.FilterSnapTemprary.Sort.SortOrder == "asc") {
                this.FilterSnapTemprary.Sort.SortOrder = "desc";
            }
            else {
                this.FilterSnapTemprary.Sort.SortOrder = "asc";
            }
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.SortApply) {
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.SortReset) {
            if (Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(this.FilterSnapTemprary.Sort)) {
                this.FilterSnapTemprary.Sort = {};
            }
            this.FilterSnapTemprary.Sort.SortOrder = "desc";
            this.FilterSnapTemprary.Sort.SortColumn = null;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.Page) {
            this.FilterSnapTemprary.ActivePage = event;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = false;
        }
        else if (Type == this.AppConfig.ListToggleOption.MultiSelect) {
            this.FilterSnapTemprary.ActivePage = event;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.Refresh) {
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.Refresh) {
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.ResetOffset) {
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
            ResetOffset = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.ApplyFilter) {
            ResetOffset = true;
            this.FilterSnapTemprary.RefreshData = true;
            this.FilterSnapTemprary.RefreshCount = true;
        }
        else if (Type == this.AppConfig.ListToggleOption.CancelFilter) {
        }
        else if (Type == this.AppConfig.ListToggleOption.Other) {
            var data = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(event.data[0]);
            data.element = undefined;
            data.OtherType = SubType;
            var CurrentIndex = this.FilterSnapTemprary.OtherFilters.findIndex(function (filter) { return (filter.data[0].OtherType == SubType); });
            if (CurrentIndex == -1) {
                this.FilterSnapTemprary.OtherFilters.push({ data: [data], value: event.value });
            }
            else {
                this.FilterSnapTemprary.OtherFilters[CurrentIndex] = { data: [data], value: event.value };
            }
            CurrentIndex = data;
        }
        if (ResetOffset == true) {
            this.FilterSnapTemprary.ActivePage = 1;
            this.FilterSnapTemprary.TotalRecords = 0;
            this.FilterSnapTemprary.ShowingStart = 0;
            this.FilterSnapTemprary.ShowingEnd = 0;
        }
    };
    HelperService.prototype.MakeFilterSnapPermanent = function () {
        this.FilterSnap = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.FilterSnapTemprary);
    };
    HelperService.prototype.ResetFilterSnap = function () {
        var index = (this.FilterSnap.id < 1) ? 0 : (this.FilterSnap.id - 1);
        this.FilterSnapTemprary = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.Active_FilterOptions[index]);
        this.FilterSnap = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_18__(this.Active_FilterOptions[index]);
    };
    //   public _FileSelect_Icon_onAdded(event: any) {
    //     var _FileName: string = event.file.name;
    //     var _File: File = event.file;
    //     var _FileReader: FileReader = new FileReader();
    //     _FileReader.onloadend = e => {
    //       var _FileContent = _FileReader.result;
    //       this._FileSelect_Icon_Processing = true;
    //       var _Image = new Image();
    //       _Image.src = _FileContent.toString();
    //       _Image.onload = () => {
    //         if (_Image.width == this._FileSelect_Icon_Data.Width && _Image.height == this._FileSelect_Icon_Data.Height) {
    //           var FileSelect_Icon_Data = this.GetImageDetails(_FileContent) as OStorageContent;
    //           this._FileSelect_Icon_Data.Content = FileSelect_Icon_Data.Content;
    //           this._FileSelect_Icon_Data.Extension = FileSelect_Icon_Data.Extension;
    //           this._FileSelect_Icon_Data.Name = _FileName;
    //           this._FileSelect_Icon_Data.Width = _Image.width;
    //           this._FileSelect_Icon_Data.Height = _Image.height;
    //         }
    //       else {
    //         this.NotifyWarning(this.AppConfig.CommonResource.InvalidImage);
    //         if (this._Ng2FileInputService.getCurrentFiles(this._FileSelect_Icon).length > 0) {
    //           this._Ng2FileInputService.reset(this._FileSelect_Icon);
    //         }
    //         this._FileSelect_Icon_Reset();
    //       }
    //     };
    //   };
    //   _FileReader.readAsDataURL(_File);
    // }
    HelperService.prototype._FileSelect_Icon_Reset = function () {
        this._FileSelect_Icon_Data.Name = null;
        this._FileSelect_Icon_Data.Content = null;
        this._FileSelect_Icon_Data.Extension = null;
        // this._FileSelect_Icon_Data.Height = 0;
        // this._FileSelect_Icon_Data.Width = 0;
        if (this._Ng2FileInputService.getCurrentFiles(this._FileSelect_Icon).length >
            0) {
            this._Ng2FileInputService.reset(this._FileSelect_Icon);
        }
        // if (this._FileSelect_Icon_Data.Content != null) {
        //   this._Ng2FileInputService.reset(this._FileSelect_Icon);
        // }
    };
    HelperService.prototype.Get_UserAccountDetails = function (ShowHeader) {
        var _this = this;
        this.AppConfig.ShowHeader = ShowHeader;
        this.IsFormProcessing = true;
        var pData = {
            Task: this.AppConfig.Api.Core.GetUserAccount,
            Reference: this.GetSearchConditionStrict("", "ReferenceKey", this.AppConfig.DataType.Text, this.AppConfig.ActiveReferenceKey, "="),
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this.StatusSuccess) {
                _this._UserAccount = _Response.Result;
                _this._UserAccount.DateOfBirth = _this.GetDateS(_this._UserAccount.DateOfBirth);
                _this._UserAccount.LastLoginDateS = _this.GetDateTimeS(_this._UserAccount.LastLoginDate);
                _this._UserAccount.CreateDateS = _this.GetDateTimeS(_this._UserAccount.CreateDate);
                _this._UserAccount.ModifyDateS = _this.GetDateTimeS(_this._UserAccount.ModifyDate);
                _this._UserAccount.StatusI = _this.GetStatusIcon(_this._UserAccount.StatusCode);
                _this._UserAccount.StatusB = _this.GetStatusBadge(_this._UserAccount.StatusCode);
                _this._UserAccount.StatusC = _this.GetStatusColor(_this._UserAccount.StatusCode);
                if (_this._UserAccount.CreatedByDisplayName != undefined) {
                    if (_this._UserAccount.CreatedByDisplayName.length > 8) {
                        _this._UserAccount.CreatedByDisplayNameShort =
                            _this._UserAccount.CreatedByDisplayName.substring(0, 8) + "..";
                    }
                    else {
                        _this._UserAccount.CreatedByDisplayNameShort = _this._UserAccount.CreatedByDisplayName;
                    }
                }
                if (_this._UserAccount.ModifyByDisplayName != undefined) {
                    if (_this._UserAccount.ModifyByDisplayName.length > 8) {
                        _this._UserAccount.ModifyByDisplayNameShort =
                            _this._UserAccount.ModifyByDisplayName.substring(0, 8) + "..";
                    }
                    else {
                        _this._UserAccount.ModifyByDisplayNameShort = _this._UserAccount.ModifyByDisplayName;
                    }
                }
                _this.IsFormProcessing = false;
            }
            else {
                _this.IsFormProcessing = false;
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.NotifySwalSuccess = function (Title, Message) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_15___default()({
            type: "success",
            title: Title,
            text: Message,
            position: this.AppConfig.Alert_Position,
            animation: this.AppConfig.Alert_AllowAnimation,
            customClass: this.AppConfig.Alert_Animation,
            allowOutsideClick: this.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this.AppConfig.Alert_AllowEscapeKey,
        }).then(function (result) { });
    };
    HelperService.prototype.NotifySwalError = function (Title, Message) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_15___default()({
            type: "error",
            title: Title,
            text: Message,
            position: this.AppConfig.Alert_Position,
            animation: this.AppConfig.Alert_AllowAnimation,
            customClass: this.AppConfig.Alert_Animation,
            allowOutsideClick: this.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this.AppConfig.Alert_AllowEscapeKey,
        }).then(function (result) { });
    };
    HelperService.prototype.NotifySuccess = function (Message) {
        notie__WEBPACK_IMPORTED_MODULE_12___default.a.alert({
            type: "1",
            text: Message,
        });
    };
    HelperService.prototype.NotifyWarning = function (Message) {
        notie__WEBPACK_IMPORTED_MODULE_12___default.a.alert({
            type: "2",
            text: Message,
        });
    };
    HelperService.prototype.NotifyError = function (Message) {
        notie__WEBPACK_IMPORTED_MODULE_12___default.a.alert({
            type: "3",
            text: Message,
        });
    };
    HelperService.prototype.NotifyInfo = function (Message) {
        notie__WEBPACK_IMPORTED_MODULE_12___default.a.alert({
            type: "4",
            text: Message,
        });
    };
    HelperService.prototype.HandleException = function (Exception) {
        this.IsFormProcessing = false;
        // console.log(Exception);
        if (Exception.status != undefined &&
            Exception.error != null &&
            Exception.status == 401) {
            var ResponseData = JSON.parse(atob(Exception.error.zx));
            this.NotifyError(ResponseData.Message);
            setTimeout(function () {
                window.location.href = "/account/login";
            }, 1500);
        }
        else {
            if (Exception.error instanceof Error) {
                this.NotifyError("Sorry, error occured while connecting server:" +
                    Exception.error.message);
            }
            else {
                this.NotifyError("Sorry, error occured while connecting server : " +
                    Exception.message +
                    " Response Code -" +
                    Exception.status);
            }
        }
    };
    HelperService.prototype.GetRandomNumber = function () {
        this.RandomNumber = Math.floor(1000 + Math.random() * 9000);
        return this.RandomNumber;
    };
    HelperService.prototype.GeneratePassoword = function () {
        var plength = 14;
        var keylistalpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var keylistint = "123456789";
        var keylistspec = "!@#_";
        var temp = "";
        var len = plength / 2;
        var len = len - 1;
        var lenspec = plength - len - len;
        for (var index = 0; index < len; index++)
            temp += keylistalpha.charAt(Math.floor(Math.random() * keylistalpha.length));
        for (var index = 0; index < lenspec; index++)
            temp += keylistspec.charAt(Math.floor(Math.random() * keylistspec.length));
        for (var index = 0; index < len; index++)
            temp += keylistint.charAt(Math.floor(Math.random() * keylistint.length));
        temp = temp
            .split("")
            .sort(function () {
            return 0.5 - Math.random();
        })
            .join("");
        this.RandomPassword = temp;
        return temp;
    };
    HelperService.prototype.RemoveItemFromArray = function (ItemToRemove, DataSet) {
        var Index = DataSet.indexOf(ItemToRemove, 0);
        if (Index > -1) {
            DataSet.splice(Index, 1);
        }
        return DataSet;
    };
    HelperService.prototype.OpenModal = function (ModalId) {
        $("#" + ModalId).modal("show");
    };
    HelperService.prototype.CloseModal = function (ModalId) {
        $("#" + ModalId).modal("hide");
    };
    HelperService.prototype.canActivate = function (route, state) {
        this.Poster_Crop_Clear();
        this.Icon_Crop_Clear();
        this.AppConfig.ShowHeader = true;
        this.FullContainer = false;
        if (this.GetStorage(this.AppConfig.Storage.Account) != null) {
            return true;
        }
        else {
            this._Router.navigate([this.AppConfig.Pages.System.Login]);
            return false;
        }
        return true;
    };
    HelperService.prototype.canActivateChild = function (childRoute, state) {
        var _this = this;
        // this.AppConfig.ActiveOwnerAccountTypeCode = null;
        // this.AppConfig.ActiveOwnerAccountTypeId = null;
        this.AppConfig.ShowHeader = true;
        this.FullContainer = false;
        var AccountTypeCode = childRoute.data.accounttypecode;
        if (AccountTypeCode != undefined) {
            this.AppConfig.ActiveReferenceAccountTypeCode = AccountTypeCode;
        }
        else {
            this.AppConfig.ActiveReferenceAccountTypeCode = null;
        }
        var ActiveReferenceListType = childRoute.data.listtype;
        if (ActiveReferenceListType != undefined) {
            this.AppConfig.ActiveReferenceListType = ActiveReferenceListType;
        }
        else {
            this.AppConfig.ActiveReferenceListType = null;
        }
        if (this.UserAccount != null) {
            var PageName = childRoute.data.PageName;
            if (PageName != undefined) {
                this._TranslateService.get(PageName).subscribe(function (_PageName) {
                    _this.AppConfig.ActivePageName = _PageName;
                });
            }
            return true;
        }
        else {
            this._Router.navigate([this.AppConfig.Pages.System.Login]);
            return false;
        }
        return true;
        // if (childRoute.params.referencekey != undefined) {
        //     this._HelperService.ActiveReferenceKey = childRoute.params.referencekey;
        // }
        // else {
        //     this._HelperService.ActiveReferenceKey = null;
        // }
        // if (childRoute.data.accounttypecode != undefined) {
        //     this._HelperService.ActiveReferenceAccountTypeCode = childRoute.data.accounttypecode;
        // }
        // else {
        //     this._HelperService.ActiveReferenceAccountTypeCode = null;
        // }
        // if (childRoute.data.listtype != undefined) {
        //     this._HelperService.ActiveListType = childRoute.data.listtype;
        // }
        // else {
        //     this._HelperService.ActiveListType = null;
        // }
        return true;
    };
    HelperService.prototype.GetDateTime = function (Date) {
        return SystemHelper.GetDateTime(Date, this.AppConfig.TimeZone);
    };
    HelperService.prototype.GetTimeS = function (Date) {
        return SystemHelper.GetTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.TimeFormat);
    };
    HelperService.prototype.GetDateS = function (Date) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, this.AppConfig.DateFormat);
    };
    HelperService.prototype.GetDayS = function (Date) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, this.AppConfig.DateDayFormat);
    };
    HelperService.prototype.GetDateMonthS = function (Date) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, this.AppConfig.DateMonthFormat);
    };
    HelperService.prototype.GetDateYearS = function (Date) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, this.AppConfig.DateYearFormat);
    };
    HelperService.prototype.TriggerClick = function (Item) {
        return SystemHelper.TriggerClick(Item);
    };
    HelperService.prototype.CheckDateIsAfter = function (Date, CompareTo) {
        return SystemHelper.CheckDateIsAfter(Date, CompareTo);
    };
    HelperService.prototype.CheckDateIsBefore = function (Date, CompareTo) {
        return SystemHelper.CheckDateIsBefore(Date, CompareTo);
    };
    HelperService.prototype.GetTimeDifference = function (Date, CompareTo) {
        return SystemHelper.GetTimeDifference(Date, CompareTo);
    };
    HelperService.prototype.GetTimeDifferenceS = function (Date, CompareTo) {
        return SystemHelper.GetTimeDifferenceS(Date, CompareTo);
    };
    HelperService.prototype.GetDateTimeS = function (Date) {
        return SystemHelper.GetDateTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.DateTimeFormat);
    };
    HelperService.prototype.GetDateTimeSForChart = function (Date) {
        return SystemHelper.GetDateTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.DateTimeFormatForChart);
    };
    HelperService.prototype.GetDateTimeSChart = function (Date) {
        return SystemHelper.GetDateTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.DateTimeFormatForChartX);
    };
    HelperService.prototype.GetTimeInterval = function (Date, CompareTo) {
        return SystemHelper.GetTimeInterval(Date, CompareTo, this.AppConfig.TimeZone);
    };
    HelperService.prototype.newGuid = function () {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0, v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    };
    HelperService.prototype.dec2hex = function (dec) {
        return ("0" + dec.toString(16)).substr(-2);
    };
    HelperService.prototype.GenerateGuid = function () {
        var arr = new Uint8Array(40 / 2);
        window.crypto.getRandomValues(arr);
        return Array.from(arr, this.dec2hex).join("");
    };
    HelperService.prototype.GenerateId = function () {
        return Math.floor(Math.random() * 90000) + 10000;
    };
    HelperService.prototype.DivideTwoNumbers = function (num, din) {
        var isDinUndefined = Object(util__WEBPACK_IMPORTED_MODULE_16__["isUndefined"])(din);
        var isDinNull = Object(util__WEBPACK_IMPORTED_MODULE_16__["isNull"])(din);
        if (!isDinUndefined && !isDinNull && din == 0) {
            return 0;
        }
        else {
            return num / din;
        }
    };
    HelperService.prototype.GetStatusBadge = function (StatusCode) {
        var ColorCyan = "badge cyan";
        var ColorSuccess = "badge badge-success";
        var ColorWarning = "badge badge-warning";
        var ColorDanger = "badge badge-danger";
        var StatusIcon = "";
        if (isNaN(StatusCode) == false) {
            if (StatusCode == 293 ||
                StatusCode == 294 ||
                StatusCode == 1 ||
                StatusCode == 450 ||
                StatusCode == 452 ||
                StatusCode == 4 ||
                StatusCode == 299 ||
                StatusCode == 298 ||
                StatusCode == 29 ||
                StatusCode == 27 ||
                StatusCode == 461 ||
                StatusCode == 38) {
                StatusIcon = ColorWarning;
            }
            if (StatusCode == 2 ||
                StatusCode == 28 ||
                StatusCode == 451 ||
                StatusCode == 295 ||
                StatusCode == 297 ||
                StatusCode == 463 ||
                StatusCode == 39) {
                StatusIcon = ColorSuccess;
            }
            if (StatusCode == 3 ||
                StatusCode == 32 ||
                StatusCode == 30 ||
                StatusCode == 453 ||
                StatusCode == 454 ||
                StatusCode == 455 ||
                StatusCode == 31 ||
                StatusCode == 296 ||
                StatusCode == 300 ||
                StatusCode == 462 ||
                StatusCode == 464 ||
                StatusCode == 301 ||
                StatusCode == 40) {
                StatusIcon = ColorDanger;
            }
            return StatusIcon;
        }
        else {
            if (StatusCode == "campaign.paused" ||
                StatusCode == "campaign.creating" ||
                StatusCode == "campaign.underreview" ||
                StatusCode == "campaign.lowbalance" ||
                StatusCode == "default.inactive" ||
                StatusCode == "default.suspended" ||
                StatusCode == "transaction.pending" ||
                StatusCode == "invoice.pending" ||
                StatusCode == "transaction.processing") {
                StatusIcon = ColorWarning;
            }
            if (StatusCode == "campaign.approved" ||
                StatusCode == "campaign.published" ||
                StatusCode == "default.active" ||
                StatusCode == "default.padi" ||
                StatusCode == "transaction.success") {
                StatusIcon = ColorSuccess;
            }
            if (StatusCode == "campaign.expired" ||
                StatusCode == "campaign.archived" ||
                StatusCode == "campaign.rejected" ||
                StatusCode == "default.disabled" ||
                StatusCode == "default.blocked" ||
                StatusCode == "transaction.error" ||
                StatusCode == "transaction.failed" ||
                StatusCode == "transaction.cancelled" ||
                StatusCode == "invoice.cancelled") {
                StatusIcon = ColorDanger;
            }
            return StatusIcon;
        }
    };
    HelperService.prototype.GetStatusColor = function (StatusCode) {
        var ColorSuccess = "#20ab68";
        var ColorWarning = "#fda61c";
        var ColorDanger = "#e9223f";
        var StatusIcon = "";
        if (isNaN(StatusCode) == false) {
            if (StatusCode == 1 ||
                StatusCode == 4 ||
                StatusCode == 450 ||
                StatusCode == 452 ||
                StatusCode == 29 ||
                StatusCode == 461 ||
                StatusCode == 27) {
                StatusIcon = ColorWarning;
            }
            if (StatusCode == 2 ||
                StatusCode == 451 ||
                StatusCode == 463 ||
                StatusCode == 28) {
                StatusIcon = ColorSuccess;
            }
            if (StatusCode == 3 ||
                StatusCode == 32 ||
                StatusCode == 453 ||
                StatusCode == 454 ||
                StatusCode == 455 ||
                StatusCode == 462 ||
                StatusCode == 464 ||
                StatusCode == 30 ||
                StatusCode == 31) {
                StatusIcon = ColorDanger;
            }
            return StatusIcon;
        }
        else {
            if (StatusCode == "default.inactive" ||
                StatusCode == "default.suspended" ||
                StatusCode == "transaction.pending" ||
                StatusCode == "transaction.processing") {
                StatusIcon = ColorWarning;
            }
            if (StatusCode == "default.active" ||
                StatusCode == "transaction.success") {
                StatusIcon = ColorSuccess;
            }
            if (StatusCode == "default.disabled" ||
                StatusCode == "default.blocked" ||
                StatusCode == "transaction.error" ||
                StatusCode == "transaction.failed" ||
                StatusCode == "transaction.cancelled") {
                StatusIcon = ColorDanger;
            }
            return StatusIcon;
        }
    };
    HelperService.prototype.GetStatusIcon = function (StatusCode) {
        var ColorPrimary = "text-primary";
        var ColorInfo = "text-info";
        var ColorSuccess = "text-success";
        var ColorWarn = "text-warn";
        var ColorWarning = "text-warning";
        var ColorDanger = "text-danger";
        var StatusIcon = "fa fa-circle text-default f-s-11 p-r-5 ";
        if (isNaN(StatusCode) == false) {
            if (StatusCode == 1 ||
                StatusCode == 4 ||
                StatusCode == 29 ||
                StatusCode == 450 ||
                StatusCode == 452 ||
                StatusCode == 461 ||
                StatusCode == 27) {
                StatusIcon = "fa fa-circle text-warning f-s-11 p-r-5 ";
            }
            if (StatusCode == 2 ||
                StatusCode == 463 ||
                StatusCode == 451 ||
                StatusCode == 28) {
                StatusIcon = "fa fa-circle text-success f-s-11 p-r-5";
            }
            if (StatusCode == 3 ||
                StatusCode == 32 ||
                StatusCode == 462 ||
                StatusCode == 464 ||
                StatusCode == 453 ||
                StatusCode == 454 ||
                StatusCode == 455 ||
                StatusCode == 30 ||
                StatusCode == 31) {
                StatusIcon = "fa fa-circle text-danger f-s-11 p-r-5";
            }
            return StatusIcon;
        }
        else {
            if (StatusCode == "default.suspended" ||
                StatusCode == "default.inactive" ||
                StatusCode == "transaction.pending" ||
                StatusCode == "transaction.processing") {
                StatusIcon = "fa fa-circle text-warning f-s-11 p-r-5 ";
            }
            if (StatusCode == "default.active" ||
                StatusCode == "transaction.success") {
                StatusIcon = "fa fa-circle text-success f-s-11 p-r-5";
            }
            if (StatusCode == "default.disabled" ||
                StatusCode == "default.blocked" ||
                StatusCode == "transaction.error" ||
                StatusCode == "transaction.failed" ||
                StatusCode == "transaction.cancelled") {
                StatusIcon = "fa fa-circle text-danger f-s-11 p-r-5";
            }
            return StatusIcon;
        }
    };
    HelperService.prototype.SaveStorage = function (StorageName, StorageValue) {
        try {
            var StringV = btoa(JSON.stringify(StorageValue));
            localStorage.setItem(StorageName, StringV);
            return true;
        }
        catch (e) {
            // console.log(e);
            alert(e);
            return false;
        }
    };
    HelperService.prototype.SaveStorageValue = function (StorageName, StorageValue) {
        try {
            localStorage.setItem(StorageName, btoa(StorageValue));
            return true;
        }
        catch (e) {
            alert(e);
            return false;
        }
    };
    HelperService.prototype.GetStorage = function (StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                return JSON.parse(atob(StorageValue));
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    };
    HelperService.prototype.GetStorageValue = function (StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                return atob(StorageValue);
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    };
    HelperService.prototype.DeleteStorage = function (StorageName) {
        localStorage.removeItem(StorageName);
        return true;
    };
    HelperService.prototype.DownloadCsv = function (Data, FileName, CsvOptions) {
        // this._Angular2CsvModule.();
        // new Angu(Data, FileName, CsvOptions);
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_6__["Angular2Csv"](Data, FileName, CsvOptions);
        //   new Angular2Csv(Data, FileName, CsvOptions);
    };
    HelperService.prototype.PreventText = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    HelperService.prototype.GetSearchCondition = function (BaseString, ColumnName, ColumnType, ColumnValue) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" &&
            ColumnValue != "") {
            if (ColumnType == "text") {
                if (SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != "") {
                    SearchExpression += " OR ";
                }
                SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
            }
            else if (ColumnType == "number") {
                if (isNaN(ColumnValue) == false) {
                    if (SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != "") {
                        SearchExpression = "( " + SearchExpression + ") AND ";
                    }
                    SearchExpression += ColumnName + " = " + ColumnValue + " ";
                }
            }
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetSortCondition = function (SortConditions) {
        var NewSortCondition = "";
        if (SortConditions != undefined ||
            SortConditions != null ||
            SortConditions.length != 0) {
            for (var index = 0; index < SortConditions.length; index++) {
                var element = SortConditions[index];
                if (index == SortConditions.length - 1) {
                    NewSortCondition = NewSortCondition + element;
                }
                else {
                    NewSortCondition = NewSortCondition + element + ",";
                }
            }
        }
        return NewSortCondition;
    };
    HelperService.prototype.GetSearchConditionStrict = function (BaseString, ColumnName, ColumnType, ColumnValue, Condition) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            // ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
        // ColumnValue != ''
        ) {
            if (ColumnType == "text") {
                if (SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != "") {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                }
                else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                }
                else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            }
            else if (ColumnType == "number") {
                if (SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != "") {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                }
                else {
                    SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                }
            }
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetSearchConditionStrictOr = function (BaseString, ColumnName, ColumnType, ColumnValue, Condition) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            // ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
        // ColumnValue != ''
        ) {
            if (ColumnType == this.AppConfig.DataType.Text) {
                if (SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != "") {
                    SearchExpression = "( " + SearchExpression + ") OR ";
                }
                // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                }
                else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                }
                else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            }
            else if (ColumnType == this.AppConfig.DataType.Number) {
                if (isNaN(ColumnValue) == false &&
                    ColumnValue.toString().indexOf(".") == -1) {
                    if (SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != "") {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    }
                    else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            }
            else if (ColumnType == this.AppConfig.DataType.Decimal) {
                if (isNaN(ColumnValue) == false) {
                    if (SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != "") {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    }
                    else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            }
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetDateCondition = function (BaseString, ColumnName, StartTime, EndTime) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            StartTime != undefined &&
            EndTime != undefined &&
            ColumnName != null &&
            StartTime != null &&
            EndTime != null &&
            ColumnName != "" &&
            StartTime != "" &&
            EndTime != "") {
            StartTime = moment(StartTime).subtract(1, "seconds");
            EndTime = moment(EndTime).add(1, "seconds");
            var FSd = new Date(StartTime);
            var TStartDateM = moment(FSd).utc().format("YYYY-MM-DD HH:mm:ss");
            var ESd = new Date(EndTime);
            var TEndTimeM = moment(ESd).utc().format("YYYY-MM-DD HH:mm:ss");
            // var TStartTime = FSd.getFullYear() + '-' + (FSd.getMonth() + 1) + '-' + (FSd.getDate()) + ' 23:59:00';
            // var TEndTime = ESd.getFullYear() + '-' + (ESd.getMonth() + 1) + '-' + (ESd.getDate()) + ' 23:59:00';
            if (SearchExpression != undefined &&
                SearchExpression != null &&
                SearchExpression != "") {
                SearchExpression = "( " + SearchExpression + ") AND ";
            }
            SearchExpression +=
                "( " +
                    ColumnName +
                    ' > "' +
                    TStartDateM +
                    '" AND ' +
                    ColumnName +
                    ' < "' +
                    TEndTimeM +
                    '")';
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetDateOnlyCondition = function (BaseString, ColumnName, StartTime) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            StartTime != undefined &&
            ColumnName != null &&
            StartTime != null &&
            ColumnName != "" &&
            StartTime != "") {
            StartTime = moment(StartTime).format("LL");
            var FSd = new Date(StartTime);
            var TStartDateM = moment(FSd).utc().format("YYYY-MM-DD");
            if (SearchExpression != undefined &&
                SearchExpression != null &&
                SearchExpression != "") {
                SearchExpression = "( " + SearchExpression + ") AND ";
            }
            SearchExpression += "( " + ColumnName + ' = "' + TStartDateM + '")';
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetSearchConditionRange = function (BaseString, ColumnName, StartRange, EndRange) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        StartRange = StartRange - 1;
        EndRange = EndRange + 1;
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            StartRange != undefined &&
            EndRange != undefined &&
            ColumnName != null &&
            StartRange != null &&
            EndRange != null &&
            ColumnName != "" &&
            StartRange != "" &&
            EndRange != "") {
            if (SearchExpression != undefined &&
                SearchExpression != null &&
                SearchExpression != "") {
                SearchExpression = "( " + SearchExpression + ") AND ";
            }
            SearchExpression +=
                "( " +
                    ColumnName +
                    ' > "' +
                    StartRange +
                    '" AND ' +
                    ColumnName +
                    ' < "' +
                    EndRange +
                    '")';
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetSearchConditionStrictFromArray = function (BaseString, ColumnName, ColumnType, ColumnValueArray, Condition) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
        ) {
            if (ColumnValueArray != undefined ||
                ColumnValueArray != null ||
                ColumnValueArray.length != 0) {
                if (ColumnType == "text") {
                    var TExpression = "";
                    for (var index = 0; index < ColumnValueArray.length; index++) {
                        var ColumnValue = ColumnValueArray[index];
                        if (TExpression != "") {
                            TExpression += " OR ";
                        }
                        if (ColumnValue != null && ColumnValue != "") {
                            TExpression +=
                                ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                        }
                        else {
                            TExpression +=
                                " " + ColumnName + " " + Condition + " null " + " ";
                        }
                    }
                    if (TExpression != "") {
                        if (SearchExpression != "") {
                            SearchExpression =
                                "( " + SearchExpression + ") AND (" + TExpression + ")";
                        }
                        else {
                            SearchExpression = TExpression;
                        }
                    }
                }
                else if (ColumnType == "number") {
                    var TColSearch = "";
                    if (ColumnValueArray.length == 1) {
                        var ColumnValue = ColumnValueArray[0];
                        if (ColumnValue != null && ColumnValue != "") {
                            TColSearch +=
                                " " + ColumnName + " " + Condition + ' "' + ColumnValue + '"';
                        }
                        else {
                            TColSearch += " " + ColumnName + " " + Condition + " " + "";
                        }
                    }
                    else {
                        for (var index = 0; index < ColumnValueArray.length; index++) {
                            var ColumnValue = ColumnValueArray[index];
                            if (TColSearch != "") {
                                TColSearch += " OR ";
                            }
                            if (ColumnValue != null && ColumnValue != "") {
                                TColSearch +=
                                    " " +
                                        ColumnName +
                                        " " +
                                        Condition +
                                        ' "' +
                                        ColumnValue +
                                        '" ';
                            }
                            else {
                                TColSearch += "  " + ColumnName + " " + Condition + " " + " ";
                            }
                        }
                    }
                    if (TColSearch != "") {
                        if (SearchExpression != "") {
                            SearchExpression =
                                "( " + SearchExpression + ") AND (" + TColSearch + ")";
                        }
                        else {
                            SearchExpression = TColSearch;
                        }
                    }
                }
            }
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetSearchConditionSubStrictFromArray = function (BaseString, ColumnName, ColumnType, ColumnValueArray, Condition) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }
        var SearchExpression = BaseString;
        if (ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
        ) {
            if (ColumnValueArray != undefined ||
                ColumnValueArray != null ||
                ColumnValueArray.length != 0) {
                if (ColumnType == "text") {
                    var TExpression = "";
                    for (var index = 0; index < ColumnValueArray.length; index++) {
                        var ColumnValue = ColumnValueArray[index];
                        if (TExpression != "") {
                            TExpression += " OR ";
                        }
                        if (ColumnValue != null && ColumnValue != "") {
                            TExpression +=
                                ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                        }
                        else {
                            TExpression +=
                                " " + ColumnName + " " + Condition + " null " + " ";
                        }
                    }
                    if (TExpression != "") {
                        if (SearchExpression != "") {
                            SearchExpression =
                                "( " + SearchExpression + ") AND (" + TExpression + ")";
                        }
                        else {
                            SearchExpression = TExpression;
                        }
                    }
                }
                else if (ColumnType == "number") {
                    var TColSearch = "";
                    if (ColumnValueArray.length == 1) {
                        var ColumnValue = ColumnValueArray[0];
                        if (ColumnValue != null && ColumnValue != "") {
                            TColSearch +=
                                " " + ColumnName + " " + Condition + ' "' + ColumnValue + '"';
                        }
                        else {
                            TColSearch += " " + ColumnName + " " + Condition + " " + "";
                        }
                    }
                    else {
                        for (var index = 0; index < ColumnValueArray.length; index++) {
                            var ColumnValue = ColumnValueArray[index];
                            if (TColSearch != "") {
                                TColSearch += " OR ";
                            }
                            if (ColumnValue != null && ColumnValue != "") {
                                TColSearch +=
                                    " " +
                                        ColumnName +
                                        " " +
                                        Condition +
                                        ' "' +
                                        ColumnValue +
                                        '" ';
                            }
                            else {
                                TColSearch += "  " + ColumnName + " " + Condition + " " + " ";
                            }
                        }
                    }
                    if (TColSearch != "") {
                        if (SearchExpression != "") {
                            SearchExpression =
                                "( " + SearchExpression + ") AND (" + TColSearch + ")";
                        }
                        else {
                            SearchExpression = TColSearch;
                        }
                    }
                }
            }
            return SearchExpression;
        }
        else {
            return BaseString;
        }
    };
    HelperService.prototype.GetImageDetails = function (ImageBase64) {
        var _ImageContent = {
            Name: null,
            Content: null,
            Extension: null,
            TypeCode: null,
            Width: 0,
            Height: 0,
        };
        if (ImageBase64 != null && ImageBase64 != "") {
            var ImageContent = ImageBase64;
            var ImageExtension = "jpg";
            var ImageBase = ImageContent.replace("data:image/png;base64,", "")
                .replace("data:image/jpg;base64,", "")
                .replace("data:image/jpeg;base64,", "")
                .replace("data:image/gif;base64,", "");
            if (ImageContent.includes("data:image/png;base64,")) {
                ImageExtension = "png";
            }
            else if (ImageContent.includes("data:image/jpg;base64,")) {
                ImageExtension = "jpg";
            }
            else if (ImageContent.includes("data:image/jpeg;base64,")) {
                ImageExtension = "jpeg";
            }
            else if (ImageContent.includes("data:image/gif;base64,")) {
                ImageExtension = "gif";
            }
            else {
                ImageExtension = "jpg";
            }
            _ImageContent.Content = ImageBase;
            _ImageContent.Extension = ImageExtension;
            return _ImageContent;
        }
        else {
            return _ImageContent;
        }
    };
    HelperService.prototype.GetTerminalStatusCount = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getterminalstatuscount",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._TerminalStatusCount = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetUserCounts = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getusercounts",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._UserCounts = _Response.Result;
                if (_this._AccountOverview.SettlementCredit != undefined &&
                    _this._AccountOverview.SettlementDebit != undefined) {
                    _this._AccountOverview.SettlementPending =
                        _this._AccountOverview.SettlementCredit -
                            _this._AccountOverview.SettlementDebit;
                }
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetSalesOverview = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        var _this = this;
        this.IsFormProcessing = true;
        this._SalesOverview.TransactionTypeLabels = [];
        this._SalesOverview.TransactionTypeData = [];
        this._SalesOverview.TransactionTypeUsersLabels = [];
        this._SalesOverview.TransactionTypeUsersData = [];
        this._SalesOverview.TransactionStatusLabels = [];
        this._SalesOverview.TransactionStatusData = [];
        this._SalesOverview.TransactionTypeSalesLabels = [];
        this._SalesOverview.TransactionTypeSalesData = [];
        var Data = {
            Task: "getsalesoverview",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._SalesOverview = _Response.Result;
                _this._SalesOverview.FirstTransactionDate = _this.GetDateTimeS(_this._SalesOverview.FirstTransactionDate);
                _this._SalesOverview.LastTransactionDateD = _this.GetTimeDifferenceS(_this._SalesOverview.LastTransactionDate, moment());
                _this._SalesOverview.LastTransactionDate = _this.GetDateTimeS(_this._SalesOverview.LastTransactionDate);
                _this._SalesOverview.TransactionTypeUsersLabels = [];
                _this._SalesOverview.TransactionTypeUsersData = [];
                _this._SalesOverview.TransactionTypeUsersLabels.push("Cash Tr Users");
                _this._SalesOverview.TransactionTypeUsersLabels.push("Card Tr. Users");
                _this._SalesOverview.TransactionTypeUsersData.push(_this._SalesOverview.TotalCashTransactionCustomer);
                _this._SalesOverview.TransactionTypeUsersData.push(_this._SalesOverview.TotalCardTransactionCustomer);
                _this._SalesOverview.TransactionTypeSalesLabels = [];
                _this._SalesOverview.TransactionTypeSalesData = [];
                _this._SalesOverview.TransactionTypeSalesLabels.push("Cash Payments");
                _this._SalesOverview.TransactionTypeSalesLabels.push("Card Payments");
                _this._SalesOverview.TransactionTypeSalesData.push(_this._SalesOverview.TotalCashTransactionInvoiceAmount);
                _this._SalesOverview.TransactionTypeSalesData.push(_this._SalesOverview.TotalCardTransactionInvoiceAmount);
                _this._SalesOverview.TransactionTypeLabels = [];
                _this._SalesOverview.TransactionTypeData = [];
                _this._SalesOverview.TransactionTypeLabels.push("Cash Transactions");
                _this._SalesOverview.TransactionTypeLabels.push("Card Transactions");
                _this._SalesOverview.TransactionTypeData.push(_this._SalesOverview.TotalCashTransaction);
                _this._SalesOverview.TransactionTypeData.push(_this._SalesOverview.TotalCardTransaction);
                _this._SalesOverview.TransactionStatusLabels = [];
                _this._SalesOverview.TransactionStatusData = [];
                _this._SalesOverview.TransactionStatusLabels.push("Success");
                _this._SalesOverview.TransactionStatusLabels.push("Failed");
                _this._SalesOverview.TransactionStatusData.push(_this._SalesOverview.TotalSuccessfulTransaction);
                _this._SalesOverview.TransactionStatusData.push(_this._SalesOverview.TotalFailedTransaction);
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetSalesSummary = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        this._SalesSummary.TransactionTypeLabels = [];
        this._SalesSummary.TransactionTypeData = [];
        this._SalesSummary.TransactionTypeUsersLabels = [];
        this._SalesSummary.TransactionTypeUsersData = [];
        this._SalesSummary.TransactionStatusLabels = [];
        this._SalesSummary.TransactionStatusData = [];
        this._SalesSummary.TransactionTypeSalesLabels = [];
        this._SalesSummary.TransactionTypeSalesData = [];
        var Data = {
            Task: "getsalessummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._SalesSummary = _Response.Result;
                _this._SalesSummary.FirstTransactionDate = _this.GetDateTimeS(_this._SalesSummary.FirstTransactionDate);
                _this._SalesSummary.LastTransactionDateD = _this.GetTimeDifferenceS(_this._SalesSummary.LastTransactionDate, moment());
                _this._SalesSummary.LastTransactionDate = _this.GetDateTimeS(_this._SalesSummary.LastTransactionDate);
                _this._SalesSummary.TransactionTypeUsersLabels = [];
                _this._SalesSummary.TransactionTypeUsersData = [];
                _this._SalesSummary.TransactionTypeUsersLabels.push("Cash Tr Users");
                _this._SalesSummary.TransactionTypeUsersLabels.push("Card Tr. Users");
                _this._SalesSummary.TransactionTypeUsersData.push(_this._SalesSummary.TotalCashTransactionCustomer);
                _this._SalesSummary.TransactionTypeUsersData.push(_this._SalesSummary.TotalCardTransactionCustomer);
                _this._SalesSummary.TransactionTypeSalesLabels = [];
                _this._SalesSummary.TransactionTypeSalesData = [];
                _this._SalesSummary.TransactionTypeSalesLabels.push("Cash Payments");
                _this._SalesSummary.TransactionTypeSalesLabels.push("Card Payments");
                _this._SalesSummary.TransactionTypeSalesData.push(_this._SalesSummary.TotalCashTransactionInvoiceAmount);
                _this._SalesSummary.TransactionTypeSalesData.push(_this._SalesSummary.TotalCardTransactionInvoiceAmount);
                _this._SalesSummary.TransactionTypeLabels = [];
                _this._SalesSummary.TransactionTypeData = [];
                _this._SalesSummary.TransactionTypeLabels.push("Cash Transactions");
                _this._SalesSummary.TransactionTypeLabels.push("Card Transactions");
                _this._SalesSummary.TransactionTypeData.push(_this._SalesSummary.TotalCashTransaction);
                _this._SalesSummary.TransactionTypeData.push(_this._SalesSummary.TotalCardTransaction);
                _this._SalesSummary.TransactionStatusLabels = [];
                _this._SalesSummary.TransactionStatusData = [];
                _this._SalesSummary.TransactionStatusLabels.push("Success");
                _this._SalesSummary.TransactionStatusLabels.push("Failed");
                _this._SalesSummary.TransactionStatusData.push(_this._SalesSummary.TotalSuccessfulTransaction);
                _this._SalesSummary.TransactionStatusData.push(_this._SalesSummary.TotalFailedTransaction);
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRewardsSummary = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardssummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RewardsSummary = _Response.Result;
                _this._RewardsSummary.FirstTransactionDateD = _this.GetDateTimeS(_this._RewardsSummary.FirstTransactionDate);
                _this._RewardsSummary.FirstTransactionDate = _this.GetDateTimeS(_this._RewardsSummary.FirstTransactionDate);
                _this._RewardsSummary.LastTransactionDateD = _this.GetTimeDifferenceS(_this._RewardsSummary.LastTransactionDate, moment());
                _this._RewardsSummary.LastTransactionDate = _this.GetDateTimeS(_this._RewardsSummary.LastTransactionDate);
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetSalesHistory = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getsaleshistory",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                var TStatusDataSet = [];
                var TStatusDataSetColor = [];
                _this._OSalesHistory.Labels = [];
                var ResponseData = _Response.Result;
                var TSaleColor = [
                    {
                        backgroundColor: [],
                    },
                ];
                var TSaleDataSet = [
                    {
                        label: "Sale",
                        fill: false,
                        borderColor: "#907eec",
                        borderDash: [5, 5],
                        backgroundColor: "#907eec",
                        pointBackgroundColor: "#fdab29",
                        pointBorderColor: "#fdab29",
                        pointHoverBackgroundColor: "#fdab29",
                        pointHoverBorderColor: "#fdab29",
                        data: [],
                    },
                ];
                var TSaleCustomersColor = [
                    {
                        backgroundColor: [],
                    },
                ];
                var TSaleCustomersDataSet = [
                    {
                        label: "Customer Visits",
                        fill: false,
                        borderColor: "#907eec",
                        borderDash: [5, 5],
                        backgroundColor: "#907eec",
                        pointBackgroundColor: "#fdab29",
                        pointBorderColor: "#fdab29",
                        pointHoverBackgroundColor: "#fdab29",
                        pointHoverBorderColor: "#fdab29",
                        data: [],
                    },
                ];
                var TTypeDataSetColor = [];
                var TTypeDataSet = [];
                var TTypeCustomersDataSetColor = [];
                var TTypeCustomersDataSet = [];
                var TTypeDataSetCardItemColor = {
                    backgroundColor: [],
                };
                var TTypeDataSetCashItemColor = {
                    backgroundColor: [],
                };
                var TTypeCustomersDataSetCardItemColor = {
                    backgroundColor: [],
                };
                var TTypeCustomersDataSetCashItemColor = {
                    backgroundColor: [],
                };
                var TTypeDataSetCardItem = {
                    label: "Card",
                    backgroundColor: [],
                    data: [],
                };
                var TTypeDataSetCashItem = {
                    label: "Cash",
                    backgroundColor: [],
                    data: [],
                };
                var TTypeCustomersDataSetCardItem = {
                    label: "Card Customers",
                    backgroundColor: [],
                    data: [],
                };
                var TTypeCustomersDataSetCashItem = {
                    label: "Cash Customers",
                    backgroundColor: [],
                    data: [],
                };
                var TStatusDataSetSuccessItemColor = {
                    backgroundColor: [],
                };
                var TStatusDataSetErrorItemError = {
                    backgroundColor: [],
                };
                var TStatusDataSetSuccessItem = {
                    label: "Success",
                    backgroundColor: [],
                    data: [],
                };
                var TStatusDataSetErrorItem = {
                    label: "Error",
                    backgroundColor: [],
                    data: [],
                };
                ResponseData.forEach(function (element) {
                    var Data = element.Data;
                    _this._OSalesHistory.Labels.push(_this.GetDateMonthS(element.Date));
                    TSaleDataSet[0].data.push(Math.round(Data.TotalSuccessfulTransactionInvoiceAmount));
                    TSaleColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                    TSaleCustomersDataSet[0].data.push(Data.TotalSuccessfulTransactionCustomer);
                    TSaleCustomersColor[0].backgroundColor.push("rgba(153, 0, 153, 0.7)");
                    TTypeDataSetCardItem.data.push(Math.round(Data.TotalCardTransactionInvoiceAmount));
                    TTypeDataSetCardItemColor.backgroundColor.push("#61c03c");
                    TTypeDataSetCashItem.data.push(Math.round(Data.TotalCashTransactionInvoiceAmount));
                    TTypeDataSetCashItemColor.backgroundColor.push("#ff9900");
                    TTypeCustomersDataSetCardItem.data.push(Data.TotalCardTransactionCustomer);
                    TTypeCustomersDataSetCardItemColor.backgroundColor.push("#990099");
                    TTypeCustomersDataSetCashItem.data.push(Data.TotalCashTransactionCustomer);
                    TTypeCustomersDataSetCashItemColor.backgroundColor.push("#00aff0");
                    TStatusDataSetSuccessItem.data.push(Data.TotalSuccessfulTransaction);
                    TStatusDataSetSuccessItemColor.backgroundColor.push("#61c03c");
                    TStatusDataSetErrorItem.data.push(Data.TotalFailedTransaction);
                    TStatusDataSetErrorItemError.backgroundColor.push("#dc3912");
                });
                TStatusDataSetColor.push(TStatusDataSetSuccessItemColor);
                TStatusDataSetColor.push(TStatusDataSetErrorItemError);
                TStatusDataSet.push(TStatusDataSetSuccessItem);
                TStatusDataSet.push(TStatusDataSetErrorItem);
                TTypeDataSetColor.push(TTypeDataSetCardItemColor);
                TTypeDataSetColor.push(TTypeDataSetCashItemColor);
                TTypeDataSet.push(TTypeDataSetCardItem);
                TTypeDataSet.push(TTypeDataSetCashItem);
                TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCardItemColor);
                TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCashItemColor);
                TTypeCustomersDataSet.push(TTypeCustomersDataSetCardItem);
                TTypeCustomersDataSet.push(TTypeCustomersDataSetCashItem);
                _this._OSalesHistory.TransactionStatusDataSet = TStatusDataSet;
                _this._OSalesHistory.TransactionStatusDataSetColors = TStatusDataSetColor;
                _this._OSalesHistory.TransactionTypeDataSet = TTypeDataSet;
                _this._OSalesHistory.TransactionTypeDataSetColors = TTypeDataSetColor;
                _this._OSalesHistory.TransactionTypeCustomersDataSet = TTypeCustomersDataSet;
                _this._OSalesHistory.TransactionTypeCustomersDataSetColors = TTypeCustomersDataSetColor;
                _this._OSalesHistory.SaleDataSet = TSaleDataSet;
                _this._OSalesHistory.SaleColors = TSaleColor;
                _this._OSalesHistory.SaleCustomersDataSet = TSaleCustomersDataSet;
                _this._OSalesHistory.SaleCustomersColors = TSaleCustomersColor;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetCardTypeSalesSummary = function (UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        this._CardTypeSalesSummary.CardTypeLabels = [];
        this._CardTypeSalesSummary.CardTypeData = [];
        var Data = {
            Task: "getcardtypesalessummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._CardTypeSalesSummary = _Response.Result;
                _this._CardTypeSalesSummary.CardTypeLabels = [];
                _this._CardTypeSalesSummary.CardTypeData = [];
                if (_this._CardTypeSalesSummary != undefined &&
                    _this._CardTypeSalesSummary.CardTypes != undefined) {
                    _this._CardTypeSalesSummary.CardTypes.forEach(function (element) {
                        _this._CardTypeSalesSummary.CardTypeLabels.push(element.CardTypeName);
                        _this._CardTypeSalesSummary.CardTypeData.push(element.TotalTransaction);
                        element.FirstTransactionDate = _this.GetDateTimeS(element.FirstTransactionDate);
                        element.LastTransactionDateD = _this.GetTimeDifferenceS(element.LastTransactionDate, moment());
                        element.LastTransactionDate = _this.GetDateTimeS(element.LastTransactionDate);
                    });
                }
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetAccountOverview = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getaccountoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._AccountOverview = _Response.Result;
                if (_this._AccountOverview.SettlementCredit != undefined &&
                    _this._AccountOverview.SettlementDebit != undefined) {
                    _this._AccountOverview.SettlementPending =
                        _this._AccountOverview.SettlementCredit -
                            _this._AccountOverview.SettlementDebit;
                }
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetAccountOverviewNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getaccountoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._AccountOverviewNext = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRewardOverview = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RewardOverview = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRewardOverviewNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RewardOverviewNext = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRewardTypeOverview = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardtypeoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RewardTypeOverview = _Response.Result;
                if (_this._RewardTypeOverview.Transactions &&
                    _this._RewardTypeOverview.Transactions > 0) {
                    _this._RewardTypeOverview.CardRewardTransactionsPerc =
                        (_this._RewardTypeOverview.CashRewardTransactions * 100) /
                            _this._RewardTypeOverview.Transactions;
                    _this._RewardTypeOverview.CashRewardTransactionsPerc =
                        (_this._RewardTypeOverview.CardRewardTransactions * 100) /
                            _this._RewardTypeOverview.Transactions;
                }
                _this._RewardTypeOverview.RewardTypeLabels = [];
                _this._RewardTypeOverview.RewardTypeData = [];
                _this._RewardTypeOverview.RewardTypeLabels.push("Cash");
                _this._RewardTypeOverview.RewardTypeLabels.push("Card");
                _this._RewardTypeOverview.RewardTypeData.push(_this._RewardTypeOverview.CashRewardPurchaseAmount);
                _this._RewardTypeOverview.RewardTypeData.push(_this._RewardTypeOverview.CardRewardPurchaseAmount);
                _this._RewardTypeOverview.AppUsersCardTypeLabel = [];
                _this._RewardTypeOverview.AppUsersCardTypeDataUsers = [];
                _this._RewardTypeOverview.AppUsersCardTypeDataTransactions = [];
                _this._RewardTypeOverview.AppUsersCardTypeDataPurchase = [];
                _this._RewardTypeOverview.AppUsersCardType.forEach(function (element) {
                    if (_this._RewardTypeOverview.Transactions &&
                        _this._RewardTypeOverview.Transactions > 0) {
                        element.TransactionPerc =
                            (element.Purchase * 100) /
                                _this._RewardTypeOverview.Transactions;
                    }
                    _this._RewardTypeOverview.AppUsersCardTypeLabel.push(element.Name);
                    _this._RewardTypeOverview.AppUsersCardTypeDataUsers.push(element.Users);
                    _this._RewardTypeOverview.AppUsersCardTypeDataTransactions.push(element.Transactions);
                    _this._RewardTypeOverview.AppUsersCardTypeDataPurchase.push(Math.round(element.Purchase));
                    element.Purchase = Math.round(element.Purchase);
                });
                // this._RewardTypeOverview.BankCompareLabel = [];
                // this._RewardTypeOverview.BankCompareDataUsers = [];
                // this._RewardTypeOverview.BankCompareDataTransactions = [];
                // this._RewardTypeOverview.BankCompareDataPurchase = [];
                // this._RewardTypeOverview.AppUsersBank.forEach(element => {
                //   this._RewardTypeOverview.BankCompareLabel.push(element.Name);
                //   this._RewardTypeOverview.AppUsersCardTypeDataPurchase.push(
                //     Math.round(element.Purchase)
                //   );
                //   element.Purchase = Math.round(element.Purchase);
                // });
                _this._RewardTypeOverview.AppUsersBankLabel = [];
                _this._RewardTypeOverview.AppUsersBankDataUsers = [];
                _this._RewardTypeOverview.AppUsersBankDataTransactions = [];
                _this._RewardTypeOverview.AppUsersBankDataPurchase = [];
                if (_this._RewardTypeOverview.AppUsersBank != undefined) {
                    _this._RewardTypeOverview.AppUsersBank.forEach(function (element) {
                        element.Purchase = Math.round(element.Purchase);
                        if (element.Users > 0) {
                            _this._RewardTypeOverview.AppUsersBankLabel.push(element.Name);
                            _this._RewardTypeOverview.AppUsersBankDataUsers.push(element.Users);
                            _this._RewardTypeOverview.AppUsersBankDataTransactions.push(element.Transactions);
                            _this._RewardTypeOverview.AppUsersBankDataPurchase.push(Math.round(element.Purchase));
                        }
                    });
                }
                _this._RewardTypeOverview.AppUsersBankLabel = [];
                _this._RewardTypeOverview.AppUsersBankDataUsers = [];
                _this._RewardTypeOverview.AppUsersBankDataTransactions = [];
                _this._RewardTypeOverview.AppUsersBankDataPurchase = [];
                if (_this._RewardTypeOverview.AppUsersBank != undefined) {
                    _this._RewardTypeOverview.AppUsersBank.forEach(function (element) {
                        element.Purchase = Math.round(element.Purchase);
                        if (element.Users > 0) {
                            _this._RewardTypeOverview.AppUsersBankLabel.push(element.Name);
                            _this._RewardTypeOverview.AppUsersBankDataUsers.push(element.Users);
                            _this._RewardTypeOverview.AppUsersBankDataTransactions.push(element.Transactions);
                            _this._RewardTypeOverview.AppUsersBankDataPurchase.push(Math.round(element.Purchase));
                        }
                    });
                }
                _this._RewardTypeOverview.AppUsersBankLabel = [];
                _this._RewardTypeOverview.AppUsersBankDataUsers = [];
                _this._RewardTypeOverview.AppUsersBankDataTransactions = [];
                _this._RewardTypeOverview.AppUsersBankDataPurchase = [];
                if (_this._RewardTypeOverview.AppUsersBank != undefined) {
                    _this._RewardTypeOverview.AppUsersBank.forEach(function (element) {
                        element.Purchase = Math.round(element.Purchase);
                        if (element.Users > 0) {
                            _this._RewardTypeOverview.AppUsersBankLabel.push(element.Name);
                            _this._RewardTypeOverview.AppUsersBankDataUsers.push(element.Users);
                            _this._RewardTypeOverview.AppUsersBankDataTransactions.push(element.Transactions);
                            _this._RewardTypeOverview.AppUsersBankDataPurchase.push(Math.round(element.Purchase));
                        }
                    });
                }
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRewardTypeOverviewNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardtypeoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RewardTypeOverviewNext = _Response.Result;
                _this._RewardTypeOverviewNext.RewardTypeLabels = [];
                _this._RewardTypeOverviewNext.RewardTypeData = [];
                _this._RewardTypeOverviewNext.RewardTypeLabels.push("Cash");
                _this._RewardTypeOverviewNext.RewardTypeLabels.push("Card");
                _this._RewardTypeOverviewNext.RewardTypeData.push(_this._RewardTypeOverviewNext.CashRewardTransactions);
                _this._RewardTypeOverviewNext.RewardTypeData.push(_this._RewardTypeOverviewNext.CardRewardTransactions);
                _this._RewardTypeOverviewNext.AppUsersCardTypeLabel = [];
                _this._RewardTypeOverviewNext.AppUsersCardTypeDataUsers = [];
                _this._RewardTypeOverviewNext.AppUsersCardTypeDataTransactions = [];
                _this._RewardTypeOverviewNext.AppUsersCardTypeDataPurchase = [];
                _this._RewardTypeOverviewNext.AppUsersCardType.forEach(function (element) {
                    _this._RewardTypeOverviewNext.AppUsersCardTypeLabel.push(element.Name);
                    _this._RewardTypeOverviewNext.AppUsersCardTypeDataUsers.push(element.Users);
                    _this._RewardTypeOverviewNext.AppUsersCardTypeDataTransactions.push(element.Transactions);
                    _this._RewardTypeOverviewNext.AppUsersCardTypeDataPurchase.push(Math.round(element.Purchase));
                    element.Purchase = Math.round(element.Purchase);
                });
                _this._RewardTypeOverviewNext.AppUsersBankLabel = [];
                _this._RewardTypeOverviewNext.AppUsersBankDataUsers = [];
                _this._RewardTypeOverviewNext.AppUsersBankDataTransactions = [];
                _this._RewardTypeOverviewNext.AppUsersBankDataPurchase = [];
                _this._RewardTypeOverviewNext.AppUsersBank.forEach(function (element) {
                    element.Purchase = Math.round(element.Purchase);
                    if (element.Users > 0) {
                        _this._RewardTypeOverviewNext.AppUsersBankLabel.push(element.Name);
                        _this._RewardTypeOverviewNext.AppUsersBankDataUsers.push(element.Users);
                        _this._RewardTypeOverviewNext.AppUsersBankDataTransactions.push(element.Transactions);
                        _this._RewardTypeOverviewNext.AppUsersBankDataPurchase.push(Math.round(element.Purchase));
                    }
                });
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRedeemOverview = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getredeemoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RedeemOverview = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetRedeemOverviewNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getredeemoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._RedeemOverviewNext = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetAppUsersOverview = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getappusersoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            ParentKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._AppUsersOverview = _Response.Result;
                _this._AppUsersOverview.AppUsersLabelsByGender = [];
                _this._AppUsersOverview.AppUsersDataByGender = [];
                _this._AppUsersOverview.AppUsersLabelsByAgeGroup = [];
                _this._AppUsersOverview.AppUsersVisitByAgeGroup = [];
                _this._AppUsersOverview.AppUsersCountByAgeGroup = [];
                _this._AppUsersOverview.AppUsersPurchaseByAgeGroup = [];
                _this._AppUsersOverview.AppUsersLabelsByGender.push("Male");
                _this._AppUsersOverview.AppUsersLabelsByGender.push("Female");
                _this._AppUsersOverview.AppUsersDataByGender.push(_this._AppUsersOverview.AppUsersMale);
                _this._AppUsersOverview.AppUsersDataByGender.push(_this._AppUsersOverview.AppUsersFemale);
                _this._AppUsersOverview.AppUsersByAgeGroup.forEach(function (element) {
                    if (element.Name != "Unknown") {
                        _this._AppUsersOverview.AppUsersLabelsByAgeGroup.push(element.Name);
                        _this._AppUsersOverview.AppUsersVisitByAgeGroup.push(element.Visits);
                        _this._AppUsersOverview.AppUsersCountByAgeGroup.push(element.Users);
                        _this._AppUsersOverview.AppUsersPurchaseByAgeGroup.push(Math.round(element.Purchase));
                    }
                });
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetAppUsersOverviewNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getappusersoverview",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            ParentKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._AppUsersOverviewNext = _Response.Result;
                _this._AppUsersOverviewNext.AppUsersLabelsByGender = [];
                _this._AppUsersOverviewNext.AppUsersDataByGender = [];
                _this._AppUsersOverviewNext.AppUsersLabelsByAgeGroup = [];
                _this._AppUsersOverviewNext.AppUsersVisitByAgeGroup = [];
                _this._AppUsersOverviewNext.AppUsersCountByAgeGroup = [];
                _this._AppUsersOverviewNext.AppUsersPurchaseByAgeGroup = [];
                _this._AppUsersOverviewNext.AppUsersLabelsByGender.push("Male");
                _this._AppUsersOverviewNext.AppUsersLabelsByGender.push("Female");
                _this._AppUsersOverviewNext.AppUsersDataByGender.push(_this._AppUsersOverviewNext.AppUsersMale);
                _this._AppUsersOverviewNext.AppUsersDataByGender.push(_this._AppUsersOverviewNext.AppUsersFemale);
                _this._AppUsersOverviewNext.AppUsersByAgeGroup.forEach(function (element) {
                    if (element.Name != "Unknown") {
                        _this._AppUsersOverviewNext.AppUsersLabelsByAgeGroup.push(element.Name);
                        _this._AppUsersOverviewNext.AppUsersVisitByAgeGroup.push(element.Visits);
                        _this._AppUsersOverviewNext.AppUsersCountByAgeGroup.push(element.Users);
                        _this._AppUsersOverviewNext.AppUsersPurchaseByAgeGroup.push(Math.round(element.Purchase));
                    }
                });
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetVisitHistory = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getvisithistory",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                var ChartResponse = _Response.Result;
                _this._UserOverviewPlot.HourlyVisitLabel = [];
                _this._UserOverviewPlot.HourlyVisitData = [];
                ChartResponse.DateRange.forEach(function (RangeItem) {
                    _this._UserOverviewPlot.HourlyVisitLabel.push(_this.GetTimeS(RangeItem.StartTime));
                });
                var TPlotDataSet = [];
                if (ChartResponse.DateRange.length > 0) {
                    var RangeItem = ChartResponse.DateRange[0].Data;
                    RangeItem.forEach(function (element) {
                        var DataSetItem = {
                            label: element.Name,
                            data: [],
                        };
                        TPlotDataSet.push(DataSetItem);
                    });
                    ChartResponse.DateRange.forEach(function (RangeItem) {
                        var Data = RangeItem.Data;
                        Data.forEach(function (element) {
                            TPlotDataSet[0].data.push(parseInt(element.Value));
                            var DataItem = TPlotDataSet.find(function (x) { return x.label == element.Name; });
                            if (DataItem != undefined) {
                                DataItem.data.push(parseInt(element.Value));
                            }
                        });
                    });
                }
                _this._UserOverviewPlot.HourlyVisitData = TPlotDataSet;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    HelperService.prototype.GetVisitHistoryNext = function (UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getvisithistory",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                var ChartResponse = _Response.Result;
                _this._UserOverviewPlot.HourlyVisitNextLabel = [];
                _this._UserOverviewPlot.HourlyVisitNextData = [];
                ChartResponse.DateRange.forEach(function (RangeItem) {
                    _this._UserOverviewPlot.HourlyVisitNextLabel.push(_this.GetTimeS(RangeItem.StartTime));
                });
                var TPlotDataSet = [];
                if (ChartResponse.DateRange.length > 0) {
                    var RangeItem = ChartResponse.DateRange[0].Data;
                    RangeItem.forEach(function (element) {
                        var DataSetItem = {
                            label: element.Name,
                            data: [],
                        };
                        TPlotDataSet.push(DataSetItem);
                    });
                    ChartResponse.DateRange.forEach(function (RangeItem) {
                        var Data = RangeItem.Data;
                        Data.forEach(function (element) {
                            TPlotDataSet[0].data.push(parseInt(element.Value));
                            var DataItem = TPlotDataSet.find(function (x) { return x.label == element.Name; });
                            if (DataItem != undefined) {
                                DataItem.data.push(parseInt(element.Value));
                            }
                        });
                    });
                }
                _this._UserOverviewPlot.HourlyVisitNextData = TPlotDataSet;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    // Sub AccountOverview Region Start
    HelperService.prototype.GetSubAccountOverviewLite = function (TodayStartTime, TodayEndTime) {
        var _this = this;
        this.IsFormProcessing = true;
        var Data = {
            Task: "getaccountoverview",
            StartTime: TodayStartTime,
            EndTime: TodayEndTime,
            AccountKey: this.UserAccount.AccountKey,
            AccountId: this.UserAccount.AccountId,
            SubAccountId: this.AppConfig.ActiveReferenceId,
            SubAccountKey: this.AppConfig.ActiveReferenceKey,
        };
        var _OResponse;
        _OResponse = this.PostData(this.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(function (_Response) {
            _this.IsFormProcessing = false;
            if (_Response.Status == _this.StatusSuccess) {
                _this._SubAccountOverview = _Response.Result;
            }
            else {
                _this.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this.IsFormProcessing = false;
            _this.HandleException(_Error);
        });
    };
    // Sub AccountOverview Region End
    HelperService.prototype.SetNetwork = function () {
        var DeviceSerialNumber = null;
        var _Storage_UserDevice = this.GetStorage(this.AppConfig.Storage.Device);
        if (_Storage_UserOReqH == null) {
            var DeviceInformation = {
                SerialNumber: this.newGuid(),
            };
            this.SaveStorage(this.AppConfig.Storage.Device, DeviceInformation);
        }
        else {
            DeviceSerialNumber = _Storage_UserDevice.SerialNumber;
        }
        var _Storage_UserOReqH = this.GetStorage(this.AppConfig.Storage.OReqH);
        if (_Storage_UserOReqH == null) {
            var ehcak = btoa(this.AppConfig.AppKey);
            var ehcavk = btoa(this.AppConfig.AppVersionKey);
            var ehctk = btoa("na");
            var ehcudv = btoa(DeviceSerialNumber);
            var ehcudlt = btoa("0");
            var ehcudln = btoa("0");
            var ekuak = "";
            var ehcupk = btoa("0");
            var ehcudk = btoa("0");
            var OReqH = {
                "Content-Type": "application/json; charset=utf-8",
                hcak: ehcak,
                hcavk: ehcavk,
                hctk: ehctk,
                hcudv: ehcudv,
                hcudlt: ehcudlt,
                hcudln: ehcudln,
                hcuak: ekuak,
                hcupk: ehcupk,
                hcudk: ehcudk,
                hcuata: "",
                hcuatp: "",
            };
            this.AppConfig.ClientHeader = OReqH;
            this.SaveStorage(this.AppConfig.Storage.OReqH, OReqH);
        }
    };
    HelperService.prototype.PostData = function (PostLocation, Data) {
        this.AppConfig.ClientHeader.hcuata = "";
        this.AppConfig.ClientHeader.hcuatp = "";
        var NetworkLocation = this.AppConfig.HostConnect + PostLocation + Data.Task;
        this.AppConfig.ClientHeader.hctk = btoa(Data.Task);
        if (Data.AuthPin != undefined &&
            Data.AuthPin != null &&
            Data.AuthPin != "") {
            this.AppConfig.ClientHeader.hcuata = btoa(Data.AuthPin);
        }
        if (Data.AuthPassword != undefined &&
            Data.AuthPassword != null &&
            Data.AuthPassword != "") {
            this.AppConfig.ClientHeader.hcuatp = btoa(Data.AuthPassword);
        }
        var _Headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](this.AppConfig.ClientHeader);
        var ORequest = {
            zx: btoa(JSON.stringify(Data)),
        };
        var RequestData = JSON.stringify(ORequest);
        return this._Http
            .post(NetworkLocation, RequestData, {
            headers: _Headers,
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["map"])(function (_Response) { return JSON.parse(atob(_Response.zx)); }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["catchError"])(function (error) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_13__["throwError"])(error); }));
    };
    HelperService.prototype._DateRangeDifference = function (DatePrev, DateNex) {
        var _Diff = {};
        var seconds = Math.floor((Date.parse(DateNex).valueOf() - Date.parse(DatePrev).valueOf()) / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        _Diff.days = Math.floor(hours / 24);
        _Diff.hours = hours - _Diff.days * 24;
        _Diff.minutes = minutes - _Diff.days * 24 * 60 - hours * 60;
        _Diff.seconds =
            seconds - _Diff.days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60;
        // console.log(_Diff);
        return _Diff;
    };
    HelperService.prototype.GoogleAddressArrayToJson = function (address_components) {
        var addressJson = {};
        for (var index = 0; index < address_components.length; index++) {
            var element = address_components[index];
            addressJson[element.types[0]] = element.long_name;
        }
        return addressJson;
    };
    HelperService.prototype.ResetSortedTerminals = function () {
        this.SortedTerminals.Active = [];
        this.SortedTerminals.Idle = [];
        this.SortedTerminals.Dead = [];
        this.SortedTerminals.UnUsed = [];
    };
    HelperService.prototype.SortTerminals = function (Data) {
        this.ResetSortedTerminals();
        this.SortedTerminals.All = Data;
        var SevenDayBefore = moment().startOf("day").subtract(7, "d");
        var ThirtyDayBefore = moment().startOf("day").subtract(30, "d");
        for (var index = 0; index < Data.length; index++) {
            var TClone = Object.assign({}, Data[index]);
            var TCloneDate = moment(TClone.CreateDate);
            if (TCloneDate.isBefore(ThirtyDayBefore)) {
                this.SortedTerminals.Dead.push(TClone);
            }
            else if (TCloneDate.isBefore(SevenDayBefore)) {
                this.SortedTerminals.Idle.push(TClone);
            }
            else if (TCloneDate.isAfter(SevenDayBefore)) {
                this.SortedTerminals.Active.push(TClone);
            }
            else {
                this.SortedTerminals.UnUsed.push(TClone);
            }
        }
    };
    HelperService.prototype.toogleFormProcessingFlag = function (value) {
        this.IsFormProcessing = value;
        this.isprocessingtoogle.emit(0);
    };
    //#endregion
    HelperService.prototype.CalculateLastSevenDay = function (momentdate) {
        var DateArray = [momentdate.startOf("day")];
        var DateArrayFormat = [
            this.GetDateTimeSChart(moment().startOf("day")),
        ];
        for (var index = 1; index < 7; index++) {
            var element = DateArray[index - 1].subtract(1, "d");
            DateArray.push(element);
            DateArrayFormat.push(this.GetDateTimeSChart(element._d));
        }
        return DateArrayFormat.reverse();
    };
    //#region calculate intermediate dates
    HelperService.prototype.CalculateIntermediateDate = function (momentDateStart, momentDateEnd) {
        var intermediatecount = 0;
        var currDate = momentDateStart.startOf("day");
        var lastDate = momentDateEnd.startOf("day");
        //#region push start date
        var DateArray = [moment(momentDateStart).startOf("day")];
        var DateArrayFormat = [
            this.GetDateTimeSChart(moment(momentDateStart).endOf("day")),
        ];
        //#endregion
        //#region push internal dates
        while (currDate.add(1, "days").diff(lastDate) < 0) {
            intermediatecount += 1;
            DateArray.push(currDate.clone().toDate());
            DateArrayFormat.push(this.GetDateTimeSChart(currDate.clone().toDate()));
        }
        //#endregion
        //#region push final dates
        if (intermediatecount != 0) {
            DateArray.push(lastDate.clone().toDate());
            DateArrayFormat.push(this.GetDateTimeSChart(lastDate.clone().toDate()));
        }
        else {
            //if start date and end date are different
            if (momentDateStart.startOf("day").isSame(momentDateEnd.startOf("day"))) {
                DateArray.push(lastDate.clone().toDate());
                DateArrayFormat.push(this.GetDateTimeSChart(lastDate.clone().toDate()));
            }
        }
        //#endregion
        return DateArrayFormat;
    };
    HelperService.prototype.CloneJson = function (Json) {
        try {
            return JSON.parse(JSON.stringify(Json));
        }
        catch (error) {
            return null;
        }
    };
    HelperService.prototype.deepCopy = function (sourceObject, destinationObject) {
        for (var key in sourceObject) {
            if (typeof sourceObject[key] != "object") {
                destinationObject[key] = sourceObject[key];
            }
            else {
                destinationObject[key] = {};
                this.deepCopy(sourceObject[key], destinationObject[key]);
            }
        }
    };
    HelperService.prototype.DataReloadEligibility = function (Type) {
        return (Type == this.AppConfig.ListToggleOption.Search
            || Type == this.AppConfig.ListToggleOption.Page
            || Type == this.AppConfig.ListToggleOption.Refresh);
    };
    HelperService.prototype.AreTwoFilterSnapEqual = function (FilterSnapClone, ElementClone) {
        try {
            console.log('=>');
            return JSON.stringify(lodash__WEBPACK_IMPORTED_MODULE_19__["omit"](FilterSnapClone, lodash__WEBPACK_IMPORTED_MODULE_19__["functions"](FilterSnapClone))) ==
                JSON.stringify(lodash__WEBPACK_IMPORTED_MODULE_19__["omit"](ElementClone, lodash__WEBPACK_IMPORTED_MODULE_19__["functions"](ElementClone)));
        }
        catch (error) {
            return false;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"])("window:resize", ["$event"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], HelperService.prototype, "ContainerHeight", void 0);
    HelperService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            ng2_file_input__WEBPACK_IMPORTED_MODULE_10__["Ng2FileInputService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HelperService);
    return HelperService;
}());



/***/ }),

/***/ "./src/app/service/object.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/object.service.ts ***!
  \*******************************************/
/*! exports provided: OResponse, OListResponse, OStorageContent, OSelect, OSelectField, OAccessUser, OAccessUserAccount, OAccessUserAccountOwner, OAccessUserCountry, OAccessUserAccountRole, OAccessUserAccountRolePermission, OAccessUserLocation, OAccessUserDevice, OList, OListSort, OListField, OCoreHelper, OCoreCommon, OCoreParameter, OUserDetails, OOverview, OUserCounts, OCardTypeSalesSummary, OSalesSummary, ORewardsSummary, OSalesOverview, OAccountOverview, LastTransactionDetails, ORewardOverview, ORewardTypeOverview, ORedeemOverview, OAppUsersOverview, OInvoiceDetails, OInvoiceItemDetails, OTerminalStatusCount */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OResponse", function() { return OResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OListResponse", function() { return OListResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OStorageContent", function() { return OStorageContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSelect", function() { return OSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSelectField", function() { return OSelectField; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUser", function() { return OAccessUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccount", function() { return OAccessUserAccount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountOwner", function() { return OAccessUserAccountOwner; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserCountry", function() { return OAccessUserCountry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountRole", function() { return OAccessUserAccountRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountRolePermission", function() { return OAccessUserAccountRolePermission; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserLocation", function() { return OAccessUserLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccessUserDevice", function() { return OAccessUserDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OList", function() { return OList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OListSort", function() { return OListSort; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OListField", function() { return OListField; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OCoreHelper", function() { return OCoreHelper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OCoreCommon", function() { return OCoreCommon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OCoreParameter", function() { return OCoreParameter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OUserDetails", function() { return OUserDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OOverview", function() { return OOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OUserCounts", function() { return OUserCounts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OCardTypeSalesSummary", function() { return OCardTypeSalesSummary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSalesSummary", function() { return OSalesSummary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORewardsSummary", function() { return ORewardsSummary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSalesOverview", function() { return OSalesOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAccountOverview", function() { return OAccountOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LastTransactionDetails", function() { return LastTransactionDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORewardOverview", function() { return ORewardOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORewardTypeOverview", function() { return ORewardTypeOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORedeemOverview", function() { return ORedeemOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OAppUsersOverview", function() { return OAppUsersOverview; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OInvoiceDetails", function() { return OInvoiceDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OInvoiceItemDetails", function() { return OInvoiceItemDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OTerminalStatusCount", function() { return OTerminalStatusCount; });
var OResponse = /** @class */ (function () {
    function OResponse() {
    }
    return OResponse;
}());

var OListResponse = /** @class */ (function () {
    function OListResponse() {
        this.Offset = 0;
        this.Limit = 0;
        this.TotalRecords = 0;
    }
    return OListResponse;
}());

var OStorageContent = /** @class */ (function () {
    function OStorageContent() {
        this.Width = 0;
        this.Height = 0;
    }
    return OStorageContent;
}());

var OSelect = /** @class */ (function () {
    function OSelect() {
    }
    return OSelect;
}());

var OSelectField = /** @class */ (function () {
    function OSelectField() {
    }
    return OSelectField;
}());

var OAccessUser = /** @class */ (function () {
    function OAccessUser() {
        this.AddressLatitude = 0;
        this.AddressLongitude = 0;
        this.EmailVerificationStatus = 0;
        this.ContactNumberVerificationStatus = 0;
    }
    return OAccessUser;
}());

var OAccessUserAccount = /** @class */ (function () {
    function OAccessUserAccount() {
        this.AccountId = 0;
        this.IsAccessPinSet = 0;
    }
    return OAccessUserAccount;
}());

var OAccessUserAccountOwner = /** @class */ (function () {
    function OAccessUserAccountOwner() {
        this.AccountTypeCode = 0;
        this.AccountId = 0;
    }
    return OAccessUserAccountOwner;
}());

var OAccessUserCountry = /** @class */ (function () {
    function OAccessUserCountry() {
    }
    return OAccessUserCountry;
}());

var OAccessUserAccountRole = /** @class */ (function () {
    function OAccessUserAccountRole() {
    }
    return OAccessUserAccountRole;
}());

var OAccessUserAccountRolePermission = /** @class */ (function () {
    function OAccessUserAccountRolePermission() {
        this.IsDefault = 0;
        this.IsAccessPinRequired = 0;
        this.IsPasswordRequired = 0;
    }
    return OAccessUserAccountRolePermission;
}());

var OAccessUserLocation = /** @class */ (function () {
    function OAccessUserLocation() {
    }
    return OAccessUserLocation;
}());

var OAccessUserDevice = /** @class */ (function () {
    function OAccessUserDevice() {
    }
    return OAccessUserDevice;
}());

var OList = /** @class */ (function () {
    function OList() {
        this.ListType = 0;
        this.Title = "List";
        this.ReferenceId = null;
        this.SubReferenceId = null;
        this.SubReferenceKey = null;
        this.ReferenceKey = null;
        this.Type = null;
        this.RefreshCount = true;
        this.TitleResourceId = null;
        this.RefreshData = false;
        this.IsDownload = false;
        this.ToggleFilter = false;
        this.ActivePage = 1;
        this.PageRecordLimit = 10;
        this.TotalRecords = 0;
        this.ShowingStart = 0;
        this.ShowingEnd = 0;
        this.StartTime = null;
        this.EndTime = null;
        this.StartDate = null;
        this.EndDate = null;
        this.StatusType = "default";
        this.Status = 0;
    }
    return OList;
}());

var OListSort = /** @class */ (function () {
    function OListSort() {
        this.SortDefaultOrder = "desc";
        this.SortOrder = "desc";
    }
    return OListSort;
}());

var OListField = /** @class */ (function () {
    function OListField() {
        this.DefaultValue = "--";
        this.DataType = "text";
        this.Class = "";
        this.ResourceId = "";
        this.Sort = true;
        this.Show = true;
        this.Search = true;
        this.NavigateLink = "";
        this.NavigateField = "";
        this.IsDateSearchField = false;
        this.TextCenter = "";
    }
    return OListField;
}());

var OCoreHelper = /** @class */ (function () {
    function OCoreHelper() {
        this.Sequence = 0;
        this.StatusId = 0;
    }
    return OCoreHelper;
}());

var OCoreCommon = /** @class */ (function () {
    function OCoreCommon() {
        this.Sequence = 0;
        this.Count = 0;
        this.SubItemsCount = 0;
        this.StatusId = 0;
    }
    return OCoreCommon;
}());

var OCoreParameter = /** @class */ (function () {
    function OCoreParameter() {
        this.Sequence = 0;
        this.Count = 0;
        this.StatusId = 0;
    }
    return OCoreParameter;
}());

var OUserDetails = /** @class */ (function () {
    function OUserDetails() {
        this.SubAccounts = 0;
        this.ReferenceId = 0;
        this.Latitude = 0;
        this.Longitude = 0;
        this.EmailVerificationStatus = 0;
        this.NumberVerificationStatus = 0;
        this.StatusId = 0;
    }
    return OUserDetails;
}());

var OOverview = /** @class */ (function () {
    function OOverview() {
        this.Merchants = 0;
        this.Stores = 0;
        this.Acquirers = 0;
        this.Terminals = 0;
        this.ActiveTerminals = 0;
        this.DeadTerminals = 0;
        this.IdleTerminals = 0;
        this.Ptsp = 0;
        this.Pssp = 0;
        this.Cashiers = 0;
        this.RewardCards = 0;
        this.RewardCardsUsed = 0;
        this.ThankUCashPlus = 0;
        this.ThankUCashPlusForMerchant = 0;
        this.ThankUCashPlusBalanceValidity = 0;
        this.ThankUCashPlusMinRedeemAmount = 0;
        this.ThankUCashPlusMinTransferAmount = 0;
        this.RewardPercentage = 0;
        this.CommissionPercentage = 0;
        this.Balance = 0;
        this.ClaimedReward = 0;
        this.ClaimedRewardTransations = 0;
        this.Charge = 0;
        this.CashRewardAmount = 0;
        this.CashRewardPurchaseAmount = 0;
        this.CashRewardTransactions = 0;
        this.CardRewardAmount = 0;
        this.CardRewardPurchaseAmount = 0;
        this.CardRewardPurchaseAmountOther = 0;
        this.CardRewardTransactions = 0;
        this.CardRewardTransactionsOther = 0;
        this.OtherRewardAmount = 0;
        this.OtherRewardPurchaseAmount = 0;
        this.OtherRewardTransactions = 0;
        this.RewardTransactions = 0;
        this.RewardAmount = 0;
        this.RewardUserAmount = 0;
        this.RewardChargeAmount = 0;
        this.RewardPurchaseAmount = 0;
        this.RedeemTransactions = 0;
        this.RedeemAmount = 0;
        this.RedeemPurchaseAmount = 0;
        this.Credit = 0;
        this.Debit = 0;
        this.Transactions = 0;
        this.TransactionsPercentage = 0;
        this.NewTransactions = 0;
        this.NewTransactionsPercentage = 0;
        this.RepeatingTransactions = 0;
        this.RepeatingTransactionsPercentage = 0;
        this.ReferralTransactions = 0;
        this.ReferralTransactionsPercentage = 0;
        this.PurchaseAmount = 0;
        this.PurchaseAmountPercentage = 0;
        this.NewPurchaseAmount = 0;
        this.NewPurchaseAmountPercentage = 0;
        this.RepeatingPurchaseAmount = 0;
        this.RepeatingPurchaseAmountPercentage = 0;
        this.ReferralPurchaseAmount = 0;
        this.ReferralPurchaseAmountPercentage = 0;
        this.TUCPlusRewardTransactions = 0;
        this.TUCPlusBalance = 0;
        this.TUCPlusReward = 0;
        this.TUCPlusRewardAmount = 0;
        this.TUCPlusUserRewardAmount = 0;
        this.TUCPlusRewardChargeAmount = 0;
        this.TUCPlusRewardPurchaseAmount = 0;
        this.TUCPlusRewardClaimedAmount = 0;
        this.TUCPlusRewardClaimedTransactions = 0;
        this.TUCPlusPurchaseAmount = 0;
        this.Commission = 0;
        this.IssuerCommissionAmount = 0;
        this.CommissionAmount = 0;
        this.SettlementCredit = 0;
        this.SettlementDebit = 0;
        this.SettlementPending = 0;
        this.UserAmount = 0;
        this.ThankUAmount = 0;
        this.AppUsers = 0;
        this.AppUsersPercentage = 0;
        this.OwnAppUsers = 0;
        this.OwnAppUsersPercentage = 0;
        this.UniqueAppUsers = 0;
        this.RepeatingAppUsers = 0;
        this.RepeatingAppUsersPercentage = 0;
        this.ReferralAppUsers = 0;
        this.ReferralAppUsersPercentage = 0;
        this.AppUsersMale = 0;
        this.AppUsersFemale = 0;
        this.AppUsersOther = 0;
        this.TransactionIssuerAmountCredit = 0;
        this.TransactionIssuerAmountDebit = 0;
        this.TransactionIssuerChargeCredit = 0;
        this.TransactionIssuerChargeDebit = 0;
        this.TransactionIssuerTotalCreditAmount = 0;
        this.TransactionIssuerTotalDebitAmount = 0;
    }
    return OOverview;
}());

var OUserCounts = /** @class */ (function () {
    function OUserCounts() {
        this.TotalMerchant = 0;
        this.TotalStore = 0;
        this.TotalPtsp = 0;
        this.TotalPssp = 0;
        this.TotalAcquirer = 0;
        this.TotalTerminal = 0;
        this.TotalCustomer = 0;
        this.TotalCustomerNew = 0;
        this.TotalCashier = 0;
        this.TotalRm = 0;
        this.TotalManager = 0;
        this.TotalCustomerUnique = 0;
        this.TotalCustomerRepeating = 0;
        this.ThankUCashPlus = 0;
        this.RewardPercentage = 0;
        this.ThankUCashPlusBalanceValidity = 0;
        this.ThankUCashPlusMinRedeemAmount = 0;
        this.ThankUCashPlusMinTransferAmount = 0;
    }
    return OUserCounts;
}());

var OCardTypeSalesSummary = /** @class */ (function () {
    function OCardTypeSalesSummary() {
    }
    return OCardTypeSalesSummary;
}());

var OSalesSummary = /** @class */ (function () {
    function OSalesSummary() {
        this.TotalTransaction = 0;
        this.TotalTransactionCustomer = 0;
        this.TotalTransactionInvoiceAmount = 0;
        this.TotalSuccessfulTransaction = 0;
        this.TotalSuccessfulTransactionCustomer = 0;
        this.TotalSuccessfulTransactionInvoiceAmount = 0;
        this.TotalFailedTransaction = 0;
        this.TotalFailedTransactionCustomer = 0;
        this.TotalFailedTransactionInvoiceAmount = 0;
        this.TotalCardTransaction = 0;
        this.TotalCardTransactionCustomer = 0;
        this.TotalCardTransactionInvoiceAmount = 0;
        this.TotalCashTransaction = 0;
        this.TotalCashTransactionCustomer = 0;
        this.TotalCashTransactionInvoiceAmount = 0;
    }
    return OSalesSummary;
}());

var ORewardsSummary = /** @class */ (function () {
    function ORewardsSummary() {
        this.TucBalance = 0;
        this.TucPlusBalance = 0;
        this.TucBalanceCredit = 0;
        this.TucBalanceDebit = 0;
        this.TucBalanceTransaction = 0;
        this.TucPlusBalanceCredit = 0;
        this.TucPlusBalanceDebit = 0;
        this.TucPlusBalanceTransaction = 0;
        this.TotalTransaction = 0;
        this.TotalTransactionCustomer = 0;
        this.TotalTransactionInvoiceAmount = 0;
        this.TotalRewardTransaction = 0;
        this.TotalRewardTransactionCustomer = 0;
        this.TotalRewardTransactionAmount = 0;
        this.TotalRewardTransactionInvoiceAmount = 0;
        this.TotalTucRewardTransaction = 0;
        this.TotalTucRewardTransactionCustomer = 0;
        this.TotalTucRewardTransactionAmount = 0;
        this.TotalTucRewardTransactionInvoiceAmount = 0;
        this.TotalTucPlusRewardTransaction = 0;
        this.TotalTucPlusRewardTransactionCustomer = 0;
        this.TotalTucPlusRewardTransactionAmount = 0;
        this.TotalTucPlusRewardTransactionInvoiceAmount = 0;
        this.TotalTucPlusRewardClaimTransaction = 0;
        this.TotalTucPlusRewardClaimTransactionCustomer = 0;
        this.TotalTucPlusRewardClaimTransactionAmount = 0;
        this.TotalTucPlusRewardClaimTransactionInvoiceAmount = 0;
        this.TotalRedeemTransaction = 0;
        this.TotalRedeemTransactionCustomer = 0;
        this.TotalRedeemTransactionAmount = 0;
        this.TotalRedeemTransactionInvoiceAmount = 0;
    }
    return ORewardsSummary;
}());

var OSalesOverview = /** @class */ (function () {
    function OSalesOverview() {
        this.TotalTransaction = 0;
        this.TotalTransactionCustomer = 0;
        this.TotalTransactionInvoiceAmount = 0;
        this.TotalSuccessfulTransaction = 0;
        this.TotalSuccessfulTransactionCustomer = 0;
        this.TotalSuccessfulTransactionInvoiceAmount = 0;
        this.TotalFailedTransaction = 0;
        this.TotalFailedTransactionCustomer = 0;
        this.TotalFailedTransactionInvoiceAmount = 0;
        this.TotalCardTransaction = 0;
        this.TotalCardTransactionCustomer = 0;
        this.TotalCardTransactionInvoiceAmount = 0;
        this.TotalCashTransaction = 0;
        this.TotalCashTransactionCustomer = 0;
        this.TotalCashTransactionInvoiceAmount = 0;
    }
    return OSalesOverview;
}());

var OAccountOverview = /** @class */ (function () {
    function OAccountOverview() {
        this.UnusedTerminals = 0;
        this.TransactionSuccess = 0;
        this.TransactionFailed = 0;
        this.ReferredAppUsers = 0;
        this.ReferredMerchants = 0;
        this.ReferredReferredStores = 0;
        this.ReferredAppUsersVisit = 0;
        this.ReferredAppUsersPurchase = 0;
        this.ReferredMerchantVisits = 0;
        this.ReferredMerchantSale = 0;
        this.Merchants = 0;
        this.Stores = 0;
        this.Acquirers = 0;
        this.Terminals = 0;
        this.ActiveTerminals = 0;
        this.DeadTerminals = 0;
        this.IdleTerminals = 0;
        this.Ptsp = 0;
        this.Pssp = 0;
        this.Cashiers = 0;
        this.RewardCards = 0;
        this.RewardCardsUsed = 0;
        this.ThankUCashPlus = 0;
        this.ThankUCashPlusForMerchant = 0;
        this.ThankUCashPlusBalanceValidity = 0;
        this.ThankUCashPlusMinRedeemAmount = 0;
        this.ThankUCashPlusMinTransferAmount = 0;
        this.RewardPercentage = 0;
        this.CommissionPercentage = 0;
        this.Balance = 0;
        this.ClaimedReward = 0;
        this.ClaimedRewardTransations = 0;
        this.Charge = 0;
        this.CashRewardAmount = 0;
        this.CashRewardPurchaseAmount = 0;
        this.CashRewardTransactions = 0;
        this.CardRewardAmount = 0;
        this.CardRewardPurchaseAmount = 0;
        this.CardRewardPurchaseAmountOther = 0;
        this.CardRewardTransactions = 0;
        this.CardRewardTransactionsOther = 0;
        this.OtherRewardAmount = 0;
        this.OtherRewardPurchaseAmount = 0;
        this.OtherRewardTransactions = 0;
        this.RewardTransactions = 0;
        this.RewardAmount = 0;
        this.RewardUserAmount = 0;
        this.RewardChargeAmount = 0;
        this.RewardPurchaseAmount = 0;
        this.RedeemTransactions = 0;
        this.RedeemAmount = 0;
        this.RedeemPurchaseAmount = 0;
        this.Credit = 0;
        this.Debit = 0;
        this.Transactions = 0;
        this.TransactionsPercentage = 0;
        this.NewTransactions = 0;
        this.NewTransactionsPercentage = 0;
        this.RepeatingTransactions = 0;
        this.RepeatingTransactionsPercentage = 0;
        this.ReferralTransactions = 0;
        this.ReferralTransactionsPercentage = 0;
        this.PurchaseAmount = 0;
        this.PurchaseAmountPercentage = 0;
        this.NewPurchaseAmount = 0;
        this.NewPurchaseAmountPercentage = 0;
        this.RepeatingPurchaseAmount = 0;
        this.RepeatingPurchaseAmountPercentage = 0;
        this.ReferralPurchaseAmount = 0;
        this.ReferralPurchaseAmountPercentage = 0;
        this.TUCPlusRewardTransactions = 0;
        this.TUCPlusBalance = 0;
        this.TUCPlusReward = 0;
        this.TUCPlusRewardAmount = 0;
        this.TUCPlusUserRewardAmount = 0;
        this.TUCPlusRewardChargeAmount = 0;
        this.TUCPlusRewardPurchaseAmount = 0;
        this.TUCPlusRewardClaimedAmount = 0;
        this.TUCPlusRewardClaimedTransactions = 0;
        this.TUCPlusPurchaseAmount = 0;
        this.Commission = 0;
        this.IssuerCommissionAmount = 0;
        this.CommissionAmount = 0;
        this.SettlementCredit = 0;
        this.SettlementDebit = 0;
        this.SettlementPending = 0;
        this.UserAmount = 0;
        this.ThankUAmount = 0;
        this.AppUsers = 0;
        this.AppUsersPercentage = 0;
        this.OwnAppUsers = 0;
        this.OwnAppUsersPercentage = 0;
        this.UniqueAppUsers = 0;
        this.RepeatingAppUsers = 0;
        this.RepeatingAppUsersPercentage = 0;
        this.ReferralAppUsers = 0;
        this.ReferralAppUsersPercentage = 0;
        this.AppUsersMale = 0;
        this.AppUsersFemale = 0;
        this.AppUsersOther = 0;
        this.TransactionIssuerAmountCredit = 0;
        this.TransactionIssuerAmountDebit = 0;
        this.TransactionIssuerChargeCredit = 0;
        this.TransactionIssuerChargeDebit = 0;
        this.TransactionIssuerTotalCreditAmount = 0;
        this.TransactionIssuerTotalDebitAmount = 0;
    }
    return OAccountOverview;
}());

var LastTransactionDetails = /** @class */ (function () {
    function LastTransactionDetails() {
        this.ReferenceId = 0;
        this.InvoiceAmount = 0;
        this.RewardAmount = 0;
        this.Amount = 0;
    }
    return LastTransactionDetails;
}());

var ORewardOverview = /** @class */ (function () {
    function ORewardOverview() {
        this.RewardAmount = 0;
        this.RewardUserAmount = 0;
        this.RewardChargeAmount = 0;
        this.RewardPurchaseAmount = 0;
        this.RewardTransactions = 0;
        this.TUCPlusRewardAmount = 0;
        this.TUCPlusRewardChargeAmount = 0;
        this.TUCPlusUserRewardAmount = 0;
        this.TUCPlusRewardPurchaseAmount = 0;
        this.TUCPlusRewardTransactions = 0;
        this.TUCPlusRewardClaimedAmount = 0;
        this.TUCPlusRewardClaimedTransactions = 0;
    }
    return ORewardOverview;
}());

var ORewardTypeOverview = /** @class */ (function () {
    function ORewardTypeOverview() {
        this.CashRewardTransactions = 0;
        this.CashRewardAmount = 0;
        this.CashRewardPurchaseAmount = 0;
        this.CardRewardTransactions = 0;
        this.CardRewardAmount = 0;
        this.CardRewardPurchaseAmount = 0;
        this.OtherRewardAmount = 0;
        this.OtherRewardPurchaseAmount = 0;
        this.OtherRewardTransactions = 0;
        this.Transactions = 0;
        this.CardRewardTransactionsPerc = 0;
        this.CashRewardTransactionsPerc = 0;
    }
    return ORewardTypeOverview;
}());

var ORedeemOverview = /** @class */ (function () {
    function ORedeemOverview() {
        this.RedeemAmount = 0;
        this.RedeemPurchaseAmount = 0;
        this.RedeemTransactions = 0;
    }
    return ORedeemOverview;
}());

var OAppUsersOverview = /** @class */ (function () {
    function OAppUsersOverview() {
        this.AppUsers = 0;
        this.AppUsersMale = 0;
        this.AppUsersFemale = 0;
        this.AppUsersOther = 0;
        this.OwnAppUsers = 0;
        this.ReferralAppUsers = 0;
        this.RepeatingAppUsers = 0;
        this.UniqueAppUsers = 0;
    }
    return OAppUsersOverview;
}());

var OInvoiceDetails = /** @class */ (function () {
    function OInvoiceDetails() {
    }
    return OInvoiceDetails;
}());

var OInvoiceItemDetails = /** @class */ (function () {
    function OInvoiceItemDetails() {
    }
    return OInvoiceItemDetails;
}());

var OTerminalStatusCount = /** @class */ (function () {
    function OTerminalStatusCount() {
    }
    return OTerminalStatusCount;
}());



/***/ }),

/***/ "./src/app/service/service.ts":
/*!************************************!*\
  !*** ./src/app/service/service.ts ***!
  \************************************/
/*! exports provided: HttpLoaderFactory, TranslaterModule, OResponse, OListResponse, OStorageContent, OSelect, OSelectField, OAccessUser, OAccessUserAccount, OAccessUserAccountOwner, OAccessUserCountry, OAccessUserAccountRole, OAccessUserAccountRolePermission, OAccessUserLocation, OAccessUserDevice, OList, OListSort, OListField, OCoreHelper, OCoreCommon, OCoreParameter, OUserDetails, OOverview, OUserCounts, OCardTypeSalesSummary, OSalesSummary, ORewardsSummary, OSalesOverview, OAccountOverview, LastTransactionDetails, ORewardOverview, ORewardTypeOverview, ORedeemOverview, OAppUsersOverview, OInvoiceDetails, OInvoiceItemDetails, OTerminalStatusCount, HelperService, DataHelperService, OSearchFilter, FilterHelperService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _translate_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./translate.service */ "./src/app/service/translate.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return _translate_service__WEBPACK_IMPORTED_MODULE_0__["HttpLoaderFactory"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TranslaterModule", function() { return _translate_service__WEBPACK_IMPORTED_MODULE_0__["TranslaterModule"]; });

/* harmony import */ var _object_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./object.service */ "./src/app/service/object.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OResponse", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OResponse"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OListResponse", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OListResponse"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OStorageContent", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OStorageContent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OSelect", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OSelect"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OSelectField", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OSelectField"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUser", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUser"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccount", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserAccount"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountOwner", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserAccountOwner"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserCountry", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserCountry"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountRole", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserAccountRole"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserAccountRolePermission", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserAccountRolePermission"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserLocation", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserLocation"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccessUserDevice", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccessUserDevice"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OList", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OList"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OListSort", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OListSort"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OListField", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OListField"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OCoreHelper", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OCoreHelper"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OCoreCommon", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OCoreCommon"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OCoreParameter", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OCoreParameter"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OUserDetails", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OUserDetails"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OUserCounts", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OUserCounts"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OCardTypeSalesSummary", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OCardTypeSalesSummary"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OSalesSummary", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OSalesSummary"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ORewardsSummary", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["ORewardsSummary"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OSalesOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OSalesOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAccountOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAccountOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LastTransactionDetails", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["LastTransactionDetails"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ORewardOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["ORewardOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ORewardTypeOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["ORewardTypeOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ORedeemOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["ORedeemOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OAppUsersOverview", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OAppUsersOverview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OInvoiceDetails", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OInvoiceDetails"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OInvoiceItemDetails", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OInvoiceItemDetails"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OTerminalStatusCount", function() { return _object_service__WEBPACK_IMPORTED_MODULE_1__["OTerminalStatusCount"]; });

/* harmony import */ var _helper_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./helper.service */ "./src/app/service/helper.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HelperService", function() { return _helper_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"]; });

/* harmony import */ var _datahelper_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./datahelper.service */ "./src/app/service/datahelper.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataHelperService", function() { return _datahelper_service__WEBPACK_IMPORTED_MODULE_3__["DataHelperService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OSearchFilter", function() { return _datahelper_service__WEBPACK_IMPORTED_MODULE_3__["OSearchFilter"]; });

/* harmony import */ var _filterhelper_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filterhelper.service */ "./src/app/service/filterhelper.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FilterHelperService", function() { return _filterhelper_service__WEBPACK_IMPORTED_MODULE_4__["FilterHelperService"]; });








/***/ }),

/***/ "./src/app/service/translate.service.ts":
/*!**********************************************!*\
  !*** ./src/app/service/translate.service.ts ***!
  \**********************************************/
/*! exports provided: HttpLoaderFactory, TranslaterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TranslaterModule", function() { return TranslaterModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");





function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_4__["TranslateHttpLoader"](http, "/assets/i18n/", ".json");
}
var TranslaterModule = /** @class */ (function () {
    function TranslaterModule() {
    }
    TranslaterModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]]
                    }
                })
            ],
            exports: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"]]
        })
    ], TranslaterModule);
    return TranslaterModule;
}());

// "Activity": "Activity",
// "Dashboard": "Dashboard",
// "LoginHistory": "Login History",
// "LastLogin": "Last Login",
// "AccountType": "Account Type",
// "AccountType_ER_Required": "Select account type",
// "AccountOperationType": "Account Operations",
// "AccountOperationType_ER_Required": "Select account operation type",
// "Owner": "Owner",
// "Owner_ER_Required": "Select Owner",
// "ReferralCode": "Referral Code",
// "ReferralCode_ER_Required": "enter referral code",
// "ReferralUrl": "Referral Url",
// "ReferralUrl_ER_Required": "enter referral url",
// "Description": "Description",
// "Description_ER_Required": "enter description",
// "ApplicationStatus": "Application Status",
// "RegistrationSource": "Registration Source",
// "And": "&",
// "Registration": "Registration",
// "Count": "Count",
// "Value": "Value",
// 
//
// 
// 
// 
// "ContactNumber": "Contact Number",
// "Gender": "Gender",
// "DateOfBirth": "DOB",
// "Address": "Address",
// "Latitude": "Latitude",
// "Longitude": "Longitude",
// 
// 
// "SecondaryPassword": "Secondary Password",
// "Role": "Role",
// "AppVersion": "App Version",
// "AppKey": "App",
// "AccessPin": "Access Pin",
// "Country": "Country",
// "Region": "Region",
// "RegionArea": "Region Area",
// "City": "City",
// "CreateDate": "Reg. On",
// 
// "Account": "Account",
// "Details": "Details",
// "AccountCode": "Account Code",
// "UpdateProfile": "Update Profile",
// "EmailVerificationStatus": "Email Verification Status",
// "EmailVerificationDate": "Email Verification Date",
// "NumberVerificationStatus": "Number Verification Status",
// "NumberVerificationStatusDate": "Number Verification Date",
// 
// "Poster": "Poster",
// "Male": "Male",
// "Female": "Female",
// "Profile": "Profile",
// "Credentials": "Credentials",
// 
// "OldPassword": "Old Password",
// "OldPassword_ER_Required": "enter old password",
// "NewPassword": "New password",
// "NewPassword_ER_Required": "enter new password",
// "NewPassword_ER_Length": "Password must be at least 8 characters in length and include at least one uppercase, one lowercase, one number and one special character.",
// "OldAccessPin": "Old Access Pin",
// "NewAccessPin": "New Access Pin",
// 
// "NewAccessPin_ER_Length": "new access pin must be 4 digit number",
// "OldAccessPin_ER_Length": "old access pin must be 4 digit number",
// "OldAccessPin_ER_Required": "enter old access pin",
// "NewAccessPin_ER_Required": "enter new access pin",
// 
// 
// "AccessPin_ER_Required": "enter access pin",
// 
// 
// "Latitude_ER_Required": "enter latitude",
// "Longitude_ER_Required": "enter longitude",
//
//
// 
// 
// 
// "AccessPin_ER_Length": "access pin must be 4 digit number",
// 
// "LocateAddress": "Locate Address",
// "LocationMap": "Location Map",
// "TerminalId": "Terminal Id",
// "Merchant": "Merchant",
// "Provider": "Provider",
// "Acquirer": "Acquirer",
// "Store": "Store"


/***/ }),

/***/ "./src/assets/js/systemhelper.js":
/*!***************************************!*\
  !*** ./src/assets/js/systemhelper.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {


var SystemHelper = (function () {
    "use strict";
    return {
        Notify: (function (Type, Message) {
            Messenger({
                extraClasses: 'messenger-fixed messenger-on-top messenger-on-right',
                theme: 'flat',
            }).post({
                message: Message,
                type: Type,
                showCloseButton: true
            });
            return 'done';
        }),
        TriggerClick: (function (Item) {
            $(Item).click();
            return 'done';
        }),
        GetUserTimeZone: (function () {
            return moment.tz.guess();
        }),
        GetDateTime: (function (Date, TimeZone) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone);
                    return FDate;
                }
                else {
                    return new Date();
                }
            } catch (error) {
                return new Date();
            }

        }),
        GetTimeS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),
        GetDateS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),

        GetDateTimeS: (function (Date, TimeZone, Format) {
            try {
                if (Date != undefined && Date != null) {
                    var TDate = moment.tz(Date, 'GMT');
                    var FDate = moment.tz(TDate, TimeZone).format(Format);
                    return FDate;
                }
                else {
                    return null;
                }
            } catch (error) {
                return null;
            }
        }),
        GetPageName: (function () {
            return location.pathname.replace("/system/", "");
        }),
        CheckDateIsAfter: (function (Date, CompareTo) {
            return moment(Date).isAfter(CompareTo);
        }),
        CheckDateIsBefore: (function (Date, CompareTo) {
            return moment(Date).isBefore(CompareTo);
        }),
        GetTimeDifference: (function (Date, CompareTo) {
            var date1 = moment.tz(Date, 'GMT'),
                date2 = moment.tz(CompareTo, 'GMT');
            var Duration = moment.duration(date2.diff(date1));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            return DiffernceO;
        }),
        GetTimeDifferenceS: (function (Date, CompareTo) {
            var date1 = moment.tz(Date, 'GMT'),
                date2 = moment.tz(CompareTo, 'GMT');
            var Duration = moment.duration(date2.diff(date1));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            if (DiffernceO.Years > 0) {
                return DiffernceO.Years + ' years';
            }
            else if (DiffernceO.Month > 0) {
                return DiffernceO.Years + ' months';
            }
            else if (DiffernceO.Days > 0) {
                return DiffernceO.Days + ' days';
            }
            else if ((DiffernceO.Hours) > 0) {
                return (DiffernceO.Hours) + ' hours ' + (DiffernceO.Minutes - ((DiffernceO.Hours - 1) * 60)) + ' mins';
            }
            else if (DiffernceO.Minutes > 0) {
                return DiffernceO.Minutes + ' min';
            }
            else {
                return DiffernceO.Seconds + ' sec';
            }
        }),
        GetTimeInterval: (function (Date, CompareTo, TimeZone) {
            var TDate = moment.tz(Date, 'GMT');
            var NDate = moment.tz(TDate, TimeZone);
            var TCompareTo = moment.tz(CompareTo, 'GMT');
            var NCompareToDate = moment.tz(TCompareTo, TimeZone);
            var Duration = moment.duration(NCompareToDate.diff(NDate));
            var DiffernceO =
            {
                Years: 0,
                Month: 0,
                Weeks: 0,
                Days: 0,
                Hours: 0,
                Minutes: 0,
                Seconds: 0
            }
            DiffernceO.Years = Math.round(Math.abs(Duration.asYears()));
            DiffernceO.Month = Math.round(Math.abs(Duration.asMonths()));
            DiffernceO.Weeks = Math.round(Math.abs(Duration.asWeeks()));
            DiffernceO.Days = Math.round(Math.abs(Duration.asDays()));
            DiffernceO.Hours = Math.round(Math.abs(Duration.asHours()));
            DiffernceO.Minutes = Math.round(Math.abs(Duration.asMinutes()));
            DiffernceO.Seconds = Math.round(Math.abs(Duration.asSeconds()));
            if (DiffernceO.Years > 0) {
                var TDate = moment.tz(Date, 'GMT');
                var FDate = moment.tz(TDate, TimeZone).format('DD MMM YYYY h:mm a');
                return FDate;
            }
            else if (DiffernceO.Month > 0) {
                return DiffernceO.Month + ' months';
            }
            else if (DiffernceO.Days > 0) {
                return DiffernceO.Days + ' days';
            }
            else if (DiffernceO.Hours > 0) {
                return DiffernceO.Hours + ' hours';
            }
            else if (DiffernceO.Minutes > 0) {
                return DiffernceO.Minutes + ' minutes';
            }
            else {
                return DiffernceO.Seconds + ' seconds';
            }
        }),
        ValidatePermissions: (function (PermissionsList) {
            $("[data-permission]").each(function () {
                if (PermissionsList != null) {
                    if (PermissionsList.UserRoles != undefined && PermissionsList.UserRoles != null && PermissionsList.UserRoles.length > 0) {
                        var PermissionName = $(this).data('permission');
                        var FoundCount = 0;
                        for (let index = 0; index < PermissionsList.UserRoles.length; index++) {
                            var PermissionRole = PermissionsList.UserRoles[index];
                            for (let kindex = 0; kindex < PermissionRole.RolePermissions.length; kindex++) {
                                var PermissionSystemName = PermissionRole.RolePermissions[kindex].SystemName;
                                if (PermissionSystemName != undefined && PermissionSystemName != null) {
                                    if (PermissionName == PermissionSystemName) {
                                        $(this).addClass('pvalid');
                                        FoundCount = 1;
                                    }
                                }
                            }
                        }
                        if (FoundCount == 1) {
                            $(this).removeClass('pnvalid');
                            $(this).addClass('pvalid');
                        }
                        else {
                            $(this).removeClass('pvalid');
                            $(this).addClass('pnvalid');
                        }
                        FoundCount = 0;
                    }
                }
            });
        }),
        SelectText(containerid) {
            document.getElementById(containerid).select();
            document.execCommand('Copy');
            // if (document.selection) {
            //     var range = document.body.createTextRange();
            //     range.moveToElementText(document.getElementById(containerid));
            //     range.select();
            // } else if (window.getSelection) {
            //     var range = document.createRange();
            //     range.selectNode(document.getElementById(containerid));
            //     window.getSelection().removeAllRanges();
            //     window.getSelection().addRange(range);
            // }
        },
        RefreshMenu: (function () {

            try {
                var sidebar = $('.page-sidebar');
                var sidebarWrapper = $('.page-sidebar .page-sidebar-wrapper');
                sidebar.find('li > a').on('click', function (e) {
                    if ($(this).next().hasClass('sub-menu') === false) {
                        return;
                    }
                    var parent = $(this).parent().parent();
                    parent.children('li.open').children('a').children('.arrow').removeClass('open');
                    parent.children('li.open').children('a').children('.arrow').removeClass('active');
                    parent.children('li.open').children('.sub-menu').slideUp(200);
                    parent.children('li').removeClass('open');

                    var sub = jQuery(this).next();
                    if (sub.is(":visible")) {
                        jQuery('.arrow', jQuery(this)).removeClass("open");
                        jQuery(this).parent().removeClass("active");
                        sub.slideUp(200, function () {
                        });
                    } else {
                        jQuery('.arrow', jQuery(this)).addClass("open");
                        jQuery(this).parent().addClass("open");
                        sub.slideDown(200, function () {
                        });
                    }
                    e.preventDefault();
                });
                //Auto close open menus in Condensed menu
                if (sidebar.hasClass('mini')) {
                    var elem = jQuery('.page-sidebar ul');
                    elem.children('li.open').children('a').children('.arrow').removeClass('open');
                    elem.children('li.open').children('a').children('.arrow').removeClass('active');
                    elem.children('li.open').children('.sub-menu').slideUp(200);
                    elem.children('li').removeClass('open');
                }
                $.fn.scrollbar && sidebarWrapper.scrollbar();

                $.fn.scrollbar && $('.scroller').each(function () {
                    var h = $(this).attr('data-height');
                    $(this).scrollbar({
                        ignoreMobile: true
                    });
                    if (h != null || h != "") {
                        if ($(this).parent('.scroll-wrapper').length > 0)
                            $(this).parent().css('max-height', h);
                        else
                            $(this).css('max-height', h);
                    }
                });
            } catch (error) {
                alert(error);
            }
        }),
    };
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:0 ./src/main.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/admin/Development/BankPanel/acquirer/node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0 */"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0");
module.exports = __webpack_require__(/*! /Users/admin/Development/BankPanel/acquirer/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map