(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panels-acquirer-acquirer-module"],{

/***/ "./src/app/panels/acquirer/acquirer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/panels/acquirer/acquirer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"aside aside-fixed\">\n  <div class=\"aside-header\">\n    <a routerLink=\"{{\n      _HelperService.AppConfig.Pages.ThankUCash.Dashboard.Acquirer\n    }}\" [routerLinkActive]=\"['active']\" class=\"aside-logo nav-link pd-l-10\"><img src=\"../../assets/img/thankucash.svg\"\n        height=\"20px\" width=\"128px\" alt=\".\" /></a>\n    <a href=\"\" class=\"aside-menu-link cursor-pointer\">\n      <i data-feather=\"menu\"></i>\n      <i data-feather=\"x\"></i>\n    </a>\n  </div>\n\n  <div class=\"aside-body\">\n    <div class=\"aside-loggedin\">\n      <div class=\"d-flex align-items-center justify-content-start\">\n        <a class=\"avatar\"><img src=\"{{ _HelperService.UserAccount.IconUrl }}\" class=\"rounded-circle\" alt=\"\"></a>\n        <div class=\"aside-alert-link\">\n          <a class=\"\" data-toggle=\"tooltip\" title=\"You have 0  unread messages\"><i\n              data-feather=\"message-square\"></i></a>\n          <a class=\"\" data-toggle=\"tooltip\" title=\"You have 0 new notifications\"><i data-feather=\"bell\"></i></a>\n          <a class=\"cursor-pointer\" (click)=\"ProcessLogout()\" data-toggle=\"tooltip\" title=\"Sign out\"><i\n              data-feather=\"log-out\"></i></a>\n        </div>\n      </div>\n      <div class=\"aside-loggedin-user\">\n        <a href=\"#loggedinMenu\" class=\"d-flex align-items-center justify-content-between mg-b-2\" data-toggle=\"collapse\">\n          <h6 class=\"tx-semibold mg-b-0\">{{ _HelperService.UserAccount.DisplayName }}\n\n          </h6>\n          <i data-feather=\"chevron-down\"></i>\n        </a>\n        <p class=\"tx-color-03 tx-12 mg-b-0\">Administrator</p>\n      </div>\n      <div class=\"collapse\" id=\"loggedinMenu\">\n        <ul class=\"nav nav-aside mg-b-0\">\n          <!-- <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{\n    _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Profile\n    }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"edit\"></i> <span>Edit\n                Profile</span></a>\n          </li> -->\n          <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{\n    _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Profile\n    }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\">\n\n\n              <i data-feather=\"user-check\"></i>\n              <span class=\"\">{{\"System.Menu.Account\" | translate}}</span></a>\n          </li>\n          <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Subadmins\n          }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"user\"></i>\n              <span>SubAccounts</span></a></li>\n\n          <!-- <li class=\"nav-item\"><a class=\"nav-link\"><i data-feather=\"settings\"></i> <span>Account\n                Settings</span></a></li> -->\n          <!-- <li class=\"nav-item\"><a class=\"nav-link\"><i data-feather=\"help-circle\"></i> <span>Help\n                Center</span></a></li> -->\n          <li class=\"nav-item cursor-pointer\"><a (click)=\"ProcessLogout()\" class=\"nav-link\"><i\n                data-feather=\"log-out\"></i> <span>Sign\n                Out</span></a>\n          </li>\n        </ul>\n      </div>\n    </div><!-- aside-loggedin -->\n    <ul class=\"nav nav-aside mg-b-50\">\n      <!-- <li class=\"nav-label\">Dashboard</li> -->\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{\n    _HelperService.AppConfig.Pages.ThankUCash.Dashboard.Acquirer\n    }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"home\"></i>\n          <span>{{\"System.Menu.Overview\" | translate}}</span></a></li>\n\n      <li class=\"nav-label mg-t-20\">Merchant Management</li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\">\n        <a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchants}}\"\n          [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"briefcase\"></i> <span>Merchants</span></a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer\n    .Terminals\n    }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"smartphone\"></i>\n          <span>{{\"System.Menu.PosTerminals\" | translate}}</span></a></li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Stores\n            }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"map-pin\"></i>\n          <span>{{\"System.Menu.Stores\" | translate}}</span></a></li>\n      <!-- <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer\n                    .Live.Terminals\n                    }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"activity\"></i>\n          <span>{{\"System.Menu.LiveTerminals\" | translate}}</span></a></li> -->\n\n      <!-- Add Merchant  -->\n\n      <!-- <li class=\"nav-item with-sub\" [routerLinkActive]=\"['active']\">\n            <a href=\"\" class=\"nav-link\"><i data-feather=\"user-plus\"></i> <span>Add Merchants</span></a>\n            <ul>\n              <li  [routerLinkActive]=\"['active']\"><a\n                  routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.MerchantOnboarding}}\"\n                  [routerLinkActive]=\"['active']\">Onboard Merchant</a></li>\n              <li [routerLinkActive]=\"['active']\">\n                <a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.MerchantUpload}}\"\n                  [routerLinkActive]=\"['active']\">Upload Merchants</a>\n              </li>\n              <li [routerLinkActive]=\"['active']\"> <a routerLink=\"{{ _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AddTerminal}}\"\n                  [routerLinkActive]=\"['active']\">Assign Terminals</a>\n              </li>\n            </ul>\n          </li> -->\n\n      <!-- <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AddAccount.AddBranch\n            }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"plus-circle\"></i>\n                  <span>{{\"System.Menu.AddBranches\" | translate}}</span></a></li> -->\n      <li class=\"nav-label mg-t-20\">Bank Management</li>\n\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Branch\n                  }}\" [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"codesandbox\"></i>\n          <span>Branches/Divisions</span></a></li>\n\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\" *ngIf=\"\n                        _HelperService.UserAccount.AccountTypeCode ==\n                        _HelperService.AppConfig.AccountType.Merchant ||\n                        _HelperService.UserAccount.AccountTypeCode ==\n                        _HelperService.AppConfig.AccountType.Acquirer\n                        \"><a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.SubAccounts}}\"\n          [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"users\"></i>\n          <span>Manager</span></a></li>\n\n\n      <li class=\"nav-label mg-t-20\">Reward Campaigns</li>\n      <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a\n          routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns}}\"\n          [routerLinkActive]=\"['active']\" class=\"nav-link\"><i data-feather=\"target\"></i>\n          <span>Campaigns</span></a></li>\n\n\n\n\n\n      <!-- <li class=\"nav-item with-sub\" [routerLinkActive]=\"['active']\">\n        <a href=\"\" class=\"nav-link\"><i data-feather=\"settings\"></i> <span>Settings</span></a>\n        <ul>\n\n          <li [routerLinkActive]=\"['active']\">\n            <a routerLink=\"{{\n    _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Subadmins\n    }}\" [routerLinkActive]=\"['active']\">\n              <span>Sub- Admins</span></a>\n          </li>\n          <li [routerLinkActive]=\"['active']\">\n            <a routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Rms}}\"\n              [routerLinkActive]=\"['active']\">\n              <span>{{ \"System.Menu.Rms\" | translate }}</span></a>\n          </li>\n        </ul>\n      </li> -->\n\n      <li class=\"nav-label mg-t-20\">Report & Analysis</li>\n\n      <li class=\"nav-item with-sub\" [routerLinkActive]=\"['active']\">\n        <a href=\"\" class=\"nav-link\"><i data-feather=\"pie-chart\"></i> <span>POS Reports</span></a>\n        <ul>\n\n\n          <li [routerLinkActive]=\"['active']\"><a\n              routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Activity}}\"\n              [routerLinkActive]=\"['active']\"> <span>Activity</span></a></li>\n\n          <!-- <li [routerLinkActive]=\"['active']\"><a\n              routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.SalesReport}}\"\n              [routerLinkActive]=\"['active']\">\n              <span>Sales Report</span></a></li> -->\n\n          <li [routerLinkActive]=\"['active']\"><a\n              routerLink=\"{{ _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.SalesHistory}}\"\n              [routerLinkActive]=\"['active']\">\n              <span>Merchant Transaction</span></a></li>\n\n          <!-- \n            <li  [routerLinkActive]=\"['active']\"><a\n              routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.TerminalsSale}}\"\n              [routerLinkActive]=\"['active']\">\n              <span>Terminal Transaction</span></a></li> -->\n        </ul>\n      </li>\n\n      <!-- <li class=\"nav-item\" [routerLinkActive]=\"['active']\"><a \n        routerLink=\"{{ _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.RMReport}}\" \n        [routerLinkActive]=\"['active']\" class=\"nav-link\"><i\n            data-feather=\"bar-chart-2\"></i>\n          <span>RM Reports</span></a></li> -->\n\n\n\n\n      <!-- <li class=\"nav-label mg-t-25\">Web Apps</li>\n    <li class=\"nav-item\"><a href=\"app-calendar.html\" class=\"nav-link\"><i data-feather=\"calendar\"></i> <span>Calendar</span></a></li>\n    <li class=\"nav-item\"><a href=\"app-chat.html\" class=\"nav-link\"><i data-feather=\"message-square\"></i> <span>Chat</span></a></li>\n    <li class=\"nav-item\"><a href=\"app-contacts.html\" class=\"nav-link\"><i data-feather=\"users\"></i> <span>Contacts</span></a></li>\n    <li class=\"nav-item\"><a href=\"app-file-manager.html\" class=\"nav-link\"><i data-feather=\"file-text\"></i> <span>File Manager</span></a></li>\n    <li class=\"nav-item\"><a href=\"app-mail.html\" class=\"nav-link\"><i data-feather=\"mail\"></i> <span>Mail</span></a></li> -->\n\n      <!-- <li class=\"nav-label mg-t-25\">Pages</li>\n    <li class=\"nav-item with-sub\" [routerLinkActive]=\"['active']\">\n    <a href=\"\" class=\"nav-link\"><i data-feather=\"user\"></i> <span>Reward Campaigns</span></a>\n    <ul>\n    <li><a routerLink=\"{{\n    _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer\n    .Campaigns.Campaigns\n    }}\" [routerLinkActive]=\"['active']\">Campaigns</a></li>\n    </ul>\n    </li> -->\n\n      <!-- <li class=\"nav-item with-sub\">\n    <a href=\"\" class=\"nav-link\"><i data-feather=\"file\"></i> <span>Other Pages</span></a>\n    <ul>\n    <li><a href=\"page-timeline.html\">Timeline</a></li>\n    </ul>\n    </li> -->\n      <!-- <li class=\"nav-label mg-t-25\">User Interface</li>\n      <li class=\"nav-item\"><a href=\"../../components\" class=\"nav-link disabled\"><i data-feather=\"layers\"></i>\n          <span>Components</span></a></li>\n      <li class=\"nav-item\"><a href=\"../../collections\" class=\"nav-link disabled\"><i data-feather=\"box\"></i>\n          <span>Collections</span></a></li> -->\n\n\n    </ul>\n\n\n\n  </div>\n\n  <span class=\"aside-log  bg-white bd w-100 text-center\" style=\"position: fixed; bottom:0%; margin-top: -2.5em ;\">\n    <div class=\"mg-r-20 pd-b-10\">\n      <!-- <a routerLink=\"{{\n        _HelperService.AppConfig.Pages.ThankUCash.Dashboard.Acquirer\n      }}\" [routerLinkActive]=\"['active']\" class=\"aside-logo nav-link pd-l-10\"><img\n          src=\"../../assets/img/thankucash.svg\" height=\"25px\" alt=\".\" /></a> -->\n\n<a class=\"aside-logo tx-10 tx-spacing-3 p-0 m-0\" target=\"_blank\"   href=\"http://connectedanalytics.xyz/\"> <span class=\"tx-10 tx-color-03 text-uppercase\" style=\"font-size: 10px !important;\">  Powered by </span> <br> <span class=\"tx-18 pd-b-20\">  <img  src=\"http://connectedanalytics.xyz/assets/images/connected-analytics.svg\" height=\"25\"  width=\"135\" alt=\"\">  </span></a>\n\n\n    </div>\n  </span>\n\n\n</aside>\n\n<div class=\"content ht-100v pd-0\">\n  <div class=\"content-header justify-content-start\">\n    <a class=\"cursor-pointer\" (click)=\"_HelperService.NavigateBack()\">\n      <i data-feather=\"arrow-left-circle\"></i>\n    </a>\n    <span class=\"text-uppercase ml-3\">\n      {{ _HelperService.AppConfig.ActivePageName }}</span>\n  </div>\n  <div [className]=\"_HelperService.FullContainer ? '' : 'content-body skin'\">\n    <div class=\"container\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>\n\n\n\n\n<div class=\"df-settings\">\n  <a id=\"dfSettingsShow\" href=\"\" class=\"df-settings-link\"><i data-feather=\"settings\"></i></a>\n  <div class=\"df-settings-body\">\n    <div class=\"pd-y-20\">\n      <h3 class=\"tx-bold tx-spacing--2 tx-brand-02 mg-b-0\"><img src=\"../../assets/img/thankucash.svg\" alt=\"\"></h3>\n      <span class=\"tx-sans tx-9 tx-uppercase tx-semibold tx-spacing-1 tx-color-03\"> Skin Customizer</span>\n    </div>\n\n    <div class=\"pd-y-20 bd-t\">\n      <label class=\"tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15\">Skin Mode</label>\n      <div class=\"row row-xs\">\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-mode df-mode-default active\" data-title=\"classic\"></a>\n          <span class=\"df-skin-name\">Classic</span>\n        </div>\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-mode df-mode-light\" data-title=\"light\">\n            <span class=\"bg-ui-01\"></span>\n            <span class=\"bg-ui-02\"></span>\n          </a>\n          <span class=\"df-skin-name\">Light</span>\n        </div>\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-mode df-mode-cold\" data-title=\"cool\">\n            <span class=\"bg-primary op-1\"></span>\n            <span class=\"bg-primary op-2\"></span>\n          </a>\n          <span class=\"df-skin-name\">Cool</span>\n        </div>\n        <div class=\"col-4 mg-t-15\">\n          <a href=\"\" class=\"df-mode df-mode-dark disabled\" data-title=\"dark\">\n            <span class=\"bg-gray-700\"></span>\n            <span class=\"bg-gray-900\"></span>\n          </a>\n          <span class=\"df-skin-name tx-nowrap\">Dark <span class=\"tx-normal\">(Coming Soon)</span></span>\n        </div>\n      </div><!-- row -->\n    </div>\n\n    <div class=\"pd-y-20 bd-t\">\n      <label class=\"tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15\">Navigation Skin</label>\n      <div class=\"row row-xs\">\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-skin df-skin-default active\" data-title=\"default\"></a>\n          <span class=\"df-skin-name\">Default</span>\n        </div>\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-skin df-skin-deepblue\" data-title=\"deepblue\">\n            <span></span>\n            <span></span>\n          </a>\n          <span class=\"df-skin-name\">Deep Blue</span>\n        </div>\n        <div class=\"col-4\">\n          <a href=\"\" class=\"df-skin df-skin-charcoal\" data-title=\"charcoal\">\n            <span></span>\n            <span></span>\n          </a>\n          <span class=\"df-skin-name\">Charcoal</span>\n        </div>\n        <div class=\"col-4 mg-t-15\">\n          <a href=\"\" class=\"df-skin df-skin-gradient1\" data-title=\"gradient1\">\n            <span></span>\n            <span></span>\n          </a>\n          <span class=\"df-skin-name\">Gradient 1</span>\n        </div>\n      </div><!-- row -->\n    </div>\n    <div class=\"pd-y-20 bd-t\">\n      <label class=\"tx-sans tx-10 tx-uppercase tx-bold tx-spacing-1 tx-color-02 mg-b-15\">Font Family Base</label>\n      <div class=\"row row-xs\">\n        <div class=\"col-7\">\n          <a href=\"\" id=\"setFontBase\" class=\"btn btn-xs btn-white btn-uppercase btn-block\">IBM Plex Sans</a>\n        </div><!-- col -->\n        <div class=\"col-5\">\n          <a href=\"\" id=\"setFontRoboto\" class=\"btn btn-xs btn-white btn-uppercase btn-block active-primary\">Roboto</a>\n        </div><!-- col -->\n      </div><!-- row -->\n    </div>\n  </div><!-- df-settings-body -->\n</div><!-- df-settings -->"

/***/ }),

/***/ "./src/app/panels/acquirer/acquirer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/panels/acquirer/acquirer.component.ts ***!
  \*******************************************************/
/*! exports provided: TUAcquirerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUAcquirerComponent", function() { return TUAcquirerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_5__);






var TUAcquirerComponent = /** @class */ (function () {
    function TUAcquirerComponent(_DataHelperService, _Router, _HelperService, renderer2) {
        this._DataHelperService = _DataHelperService;
        this._Router = _Router;
        this._HelperService = _HelperService;
        this.renderer2 = renderer2;
    }
    TUAcquirerComponent.prototype.ngAfterViewInit = function () {
    };
    TUAcquirerComponent.prototype.ngOnInit = function () {
        var s = this.renderer2.createElement('script');
        s.type = 'text/javascript';
        s.src = '../../../assets/js/dashforge.aside.js';
        s.text = "";
        this.renderer2.appendChild(document.body, s);
        var s2 = this.renderer2.createElement('script');
        s2.type = 'text/javascript';
        s2.src = '../../../../assets/js/dashforge.settings.js';
        s2.text = "";
        this.renderer2.appendChild(document.body, s2);
        feather_icons__WEBPACK_IMPORTED_MODULE_5__["replace"]();
        // if (this._HelperService.UserAccount == null) {
        //     window.location.href = "/account/login";
        // }
        // else {
        //     $('[data-toggle="tooltip"]').tooltip()
        //     const asideBody = new PerfectScrollbar('.aside-body', {
        //         suppressScrollX: true
        //     });
        //     if ($('.aside-backdrop').length === 0) {
        //         $('body').append('<div class="aside-backdrop"></div>');
        //     }
        //     var mql = window.matchMedia('(min-width:992px) and (max-width: 1199px)');
        //     $('.aside-menu-link').on('click', function (e) {
        //         e.preventDefault()
        //         if (window.matchMedia('(min-width: 992px)').matches) {
        //             $(this).closest('.aside').toggleClass('minimize');
        //         } else {
        //             $('body').toggleClass('show-aside');
        //         }
        //         asideBody.update()
        //     })
        //     $('.nav-aside .with-sub').on('click', '.nav-link', function (e) {
        //         e.preventDefault();
        //         $(this).parent().siblings().removeClass('show');
        //         $(this).parent().toggleClass('show');
        //         asideBody.update()
        //     })
        //     $('body').on('mouseenter', '.minimize .aside-body', function (e) {
        //         console.log('e');
        //         $(this).parent().addClass('maximize');
        //     })
        //     $('body').on('mouseleave', '.minimize .aside-body', function (e) {
        //         $(this).parent().removeClass('maximize');
        //         asideBody.update()
        //     })
        //     $('body').on('click', '.aside-backdrop', function (e) {
        //         $('body').removeClass('show-aside');
        //     })
        // }
    };
    TUAcquirerComponent.prototype.ProcessLogout = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.LogoutTitle,
            text: "Click on Logout button to confirm",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                var pData = {
                    Task: _this._HelperService.AppConfig.Api.Logout
                };
                var _OResponse = void 0;
                _OResponse = _this._HelperService.PostData(_this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse.subscribe(function (_Response) {
                    if (_Response.Status == _this._HelperService.StatusSuccess) {
                        _this._HelperService.NotifySuccess(_Response.Message);
                    }
                    else {
                        _this._HelperService.NotifyError(_Response.Message);
                    }
                }, function (_Error) {
                    console.log(_Error);
                });
                _this._HelperService.DeleteStorage(_this._HelperService.AppConfig.Storage.Account);
                _this._HelperService.DeleteStorage(_this._HelperService.AppConfig.Storage.Location);
                _this._HelperService.DeleteStorage(_this._HelperService.AppConfig.Storage.OReqH);
                _this._HelperService.DeleteStorage(_this._HelperService.AppConfig.Storage.Permissions);
                _this._Router.navigate([_this._HelperService.AppConfig.Pages.System.Login]);
            }
            else { }
        });
    };
    TUAcquirerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'acquirer-root',
            template: __webpack_require__(/*! ./acquirer.component.html */ "./src/app/panels/acquirer/acquirer.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_service__WEBPACK_IMPORTED_MODULE_2__["DataHelperService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _service_service__WEBPACK_IMPORTED_MODULE_2__["HelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], TUAcquirerComponent);
    return TUAcquirerComponent;
}());



/***/ }),

/***/ "./src/app/panels/acquirer/acquirer.module.ts":
/*!****************************************************!*\
  !*** ./src/app/panels/acquirer/acquirer.module.ts ***!
  \****************************************************/
/*! exports provided: TUAcquirerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUAcquirerModule", function() { return TUAcquirerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _acquirer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./acquirer.component */ "./src/app/panels/acquirer/acquirer.component.ts");
/* harmony import */ var _acquirer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./acquirer.routing.module */ "./src/app/panels/acquirer/acquirer.routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var TUAcquirerModule = /** @class */ (function () {
    function TUAcquirerModule() {
    }
    TUAcquirerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _acquirer_component__WEBPACK_IMPORTED_MODULE_4__["TUAcquirerComponent"],
            ],
            imports: [
                _acquirer_routing_module__WEBPACK_IMPORTED_MODULE_5__["TUAcquirerRoutingModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            ],
            providers: [],
            bootstrap: [_acquirer_component__WEBPACK_IMPORTED_MODULE_4__["TUAcquirerComponent"]]
        })
    ], TUAcquirerModule);
    return TUAcquirerModule;
}());



/***/ }),

/***/ "./src/app/panels/acquirer/acquirer.routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/panels/acquirer/acquirer.routing.module.ts ***!
  \************************************************************/
/*! exports provided: TUAcquirerRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUAcquirerRoutingModule", function() { return TUAcquirerRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _acquirer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./acquirer.component */ "./src/app/panels/acquirer/acquirer.component.ts");
/* harmony import */ var _service_helper_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/helper.service */ "./src/app/service/helper.service.ts");





var routes = [
    {
        path: "acquirer",
        component: _acquirer_component__WEBPACK_IMPORTED_MODULE_3__["TUAcquirerComponent"],
        canActivateChild: [_service_helper_service__WEBPACK_IMPORTED_MODULE_4__["HelperService"]],
        children: [
            //Branch ANd Division
            { path: "branch", data: { permission: "terminals", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/accounts/branch/branch.module#BranchModule" },
            { path: "branchdetails", data: { permission: "terminals", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/accountdetails/BranchDetails/branchdetails.module#BranchDetailsModule" },
            { path: "branchdetails/editbranch/:referencekey/:referenceid", data: { permission: "Branch", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/BranchDivision/EditBranch/EditBranch.module#EditBranchModule" },
            //End 
            { path: "activity", data: { permission: "activity", PageName: "System.Menu.Activity" }, loadChildren: "./../../modules/dashboards/tuactivity/acquirer/tuactivity.module#TUActivityModule" },
            { path: "salesreport", data: { permission: "salesreport", PageName: "System.Menu.SaleReport" }, loadChildren: "./../../modules/dashboards/tusalesreport/acquirer/tusalesreport.module#TUSalesReportModule" },
            { path: "rmreports", data: { permission: "rmreport", PageName: "System.Menu.RMReport" }, loadChildren: "./../../modules/dashboards/turmreport/acquirer/turmreport.module#TURmReportModule" },
            { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/acquirer/dashboards/root/dashboard.module#TUDashboardModule" },
            { path: "salesanalytics", data: { permission: "dashboard", PageName: "System.Menu.SalesAnalytics" }, loadChildren: "./../../panel/acquirer/analytics/salesanalytics/salesanalytics.module#TUSalesAnalyticsModule" },
            { path: "addcampaign", data: { permission: "terminals", PageName: "System.Menu.AddCampaign" }, loadChildren: "./../../panel/acquirer/campaigns/addcampaign/addcampaign.module#TUAddCampaignModule" },
            { path: "campaigns", data: { permission: "terminals", PageName: "System.Menu.Campaigns" }, loadChildren: "./../../panel/acquirer/campaigns/campaigns/campaigns.module#TUCampaignsModule" },
            { path: "campaign/:referencekey", data: { permission: "terminals", PageName: "System.Menu.Campaign" }, loadChildren: "./../../panel/acquirer/campaigns/campaign/campaign.module#TUCampaignModule" },
            { path: "rm/:referencekey/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Rm" }, loadChildren: "./../../panel/acquirer/accountdetails/rm/rm.module#TURelationshipManagerModule" },
            { path: "rms", data: { permission: "terminals", PageName: "System.Menu.Rms" }, loadChildren: "./../../panel/acquirer/accounts/relationshipmanagers/relationshipmanagers.module#TURelationshipManagersModule" },
            { path: "addrm", data: { permission: "terminals", PageName: "System.Menu.AddRm" }, loadChildren: "./../../panel/acquirer/accountcreation/rm/tuaddrm.module#TUAddRmModule" },
            { path: "addbranch", data: { permission: "branch", PageName: "System.Menu.Branch" }, loadChildren: "./../../panel/acquirer/accountcreation/addbranch/addbranch.module#AddbranchModule" },
            { path: "merchants", data: { permission: "terminals", PageName: "System.Menu.Merchants" }, loadChildren: "./../../modules/accounts/tumerchants/acquirer/tumerchants.module#TUMerchantsModule" },
            { path: "stores", data: { permission: "stores", PageName: "System.Menu.Stores" }, loadChildren: "./../../modules/accounts/tustores/acquirer/tustores.module#TUStoresModule" },
            { path: "temp", data: { permission: "stores", PageName: "System.Menu.Stores" }, loadChildren: "./../../modules/accounts/temp/hclist.module#HCListModule" },
            { path: 'subaccounts', data: { 'permission': 'stores', PageName: 'System.Menu.Managers' }, loadChildren: './../../modules/accounts/tusubaccounts/acquirer/tusubaccounts.module#TUSubAccountsModule' },
            { path: 'saleshistory', data: { 'permission': 'merchants', PageName: 'System.Menu.SalesHistory' }, loadChildren: './../../modules/transactions/tusale/acquirer/tusale.module#TUSaleModule' },
            { path: 'terminal', data: { 'permission': 'customers', PageName: 'System.Menu.Terminal' }, loadChildren: './../../modules/accounts/tuterminal/tuterminal.module#TUTerminalModule' },
            { path: 'terminals', data: { 'permission': 'terminals', PageName: 'System.Menu.Terminals' }, loadChildren: './../../modules/accounts/tuterminals/acquirer/tuterminals.module#TUTerminalsModule' },
            { path: "customers", data: { permission: "customers", PageName: "System.Menu.Customers" }, loadChildren: "./../../pages/accounts/tuappusers/tuappusers.module#TUAppUsersListModule" },
            { path: "live/terminals", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../modules/dashboards/tuliveterminals/acquirer/tuliveterminals.module#TULiveTerminalsModule" },
            { path: "live/merchants", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../modules/dashboards/tulivemerchants/acquirer/tulivemerchants.module#TULiveMerchantsModule" },
            { path: "merchant", data: { permission: "merchant", PageName: "System.Menu.Merchant" }, loadChildren: "./../../modules/accountdetails/tumerchant/acquirer/tumerchant.module#TUMerchantModule" },
            { path: "terminal", data: { permission: "terminal", PageName: "System.Menu.Terminal" }, loadChildren: "./../../modules/accountdetails/tuterminal/acquirer/tuterminal.module#TUTerminalModule" },
            { path: "store", data: { permission: "terminal", PageName: "System.Menu.Store" }, loadChildren: "./../../modules/accountdetails/tustore/acquirer/tustore.module#TUStoreModule" },
            { path: "manager", data: { permission: "manager", PageName: "System.Menu.Manager" }, loadChildren: "./../../modules/accountdetails/tumanager/acquirer/tumanager.module#TUManagerModule" },
            { path: "campaign", data: { permission: "campaign", PageName: "System.Menu.Campaign" }, loadChildren: "./../../modules/accountdetails/tucampaign/acquirer/tucampaign.module#TUCampaignModule" },
            {
                path: "referredmerchants",
                data: { permission: "terminals", PageName: "System.Menu.Merchants" },
                loadChildren: "./../../pages/accounts/tuacquirermerchants/tuacquirermerchants.module#TUAcquirerMerchantsModule"
            },
            {
                path: "subadmindetails/:referencekey/:referenceid",
                data: { permission: "admin", PageName: "System.Menu.SubAdmin" },
                loadChildren: "./../../panel/acquirer/accountdetails/subadmindetails/subadmindetails.module#SubadmindetailsModule"
            },
            {
                path: "subadmins",
                data: { permission: "admin", PageName: "System.Menu.SubAccounts" },
                loadChildren: "./../../panel/acquirer/accounts/subadmins/subadmins.module#SubadminsModule"
            },
            { path: "profile", data: { permission: "customers", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcprofile/hcprofile.module#HCProfileModule" },
            {
                path: "terminallive",
                data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
                loadChildren: "./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module#TUMerchantTerminalsLiveModule"
            },
            {
                path: "terminalssale",
                data: {
                    permission: "merchants",
                    PageName: "System.Menu.CommissionHistory"
                },
                loadChildren: "./../../pages/transactions/tuterminalssale/tuterminalssale.module#TUTerminalsSaleModule"
            },
            {
                path: "customerupload",
                data: { permission: "stores", PageName: "System.Menu.CustomerManager" },
                loadChildren: "./../../pages/useronboarding/customerupload/customerupload.module#TUCustomerUploadModule"
            },
            {
                path: "customeronboarding",
                data: { permission: "stores", PageName: "System.Menu.CustomerManager" },
                loadChildren: "./../../pages/useronboarding/customeronboarding/customeronboarding.module#TUCustomerOnboardingModule"
            },
            {
                path: "merchantupload",
                data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
                loadChildren: "./../../pages/useronboarding/merchantupload/merchantupload.module#TUMerchantUploadModule"
            },
            {
                path: "merchantonboarding",
                data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
                loadChildren: "./../../pages/useronboarding/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule"
            },
            {
                path: "merchantmanager/:referencekey",
                data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
                loadChildren: "./../../pages/useronboarding/merchantmanager/merchantmanager.module#TUMerchantManagerModule"
            },
            {
                path: "addterminal",
                data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
                loadChildren: "./../../pages/useronboarding/tuaddposterminal/tuaddposterminal.module#TUAddPosTerminalModule"
            }
        ]
    }
];
var TUAcquirerRoutingModule = /** @class */ (function () {
    function TUAcquirerRoutingModule() {
    }
    TUAcquirerRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TUAcquirerRoutingModule);
    return TUAcquirerRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=panels-acquirer-acquirer-module.js.map