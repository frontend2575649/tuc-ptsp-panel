(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-accountdetails-tucampaign-acquirer-tucampaign-module"],{

/***/ "./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--campaign--details -->\n<div id=\"accordion\">\n    <div class=\"card shadow-none gredientBorder\">\n        <div class=\"card-header bd-b-0\" id=\"headingOne\">\n\n            <div class=\"row \">\n\n                <div class=\"col-sm-12 col-md-12 col-lg-1 text-center\">\n                    <div class=\"badge badge-success\" [ngClass]=\"_CampaignDetails.StatusB\">\n                        {{_CampaignDetails.StatusName}} </div>\n                    <div class=\"tx-teal tx-18 tx-semibold mg-t-5\"> {{_DateDiff.days}} Days {{_DateDiff.hours}} Hours\n                    </div>\n                    <div class=\"text-muted tx-14\"> Duration </div>\n\n                </div>\n\n                <div class=\"col-sm-12 col-md-12 col-lg-5 d-md-flex d-lg-flex Hm-AlignCenter Hm-media768\">\n                    <div class=\"media  Hm-textCenterMd\">\n                        <div class=\"media-body\">\n                            <h6 class=\"tx-20 tx-lg-24 tx-semibold text-capitalize  mg-b-2\">\n                                {{_CampaignDetails.Name}}</h6>\n                            <div class=\"d-md-flex d-lg-flex Hm-mg-y Hm-AlignCenter align-items-baseline\">\n                                <h2 class=\"tx-14 tx-lg-14 tx-color-03 tx-normal   lh-2 mg-b-5\">\n                                    Start Date : {{_CampaignDetails.StartDate}}</h2>\n                            </div>\n                            <div class=\"d-md-flex d-lg-flex Hm-AlignCenter align-items-baseline\">\n                                <h2 class=\"tx-14 tx-lg-14 tx-color-03 tx-normal   lh-2 mg-b-0\">\n                                    End Date : {{_CampaignDetails.EndDate}}\n                                </h2>\n                            </div>\n                        </div><!-- media-body -->\n                    </div><!-- media -->\n                </div>\n                <div class=\"col-lg-6 Hm-media768 \" id=\"StoresHide\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6 col-lg-6\">\n                            <div class=\"row flex-nowrap\">\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n\n                                    <div class=\" text-center stores\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\" tx-26  tx-teal mg-b-5 mg-r-5 lh-1\">\n                                                {{_CampaignDetails.TypeName | campaignText}} </h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0 \"> Campaign Type</p>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-sm-6 col-md-6 col-lg-6\">\n                                    <div class=\" text-center Terminals\">\n                                        <div class=\"text-center\">\n                                            <h3 class=\"tx-26 tx-rubik tx-verve mg-b-5 mg-r-5 lh-1 \">\n                                                {{_CampaignDetails.SubTypeValue}}</h3>\n                                        </div>\n                                        <p class=\"tx-12 tx-color-03 mg-b-0\"> Reward</p>\n\n                                    </div>\n\n                                </div>\n\n\n                            </div>\n                        </div>\n                        <div class=\"col-sm-12 col-md-6 col-lg-6 col-lg-6 d-flex Hm-media768 justify-content-end\">\n                            <div class=\"row\">\n                                <div class=\"manageButton mg-t-0 pd-t-0\">\n                                    <button (click)=\"Form_EditUser_Show()\" type=\"button\"\n                                        class=\"btn btn-xs btn-transparent tx-color-03 bd bd-secondary rounded-pill\">Manage\n                                        Account</button>\n                                </div>\n\n                                <!-- <div class=\"collapseButton\">\n                                    <button [class.button-open]=\"!slideOpen\" [class.button-close]=\"slideOpen\"\n                                        (click)=\"changeSlide() ; myFunction()\" class=\"btn  btn-link\"\n                                        data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"false\"\n                                        aria-controls=\"collapseOne\">\n                                        <div class=\"tx-15 tx-lg-18 lh-0 tx-color-03\"><i class=\"fas fa-chevron-down\"\n                                                [class.clicked]=\"slideOpen\"></i>\n                                        </div>\n                                    </button>\n                                </div> -->\n\n                            </div>\n\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n\n\n    </div>\n\n\n</div>\n\n<div class=\"card bd-b-0 pd-x-20 pd-b-20 pd-t-10 mt-3\">\n    <ul class=\"nav nav-line d-flex justify-content-between\" id=\"myTab5\" role=\"tablist\">\n\n        <div class=\"d-flex flex-wrap\">\n            <!-- <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\" >\n            <a class=\"nav-link \" id=\"home-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"   routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Dashboard}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\" role=\"tab\"\n                aria-controls=\"home\" aria-selected=\"true\">Overview</a>\n        </li> -->\n            <li class=\"nav-item cursor-pointer\" [routerLinkActive]=\"['active']\">\n                <a class=\"nav-link\" id=\"contact-tab5\" data-toggle=\"tab\" [routerLinkActive]=\"['active']\"\n                    routerLink=\"{{_HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Sales}}/{{this._HelperService.AppConfig.ActiveReferenceKey}}/{{ this._HelperService.AppConfig.ActiveReferenceId}}\"\n                    role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\">Transaction</a>\n            </li>\n\n            <!-- <li class=\"nav-item cursor-pointer\">\n            <a class=\"nav-link\" id=\"contact-tab5\" data-toggle=\"tab\"\n                routerLink=\"{{ _HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.SalesHistory}}/{{ _HelperService.AppConfig.ActiveReferenceKey}}/{{ _HelperService.AppConfig.ActiveReferenceId}}\"\n                role=\"tab\" aria-controls=\"contact\" aria-selected=\"false\"></a>\n        </li> -->\n        </div>\n        <!-- <div class=\"row  d-flex no-gutters pd-r-10\">\n    <div class=\" col-sm-12 col-md-6 d-flex mg-md-t-0 mg-t-10\" *ngIf=\"_HelperService.ShowDateRange\">\n        <div class=\" order-1\">\n            <input type=\"text\"  style=\"height: 30px;\" class=\"form-control tx-13 form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                [options]=\"_HelperService.AppConfig.DatePickerOptions\"\n                (selected)=\"_HelperService.SetDateRange($event)\" />\n        </div>\n    </div>\n\n    <div class=\"col-md-6 col-sm-12 btn-group mg-md-t-0 mg-t-10\" style=\"height: 30px;\" data-toggle=\"buttons\" *ngIf=\"_HelperService.ShowDateRange\">\n        <label class=\"btn btn-xs pb-0 mb-0 tx-13 btn-white  active d-flex align-items-center\"\n            (click)=\"_HelperService.ToogleRange('A')\">\n            <input type=\"radio\" name=\"options\" style=\"display: none;\" checked>\n            <span class=\"tx-13 mg-b-5\"> Month </span>\n        </label>\n        <label class=\"btn btn-xs  pb-0 mb-0 tx-13 btn-white d-flex align-items-center\"\n            (click)=\"_HelperService.ToogleRange('B')\">\n            <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n            <span class=\"tx-13 mg-b-5\">Week</span>\n        </label>\n        <label class=\"btn pb-0 mb-0 btn-xs tx-13 btn-white  d-flex align-items-center\"\n            (click)=\"_HelperService.ToogleRange('C')\">\n            <input class=\"tx-13\" type=\"radio\" name=\"options\" style=\"display: none;\">\n            <span class=\"tx-13 mg-b-5\"> Day </span>\n        </label>\n    </div>\n</div> -->\n\n\n\n    </ul>\n</div>\n\n<router-outlet></router-outlet>\n<div id=\"Form_EditUser_Content\" class=\"modal fade\" data-backdrop=\"true\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <form [formGroup]=\"Form_EditUser\" (ngSubmit)=\"Form_EditUser_Process(Form_EditUser.value)\" role=\"form\">\n                <div class=\"modal-header\">\n                    <h5 class=\"modal-title tx-14\">Manage Campaign</h5>\n                </div>\n                <div class=\"modal-body  p-lg\">\n                    <div class=\"row\">\n                        <div class=\"col-md-12\">\n\n                            <ul class=\"list-inline mg-t-20 mg-sm-t-10 mg-md-t-0 mg-b-0\">\n                                <li class=\"list-inline-item d-flex align-items-center\">\n                                    <span class=\"d-block wd-15 ht-16 bg-success rounded-50 mg-r-5\"></span>\n                                    <span class=\"tx-sans  tx-14 tx-bold\">{{_CampaignDetails.Name}}</span>\n                                </li>\n                                <li>\n                                <li class=\"list-inline-item d-flex align-items-center\">\n                                    <span class=\"d-block-none wd-15 ht-16 bg-white rounded-50 mg-r-5\"></span>\n                                    <span class=\"tx-sans  tx-color-03 tx-semibold tx-14\">Campaign Type:\n                                        {{_CampaignDetails.TypeName}} </span>\n                                </li>\n                                <li class=\"list-inline-item d-flex align-items-center\">\n                                    <span class=\"d-block-none wd-15 ht-16 bg-white rounded-50 mg-r-5\"></span>\n                                    <span class=\"tx-sans  tx-color-03 tx-semibold tx-14\">Reward type:\n                                        {{_CampaignDetails.SubTypeName}}</span>\n                                </li>\n                                <li class=\"list-inline-item d-flex align-items-center\">\n                                    <span class=\"d-block-none wd-15 ht-16 bg-white rounded-50 mg-r-5\"></span>\n                                    <span class=\"tx-sans  tx-color-03 tx-semibold tx-14\">Reward\n                                        Amount:{{_CampaignDetails.SubTypeValue}}</span>\n                                </li>\n\n                            </ul>\n                            <div class=\"custom-control custom-checkbox mt-3\">\n                                <!-- <input class=\"custom-control-input\" type=\"checkbox\" name=\"browser[]\" value=\"1\"\n                                    data-parsley-mincheck=\"2\" data-parsley-class-handler=\"#cbWrapper\"\n                                    data-parsley-errors-container=\"#cbErrorContainer\" required=\"\"\n                                    data-parsley-multiple=\"browser\" id=\"b1\"> -->\n                                <input type=\"radio\" class=\"custom-control-input\" id=\"b1\" name=\"isExtendActive\"\n                                    [checked]=\"isExtendActive\" (change)=\"ToogleExtend()\">\n                                <label class=\"custom-control-label tx-sans  tx-14 tx-bold\" for=\"b1\">Extend\n                                    campaign</label>\n                            </div>\n\n                            <div class=\"mt-3\" *ngIf=\"isExtendActive\">\n                                <div class=\"form-group\">\n                                    <label class=\"tx-sans  text-muted tx-14\">End Date</label>\n                                    <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\"\n                                        daterangepicker [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                                        (selected)=\"Date_Toogle($event)\" />\n                                    <span class=\" mt-3 tx-color-03\"> If you want, you can extend\n                                        this campaign. Just\n                                        update End date for this campaign</span>\n                                </div>\n                            </div>\n                            <div class=\"custom-control custom-checkbox mt-3\">\n                                <!-- <input class=\"custom-control-input\" type=\"checkbox\" name=\"browser[]\" value=\"1\"\n                                    data-parsley-mincheck=\"2\" data-parsley-class-handler=\"#cbWrapper\"\n                                    data-parsley-errors-container=\"#cbErrorContainer\" required=\"\"\n                                    data-parsley-multiple=\"browser\" id=\"b2\"> -->\n                                <input type=\"radio\" class=\"custom-control-input\" id=\"b2\" name=\"isCloseCamp\"\n                                    [checked]=\"isCloseCamp\" (change)=\"ToogleCloseCamp()\">\n                                <label class=\"custom-control-label tx-sans  tx-14 tx-bold\" for=\"b2\">Close\n                                    Campaign</label> <br>\n                                <span class=\"mt-2 tx-color-03\" *ngIf=\"isCloseCamp\"> If you close this campaign, you\n                                    will not able to Resume\n                                    this campaign in future.Do you really want to close this campaign permanently?\n                                </span>\n                            </div>\n                            <div class=\"custom-control custom-checkbox mt-3 mb-5 \">\n                                <!-- <input class=\"custom-control-input\" type=\"checkbox\" name=\"browser[]\" value=\"1\"\n                                    data-parsley-mincheck=\"2\" data-parsley-class-handler=\"#cbWrapper\"\n                                    data-parsley-errors-container=\"#cbErrorContainer\" required=\"\"\n                                    data-parsley-multiple=\"browser\" id=\"b3\"> -->\n                                <input type=\"radio\" class=\"custom-control-input\" id=\"b3\" name=\"isPauseCamp\"\n                                    [checked]=\"isPauseCamp\" (change)=\"TooglePauseCamp()\">\n                                <label class=\"custom-control-label tx-sans  tx-14 tx-bold\" for=\"b3\">Pause\n                                    Campaign</label> <br>\n                                <span class=\"mt-2 tx-color-03\" *ngIf=\"isPauseCamp\"> Campaign will pause, you can resume\n                                    it also,whenever you\n                                    want. </span>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\"\n                        (click)=\"Form_EditUser_Close()\">\n                        {{ \"Common.Close\" | translate }}\n                    </button>\n                    <button type=\"submit\" class=\"btn btn-primary warn mr-2\"\n                        (click)=\"Form_EditUser.value.OperationType = 'new'\"\n                        [disabled]=\"!Form_EditUser.valid || _HelperService.IsFormProcessing\">\n                        <span *ngIf=\"!_HelperService.IsFormProcessing\">\n                            {{ \"Common.Save\" | translate }}\n                        </span>\n                        <span *ngIf=\"_HelperService.IsFormProcessing\">\n                            <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                            {{ \"Common.PleaseWait\" | translate }}...\n                        </span>\n                    </button>\n                    <!-- <button type=\"submit\" class=\"btn btn-outline b-warn text-warn mr-2\"\n                        (click)=\"Form_EditUser.value.OperationType = 'close'\"\n                        [disabled]=\"!Form_EditUser.valid || _HelperService.IsFormProcessing\">\n                        <span *ngIf=\"!_HelperService.IsFormProcessing\">\n                            {{ \"Common.SaveAndClose\" | translate }}\n                        </span>\n                        <span *ngIf=\"_HelperService.IsFormProcessing\">\n                            <i class=\"{{ 'Common.SpinnerIcon' | translate }}\"></i>\n                            {{ \"Common.PleaseWait\" | translate }}...\n                        </span>\n                    </button> -->\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.ts ***!
  \************************************************************************************/
/*! exports provided: TuCampaignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TuCampaignComponent", function() { return TuCampaignComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");







var TuCampaignComponent = /** @class */ (function () {
    function TuCampaignComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this.slideOpen = false;
        this._DateDiff = {};
        this.CampaignKey = null;
        this.CampaignRewardType_FixedAmount = "fixedamount";
        this.CampaignRewardType_AmountPercentage = "amountpercentage";
        this._CampaignDetails = {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        };
        this.isCloseCamp = false;
        this.isPauseCamp = false;
        this.isExtendActive = false;
    }
    TuCampaignComponent.prototype.changeSlide = function () {
        this.slideOpen = !this.slideOpen;
        if (this.slideOpen) {
            this._HelperService._MapCorrection();
        }
    };
    TuCampaignComponent.prototype.myFunction = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-HideDiv");
        element.classList.remove("Hm-DisplayDiv");
    };
    TuCampaignComponent.prototype.DisplayDiv = function () {
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-DisplayDiv");
        element.classList.remove("Hm-HideDiv");
    };
    TuCampaignComponent.prototype.ngOnInit = function () {
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this.Form_EditUser_Load();
        var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCampaign);
        if (StorageDetails != null) {
            this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
            this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
            this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
            this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
        }
        this.GetCampaignDetails();
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.PosTerminal;
        this._HelperService.ContainerHeight = window.innerHeight;
        this._HelperService.FullContainer = false;
    };
    //#region ToogleRegion
    TuCampaignComponent.prototype.ToogleCloseCamp = function () {
        if (!this.isCloseCamp) {
            this.isCloseCamp = !this.isCloseCamp;
            this.isPauseCamp = false;
            this.isExtendActive = false;
        }
        // if (this.isCloseCamp) {
        //   this.Form_EditUser.controls['StatusCode'].setValue(this._HelperService.AppConfig.Status.Campaign.Archived);
        // } else {
        //   this.Form_EditUser.controls['StatusCode'].setValue(this._CampaignDetails.StatusCode);
        // }
    };
    TuCampaignComponent.prototype.TooglePauseCamp = function () {
        if (!this.isPauseCamp) {
            this.isPauseCamp = !this.isPauseCamp;
            this.isCloseCamp = false;
            this.isExtendActive = false;
        }
    };
    TuCampaignComponent.prototype.ToogleExtend = function () {
        if (!this.isExtendActive) {
            this.isExtendActive = !this.isExtendActive;
            this.isCloseCamp = false;
            this.isPauseCamp = false;
        }
    };
    //#endregion
    TuCampaignComponent.prototype.GetCampaignDetails = function () {
        var _this = this;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaign,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        };
        var _OResponse;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
        _OResponse.subscribe(function (_Response) {
            if (_Response.Status == _this._HelperService.StatusSuccess) {
                _this._HelperService.IsFormProcessing = false;
                _this._CampaignDetails = _Response.Result;
                if (_this._DaterangePickerComponent != undefined) {
                    _this._DaterangePickerComponent.datePicker.setStartDate(moment(_this._CampaignDetails.StartDate).utc().format("YYYY-MM-DD"));
                    _this._DaterangePickerComponent.datePicker.setEndDate(moment(_this._CampaignDetails.EndDate).utc().format("YYYY-MM-DD"));
                }
                _this._DateDiff = _this._HelperService._DateRangeDifference(_this._CampaignDetails.StartDate, _this._CampaignDetails.EndDate);
                _this._CampaignDetails.StartDateS = _this._HelperService.GetDateS(_this._CampaignDetails.StartDate);
                _this._CampaignDetails.EndDateS = _this._HelperService.GetDateS(_this._CampaignDetails.EndDate);
                _this._CampaignDetails.CreateDateS = _this._HelperService.GetDateTimeS(_this._CampaignDetails.CreateDate);
                _this._CampaignDetails.ModifyDateS = _this._HelperService.GetDateTimeS(_this._CampaignDetails.ModifyDate);
                _this._CampaignDetails.StatusI = _this._HelperService.GetStatusIcon(_this._CampaignDetails.StatusCode);
                _this._CampaignDetails.StatusB = _this._HelperService.GetStatusBadge(_this._CampaignDetails.StatusCode);
                _this._CampaignDetails.StatusC = _this._HelperService.GetStatusColor(_this._CampaignDetails.StatusCode);
                console.log("=>", _this._CampaignDetails.StatusB);
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],
                // : [null, Validators.compose([Validators.required])],
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
                // : 0,
                // : 0,
                // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(5)])],
                // StartDate: [null, Validators.compose([Validators.required])],
                // EndDate: [null, Validators.compose([Validators.required])],
            }
            else {
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.NotifyError(_Response.Message);
            }
        }, function (_Error) {
            _this._HelperService.IsFormProcessing = false;
            _this._HelperService.HandleException(_Error);
        });
    };
    TuCampaignComponent.prototype.GoTo_Overview = function () {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Dashboard, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId]);
    };
    TuCampaignComponent.prototype.GoTo_Transaction = function () {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Sales, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId]);
    };
    //Form_EditUser: FormGroup;
    TuCampaignComponent.prototype.Form_EditUser_Show = function () {
        this.isPauseCamp = false;
        this.isCloseCamp = false;
        this.isExtendActive = false;
        this.Form_EditUser_Load();
        this._HelperService.OpenModal("Form_EditUser_Content");
    };
    TuCampaignComponent.prototype.Form_EditUser_Close = function () {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.CloseModal("Form_EditUser_Content");
    };
    // Form_EditUser_Load() {
    //   this._HelperService._FileSelect_Icon_Data.Width = 128;
    //   this._HelperService._FileSelect_Icon_Data.Height = 128;
    //   this._HelperService._FileSelect_Poster_Data.Width = 800;
    //   this._HelperService._FileSelect_Poster_Data.Height = 400;
    //   this.Form_EditUser = this._FormBuilder.group({
    //     OperationType: "new",
    //     Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
    //     OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
    //     FirstName: [
    //       null,
    //       Validators.compose([
    //         Validators.required,
    //         Validators.minLength(2),
    //         Validators.maxLength(128)
    //       ])
    //     ],
    //     StatusCode: this._HelperService.AppConfig.Status.Inactive,
    //     Owners: [],
    //     Configuration: []
    //   });
    // }
    TuCampaignComponent.prototype.Form_EditUser_Clear = function () {
        this.Form_EditUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_EditUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    };
    TuCampaignComponent.prototype.Form_EditUser_Load = function () {
        this.Form_EditUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateCampaign,
            ReferenceKey: this._CampaignDetails.ReferenceKey,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            StartDate: [this._CampaignDetails.StartDate, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            EndDate: [this._CampaignDetails.EndDate, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            StatusCode: [this._CampaignDetails.StatusCode, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
        if (this._CampaignDetails.StatusCode == this._HelperService.AppConfig.Status.Campaign.Archived) {
            this.isCloseCamp = false;
            this.ToogleCloseCamp();
        }
        if (this._CampaignDetails.StatusCode == this._HelperService.AppConfig.Status.Campaign.Paused) {
            this.isPauseCamp = false;
            this.TooglePauseCamp();
        }
    };
    TuCampaignComponent.prototype.Form_EditUser_Process = function (_FormValue) {
        var _this = this;
        if (this.isCloseCamp) {
            var pData = {
                Task: "deletecampaign",
                ReferenceKey: this._CampaignDetails.ReferenceKey,
            };
            this._HelperService.IsFormProcessing = true;
            var _OResponse = void 0;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
            _OResponse.subscribe(function (_Response) {
                _this._HelperService.IsFormProcessing = false;
                if (_Response.Status == _this._HelperService.StatusSuccess) {
                    _this._HelperService.NotifySuccess("Campaign deleted successfully");
                    _this._HelperService.CloseModal('Form_EditUser_Content');
                    _this._Router.navigate([_this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
                }
                else {
                    _this._HelperService.NotifyError(_Response.Message);
                }
            }, function (_Error) {
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.HandleException(_Error);
            });
        }
        else if (this.isExtendActive || this.isPauseCamp) {
            if (this.isExtendActive) {
                _FormValue.StatusCode = this._CampaignDetails.StatusCode;
            }
            else {
                _FormValue.StartDate = this._CampaignDetails.StartDate;
                _FormValue.EndDate = this._CampaignDetails.EndDate;
                _FormValue.StatusCode = this._HelperService.AppConfig.Status.Campaign.Paused;
            }
            _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
            this._HelperService.IsFormProcessing = true;
            var _OResponse = void 0;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
            _OResponse.subscribe(function (_Response) {
                _this._HelperService.IsFormProcessing = false;
                if (_Response.Status == _this._HelperService.StatusSuccess) {
                    _this.GetCampaignDetails();
                    _this.Form_EditUser_Close();
                    _this.GetCampaignDetails();
                    _this._HelperService.NotifySuccess("Campaign updated successfully");
                }
                else {
                    _this._HelperService.NotifyError(_Response.Message);
                }
            }, function (_Error) {
                // console.log(_Error);
                _this._HelperService.IsFormProcessing = false;
                _this._HelperService.HandleException(_Error);
            });
        }
        else {
        }
    };
    TuCampaignComponent.prototype.Date_Toogle = function (event) {
        this.Form_EditUser.controls['StartDate'].setValue(event.start);
        this.Form_EditUser.controls['EndDate'].setValue(event.end);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_5__["DaterangePickerComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_5__["DaterangePickerComponent"])
    ], TuCampaignComponent.prototype, "_DaterangePickerComponent", void 0);
    TuCampaignComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "tu-campaign",
            template: __webpack_require__(/*! ./tucampaign.component.html */ "./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_6__["DataHelperService"]])
    ], TuCampaignComponent);
    return TuCampaignComponent;
}());



/***/ }),

/***/ "./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TUCampaignRoutingModule, TUCampaignModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignRoutingModule", function() { return TUCampaignRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUCampaignModule", function() { return TUCampaignModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _tucampaign_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./tucampaign.component */ "./src/app/modules/accountdetails/tucampaign/acquirer/tucampaign.component.ts");
/* harmony import */ var src_app_service_main_pipe_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/service/main-pipe.module */ "./src/app/service/main-pipe.module.ts");













var routes = [
    {
        path: "",
        component: _tucampaign_component__WEBPACK_IMPORTED_MODULE_11__["TuCampaignComponent"],
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/campaign/acquirer/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/campaign/acquirer/dashboard.module#TUDashboardModule" },
            { path: 'sales/saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.SaleHistory' }, loadChildren: '../../../../panel/acquirer/campaigns/sales/tusale.module#TUSaleModule' },
        ]
    }
];
var TUCampaignRoutingModule = /** @class */ (function () {
    function TUCampaignRoutingModule() {
    }
    TUCampaignRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUCampaignRoutingModule);
    return TUCampaignRoutingModule;
}());

var TUCampaignModule = /** @class */ (function () {
    function TUCampaignModule() {
    }
    TUCampaignModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"],
                TUCampaignRoutingModule,
                _agm_core__WEBPACK_IMPORTED_MODULE_10__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
                }),
                src_app_service_main_pipe_module__WEBPACK_IMPORTED_MODULE_12__["MainPipe"]
            ],
            declarations: [_tucampaign_component__WEBPACK_IMPORTED_MODULE_11__["TuCampaignComponent"]]
        })
    ], TUCampaignModule);
    return TUCampaignModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-accountdetails-tucampaign-acquirer-tucampaign-module.js.map