(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["panel-acquirer-accounts-branch-branch-module"],{

/***/ "./src/app/panel/acquirer/accounts/branch/branch.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/panel/acquirer/accounts/branch/branch.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- acquirer--branches--list -->\n<div class=\"card\">\n  <div [className]=\"_HelperService._Assets.Box.header\">\n    <div class=\"tx-14 tx-bold tx-uppercase\"> {{BranchList_Config.Title}} </div>\n    <div [className]=\"_HelperService._Assets.Box.headerTitleRight\">\n      <nav class=\"nav nav-icon-only mg-l-auto\">\n        <button class=\"btn tx-12 btn-primary rounded-50 pd-x-30\" (click)=\"Form_AddUser_Open()\"> New Branch </button>\n      </nav>\n    </div>\n  </div>\n  <div>\n    <div class=\"box-header px-3 pd-t-15 pd-b-10 d-flex align-items-center justify-content-between\">\n      <div class=\"form-inline\">\n        <div class=\"input-group input-group-sm mr-3\">\n          <input type=\"text\" class=\"form-control\" [value]=\"BranchList_Config.SearchParameter\" maxlength=\"100\"\n            class=\"form-control\" placeholder=\"{{ 'Common.Search' | translate }}\"\n            (keyup)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\">\n          <div class=\"input-group-append\">\n            <button class=\"btn btn-outline-light\" type=\"button\"\n              (click)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Search)\"><i\n                data-feather=\"search\"></i> </button>\n          </div>\n        </div>\n\n\n      </div>\n\n      <div class=\"d-flex ml-auto\">\n        <button type=\"button\" class=\"btn  tx-12 rounded-0 bd-gray-200 bd-r-0  btn-outline-danger btn-icon\"\n          (click)=\"Delete_Filter()\">\n          <i data-feather=\"minus-circle\"></i> </button>\n\n        <div class=\"form-group my-auto shadow-none ml-auto \" *ngIf=\"_HelperService._RefreshUI\">\n          <select2 name=\"itemId0\" [options]=\"_DataHelperService.S2_NewSavedFilters_Option\" class=\"wd-30 select2\"\n            [data]=\"_HelperService.Active_FilterOptions\" [value]=\"_HelperService.Active_FilterOptions[0]\"\n            (valueChanged)=\"Active_FilterValueChanged( $event )\">\n          </select2>\n        </div>\n        <button type=\"button\" class=\" btn btn-outline-success tx-12 bd-l-0 mr-3 rounded-0 bd-gray-200 btn-icon\"\n          (click)=\"Save_NewFilter()\"> <i data-feather=\"plus-circle\"></i> </button>\n\n      </div>\n\n      <nav class=\"nav nav-with-icon tx-20\">\n        <div class=\"dropdown dropleft\">\n          <a href=\"\" class=\"nav-link mr-2\" id=\"BranchList_sdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n            aria-expanded=\"false\"> <i data-feather=\"align-left\" class=\"tx-20\"></i> </a>\n          <div class=\"dropdown-menu tx-13\">\n            <form class=\"wd-250 pd-15\">\n              <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Sort\n                <div class=\"float-right tx-20\" *ngIf=\"BranchList_Config.Sort.SortOrder == 'desc'\"\n                  (click)=\"BranchList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                  <i class=\"fas fa-sort-alpha-down-alt\"></i>\n                </div>\n                <div class=\"float-right tx-20\" *ngIf=\"BranchList_Config.Sort.SortOrder == 'asc'\"\n                  (click)=\"BranchList_ToggleOption( null, _HelperService.AppConfig.ListToggleOption.SortOrder)\">\n                  <i class=\"fas fa-sort-alpha-up-alt\"></i>\n                </div>\n              </h6>\n              <a class=\"dropdown-item\" *ngFor=\"let TItem of BranchList_Config.Sort.SortOptions\"\n                (click)=\"BranchList_ToggleOption( TItem, _HelperService.AppConfig.ListToggleOption.Sort)\">\n                {{TItem.Name}}\n              </a>\n              <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                Reset</button>\n              <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n            </form>\n          </div>\n        </div>\n        <div class=\"dropdown dropleft\">\n          <a href=\"\" class=\"nav-link\" id=\"BranchList_fdropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n            aria-expanded=\"false\"> <i data-feather=\"filter\"></i> </a>\n          <div class=\"dropdown-menu tx-13\">\n            <form class=\"wd-250 pd-15\">\n              <h6 class=\"dropdown-header tx-uppercase tx-12 tx-bold tx-inverse pd-b-20 pd-l-0 pd-t-0\">Filters\n              </h6>\n              <div>\n                <div class=\"form-group\">\n                  <label>Date</label>\n                  <input type=\"text\" class=\"form-control  form-daterangepicker\" name=\"daterangeInput\" daterangepicker\n                    [options]=\"_HelperService.AppConfig.DateRangeOptions\"\n                    (selected)=\"BranchList_ToggleOption($event, _HelperService.AppConfig.ListToggleOption.Date)\" />\n                </div>\n              </div>\n              <div>\n                <div class=\"form-group\">\n                  <label>Status</label>\n\n                  <select2 [options]=\"_DataHelperService.S2_Status_Option\" class=\"wd-100p\"\n                    [data]=\"BranchList_Config.StatusOptions\"\n                    (valueChanged)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Status)\">\n                  </select2>\n                </div>\n              </div>\n              <button type=\"button\" class=\"btn btn-light btn-sm mr-2\"\n                (click)=\"ResetFilters( null, _HelperService.AppConfig.ListToggleOption.ResetOffset)\">\n                Reset</button>\n              <button type=\"button\" class=\"btn btn-primary btn-sm \"\n                (click)=\"ApplyFilters( null, _HelperService.AppConfig.ListToggleOption.ApplyFilter)\">Apply</button>\n\n            </form>\n          </div>\n        </div>\n\n        <a class=\"nav-link ml-2 RefreshColor\"\n          (click)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Refresh)\"> <i\n            data-feather=\"refresh-cw\"></i> </a>\n      </nav>\n    </div>\n\n    <div class=\"pt-0 pd-x-15 pd-b-15\">\n      <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"(!_HelperService.FilterSnap.Status) || (_HelperService.FilterSnap.Status==0)\"\n        (click)=\"RemoveFilterComponent('Status')\">\n        {{_HelperService.FilterSnap.StatusName}}\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n            class=\"fas fa-times\"></i> </span>\n      </div>\n      <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"!_HelperService.FilterSnap.Sort.SortColumn || (_HelperService.FilterSnap.Sort.SortColumn=='CreateDate' && _HelperService.FilterSnap.Sort.SortOrder=='desc')\"\n        (click)=\"RemoveFilterComponent('Sort')\">\n        <span>\n          {{_HelperService.FilterSnap.Sort.SortColumn + \"--\" + _HelperService.FilterSnap.Sort.SortOrder}}</span>\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n            class=\"fas fa-times\"></i> </span>\n      </div>\n      <div class=\"badge badge-transparent pd-y-10 pd-x-15 mx-1 bd bd-1 bd-gray-500 rounded-50 tx-color-03\"\n        [hidden]=\"!_HelperService.FilterSnap.StartTime\" (click)=\"RemoveFilterComponent('Time')\">\n        {{_HelperService.GetDateTimeS(_HelperService.FilterSnap.StartTime) + \"--\" + _HelperService.GetDateTimeS(_HelperService.FilterSnap.EndTime)}}\n        <span class=\"bd bd-1  mg-l-5 pd-x-5 pd-y-3 rounded-circle bg-primary text-white cursor-pointer\"><i\n            class=\"fas fa-times\"></i> </span>\n      </div>\n\n      <span class=\"badge badge-pill badge-primary\"\n        *ngFor=\"let TItem of BranchList_Config.Filters\">{{TItem.Title}}</span>\n    </div>\n\n    <div class=\"card-body p-0\">\n      <table class=\"{{_HelperService.AppConfig.TablesConfig.DefaultClass}}\">\n        <thead>\n          <tr>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[0].Value\">\n            </th>\n            <th>\n              Branch ID\n            </th>\n            <th class=\"\" *ngIf=\"ColoumConfig[1].Value\">\n              Branch Details\n            </th>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[2].Value\">\n              City\n            </th>\n            <th class=\"text-left\" *ngIf=\"ColoumConfig[3].Value\">\n              Contact\n            </th>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[4].Value\">\n              Stores\n            </th>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[5].Value\">\n              POS\n            </th>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[6].Value\">\n              ActivePOS\n            </th>\n            <th class=\"text-center\" *ngIf=\"ColoumConfig[7].Value\">\n              Manager\n            </th>\n            <th *ngIf=\"ColoumConfig[8].Value\">\n              Added On\n            </th>\n\n            <th class=\"d-flex\">\n              <span class=\"btn cursor-pointer text-primary p-0\" style=\"position: absolute; z-index: 3;right: -10px;\"\n                data-toggle=\"tooltip\" title=\"Edit Your Column\" (click)=\"EditColConfig()\">\n                <i style=\"height: 20px; width: 20px;\" data-feather=\"plus-circle\"></i>\n              </span>\n            </th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr class=\"cursor-pointer\" (click)=\"BranchList_RowSelected(TItem)\"\n            *ngFor=\"let TItem of BranchList_Config.Data  | paginate: { id: 'BranchList_Paginator',itemsPerPage: BranchList_Config.PageRecordLimit, currentPage: BranchList_Config.ActivePage,  totalItems: BranchList_Config.TotalRecords }\">\n            <td class=\"m-l-auto\" *ngIf=\"ColoumConfig[0].Value\">\n              <span class=\"d-block wd-15 ht-15 rounded-circle ml-auto\" [style.background-color]=\"TItem.StatusC\"></span>\n            </td>\n            <td>\n              <div>{{TItem.BranchCode  || \"--\"}}</div>\n            </td>\n\n\n            <td *ngIf=\"ColoumConfig[1].Value\">\n              <div class=\"tx-14 tx-semibold \">{{TItem.DisplayName  || \"--\"}}</div>\n              <small class=\"tx-12 text-muted WordWrap\">\n                {{TItem.Address | truncateText:40  || \"--\"}}\n              </small>\n            </td>\n\n            <td class=\"text-center\" *ngIf=\"ColoumConfig[2].Value\">\n              {{TItem.CityName || \"--\"}}\n            </td>\n            <td class=\"text-left\" *ngIf=\"ColoumConfig[3].Value\">\n              <div class=\"text-muted  tx-14 tx-verve\">{{TItem.PhoneNumber  || \"--\"}}\n\n              </div>\n              <small class=\"text-muted\">\n                {{TItem.EmailAddress  || \"--\"}}\n              </small>\n\n            </td>\n            <td class=\"text-center\" *ngIf=\"ColoumConfig[4].Value\">\n              {{TItem.Stores}}\n            </td>\n            <td class=\"text-center\" *ngIf=\"ColoumConfig[5].Value\">\n              {{TItem.Terminals}}\n            </td>\n            <td class=\"text-center text-primary\" *ngIf=\"ColoumConfig[6].Value\">\n              {{TItem.ActiveTerminals}}\n            </td>\n            <td class=\"text-center\" *ngIf=\"ColoumConfig[7].Value\">\n              {{TItem.ManagerName  || \"--\"}}\n            </td>\n            <td class=\"text-muted\" *ngIf=\"ColoumConfig[8].Value\">\n              {{TItem.CreateDate  || \"--\"}}\n            </td>\n            <td>\n\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    <div class=\"card-footer pd-20\">\n      <span class=\"mr-3\">\n        {{ 'Common.Showing' | translate }}\n        <span class=\"bold\">{{BranchList_Config.ShowingStart}} {{ 'Common.To' | translate }}\n          {{BranchList_Config.ShowingEnd}}\n        </span> {{ 'Common.Of' | translate }} {{BranchList_Config.TotalRecords}}\n      </span>\n      <span>\n        {{ 'Common.Show' | translate }}\n        <select class=\"wd-40 \" [(ngModel)]=\"BranchList_Config.PageRecordLimit\"\n          (ngModelChange)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Limit)\">\n          <option [ngValue]=\"TItem\" *ngFor=\"let TItem of _HelperService.AppConfig.ListRecordLimit\">\n            {{TItem}}\n          </option>\n        </select>\n      </span>\n      <span class=\"float-right\">\n        <pagination-controls id=\"BranchList_Paginator\" class=\"pagination pagination-space\"\n          (pageChange)=\"BranchList_ToggleOption( $event, _HelperService.AppConfig.ListToggleOption.Page)\"\n          nextLabel=\"{{ 'Common.Next' | translate }}\" previousLabel=\"{{ 'Common.Previous' | translate }}\">\n        </pagination-controls>\n      </span>\n    </div>\n  </div>\n\n</div>\n\n<div class=\"modal\" id=\"EditCol\" tabindex=\"-1\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\">Edit Column</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"form-group\" *ngFor=\"let Col of TempColumnConfig\">\n          <div class=\"form-check\">\n            <input class=\"form-check-input\" type=\"checkbox\" id=\"gridCheck\" [(ngModel)]=Col.Value>\n            <label class=\"form-check-label\" for=\"\">\n              {{Col.Name}}\n            </label>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"SaveColConfig()\">Save</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/panel/acquirer/accounts/branch/branch.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/panel/acquirer/accounts/branch/branch.component.ts ***!
  \********************************************************************/
/*! exports provided: BranchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchComponent", function() { return BranchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! feather-icons */ "./node_modules/feather-icons/dist/feather.js");
/* harmony import */ var feather_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(feather_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/service */ "./src/app/service/service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var BranchComponent = /** @class */ (function () {
    function BranchComponent(_Router, _ActivatedRoute, _FormBuilder, _HelperService, _DataHelperService, _ChangeDetectorRef, _FilterHelperService) {
        this._Router = _Router;
        this._ActivatedRoute = _ActivatedRoute;
        this._FormBuilder = _FormBuilder;
        this._HelperService = _HelperService;
        this._DataHelperService = _DataHelperService;
        this._ChangeDetectorRef = _ChangeDetectorRef;
        this._FilterHelperService = _FilterHelperService;
        this.Show = {};
        //#region ColumnConfig
        this.TempColumnConfig = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "BranchDetails",
                Value: true,
            },
            {
                Name: "City",
                Value: true,
            },
            {
                Name: "Contact",
                Value: true,
            },
            {
                Name: "Stores",
                Value: true,
            },
            {
                Name: "POS",
                Value: true,
            },
            {
                Name: "ActivePOS",
                Value: true,
            },
            {
                Name: "Manager",
                Value: true,
            },
            {
                Name: "Added",
                Value: true,
            },
        ];
        this.ColoumConfig = [
            {
                Name: "Status",
                Value: true,
            },
            {
                Name: "BranchDetails",
                Value: true,
            },
            {
                Name: "City",
                Value: true,
            },
            {
                Name: "Contact",
                Value: true,
            },
            {
                Name: "Stores",
                Value: true,
            },
            {
                Name: "POS",
                Value: true,
            },
            {
                Name: "ActivePOS",
                Value: true,
            },
            {
                Name: "Manager",
                Value: true,
            },
            {
                Name: "Added",
                Value: true,
            },
        ];
        this.BranchList_Filter_Owners_Selected = 0;
        this.Show.ListShow = "none";
        this.Show.IconShow = "block";
    }
    BranchComponent.prototype.ngOnInit = function () {
        feather_icons__WEBPACK_IMPORTED_MODULE_4__["replace"]();
        this.InitColConfig();
        this.BranchList_Setup();
        this.BranchList_Filter_Owners_Load();
    };
    BranchComponent.prototype.Form_AddUser_Open = function () {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AddAccount
                .AddBranch,
        ]);
    };
    BranchComponent.prototype.InitColConfig = function () {
        var temp = this._HelperService.GetStorage("branchtable");
        if (temp != undefined && temp != null) {
            this.ColoumConfig = temp.config;
            this.TempColumnConfig = JSON.parse(JSON.stringify(temp.config));
        }
    };
    BranchComponent.prototype.EditColConfig = function () {
        this._HelperService.OpenModal("EditCol");
    };
    BranchComponent.prototype.SaveColConfig = function () {
        this.ColoumConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
        this._HelperService.SaveStorage("branchtable", {
            config: this.ColoumConfig,
        });
        this._HelperService.CloseModal("EditCol");
    };
    //#endregion
    BranchComponent.prototype.SetOtherFilters = function () {
        this.BranchList_Config.SearchBaseConditions = [];
        this.BranchList_Config.SearchBaseCondition = null;
    };
    BranchComponent.prototype.BranchList_Setup = function () {
        this.BranchList_Config = {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            Title: "Available Branch",
            StatusType: "default",
            // Type: this._HelperService.AppConfig.ListType.SubOwner,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            // DefaultSortExpression: 'CreateDate desc',
            TableFields: [
                {
                    DisplayName: "Merchant Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "City Name",
                    SystemName: "ManagerName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "City Name",
                    SystemName: "CityName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Contact No",
                    SystemName: "PhoneNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Branch id",
                    SystemName: "BranchCode",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Email Address",
                    SystemName: "EmailAddress",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Stores",
                    SystemName: "Stores",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Terminals",
                    SystemName: "Terminals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: "Active Terminals",
                    SystemName: "ActiveTerminals",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
                        .PanelAcquirer.Merchant.Dashboard,
                },
            ],
        };
        this.BranchList_Config = this._DataHelperService.List_Initialize(this.BranchList_Config);
        this._HelperService.Active_FilterInit(this._HelperService.AppConfig.FilterTypeOption.Branch, this.BranchList_Config);
        this.BranchList_GetData();
    };
    BranchComponent.prototype.BranchList_ToggleOption = function (event, Type) {
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.BranchList_Config);
        this.BranchList_Config = this._DataHelperService.List_Operations(this.BranchList_Config, event, Type);
        if ((this.BranchList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)) {
            this.BranchList_GetData();
        }
    };
    BranchComponent.prototype.BranchList_GetData = function (Type) {
        if (Type != null &&
            Type == this._HelperService.AppConfig.ListToggleOption.Search) {
            var TConfig = this._DataHelperService.List_GetData(this.BranchList_Config);
        }
        else {
            var TConfig = this._DataHelperService.List_GetData(this.BranchList_Config, this.Show);
        }
        this.BranchList_Config = TConfig;
    };
    BranchComponent.prototype.BranchList_RowSelected = function (ReferenceData) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveBranch, {
            ReferenceKey: ReferenceData.ReferenceKey,
            ReferenceId: ReferenceData.ReferenceId,
            DisplayName: ReferenceData.DisplayName,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Branch,
        });
        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Branches
                .BranchTerminals,
            ReferenceData.ReferenceKey,
            ReferenceData.ReferenceId,
        ]);
    };
    BranchComponent.prototype.BranchList_Filter_Owners_Load = function () {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray("", "AccountTypeCode", this._HelperService.AppConfig.DataType.Text, [
            this._HelperService.AppConfig.AccountType.Merchant,
            this._HelperService.AppConfig.AccountType.Acquirer,
            this._HelperService.AppConfig.AccountType.PGAccount,
            this._HelperService.AppConfig.AccountType.PosAccount,
        ], "=");
        var _Transport = this._DataHelperService.S2_BuildList(_Select);
        this.BranchList_Filter_Owners_Option = {
            placeholder: "Sort by Referrer",
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    };
    BranchComponent.prototype.BranchList_Filter_Owners_Change = function (event) {
        if (event.value == this.BranchList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "OwnerId", this._HelperService.AppConfig.DataType.Number, this.BranchList_Filter_Owners_Selected, "=");
            this.BranchList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.BranchList_Config.SearchBaseConditions);
            this.BranchList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.BranchList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict("", "OwnerId", this._HelperService.AppConfig.DataType.Number, this.BranchList_Filter_Owners_Selected, "=");
            this.BranchList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.BranchList_Config.SearchBaseConditions);
            this.BranchList_Filter_Owners_Selected = event.value;
            this.BranchList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict("", "OwnerId", this._HelperService.AppConfig.DataType.Number, this.BranchList_Filter_Owners_Selected, "="));
        }
        this.BranchList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    };
    //#endregion
    //#region filterOperations
    BranchComponent.prototype.Active_FilterValueChanged = function (event) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.BranchList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.BranchList_GetData();
    };
    BranchComponent.prototype.Save_NewFilter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._FilterHelperService._BuildFilterName_Store(result.value);
                _this._HelperService.Save_NewFilter(_this._HelperService.AppConfig.FilterTypeOption.Branch);
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    BranchComponent.prototype.Delete_Filter = function () {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default()({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(function (result) {
            if (result.value) {
                _this._HelperService._RefreshUI = false;
                _this._ChangeDetectorRef.detectChanges();
                _this._HelperService.Delete_Filter(_this._HelperService.AppConfig.FilterTypeOption.Branch);
                _this._FilterHelperService.SetStoreConfig(_this.BranchList_Config);
                _this.BranchList_GetData();
                _this._HelperService._RefreshUI = true;
                _this._ChangeDetectorRef.detectChanges();
            }
        });
    };
    BranchComponent.prototype.ApplyFilters = function (event, Type) {
        this._HelperService.MakeFilterSnapPermanent();
        this.BranchList_GetData();
    };
    BranchComponent.prototype.ResetFilters = function (event, Type) {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.BranchList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.BranchList_GetData();
    };
    BranchComponent.prototype.RemoveFilterComponent = function (Type, index) {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.BranchList_Config);
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.BranchList_GetData();
    };
    BranchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-branch",
            template: __webpack_require__(/*! ./branch.component.html */ "./src/app/panel/acquirer/accounts/branch/branch.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["HelperService"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["DataHelperService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _service_service__WEBPACK_IMPORTED_MODULE_5__["FilterHelperService"]])
    ], BranchComponent);
    return BranchComponent;
}());



/***/ }),

/***/ "./src/app/panel/acquirer/accounts/branch/branch.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/panel/acquirer/accounts/branch/branch.module.ts ***!
  \*****************************************************************/
/*! exports provided: TUBranchRoutingModule, BranchModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TUBranchRoutingModule", function() { return TUBranchRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchModule", function() { return BranchModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-select2 */ "./node_modules/ng2-select2/ng2-select2.js");
/* harmony import */ var ng2_select2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_select2__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-daterangepicker */ "./node_modules/ng2-daterangepicker/index.js");
/* harmony import */ var ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ng2_file_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-input */ "./node_modules/ng2-file-input/dist/ng2-file-input.js");
/* harmony import */ var _branch_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./branch.component */ "./src/app/panel/acquirer/accounts/branch/branch.component.ts");
/* harmony import */ var _service_main_pipe_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../service/main-pipe.module */ "./src/app/service/main-pipe.module.ts");











var routes = [{ path: "", component: _branch_component__WEBPACK_IMPORTED_MODULE_10__["BranchComponent"] }];

var TUBranchRoutingModule = /** @class */ (function () {
    function TUBranchRoutingModule() {
    }
    TUBranchRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
        })
    ], TUBranchRoutingModule);
    return TUBranchRoutingModule;
}());

var BranchModule = /** @class */ (function () {
    function BranchModule() {
    }
    BranchModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_branch_component__WEBPACK_IMPORTED_MODULE_10__["BranchComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _service_main_pipe_module__WEBPACK_IMPORTED_MODULE_11__["MainPipe"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                TUBranchRoutingModule,
                ng2_select2__WEBPACK_IMPORTED_MODULE_6__["Select2Module"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                ng2_daterangepicker__WEBPACK_IMPORTED_MODULE_8__["Daterangepicker"],
                ng2_file_input__WEBPACK_IMPORTED_MODULE_9__["Ng2FileInputModule"]
            ]
        })
    ], BranchModule);
    return BranchModule;
}());



/***/ })

}]);
//# sourceMappingURL=panel-acquirer-accounts-branch-branch-module.js.map