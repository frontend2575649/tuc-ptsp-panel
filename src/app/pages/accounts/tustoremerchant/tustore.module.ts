import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';

import { TUStoreComponent } from './tustore.component';
const routes: Routes = [
    {
        path: '', component: TUStoreComponent,
        children: [
            { path: '/:referencekey', data: { 'permission': 'getappuser', 'menuoperations': 'ManageAppUser', 'accounttypecode': 'appuser' }, loadChildren: './dashboard/tustoredashboard.module#TUStoreDashboardModule' },
            { path: 'dashboard/:referencekey', data: { 'permission': 'getappuser', 'menuoperations': 'ManageAppUser', 'accounttypecode': 'appuser' }, loadChildren: './dashboard/tustoredashboard.module#TUStoreDashboardModule' },
            { path: 'updatestore/:referencekey', data: { 'permission': 'getappuser', 'menuoperations': 'ManageAppUser', 'accounttypecode': 'appuser' }, loadChildren: './updatestore/updatestore.module#TUUpdateStoreModule' },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUStoreRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        TUStoreRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUStoreComponent]
})
export class TUStoreModule {

}