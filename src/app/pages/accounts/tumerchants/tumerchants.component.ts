import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DataHelperService, HelperService, OList } from "../../../service/service";

@Component({
  selector: "tu-tumerchants",
  templateUrl: "./tumerchants.component.html"
})
export class TUMerchantsComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this.MerchantsList_Setup();
  }

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id:"",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetAcquirerMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Title: "Merchants",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.SubOwner,
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: "Contact",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: "Email",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: "Stores",
          SystemName: "SubAccounts",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: "Status",
          SystemName: "StatusName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.Merchant.Dashboard
        }
      ]
    };
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );
    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {
    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );
    if (this.MerchantsList_Config.RefreshData == true) {
      this.MerchantsList_GetData();
    }
  }
  MerchantsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }
  MerchantsList_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }
}
