import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../service/service';
import swal from 'sweetalert2';
declare var moment: any;

@Component({
    selector: 'tu-merchantstoresale',
    templateUrl: './tustoresmerchantsale.component.html',
})
export class TUStoresMerchantSaleComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.StoreSalesList_Setup();
    }

    public StoreSalesList_Config: OList;
    StoreSalesList_Setup() {
        this.StoreSalesList_Config =
            {
                Id:""
,                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Store Sales',
                StatusType: 'default',
                Type: this._HelperService.AppConfig.ListType.Owner,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                StartDate: new Date(2017, 0, 1, 0, 0, 0, 0),
                EndDate: moment(),
                TableFields: [
                    {
                        DisplayName: 'Store Name',
                        SystemName: 'DisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'SALE COUNT	',
                        SystemName: 'RewardCount',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Rewards Amount',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'SALE AMOUNT',
                        SystemName: 'RewardPurchaseAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Redeem Count',
                        SystemName: 'RedeemCount',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Redeem Amount',
                        SystemName: 'RedeemAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Conv. Charge',
                        SystemName: 'CommissionAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: ' text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Sale Count',
                        SystemName: 'Transactions',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: 'text-warn text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Sale Amount',
                        SystemName: 'Purchase',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-warn text-right',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',
                        NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                ]
            }
        this.StoreSalesList_Config = this._DataHelperService.List_Initialize(this.StoreSalesList_Config);
        this.StoreSalesList_GetData();
    }
    StoreSalesListDate_ToggleOption(event: any, Type: any) {
        this.StoreSalesList_Config.StartDate = event.start;
        this.StoreSalesList_Config.EndDate = event.end;
        this.StoreSalesList_GetData()
    }
    StoreSalesList_ToggleOption(event: any, Type: any) {
        this.StoreSalesList_Config = this._DataHelperService.List_Operations(this.StoreSalesList_Config, event, Type);
        if (this.StoreSalesList_Config.RefreshData == true) {
            this.StoreSalesList_GetData();
        }
    }
    StoreSalesList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.StoreSalesList_Config);
        this.StoreSalesList_Config = TConfig;
    }
    StoreSalesList_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

}