import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { TUMerchantSubAccountsComponent } from './tumerchantsubaccounts.component';
import { Ng2FileInputModule } from 'ng2-file-input';

const routes: Routes = [
    { path: '', component: TUMerchantSubAccountsComponent }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUMerchantSubAccountsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        TUMerchantSubAccountsRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule
    ],
    declarations: [TUMerchantSubAccountsComponent]
})
export class TUMerchantSubAccountsModule {

}