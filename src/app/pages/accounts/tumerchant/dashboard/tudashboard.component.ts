import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
declare var moment: any;
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  OOverview
} from "../../../../service/service";
import { DaterangePickerComponent } from "ng2-daterangepicker";

@Component({
  selector: "tu-merchantdashboard",
  templateUrl: "./tudashboard.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TUMerchantDashboardComponent implements OnInit {
  @ViewChild(DaterangePickerComponent)
  private picker: DaterangePickerComponent;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) {}

  Type = 5;
  StartTime = null;
  EndTime = null;
  CustomType = 1;
  StartTimeCustom = null;
  EndTimeCustom = null;

  ngOnInit() {
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        if (this.StartTime == undefined) {
          this.StartTime = moment().startOf("month");
          this.EndTime = moment().endOf("month");
        }

        if (this.StartTimeCustom == undefined) {
          this.StartTimeCustom = moment().startOf("day");
          this.EndTimeCustom = moment().endOf(1, "day");
        }
        this._HelperService.Get_UserAccountDetails(true);
        this.LoadData();
      }
    });
  }

  UserAnalytics_DateChange(Type) {
    this.Type = Type;
    var SDate;
    if (Type == 1) {
      SDate = {
        start: moment().startOf("day"),
        end: moment().endOf("day")
      };
    } else if (Type == 2) {
      SDate = {
        start: moment()
          .subtract(1, "days")
          .startOf("day"),
        end: moment()
          .subtract(1, "days")
          .endOf("day")
      };
    } else if (Type == 3) {
      SDate = {
        start: moment().startOf("isoWeek"),
        end: moment().endOf("isoWeek")
      };
    } else if (Type == 4) {
      SDate = {
        start: moment()
          .subtract(1, "weeks")
          .startOf("isoWeek"),
        end: moment()
          .subtract(1, "weeks")
          .endOf("isoWeek")
      };
    } else if (Type == 5) {
      SDate = {
        start: moment().startOf("month"),
        end: moment().endOf("month")
      };
    } else if (Type == 6) {
      SDate = {
        start: moment()
          .startOf("month")
          .subtract(1, "month"),
        end: moment()
          .startOf("month")
          .subtract(1, "days")
      };
    } else if (Type == 7) {
      SDate = {
        start: new Date(2017, 0, 1, 0, 0, 0, 0),
        end: moment().endOf("day")
      };
    }
    if (this.picker.datePicker != undefined) {
      this.picker.datePicker.setStartDate(SDate.start);
      this.picker.datePicker.setEndDate(SDate.end);
    }
    this.StartTime = SDate.start;
    this.EndTime = SDate.end;
    this.LoadData();
  }
  LoadData() {
    this._HelperService.GetAccountOverviewMain(
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveOwnerKey,
      this.StartTime,
      this.EndTime
    );
    this._HelperService.GetRewardTypeOverview(
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveOwnerKey,
      this.StartTime,
      this.EndTime
    );
  }
}

export class OAccountOverview {
  public AppUsers: number;
  public RepeatingAppUsers: number;
  public UniqueAppUsers: number;
  public RewardChargeAmount: number;
  public TUCPlusRewardChargeAmount: number;
  public Transactions: number;
  public PurchaseAmount: number;
  public IssuerCommissionAmount: number;
  public CashRewardPurchaseAmount: number;
  public CardRewardPurchaseAmount: number;
  public OtherRewardPurchaseAmount: number;
  public RedeemAmount: number;
  public RewardAmount: number;
  public TUCPlusRewardAmount: number;
  public TUCPlusRewardClaimedAmount: number;
  public PosOverview: any[];
}
export class OAppUsersOverview {
  public AppUsers: number;
  public OwnAppUsers: number;
  public ReferralAppUsers: number;

  public RepeatingAppUsers: number;
  public UniqueAppUsers: number;

  public AppUsersMale: number;
  public AppUsersFemale: number;
  public AppUsersOther: number;
  public GenderLabel: any[];
  public GenderData: any[];

  public AppUsersByAge: any[];
  public AppUsersByAgeLabel: any[];
  public AppUsersByAgeData: any[];

  public AppUsersPurchaseByAge: any[];
  public AppUsersPurchaseByAgeLabel: any[];
  public AppUsersPurchaseByAgeData: any[];

  public AppUsersVisitByAgeLabel: any[];
  public AppUsersVisitByAgeData: any[];
}
