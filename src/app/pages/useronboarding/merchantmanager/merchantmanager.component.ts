import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import swal from 'sweetalert2';

@Component({
    selector: 'hc-merchantmanager',
    templateUrl: './merchantmanager.component.html',
})
export class TUMerchantManagerComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            }
            else {
                this._HelperService.Get_UserAccountDetails(true);
            }
        });
    }
    ngOnInit() {
        this.GetPosAccountsList();
        this.F_AddTerminal_Load();
        this.F_AddMerchant_Load();
        this.F_AddStore_Load();
        this.GetAcquirersList();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this.GetStoresList();
            this.StoresList_Setup();
            this.PosTerminalsList_Setup();
            // if (this._CoreDataHelper.OCoreParameter.ReferenceKey == null) {
            //     this._Router.navigate([this._DataStoreService.PageLinks.Core.NotFound]);
            // }
            // else {
            //     this.Get_Details();
            // } 
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }



    public GetAcquirersOption: Select2Options;
    GetAcquirersList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAcquirersOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAcquirersListChange(event: any) {
        this.F_AddMerchant.patchValue(
            {
                OwnerKey: event.value
            }
        );
    }

    F_AddMerchant: FormGroup;
    F_AddMerchant_Address: string = null;
    F_AddMerchant_Latitude: number = 0;
    F_AddMerchant_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    F_AddMerchant_PlaceMarkerClick(event) {
        this.F_AddMerchant_Latitude = event.coords.lat;
        this.F_AddMerchant_Longitude = event.coords.lng;
    }
    public F_AddMerchant_AddressChange(address: Address) {
        this.F_AddMerchant_Latitude = address.geometry.location.lat();
        this.F_AddMerchant_Longitude = address.geometry.location.lng();
        this.F_AddMerchant_Address = address.formatted_address;
    }
    F_AddMerchant_Show() {
        // this._HelperService.OpenModal('F_AddMerchant_Content');
    }
    F_AddMerchant_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Merchants]);
        // this._HelperService.OpenModal('F_AddMerchant_Content');
    }
    F_AddMerchant_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.F_AddMerchant = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            Address: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(256)])],
            // RewardPercentage: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],
            Latitude: 0,
            Longitude: 0,
            RegionKey: '',
            RegionAreaKey: '',
            CityKey: '',
            CityAreaKey: '',
            PostalCodeKey: '',
            CountValue: 0,
            AverageValue: 0,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Configuration: [],
        });
    }
    F_AddMerchant_Clear() {
        this.F_AddMerchant_Latitude = 0;
        this.F_AddMerchant_Longitude = 0;
        this.F_AddMerchant.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.F_AddMerchant_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    F_AddMerchant_Process(_FormValue: any) {
        // if (_FormValue.RewardPercentage == undefined) {
        //     this._HelperService.NotifyError('Enter reward percentage');
        // }
        // else if (isNaN(_FormValue.RewardPercentage) == true) {
        //     this._HelperService.NotifyError('Enter valid reward percentage');
        // }
        // else if (parseFloat(_FormValue.RewardPercentage) > 100) {
        //     this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        // }
        // else if (parseFloat(_FormValue.RewardPercentage) < 0) {
        //     this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        // }
        // else {

        // }
        swal({
            position: 'top',
            title: 'Update merchant details ?',
            text: 'Please verify information and click on continue button to create merchant',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            // confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                var Config =
                {
                    SystemName: 'rewardpercentage',
                    Value: _FormValue.RewardPercentage,
                }
                _FormValue.ReferenceKey = this._HelperService._UserAccount.ReferenceKey;
                _FormValue.Configurations = [Config];
                // _FormValue.DisplayName = _FormValue.FirstName;
                // _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Merchant details updated successfully');
                            if (_FormValue.OperationType == 'close') {
                                this.F_AddMerchant_Close();
                            }
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }

    F_AddStore: FormGroup;
    F_AddStore_Address: string = null;
    F_AddStore_Latitude: number = 0;
    F_AddStore_Longitude: number = 0;
    F_AddStore_PlaceMarkerClick(event) {
        this.F_AddStore_Latitude = event.coords.lat;
        this.F_AddStore_Longitude = event.coords.lng;
    }
    public F_AddStore_AddressChange(address: Address) {
        this.F_AddStore_Latitude = address.geometry.location.lat();
        this.F_AddStore_Longitude = address.geometry.location.lng();
        this.F_AddStore_Address = address.formatted_address;
    }
    F_AddStore_Show() {
        // this._HelperService.OpenModal('F_AddStore_Content');
    }
    F_AddStore_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Stores]);
        // this._HelperService.OpenModal('F_AddStore_Content');
    }
    F_AddStore_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;
        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.F_AddStore = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: this._HelperService.UserAccount.AccountKey,
            // UserName: this._HelperService.GenerateGuid(),
            // Password: this._HelperService.RandomPassword,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            // Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            // EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            Latitude: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
            Longitude: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Owners: [],
        });
    }
    F_AddStore_Clear() {
        this.F_AddStore.reset();
        this.F_AddStore_Load();
        this.F_AddStore_Latitude = 0;
        this.F_AddStore_Longitude = 0;
        this.F_AddStore_Address = "";
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();

        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    F_AddStore_Process(_FormValue: any) {
        _FormValue.OwnerKey = this._HelperService._UserAccount.ReferenceKey;
        // _FormValue.DisplayName = _FormValue.FirstName;
        // _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Store added');
                    this.F_AddStore_Clear();
                    this.F_AddStore_Load();
                    this.StoresList_Setup();

                    if (_FormValue.OperationType == 'edit') {
                        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Store.Dashboard, _Response.Result.ReferenceKey]);
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.F_AddStore_Close();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    public StoresList_Config: OList;
    StoresList_Setup() {
        this.StoresList_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Merchants',
                StatusType: 'default',
                Type: this._HelperService.AppConfig.ListType.Owner,
                ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                // Status: ['default.active'],
                TableFields: [
                    {
                        DisplayName: 'Name',
                        SystemName: 'DisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Address',
                        SystemName: 'Address',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },

                    // {
                    //     DisplayName: 'Visits',
                    //     SystemName: 'Transactions',
                    //     DataType: this._HelperService.AppConfig.DataType.Number,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     NavigateField: 'ReferenceKey',
                    //     NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    // },
                    // {
                    //     DisplayName: 'Purchase',
                    //     SystemName: 'Purchase',
                    //     DataType: this._HelperService.AppConfig.DataType.Decimal,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     NavigateField: 'ReferenceKey',
                    //     NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    // },
                    {
                        DisplayName: 'Terminals',
                        SystemName: 'Terminals',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                    {
                        DisplayName: 'Added On',
                        SystemName: 'CreateDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
                    },
                ]
            }
        this.StoresList_Config = this._DataHelperService.List_Initialize(this.StoresList_Config);
        this.StoresList_GetData();
    }
    StoresList_ToggleOption(event: any, Type: any) {
        this.StoresList_Config = this._DataHelperService.List_Operations(this.StoresList_Config, event, Type);
        if (this.StoresList_Config.RefreshData == true) {
            this.StoresList_GetData();
        }
    }
    StoresList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
        this.StoresList_Config = TConfig;
    }
    StoresList_RowSelected(ReferenceData) {
        (ReferenceData);
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard + '/' + ReferenceData.ReferenceKey]);

    }


    public GetPosAccountsOption: Select2Options;
    GetPosAccountsList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetPosAccountsOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetPosAccountsListChange(event: any) {
        this.PosAccountKey = event.value;
        this.F_AddTerminal.patchValue(
            {
                OwnerKey: event.value
            }
        );
    }

    public StoreKey: string = null;
    public PosAccountKey: string = null;
    F_AddTerminal: FormGroup;
    F_AddTerminal_Address: string = null;
    F_AddTerminal_Latitude: number = 0;
    F_AddTerminal_Longitude: number = 0;
    public GetStoresOption: Select2Options;
    GetStoresList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                }, {
                    SystemName: 'Address',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Store,
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetStoresOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetStoresListChange(event: any) {
        this.StoreKey = event.value;
        this.F_AddTerminal.patchValue(
            {
                StoreKey: event.value
            }
        );
    }
    F_AddTerminal_PlaceMarkerClick(event) {
        this.F_AddTerminal_Latitude = event.coords.lat;
        this.F_AddTerminal_Longitude = event.coords.lng;
    }
    public F_AddTerminal_AddressChange(address: Address) {
        this.F_AddTerminal_Latitude = address.geometry.location.lat();
        this.F_AddTerminal_Longitude = address.geometry.location.lng();
        this.F_AddTerminal_Address = address.formatted_address;
    }
    F_AddTerminal_Show() {
        // this._HelperService.OpenModal('F_AddTerminal_Content');
    }
    F_AddTerminal_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PosTerminals]);
        // this._HelperService.OpenModal('F_AddTerminal_Content');
    }
    F_AddTerminal_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.F_AddTerminal = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: [null, Validators.required],
            // MerchantKey: [null, Validators.required],
            // AcquirerKey: [null, Validators.required],
            StoreKey: [null, Validators.required],
            UserName: this._HelperService.GenerateGuid(),
            Password: this._HelperService.RandomPassword,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Owners: [],
        });
    }
    F_AddTerminal_Clear() {
        this.F_AddTerminal.reset();
        this.F_AddTerminal_Load();
        this.F_AddTerminal_Latitude = 0;
        this.F_AddTerminal_Longitude = 0;
        this.F_AddTerminal_Address = "";
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();

        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    F_AddTerminal_Process(_FormValue: any) {
        var OwnersList = [
            {
                OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            },
            {
                OwnerKey: this._HelperService.AppConfig.ActiveReferenceKey,
            },
            {
                OwnerKey: this.StoreKey,
            }
        ];
        _FormValue.Owners = OwnersList;
        _FormValue.Name = _FormValue.DisplayName;
        _FormValue.UserName = _FormValue.DisplayName;
        // _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('New terminal added');
                    this.F_AddTerminal_Clear();
                    this.PosTerminalsList_Setup();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }




    public PosTerminalsList_Config: OList;
    PosTerminalsList_Setup() {
        this.PosTerminalsList_Config =
            {
                Id:"",
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetPosTerminals,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Terminals',
                StatusType: 'default',
                Type: this._HelperService.AppConfig.ListType.SubOwner,
                ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                TableFields: [
                    {
                        DisplayName: 'TID',
                        SystemName: 'DisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    },

                    // {
                    //     DisplayName: 'Merchant',
                    //     SystemName: 'MerchantDisplayName',
                    //     DataType: this._HelperService.AppConfig.DataType.Text,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     // NavigateField: 'MerchantKey',
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.Merchant.Dashboard
                    // },
                    {
                        DisplayName: 'Store',
                        SystemName: 'StoreName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    },
                    {
                        DisplayName: 'Address',
                        SystemName: 'Address',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    },
                    // {
                    //     DisplayName: 'Bank',
                    //     SystemName: 'AcquirerDisplayName',
                    //     DataType: this._HelperService.AppConfig.DataType.Text,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    // },
                    // {
                    //     DisplayName: 'Trans',
                    //     SystemName: 'Transactions',
                    //     DataType: this._HelperService.AppConfig.DataType.Number,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    // },
                    // {
                    //     DisplayName: 'Last Transaction',
                    //     SystemName: 'LastTransactionDate',
                    //     DataType: this._HelperService.AppConfig.DataType.Date,
                    //     Class: 'td-date',
                    //     Show: true,
                    //     Search: false,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    // },
                    {
                        DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                        SystemName: 'CreateDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                    }
                ]
            }
        this.PosTerminalsList_Config = this._DataHelperService.List_Initialize(this.PosTerminalsList_Config);
        this.PosTerminalsList_GetData();
    }
    PosTerminalsList_ToggleOption(event: any, Type: any) {
        this.PosTerminalsList_Config = this._DataHelperService.List_Operations(this.PosTerminalsList_Config, event, Type);
        if (this.PosTerminalsList_Config.RefreshData == true) {
            this.PosTerminalsList_GetData();
        }
    }
    PosTerminalsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.PosTerminalsList_Config);
        this.PosTerminalsList_Config = TConfig;
    }
    PosTerminalsList_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }
}