import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails } from '../../../service/service';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';

declare var $: any;

@Component({
    selector: 'hc-merchantonboarding',
    templateUrl: './merchantonboarding.component.html',
})
export class TUMerchantOnboardingComponent implements OnInit {

    public _Address: HCoreXAddress = {};
    public _AddressStore: HCoreXAddress = {};
    AddressChange(Address) {
        this._Address = Address;
        this._AddressStore = Address;
    }
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef

    ) {
        this._ContactInfo = true;
        this._StoreInfoCopy = false;
    }

    ngOnInit() {
        //#region UIInitialize 

        Feather.replace();
        $('#wizard').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            enablePagination: false,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',

            onFinished: (event, currentIndex) => {
            },
            labels: {
                next: "Save",
            }
        });

        //#endregion

        //#region FormsInit 

        this.Form_AddMerchant_Load();
        this.Form_AddStore_Load();
        this.GetAcquirersList();

        //#endregion

    }

    //#region Stage01_General 

    public MerchantSaveRequest: any;
    _SavedMerchant: any = {};

    Form_AddMerchant: FormGroup;

    //#region Stage01_MapOperations 

    _CurrentAddress: any = {};
    Form_AddMerchant_Address: string = null;
    Form_AddMerchant_Latitude: number = 0;
    Form_AddMerchant_Longitude: number = 0;

    public Form_AddMerchant_AddressChange(address: Address) {
        this.Form_AddMerchant_Latitude = address.geometry.location.lat();
        this.Form_AddMerchant_Longitude = address.geometry.location.lng();
        this.Form_AddMerchant_Address = address.formatted_address;
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        this.Form_AddMerchant.controls['Address'].setValue(address.formatted_address);


        if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.reset();

        }
        else {
            this.Form_AddMerchant.controls['Address'].setValue(address.formatted_address);
            this.Form_AddMerchant.controls['MapAddress'].setValue(address.formatted_address);
        }


    }


    reset() {
        this.Form_AddMerchant.controls['Address'].reset()
        this.Form_AddMerchant.controls['CityName'].reset()
        this.Form_AddMerchant.controls['StateName'].reset()
        this.Form_AddMerchant.controls['CountryName'].reset()
    }

    Form_AddMerchant_PlaceMarkerClick(event) {
        this.Form_AddMerchant_Latitude = event.coords.lat;
        this.Form_AddMerchant_Longitude = event.coords.lng;
    }

    //#endregion

    Form_AddMerchant_Clear() {
        this.Form_AddMerchant_Latitude = 0;
        this.Form_AddMerchant_Longitude = 0;
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    Form_AddMerchant_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddMerchant = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveMerchant,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            BusinessOwnerName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            CEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])],

            StatusCode: this._HelperService.AppConfig.Status.Active,

            // Latitude: 0,
            // Longitude: 0,
            // Address: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(256)])],
            Address: [null],
            // CityName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            // StateName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            // CountryName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],


        });

    }
    AddressBind: any;

    Form_AddMerchant_Process(value?: any) {
        var _FormValue = this.Form_AddMerchant.value;
        _FormValue.Latitude = this.Form_AddMerchant_Latitude;
        _FormValue.Longitude = this.Form_AddMerchant_Longitude;
        if (_FormValue.RewardPercentage == undefined) {
            this._HelperService.NotifyError('Enter reward percentage');
        }
        else if (isNaN(_FormValue.RewardPercentage) == true) {
            this._HelperService.NotifyError('Enter valid reward percentage');
        }
        else if (parseFloat(_FormValue.RewardPercentage) > 100) {
            this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        }
        else if (parseFloat(_FormValue.RewardPercentage) < 0) {
            this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        }
        else if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError("Please set your registered address");
        }
        else {
            swal({
                position: 'top',
                title: 'Create merchant account ?',
                text: 'Please verify information and click on continue button to create merchant',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    var Config =
                    {
                        SystemName: 'rewardpercentage',
                        Value: _FormValue.RewardPercentage,
                    }
                    _FormValue.Configurations = [Config];
                    this.MerchantSaveRequest = this.ReFormat_RequestBody();
                    this.AddressBind = this._Address;
                    this._AddressStore = this.AddressBind;
                    this.MerchantSaveRequest.Address = this._Address.Address;
                    // this._AddressStore = this.MerchantSaveRequest.Address;
                    //#region API_Call 
                    // this._HelperService.IsFormProcessing = true;
                    // let _OResponse: Observable<OResponse>;
                    // _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, this.MerchantSaveRequest);
                    // _OResponse.subscribe(
                    //     _Response => {
                    //         this._HelperService.IsFormProcessing = false;
                    //         if (_Response.Status == this._HelperService.StatusSuccess) {

                    //             this._SavedMerchant = _Response.Result;

                    //             this._HelperService.NotifySuccess(_Response.Message);
                    //             this._HelperService.CloseModal('form_AddProduct_Content');
                    //             $("#wizard").steps("next", {});
                    //             // this._StoreInfoCopy = true;
                    //             this._ToogleStoreCopy();
                    //             //this.Form_AddStore.controls['ReferenceKey'].setValue(this._SavedMerchant.ReferenceKey)

                    //         }
                    //         else {
                    //             this._HelperService.NotifyError(_Response.Message);
                    //         }
                    //     },
                    //     _Error => {
                    //         this._HelperService.IsFormProcessing = false;
                    //         this._HelperService.HandleException(_Error);
                    //     });

                    //#endregion

                    this._HelperService.NotifySuccess("General Info Validated.");
                    this._HelperService.CloseModal('form_AddProduct_Content');
                    $("#wizard").steps("next", {});
                    this._ToogleStoreCopy();
                }
            });
        }
    }

    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddMerchant.value;
        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            AccountId: formValue.AccountId,
            AccountKey: formValue.AccountKey,

            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            FirstName: formValue.FirstName,
            BusinessOwnerName: formValue.BusinessOwnerName,
            MobileNumber: formValue.MobileNumber,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,

            // 
            Address: this._Address.Address,
            AddressComponent: this._Address,
            // 

            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
                EmailAddress: formValue.EmailAddress
            },
            Stores: [],
            StatusCode: formValue.StatusCode

        };

        return formRequest;

    }






    public _ContactInfo: boolean;
    _ToogleContactInfo() {
        this._ContactInfo = !this._ContactInfo;

        if (!this._ContactInfo) {
            this.Form_AddMerchant.controls['FirstName'].patchValue(this.Form_AddMerchant.controls['Name'].value);
            this.Form_AddMerchant.controls['MobileNumber'].patchValue(this.Form_AddMerchant.controls['ContactNumber'].value);
            this.Form_AddMerchant.controls['CEmailAddress'].patchValue(this.Form_AddMerchant.controls['EmailAddress'].value);
        } else {
            this.Form_AddMerchant.controls['FirstName'].patchValue(null);
            this.Form_AddMerchant.controls['MobileNumber'].patchValue(null);
            this.Form_AddMerchant.controls['CEmailAddress'].patchValue(null);
        }
    }

    //#endregion

    //#region Stage02_Store 

    //#region Stage02_MapOperations 

    Form_AddStore_Address: string = null;
    Form_AddStore_Latitude: number = 0;
    Form_AddStore_Longitude: number = 0;

    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddStore_PlaceMarkerClick(event) {
        this.Form_AddStore_Latitude = event.coords.lat;
        this.Form_AddStore_Longitude = event.coords.lng;
    }
    public Form_AddStore_AddressChange(address: Address) {
        this.Form_AddStore_Latitude = address.geometry.location.lat();
        this.Form_AddStore_Longitude = address.geometry.location.lng();
        this.Form_AddStore_Address = address.formatted_address;
        this.Form_AddStore.controls['Address'].setValue(address.formatted_address);
        this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
        this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);

    }
    //#endregion

    Form_AddStore: FormGroup;
    _SavedStore: any = {};

    Form_AddStore_Clear() {
        this.Form_AddStore_Latitude = 0;
        this.Form_AddStore_Longitude = 0;
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddStore_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;
        var OwnerKey = this._HelperService.AppConfig.ActiveOwnerKey;
        this.Form_AddStore = this._FormBuilder.group({
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(40)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            detailstoo: [],
            detailsstore: [],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],

            CountValue: 0,
            AverageValue: 0,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Configuration: [],

            Address: [null],
            // Address: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(256)])],
            Latitude: 0,
            Longitude: 0,
        });

    }

    AddressChangeStore(Item) {
        this._AddressStore = Item;
    }

    RegisterNewStore(value?: any) {
        if (this._AddressStore.Address == undefined || this._AddressStore.Address == null || this._AddressStore.Address == "") {
            this._HelperService.NotifyError("Please enter store location")
        }
        else {
            var _FormValue = this.Form_AddStore.value;
            _FormValue.Name = _FormValue.DisplayName;
            // this.MerchantSaveRequest.Stores = [this.ReFormat_RequestBodyStore(_FormValue)];
            _FormValue.Address = this._AddressStore.Address,
                _FormValue.AddressComponent = this._AddressStore,

                this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, this.MerchantSaveRequest);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._SavedStore = _Response.Result;
                        this._SavedMerchant = _Response.Result;
                        this._HelperService.NotifySuccess('Store added');
                        this._HelperService.CloseModal('form_AddStore_Content');
                        $("#wizard").steps("next", {});
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });

        }
    }

    ReFormat_RequestBodyStore(dat: any): void {
        var formValue: any = dat;
        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            AccountId: formValue.AccountId,
            AccountKey: formValue.AccountKey,

            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            FirstName: formValue.FirstName,
            BusinessOwnerName: formValue.BusinessOwnerName,
            MobileNumber: formValue.MobileNumber,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,

            Address: {
                StreetAddress: formValue.Address,
                Address: formValue.Address,
                Latitude: formValue.Latitude,
                Longitude: formValue.Longitude,
                CityName: formValue.CityName,
                StateName: formValue.StateName,
                CountryName: formValue.CountryName,
                MapAddress: formValue.Address
            },
            ContactPerson: {
                FirstName: formValue.FirstName,
                LastName: formValue.LastName,
                MobileNumber: formValue.MobileNumber,
                EmailAddress: formValue.EmailAddress
            },
            Stores: [],
            StatusCode: formValue.StatusCode

        };

        return formRequest;

    }

    public _StoreInfoCopy: boolean;
    _ToogleStoreCopy(): void {
        this._StoreInfoCopy = !this._StoreInfoCopy;

        // if (this._StoreInfoCopy) {
        if (true) {
            this.Form_AddStore.controls['MobileNumber'].setValue(this.Form_AddMerchant.controls['MobileNumber'].value);
            this.Form_AddStore.controls['DisplayName'].setValue(this.Form_AddMerchant.controls['DisplayName'].value);
            this.Form_AddStore.controls['EmailAddress'].setValue(this.Form_AddMerchant.controls['EmailAddress'].value);
            this.Form_AddStore.controls['Address'].setValue(this.Form_AddMerchant.controls['Address'].value);
            this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddMerchant.controls['Latitude'].value);
            this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddMerchant.controls['Longitude'].value);
            this.Form_AddStore_Latitude = this.Form_AddMerchant.controls['Latitude'].value;
            this.Form_AddStore_Longitude = this.Form_AddMerchant.controls['Longitude'].value;
            this.Form_AddStore_Address = this.Form_AddMerchant.controls['Address'].value;
        } else {
            // this.Form_AddStore.controls['MobileNumber'].setValue(null);
            // this.Form_AddStore.controls['DisplayName'].setValue(null);
            // this.Form_AddStore.controls['EmailAddress'].setValue(null);
            // this.Form_AddStore.controls['Address'].setValue(null);
            // this.Form_AddStore.controls['Latitude'].setValue(null);
            // this.Form_AddStore.controls['Longitude'].setValue(null);

            // this.Form_AddStore_Latitude = 0;
            // this.Form_AddStore_Longitude = 0;
            // this.Form_AddStore_Address = null;

        }
    }

    //#endregion

    //#region Stage03_Terminals 


    //#region Dropdown_Acquirer 

    public _AcquirerKey: string = '';
    public _AcquirerId: number = 0;

    public GetAcquirersOption: Select2Options;
    public GetAcquirersOption_Selected = 0;
    GetAcquirersList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                },
            ],
        };
        // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAcquirersOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAcquirersListChange(event: any) {
        if (event.value != this.GetAcquirersOption_Selected) {
            this._ChangeDetectorRef.detectChanges();
            this.Form_AddTerminal.controls['AquirerName'].patchValue(event.data[0].text);
            this._AcquirerId = event.data[0].ReferenceId;
            this._AcquirerKey = event.data[0].ReferenceKey;
            this.GetAcquirersList();
            this._ChangeDetectorRef.detectChanges();
        }
    }

    //#endregion

    //#region Add/Remove TerminalToList 

    Form_AddTerminal = this._FormBuilder.group({
        TerminalId: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
        AquirerName: [null],

    });

    AddTerminalToList(): void {

        var terminal: any = {
            OperationType: 'edit',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
            AccountOperationTypeCode: this._HelperService.AppConfig
                .AccountOperationType.Online,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource
                .System,
            OwnerId: this._HelperService.UserAccount.AccountId,
            OwnerKey: this._HelperService.UserAccount.AccountKey,

            DisplayName: this.Form_AddTerminal.controls['TerminalId'].value,
            TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
            MerchantKey: this._SavedMerchant.ReferenceKey,
            SubOwnerKey: this._SavedMerchant.ReferenceKey,
            BankKey: this._AcquirerKey,
            StatusCode: "default.active",
            AquirerName: this.Form_AddTerminal.controls['AquirerName'].value,
            Owners: [{
                OwnerKey: this._HelperService.UserAccount.AccountKey,
            }],

        };

        var ElementIndex: number = null;

        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].TerminalId == terminal.TerminalId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex == null) {
            // this._SelectedTerminal.element = undefined;
            this._TerminalList.push(terminal);
        }

    }

    RemoveTerminal(ReferenceId: number): void {

        var ElementIndex: number = null;
        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].ReferenceId == ReferenceId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex != null) {
            this._TerminalList.splice(ElementIndex, 1);
        }

    }

    //#endregion

    public _TerminalList: any[] = [];
    AddTerminal_Process() {

        this._HelperService.terminalsavecount = 0;

        for (let index = 0; index < this._TerminalList.length; index++) {
            this._HelperService.IsFormProcessing = true;
            var _FormValue = this._TerminalList[index];
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(
                this._HelperService.AppConfig.NetworkLocation.V2.System,
                _FormValue
            );
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {

                        this._HelperService.terminalsavecount += 1;
                        // console.log('terminals count');
                        // console.log(this._HelperService.terminalsavecount);

                        this.Form_AddMerchant_Clear();
                        this.Form_AddStore_Clear()
                        this.Form_AddTerminal.controls['TerminalId'].setValue(null);

                        if (this._HelperService.terminalsavecount == this._TerminalList.length) {
                            this.ShowSuccess();
                        }
                        // if (_FormValue.OperationType == "close") {
                        //     this.Form_AddUser_Close();
                        // }
                    } else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    //#endregion

    //#region NavigationsAndModals 

    ShowSuccess() {
        this._HelperService.OpenModal('form_Success_Content');
    }
    Form_AddMerchant_Show() {
        this._HelperService.OpenModal('form_AddProduct_Content');
    }
    Form_AddStore_Show() {
        this._HelperService.OpenModal('form_AddStore_Content');
    }

    Form_AddMerchant_Close() {

        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ReferredMerchants]);
    }
    navigatetoMerchant() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Merchants]);
    }

    //#endregion

    public _UserAccount: OUserDetails = {
        ContactNumber: null,
        SecondaryEmailAddress: null,
        ReferenceId: null,
        BankDisplayName: null,
        BankKey: null,
        SubOwnerAddress: null,
        SubOwnerLatitude: null,
        SubOwnerDisplayName: null,
        SubOwnerKey: null,
        SubOwnerLongitude: null,
        AccessPin: null,
        LastLoginDateS: null,
        AppKey: null,
        AppName: null,
        AppVersionKey: null,
        CreateDate: null,
        CreateDateS: null,
        CreatedByDisplayName: null,
        CreatedByIconUrl: null,
        CreatedByKey: null,
        Description: null,
        IconUrl: null,
        ModifyByDisplayName: null,
        ModifyByIconUrl: null,
        ModifyByKey: null,
        ModifyDate: null,
        ModifyDateS: null,
        PosterUrl: null,
        ReferenceKey: null,
        StatusCode: null,
        StatusI: null,
        StatusId: null,
        StatusName: null,
        AccountCode: null,
        AccountOperationTypeCode: null,
        AccountOperationTypeName: null,
        AccountTypeCode: null,
        AccountTypeName: null,
        Address: null,
        AppVersionName: null,
        ApplicationStatusCode: null,
        ApplicationStatusName: null,
        AverageValue: null,
        CityAreaKey: null,
        CityAreaName: null,
        CityKey: null,
        CityName: null,
        CountValue: null,
        CountryKey: null,
        CountryName: null,
        DateOfBirth: null,
        DisplayName: null,
        EmailAddress: null,
        EmailVerificationStatus: null,
        EmailVerificationStatusDate: null,
        FirstName: null,
        GenderCode: null,
        GenderName: null,
        LastLoginDate: null,
        LastName: null,
        Latitude: null,
        Longitude: null,
        MobileNumber: null,
        Name: null,
        NumberVerificationStatus: null,
        NumberVerificationStatusDate: null,
        OwnerDisplayName: null,
        OwnerKey: null,
        Password: null,
        Reference: null,
        ReferralCode: null,
        ReferralUrl: null,
        RegionAreaKey: null,
        RegionAreaName: null,
        RegionKey: null,
        RegionName: null,
        RegistrationSourceCode: null,
        RegistrationSourceName: null,
        RequestKey: null,
        RoleKey: null,
        RoleName: null,
        SecondaryPassword: null,
        SystemPassword: null,
        UserName: null,
        WebsiteUrl: null
    };

}