import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import swal from 'sweetalert2';

@Component({
    selector: 'hc-customeronboarding',
    templateUrl: './customeronboarding.component.html',
})
export class TUCustomerOnboardingComponent implements OnInit {
    public SelectedDay = null;
    public SelectedMonth = null;
    public SelectedYear = null;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.Form_AddUser_Load();
        this.GetAcquirersList();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.Year = [];
        var CurrentYear = new Date().getFullYear();
        var StartYear = CurrentYear - 13;
        for (let index = 0; index < 90; index++) {
            StartYear = StartYear - 1;
            this.Year.push({
                value: StartYear,
                text: StartYear,
            });
        }
    }
    public Year =
        [
        ];
    public Day =
        [
            {
                value: '1',
                text: '1',
            },
            {
                value: '2',
                text: '2',
            },
            {
                value: '3',
                text: '3',
            },
            {
                value: '4',
                text: '4',
            },
            {
                value: '5',
                text: '5',
            },
            {
                value: '6',
                text: '6',
            },
            {
                value: '7',
                text: '7',
            },
            {
                value: '8',
                text: '8',
            },
            {
                value: '9',
                text: '9',
            },
            {
                value: '10',
                text: '10',
            },
            {
                value: '11',
                text: '11',
            },
            {
                value: '12',
                text: '12',
            },
            {
                value: '13',
                text: '13',
            },
            {
                value: '14',
                text: '14',
            },
            {
                value: '15',
                text: '15',
            },
            {
                value: '16',
                text: '16',
            },
            {
                value: '17',
                text: '17',
            },
            {
                value: '18',
                text: '18',
            },
            {
                value: '19',
                text: '19',
            },
            {
                value: '20',
                text: '20',
            },
            {
                value: '21',
                text: '21',
            },
            {
                value: '22',
                text: '22',
            },
            {
                value: '23',
                text: '23',
            },
            {
                value: '24',
                text: '24',
            },
            {
                value: '25',
                text: '25',
            },
            {
                value: '26',
                text: '26',
            },
            {
                value: '27',
                text: '27',
            },
            {
                value: '28',
                text: '28',
            },
            {
                value: '29',
                text: '29',
            },
            {
                value: '30',
                text: '30',
            },
            {
                value: '31',
                text: '31',
            },
        ];
    public Month =
        [
            {
                value: '1',
                text: '01 January'
            },
            {
                value: '2',
                text: '02 Febuary'
            },
            {
                value: '3',
                text: '03 March'
            },
            {
                value: '4',
                text: '04 April'
            },
            {
                value: '5',
                text: '05 May'
            },
            {
                value: '6',
                text: '06 June'
            },
            {
                value: '7',
                text: '07 July'
            },
            {
                value: '8',
                text: '08 Augest'
            },
            {
                value: '9',
                text: '09 September'
            },
            {
                value: '10',
                text: '10 Octomber'
            },
            {
                value: '11',
                text: '11 November'
            },
            {
                value: '12',
                text: '12 December'
            }
        ]


    public GetAcquirersOption: Select2Options;
    GetAcquirersList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAcquirersOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAcquirersListChange(event: any) {
        this.Form_AddUser.patchValue(
            {
                OwnerKey: event.value
            }
        );
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
    }
    Form_AddUser_Show() {
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Customers]);
    }
    Form_AddUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.AppUser,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.OnlineAndOffline,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.email, Validators.minLength(2)])],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            Latitude: 0,
            Longitude: 0,
            RegionKey: '',
            RegionAreaKey: '',
            CityKey: '',
            CityAreaKey: '',
            PostalCodeKey: '',
            CountValue: 0,
            AverageValue: 0,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Configuration: [],
        });
    }
    Form_AddUser_Clear() {
        this.SelectedDay = null;
        this.SelectedMonth = null;
        this.SelectedYear = null;
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Create customer account ?',
            text: 'Please verify information and click on continue button to create customer',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                var Dob = null;
                if (this.SelectedYear != null && this.SelectedYear != null && this.SelectedYear != "") {
                    if (this.SelectedYear.length == 4) {
                        if (isNaN(this.SelectedYear) == false) {
                            if (this.SelectedDay != null && this.SelectedMonth != null && this.SelectedYear != null) {
                                Dob = new Date(this.SelectedYear, (this.SelectedMonth - 1), this.SelectedDay, 0, 0, 0, 0);
                            }
                        }
                    }
                }
                _FormValue.DateOfBirth = Dob;
                _FormValue.DisplayName = _FormValue.FirstName;
                _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Customer added successfully');
                            this.Form_AddUser_Clear();
                            this.Form_AddUser_Load();

                            if (_FormValue.OperationType == 'edit') {
                                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Customer, _Response.Result.ReferenceKey]);
                            }
                            else if (_FormValue.OperationType == 'close') {
                                this.Form_AddUser_Close();
                            }
                        }
                        else {
                            this._HelperService.NotifySuccess('Account already registered with ThankUCash');
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });


    }


}