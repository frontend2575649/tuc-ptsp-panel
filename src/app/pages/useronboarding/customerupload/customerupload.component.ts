import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
type AOA = any[][];

@Component({
    selector: 'hc-customerupload',
    templateUrl: './customerupload.component.html',
})
export class TUCustomerUploadComponent implements OnInit {
    public SelectedDay = null;
    public SelectedMonth = null;
    public SelectedYear = null;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {

    }
    IsUploading = false;
    UploadCount = 0;
    TotalCount = 0;
    CustomersList: CustomerImport[] = [];
    data: AOA = [];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    fileName: string = 'SheetJS.xlsx';
    onFileChange(evt: any) {
        this.UploadCount = 0;
        this.TotalCount = 0;
        this.CustomersList = [];
        this.IsUploading = false;
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) throw new Error('Cannot use multiple files');
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
            for (let index = 1; index < this.data.length; index++) {
                const CustomerInfo = this.data[index];
                var FirstName = CustomerInfo[0];
                var LastName = CustomerInfo[1];
                var MobileNumber = CustomerInfo[2];
                var EmailAddress = CustomerInfo[3];
                var Gender = CustomerInfo[4];
                var DateOfBirth = CustomerInfo[5];
                if (Gender != undefined) {
                    if (Gender == "Male" || Gender == 'male') {
                        Gender = 'gender.male'
                    }
                    if (Gender == "Feale" || Gender == 'female') {
                        Gender = 'gender.female'
                    }
                }
                var TDob = null;
                if (DateOfBirth != undefined) {
                    try {
                        var Dob = new Date(DateOfBirth);
                        if (Dob != undefined) {
                            TDob = Dob;
                        }
                        else {
                            TDob = null;
                        }
                    } catch (error) {
                    }
                }
                if (FirstName != undefined && LastName != undefined && MobileNumber != undefined && MobileNumber != null && MobileNumber != "") {
                    var AppUserInfo: CustomerImport =
                    {
                        Name: FirstName + LastName,
                        FirstName: FirstName,
                        LastName: LastName,
                        MobileNumber: MobileNumber,
                        EmailAddress: EmailAddress,
                        Gender: Gender,
                        DateOfBirth: Dob,
                        Status: 'pending',
                        Message: '',
                    };
                    this.CustomersList.push(AppUserInfo);
                    this.TotalCount = this.TotalCount + 1;
                }
            }
        };
        reader.readAsBinaryString(target.files[0]);
    }
    export(): void {
        var Cus = this.CustomersList as any;
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(Cus);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'ImportReport');
        XLSX.writeFile(wb, this.fileName);
    }

    StartCustomerUpload() {
        if (this.CustomersList.length > 0) {
            this.IsUploading = true;
            for (let index = 0; index < this.CustomersList.length; index++) {
                const element = this.CustomersList[index];
                this.CustomersList[index].Status = "processing";
                this.CustomersList[index].Message = "sending data";
                var PostItem = {
                    OperationType: 'new',
                    Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
                    AccountTypeCode: this._HelperService.AppConfig.AccountType.AppUser,
                    AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.OnlineAndOffline,
                    RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
                    OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
                    Name: element.Name,
                    FirstName: element.FirstName,
                    LastName: element.LastName,
                    MobileNumber: element.MobileNumber,
                    EmailAddress: element.EmailAddress,
                    GenderCode: element.Gender,
                    DateOfBirth: element.DateOfBirth,
                    Latitude: 0,
                    Longitude: 0,
                    RegionKey: '',
                    RegionAreaKey: '',
                    CityKey: '',
                    CityAreaKey: '',
                    PostalCodeKey: '',
                    CountValue: 0,
                    AverageValue: 0,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PostItem);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        this.UploadCount = this.UploadCount + 1;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.CustomersList[index].Status = "success";
                            this.CustomersList[index].Message = "customer added";
                        }
                        else {
                            this.CustomersList[index].Status = "failed";
                            this.CustomersList[index].Message = "customer already present";
                        }
                        if (index == (this.CustomersList.length - 1)) {
                            this.IsUploading = false;
                            this._HelperService.NotifySuccess('Customers import successfull');
                        }


                    },
                    _Error => {
                        this.UploadCount = this.UploadCount + 1;
                        if (index == (this.CustomersList.length - 1)) {
                            this.IsUploading = false;
                            this._HelperService.NotifySuccess('Customers import successfull');
                        }
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        }
        else {
            this._HelperService.NotifyError('Select file to upload customers');
        }

    }

}





export class CustomerImport {
    public Name: string | null;
    public FirstName: string | null;
    public LastName: string | null;
    public MobileNumber: string | null;
    public EmailAddress: string | null;
    public Gender: string | null;
    public DateOfBirth: Date | null;
    public Status: string | null;
    public Message: string | null;
}
