import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";

import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent
} from "../../../service/service";

@Component({
  selector: "tu-addposterminal",
  templateUrl: "./tuaddposterminal.component.html"
})
export class TUAddPosTerminalComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this.Form_AddUser_Load();
    this.GetPosAccountsList();
    this.GetRMAccountsList();
    this.StoresList_Setup();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      if (this._HelperService.AppConfig.ActiveReferenceKey != null) {
        this._HelperService.Get_UserAccountDetails(true);
      } else {
      }
    });
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  public MerchantDisplayName: string = null;
  public StoreDisplayName: string = null;
  public StoreAddress: string = null;
  public MerchantIconUrl: string = null;
  public PosAccountKey: string = null;
  public MerchantKey: string = null;
  public StoreKey: string = null;
  public RMKey: string = null;
  public GetPosAccountsOption: Select2Options;
  GetPosAccountsList() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        },
        {
          SystemName: "StatusCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.Status.Active
        }
      ]
    };
    // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetPosAccountsOption = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false
    };
  }
  GetPosAccountsListChange(event: any) {
    this.PosAccountKey = event.value;
    this.Form_AddUser.patchValue({
      OwnerKey: event.value
    });
  }


  Form_AddUser: FormGroup;
  Form_AddUser_Address: string = null;
  Form_AddUser_Latitude: number = 0;
  Form_AddUser_Longitude: number = 0;
  @ViewChild("places") places: GooglePlaceDirective;
  Form_AddUser_PlaceMarkerClick(event) {
    this.Form_AddUser_Latitude = event.coords.lat;
    this.Form_AddUser_Longitude = event.coords.lng;
  }
  public Form_AddUser_AddressChange(address: Address) {
    this.Form_AddUser_Latitude = address.geometry.location.lat();
    this.Form_AddUser_Longitude = address.geometry.location.lng();
    this.Form_AddUser_Address = address.formatted_address;
  }
  Form_AddUser_Show() {
    // this._HelperService.OpenModal('Form_AddUser_Content');
  }
  Form_AddUser_Close() {
    this.Form_AddUser_Clear();
    this.Form_AddUser_Load();
    this.MerchantDisplayName = null;
    this.StoreDisplayName = null;
    this.StoreAddress = null;
    this.MerchantIconUrl = null;
    this.PosAccountKey = null;
    this.MerchantKey = null;
    this.StoreKey = null;
    // this.StoresList_Setup();
    this._HelperService.CloseModal("ModalAddTerminal");
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PosTerminals]);
    // this._HelperService.OpenModal('Form_AddUser_Content');
  }
  Form_AddUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      AccountOperationTypeCode: this._HelperService.AppConfig
        .AccountOperationType.Online,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource
        .System,
      OwnerKey: [null, Validators.required],
      MerchantKey: [null, Validators.required],
      AcquirerKey: [null, Validators.required],
      StoreKey: [null, Validators.required],
      UserName: this._HelperService.GenerateGuid(),
      Password: this._HelperService.RandomPassword,
      DisplayName: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25)
        ])
      ],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      Owners: []
    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this.Form_AddUser_Load();
    this.Form_AddUser_Latitude = 0;
    this.Form_AddUser_Longitude = 0;
    this.Form_AddUser_Address = "";
    this._HelperService.Icon_Crop_Clear();

    this._HelperService._FileSelect_Poster_Reset();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    var OwnersList = [
      {
        OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey
      },
      {
        OwnerKey: this.MerchantKey
      },
      {
        OwnerKey: this.StoreKey
      },
    ];
    if (this.RMKey != undefined && this.RMKey != null) {
      OwnersList.push(
        {
          OwnerKey: this.RMKey,
        });
    }
    _FormValue.SubOwnerKey = this.StoreKey;
    _FormValue.BankKey = this._HelperService.AppConfig.ActiveOwnerKey;
    _FormValue.Owners = OwnersList;
    _FormValue.Name = _FormValue.DisplayName;
    _FormValue.UserName = _FormValue.DisplayName;
    // _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Terminal added successfully");
          // this.Form_AddUser_Clear();
          this.Form_AddUser.patchValue({
            DisplayName: null,
            OwnerKey: null
          });
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public StoresList_Config: OList;
  StoresList_Setup() {
    this.StoresList_Config = {
      Id:"",
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      Title: "Merchants",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.All,
      // ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      TableFields: [
        {
          DisplayName: "Merchant",
          SystemName: "OwnerDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null

          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Store Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null

          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Address",
          SystemName: "Address",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null

          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard
        }
      ]
    };
    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );
    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {
    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );
    if (this.StoresList_Config.RefreshData == true) {
      this.StoresList_GetData();
    }
  }
  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    this.MerchantDisplayName = ReferenceData.OwnerDisplayName;
    this.StoreDisplayName = ReferenceData.DisplayName;
    this.StoreAddress = ReferenceData.Address;
    this.MerchantIconUrl = ReferenceData.IconUrl;
    this.MerchantKey = ReferenceData.OwnerKey;
    this.StoreKey = ReferenceData.ReferenceKey;

    this.Form_AddUser.patchValue({
      MerchantKey: this.MerchantKey,
      StoreKey: this.StoreKey,
      AcquirerKey: this._HelperService.AppConfig.ActiveOwnerKey
    });
    this._HelperService.OpenModal("ModalAddTerminal");
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Store.Dashboard + '/' + ReferenceData.ReferenceKey]);
  }

  public GetRMAccountsOption: Select2Options;
  GetRMAccountsList() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, '='),
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.AcquirerSubAccount
        },
        {
          SystemName: "StatusCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.Status.Active
        }
      ]
    };
    // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRMAccountsOption = {
      placeholder: PlaceHolder,
      ajax: _Transport,
      multiple: false
    };
  }
  GetRMAccountsListChange(event: any) {
    this.RMKey = event.value;
    this.Form_AddUser.patchValue({
      RMKey: event.value
    });
  }
}
