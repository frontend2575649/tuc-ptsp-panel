import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../service/service';
import swal from 'sweetalert2';

@Component({
    selector: 'tu-tuterminalssale',
    templateUrl: './tuterminalssale.component.html',
})
export class TUTerminalsSaleComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.PosTerminalsList_Setup();
    }

    public PosTerminalsList_Config: OList;
    PosTerminalsList_Setup() {
        var TableFields = [];
        if (this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Merchant
            || this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.MerchantSubAccount) {

            TableFields = [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'PTSP',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'Transactions',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'Purchase',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                }];
        }
        else if (this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Acquirer
            || this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.AcquirerSubAccount) {
            TableFields = [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'MerchantDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'PTSP',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'Transactions',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'Purchase',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    
                    // //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                }];

        }
        else if (this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.Store
            || this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.StoreSubAccount) {
            TableFields = [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'MerchantDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'PTSP',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'Transactions',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'Purchase',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                }];

        }
        else if (this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.PosAccount
            || this._HelperService.UserAccount.AccountTypeCode == this._HelperService.AppConfig.AccountType.PosSubAccount) {
            TableFields = [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'MerchantDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'Transactions',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'Purchase',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                }];

        }
        else {
            TableFields = [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'MerchantDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'PTSP',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Trans',
                    SystemName: 'Transactions',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'Purchase',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: 'ReferenceKey',
                    //NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.TerminalRewardHistory
                }
            ];

        }
        this.PosTerminalsList_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetPosTerminals,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Terminals',
                StatusType: 'default',
                StartDate: new Date(2017, 1, 1, 0, 0, 0, 0),
                EndDate: new Date(),
                Type: this._HelperService.AppConfig.ListType.SubOwner,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                TableFields: TableFields,
            }
        this.PosTerminalsList_Config = this._DataHelperService.List_Initialize(this.PosTerminalsList_Config);
        this.PosTerminalsList_GetData();
    }
    PosTerminalsList_ToggleOption(event: any, Type: any) {
        this.PosTerminalsList_Config = this._DataHelperService.List_Operations(this.PosTerminalsList_Config, event, Type);
        if (this.PosTerminalsList_Config.RefreshData == true) {
            this.PosTerminalsList_GetData();
        }
    }
    PosTerminalsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.PosTerminalsList_Config);
        this.PosTerminalsList_Config = TConfig;
    }
    PosTerminalsList_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

}