import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    public GenderChart = []

    ngOnInit() {
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('month');
            this.EndTime = moment().endOf('month');
        }

        if (this.StartTimeCustom == undefined) {
            this.StartTimeCustom = moment().startOf('day');
            this.EndTimeCustom = moment().endOf(1, 'day');
        }
        this.GetMerchantVisitTrend();
        this._OverviewToday.GenderLabel.push("male");
        this._OverviewToday.GenderData.push(0);
        this._OverviewToday.GenderLabel.push('female');
        this._OverviewToday.GenderData.push(0);

        this._Overview.GenderLabel.push('male');
        this._Overview.GenderData.push(0);
        this._Overview.GenderLabel.push('female');
        this._Overview.GenderData.push(0);

        this._OverviewToday.RewardTypeLabel.push('Cash');
        this._OverviewToday.RewardTypeData.push(0);
        this._OverviewToday.RewardTypeLabel.push('Card');
        this._OverviewToday.RewardTypeData.push(0);

        this._Overview.RewardTypeLabel.push('Cash');
        this._Overview.RewardTypeData.push(0);
        this._Overview.RewardTypeLabel.push('Card');
        this._Overview.RewardTypeData.push(0);
        this.GetOverview();
        this.GetOverviewToday();
        this.TURewardMerchant_Setup();
        this.TURedeemMerchant_Setup();
        this.AppUsers_Setup();

    }
    GetOverviewToday() {
        var Data = {
            Task: 'getaccountbalanceoverview',
            StartTime: this.StartTimeCustom,
            EndTime: this.EndTimeCustom,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._OverviewToday = _Response.Result as OOverview;
                    this._OverviewToday.RewardLastTransaction = this._HelperService.GetDateTimeS(this._OverviewToday.RewardLastTransaction);
                    this._OverviewToday.RedeemLastTransaction = this._HelperService.GetDateTimeS(this._OverviewToday.RedeemLastTransaction);
                    this._OverviewToday.LastAppUserDate = this._HelperService.GetDateTimeS(this._OverviewToday.LastAppUserDate);
                    this._OverviewToday.LastCommissionDate = this._HelperService.GetDateTimeS(this._OverviewToday.LastCommissionDate);
                    this._OverviewToday.LastIssuerCommissionDate = this._HelperService.GetDateTimeS(this._OverviewToday.LastIssuerCommissionDate);
                    this._OverviewToday.GenderData = [];
                    this._OverviewToday.RewardTypeData = [];
                    this._OverviewToday.SettlementPending = this._OverviewToday.SettlementCredit - this._OverviewToday.SettlementDebit;
                    if (this._OverviewToday.LastTransaction != undefined) {
                        this._OverviewToday.LastTransaction.TransactionDate = this._HelperService.GetDateTimeS(this._OverviewToday.LastTransaction.TransactionDate);
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    Type = 5;
    CustomType = 1;
    FStart = null;
    FEnd = null;
    StartTime = null;
    EndTime = null;
    StartTimeCustom = null;
    EndTimeCustom = null;
    UserAnalytics_DateChange_TodayCustom(CustomType) {
        this.CustomType = CustomType;
        var SDate;
        if (CustomType == 1) { // today
            SDate =
                {
                    start: moment().startOf('day'),
                    end: moment().endOf('day'),
                }
        }
        else if (CustomType == 2) { // yesterday
            SDate =
                {
                    start: moment().subtract(1, 'days').startOf('day'),
                    end: moment().subtract(1, 'days').endOf('day'),
                }
        }
        else if (CustomType == 3) {  // this week
            SDate =
                {
                    start: moment().startOf('isoWeek'),
                    end: moment().endOf('isoWeek'),
                }
        }
        else if (CustomType == 4) { // last week
            SDate =
                {
                    start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                    end: moment().subtract(1, 'weeks').endOf('isoWeek'),
                }
        }
        this.UserAnalytics_DateChangeCustom(SDate, null);
    }
    UserAnalytics_DateChangeCustom(value: any, Type: any) {
        this.StartTimeCustom = value.start;
        this.EndTimeCustom = value.end;
        this.GetOverviewToday();
    }

    UserAnalytics_DateChange_Today(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) { // today
            SDate =
                {
                    start: moment().startOf('day'),
                    end: moment().endOf('day'),
                }
        }
        else if (Type == 2) { // yesterday
            SDate =
                {
                    start: moment().subtract(1, 'days').startOf('day'),
                    end: moment().subtract(1, 'days').endOf('day'),
                }
        }
        else if (Type == 3) {  // this week
            SDate =
                {
                    start: moment().startOf('isoWeek'),
                    end: moment().endOf('isoWeek'),
                }
        }
        else if (Type == 4) { // last week
            SDate =
                {
                    start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                    end: moment().subtract(1, 'weeks').endOf('isoWeek'),
                }
        }
        else if (Type == 5) { // this month
            SDate =
                {
                    start: moment().startOf('month'),
                    end: moment().endOf('month'),
                }
        }
        else if (Type == 6) { // last month
            SDate =
                {
                    start: moment().startOf('month').subtract(1, 'month'),
                    end: moment().startOf('month').subtract(1, 'days'),
                }
        }
        else if (Type == 7) {
            SDate =
                {
                    start: new Date(2017, 0, 1, 0, 0, 0, 0),
                    end: moment().endOf('day'),
                }
        }
        this.FStart = this._HelperService.GetDateS(SDate.start);
        this.FEnd = this._HelperService.GetDateS(SDate.end);
        if (this.picker.datePicker != undefined) {
            this.picker.datePicker.setStartDate(SDate.start);
            this.picker.datePicker.setEndDate(SDate.end);
        }
        this.UserAnalytics_DateChange(SDate, null);
    }
    UserAnalytics_DateChange(value: any, Type: any) {
        this.StartTime = value.start;
        this.EndTime = value.end;
        this.GetOverview();
    }

    GetOverview() {
        var Data = {
            Task: 'getaccountbalanceoverview',
            StartTime: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: this.EndTime, // moment().add(2, 'days'),
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._Overview.GenderData = [];
                    this._Overview.RewardTypeData = [];
                    this._Overview = _Response.Result as OOverview;
                    this._Overview.RewardLastTransaction = this._HelperService.GetDateTimeS(this._Overview.RewardLastTransaction);
                    this._Overview.RedeemLastTransaction = this._HelperService.GetDateTimeS(this._Overview.RedeemLastTransaction);
                    this._Overview.LastAppUserDate = this._HelperService.GetDateTimeS(this._Overview.LastAppUserDate);
                    this._Overview.LastCommissionDate = this._HelperService.GetDateTimeS(this._Overview.LastCommissionDate);
                    this._Overview.LastIssuerCommissionDate = this._HelperService.GetDateTimeS(this._Overview.LastIssuerCommissionDate);
                    this._Overview.GenderData = [];
                    this._Overview.GenderData.push(this._Overview.AppUsersMale);
                    this._Overview.GenderData.push(this._Overview.AppUsersFemale);
                    this._Overview.RewardTypeData = [];
                    this._Overview.RewardTypeData.push(Math.round(this._Overview.CashRewardAmount));
                    this._Overview.RewardTypeData.push(Math.round(this._Overview.CardRewardAmount));
                    this._Overview.SettlementPending = this._Overview.SettlementCredit - this._Overview.SettlementDebit;
                    if (this._Overview.LastTransaction != undefined) {
                        this._Overview.LastTransaction.TransactionDate = this._HelperService.GetDateTimeS(this._Overview.LastTransaction.TransactionDate);
                    }
                    this.GetMerchantVisitTrend();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    public VisitTrendLabel = [];
    public VisitTrendData = [];

    GetMerchantVisitTrend() {
        var Data = {
            Task: 'getmerchantvisittrend',
            StartTime: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: this.EndTime, // moment().add(2, 'days'),
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var ChartResponse = _Response.Result;
                    this.VisitTrendLabel = [];
                    this.VisitTrendData = [];
                    ChartResponse.DateRange.forEach(RangeItem => {
                        this.VisitTrendLabel.push(this._HelperService.GetTimeS(RangeItem.StartTime));
                    });
                    var TPlotDataSet = [];
                    if (ChartResponse.DateRange.length > 0) {
                        var RangeItem = ChartResponse.DateRange[0].Data;
                        RangeItem.forEach(element => {
                            var DataSetItem =
                            {
                                label: element.Name,
                                data: [],
                            }
                            TPlotDataSet.push(DataSetItem);
                        });
                        ChartResponse.DateRange.forEach(RangeItem => {
                            var Data = RangeItem.Data;
                            Data.forEach(element => {
                                TPlotDataSet[0].data.push(parseInt(element.Value));
                                var DataItem = TPlotDataSet.find(x => x.label == element.Name)
                                if (DataItem != undefined) {
                                    DataItem.data.push(parseInt(element.Value));
                                }
                            });
                        });
                    }
                    this.VisitTrendData = TPlotDataSet;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    public TURewardMerchant_Config: OList;
    TURewardMerchant_Setup() {
        this.TURewardMerchant_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetTUCRewardTransctions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Reward Transactions',
                StatusType: 'transaction',
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                Type: this._HelperService.AppConfig.ListType.Owner,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                TableFields: [
                    {
                        DisplayName: 'User',
                        SystemName: 'UserAccountDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Invoice Amt',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Reward Amt',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Done By',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                ]
            }
        this.TURewardMerchant_Config = this._DataHelperService.List_Initialize(this.TURewardMerchant_Config);
        this.TURewardMerchant_GetData();
    }
    TURewardMerchant_ToggleOption(event: any, Type: any) {
        this.TURewardMerchant_Config = this._DataHelperService.List_Operations(this.TURewardMerchant_Config, event, Type);
        if (this.TURewardMerchant_Config.RefreshData == true) {
            this.TURewardMerchant_GetData();
        }
    }
    TURewardMerchant_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TURewardMerchant_Config);
        this.TURewardMerchant_Config = TConfig;
    }
    TURewardMerchant_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }



    public TURedeemMerchant_Config: OList;
    TURedeemMerchant_Setup() {
        this.TURedeemMerchant_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetRedeemTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Redeem Transactions',
                StatusType: 'transaction',
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                Type: this._HelperService.AppConfig.ListType.Owner,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                TableFields: [
                    {
                        DisplayName: 'User',
                        SystemName: 'UserAccountDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                        // NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.AppUser.Dashboard
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Invoice',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Amount',
                        SystemName: 'Amount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    // {
                    //     DisplayName: 'Merchant',
                    //     SystemName: 'ParentDisplayName',
                    //     DataType: this._HelperService.AppConfig.DataType.Text,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     // NavigateField: 'ParentKey',
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.Merchant.Dashboard
                    // },

                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Done By',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                ]
            }
        this.TURedeemMerchant_Config = this._DataHelperService.List_Initialize(this.TURedeemMerchant_Config);
        this.TURedeemMerchant_GetData();
    }
    TURedeemMerchant_ToggleOption(event: any, Type: any) {
        this.TURedeemMerchant_Config = this._DataHelperService.List_Operations(this.TURedeemMerchant_Config, event, Type);
        if (this.TURedeemMerchant_Config.RefreshData == true) {
            this.TURedeemMerchant_GetData();
        }
    }
    TURedeemMerchant_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TURedeemMerchant_Config);
        this.TURedeemMerchant_Config = TConfig;
    }
    TURedeemMerchant_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    public AppUsers_Config: OList;
    AppUsers_Setup() {
        this.AppUsers_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetAppUsers,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Merchants',
                StatusType: 'default',
                Type: this._HelperService.AppConfig.ListType.Owner,
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                TableFields: [
                    {
                        DisplayName: 'Name',
                        SystemName: 'DisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: 'Mobile',
                        SystemName: 'MobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: 'Email',
                        SystemName: 'EmailAddress',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: 'Gender',
                        SystemName: 'GenderName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: 'Total Purchase',
                        SystemName: 'Purchase',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: 'Visits',
                        SystemName: 'Transactions',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },

                    {
                        DisplayName: 'Last Visit',
                        SystemName: 'LastTransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    {
                        DisplayName: "Active Since",
                        SystemName: 'CreateDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        NavigateField: 'ReferenceKey',

                    },
                    // {
                    //     DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
                    //     SystemName: 'CreatedByDisplayName',
                    //     DataType: this._HelperService.AppConfig.DataType.Text,
                    //     Show: true,
                    //     Search: false,
                    //     Sort: true,
                    //     ResourceId: null,
                    // }
                ]
            }
        this.AppUsers_Config = this._DataHelperService.List_Initialize(this.AppUsers_Config);
        this.AppUsers_GetData();
    }
    AppUsers_ToggleOption(event: any, Type: any) {
        this.AppUsers_Config = this._DataHelperService.List_Operations(this.AppUsers_Config, event, Type);
        if (this.AppUsers_Config.RefreshData == true) {
            this.AppUsers_GetData();
        }
    }
    AppUsers_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.AppUsers_Config);
        this.AppUsers_Config = TConfig;
    }
    AppUsers_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }



    public _OverviewToday: OOverview =
        {
            TerminalsOverview: [],
            StoresOverview: [],
            Merchants: 0,
            Stores: 0,
            Acquirers: 0,
            Terminals: 0,
            Ptsp: 0,
            Pssp: 0,
            Cashiers: 0,
            RewardCards: 0,
            RewardCardsUsed: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusForMerchant: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            RewardPercentage: 0,
            CommissionPercentage: 0,
            Balance: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            RewardTransactions: 0,
            RewardAmount: 0,
            RewardPurchaseAmount: 0,
            RewardLastTransaction: null,
            RedeemTransactions: 0,
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemLastTransaction: null,
            Transactions: 0,
            TransactionsPercentage: 0,
            NewTransactions: 0,
            NewTransactionsPercentage: 0,
            RepeatingTransactions: 0,
            RepeatingTransactionsPercentage: 0,
            ReferralTransactions: 0,
            ReferralTransactionsPercentage: 0,
            PurchaseAmount: 0,
            PurchaseAmountPercentage: 0,
            NewPurchaseAmount: 0,
            NewPurchaseAmountPercentage: 0,
            RepeatingPurchaseAmount: 0,
            RepeatingPurchaseAmountPercentage: 0,
            ReferralPurchaseAmount: 0,
            ReferralPurchaseAmountPercentage: 0,
            Commission: 0,
            LastCommissionDate: null,
            IssuerCommissionAmount: 0,
            LastIssuerCommissionDate: null,
            CommissionAmount: 0,
            SettlementCredit: 0,
            SettlementDebit: 0,
            AppUsers: 0,
            AppUsersPercentage: 0,
            OwnAppUsers: 0,
            OwnAppUsersPercentage: 0,
            RepeatingAppUsers: 0,
            RepeatingAppUsersPercentage: 0,
            ReferralAppUsers: 0,
            ReferralAppUsersPercentage: 0,
            AppUsersMale: 0,
            AppUsersFemale: 0,
            LastAppUserDate: null,
            LastTransaction:
            {
                Amount: 0,
                InvoiceAmount: 0,
                MerchantName: '',
                ReferenceId: 0,
                RewardAmount: 0,
                TransactionDate: null,
                TypeName: null,
            },
            GenderLabel: [],
            GenderData: [],
            RewardTypeLabel: [],
            RewardTypeData: [],
            VisitTrendLabel: [],
            VisitTrendData: [],
            AcquirerAmountDistribution: [],
            Charge: 0,
            ClaimedReward: 0,
            ActiveTerminals: 0,
            AppUsersByAge: [],
            AppUsersOther: 0,
            AppUsersPurchaseByAge: [],
            CardRewardPurchaseAmountOther: 0,
            CardRewardTransactionsOther: 0,
            ClaimedRewardTransations: 0,
            Credit: 0,
            DeadTerminals: 0,
            Debit: 0,
            FrequentBuyers: [],
            IdleTerminals: 0,
            LastTransactionDate: null,
            MerchantOverview: [],
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            PosOverview: [],
            RewardChargeAmount: 0,
            RewardUserAmount: 0,
            SettlementPending: 0,
            TUCPlusBalance: 0,
            TUCPlusPurchaseAmount: 0,
            TUCPlusReward: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
            ThankUAmount: 0,
            TransactionIssuerAmountCredit: 0,
            TransactionIssuerAmountDebit: 0,
            TransactionIssuerChargeCredit: 0,
            TransactionIssuerChargeDebit: 0,
            TransactionIssuerTotalCreditAmount: 0,
            TransactionIssuerTotalDebitAmount: 0,
            UniqueAppUsers: 0,
            UserAmount: 0,
        }
    public _Overview: OOverview =
        {
            TerminalsOverview: [],
            StoresOverview: [],
            Merchants: 0,
            Stores: 0,
            Acquirers: 0,
            Terminals: 0,
            Ptsp: 0,
            Pssp: 0,
            Cashiers: 0,
            RewardCards: 0,
            RewardCardsUsed: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusForMerchant: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            RewardPercentage: 0,
            CommissionPercentage: 0,
            Balance: 0,
            CashRewardAmount: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardTransactions: 0,
            CardRewardAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardTransactions: 0,
            RewardTransactions: 0,
            RewardAmount: 0,
            RewardPurchaseAmount: 0,
            RewardLastTransaction: null,
            RedeemTransactions: 0,
            RedeemAmount: 0,
            RedeemPurchaseAmount: 0,
            RedeemLastTransaction: null,
            Transactions: 0,
            TransactionsPercentage: 0,
            NewTransactions: 0,
            NewTransactionsPercentage: 0,
            RepeatingTransactions: 0,
            RepeatingTransactionsPercentage: 0,
            ReferralTransactions: 0,
            ReferralTransactionsPercentage: 0,
            PurchaseAmount: 0,
            PurchaseAmountPercentage: 0,
            NewPurchaseAmount: 0,
            NewPurchaseAmountPercentage: 0,
            RepeatingPurchaseAmount: 0,
            RepeatingPurchaseAmountPercentage: 0,
            ReferralPurchaseAmount: 0,
            ReferralPurchaseAmountPercentage: 0,
            Commission: 0,
            LastCommissionDate: null,
            IssuerCommissionAmount: 0,
            LastIssuerCommissionDate: null,
            CommissionAmount: 0,
            SettlementCredit: 0,
            SettlementDebit: 0,
            AppUsers: 0,
            AppUsersPercentage: 0,
            OwnAppUsers: 0,
            OwnAppUsersPercentage: 0,
            RepeatingAppUsers: 0,
            RepeatingAppUsersPercentage: 0,
            ReferralAppUsers: 0,
            ReferralAppUsersPercentage: 0,
            AppUsersMale: 0,
            AppUsersFemale: 0,
            LastAppUserDate: null,
            LastTransaction:
            {
                Amount: 0,
                InvoiceAmount: 0,
                MerchantName: '',
                ReferenceId: 0,
                RewardAmount: 0,
                TransactionDate: null,
                TypeName: null,
            },
            GenderLabel: [],
            GenderData: [],
            RewardTypeLabel: [],
            RewardTypeData: [],
            VisitTrendLabel: [],
            VisitTrendData: [],
            AcquirerAmountDistribution: [],
            Charge: 0,
            ClaimedReward: 0,
            ActiveTerminals: 0,
            AppUsersByAge: [],
            AppUsersOther: 0,
            AppUsersPurchaseByAge: [],
            CardRewardPurchaseAmountOther: 0,
            CardRewardTransactionsOther: 0,
            ClaimedRewardTransations: 0,
            Credit: 0,
            DeadTerminals: 0,
            Debit: 0,
            FrequentBuyers: [],
            IdleTerminals: 0,
            LastTransactionDate: null,
            MerchantOverview: [],
            OtherRewardAmount: 0,
            OtherRewardPurchaseAmount: 0,
            OtherRewardTransactions: 0,
            PosOverview: [],
            RewardChargeAmount: 0,
            RewardUserAmount: 0,
            SettlementPending: 0,
            TUCPlusBalance: 0,
            TUCPlusPurchaseAmount: 0,
            TUCPlusReward: 0,
            TUCPlusRewardAmount: 0,
            TUCPlusRewardChargeAmount: 0,
            TUCPlusRewardClaimedAmount: 0,
            TUCPlusRewardClaimedTransactions: 0,
            TUCPlusRewardPurchaseAmount: 0,
            TUCPlusRewardTransactions: 0,
            TUCPlusUserRewardAmount: 0,
            ThankUAmount: 0,
            TransactionIssuerAmountCredit: 0,
            TransactionIssuerAmountDebit: 0,
            TransactionIssuerChargeCredit: 0,
            TransactionIssuerChargeDebit: 0,
            TransactionIssuerTotalCreditAmount: 0,
            TransactionIssuerTotalDebitAmount: 0,
            UniqueAppUsers: 0,
            UserAmount: 0,
        }
}