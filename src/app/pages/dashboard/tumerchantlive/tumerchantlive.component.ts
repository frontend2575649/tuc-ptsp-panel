import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
declare var moment: any;
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  OOverview
} from "../../../service/service";
import { DaterangePickerComponent } from "ng2-daterangepicker";

@Component({
  selector: "dashboard",
  templateUrl: "./tumerchantlive.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TUDashboardComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) {
    this._HelperService.AppConfig.ShowMenu = false;
    this._HelperService.AppConfig.ShowHeader = false;
    this._HelperService.ContainerHeight = window.innerHeight;
    this.todaysdate = this._HelperService.GetDateS(new Date());
    setInterval(() => {
      this.now = Date.now();
    }, 1);
  }
  public GenderChart = [];

  ngOnInit() {
    this._HelperService.FullContainer = true;
    this.LoadData();
    setInterval(() => {
      this.LoadData();
    }, 60000);
    this.startTimer();
  }
  public todaysdate: string = null;
  public now: number;
  timeLeft: number = 60;
  interval;
  StartTime = null;
  EndTime = null;
  PreStartTime = null;
  PreEndTime = null;
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 60;
      }
    }, 1000);
  }

  LoadData() {
    if (this.StartTime == undefined) {
      this.StartTime = moment().startOf("day");
      this.EndTime = moment().endOf("day");
      this.PreStartTime = moment()
        .subtract(1, "days")
        .startOf("day");
      this.PreEndTime = moment().subtract(1, "days");
    }
    this.GetAccountOverviewLite();
    this.TURewardMerchant_Setup();
    this.StoreSalesList_Setup();
    this._HelperService.GetVisitHistory(
      this._HelperService.AppConfig.ActiveOwnerKey,
      null,
      this.StartTime,
      this.EndTime
    );
  }

  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: "getaccountoverviewlite",
      StartTime: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this.EndTime, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Data
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          if (this._AccountOverview.PosOverview != undefined) {
            this._AccountOverview.PosOverview.forEach(element => {
              element.LastTransactionDateD = this._HelperService.GetDateS(
                element.LastTransactionDate
              );
              element.LastTransactionDateT = this._HelperService.GetTimeS(
                element.LastTransactionDate
              );
              element.LastTransactionDate = this._HelperService.GetDateTimeS(
                element.LastTransactionDate
              );
            });
          }
          this.GetAccountOverviewLiteNext();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetAccountOverviewLiteNext() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: "getaccountoverviewlite",
      StartTime: this.PreStartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this.PreEndTime, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Data
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverviewNext = _Response.Result as OAccountOverview;
          this._AccountOverviewDiff.AppUsers =
            this._AccountOverviewNext.AppUsers - this._AccountOverview.AppUsers;
          this._AccountOverviewDiff.CardRewardPurchaseAmount =
            this._AccountOverviewNext.CardRewardPurchaseAmount -
            this._AccountOverview.CardRewardPurchaseAmount;
          this._AccountOverviewDiff.CashRewardPurchaseAmount =
            this._AccountOverviewNext.CashRewardPurchaseAmount -
            this._AccountOverview.CashRewardPurchaseAmount;
          this._AccountOverviewDiff.IssuerCommissionAmount =
            this._AccountOverviewNext.IssuerCommissionAmount -
            this._AccountOverview.IssuerCommissionAmount;
          this._AccountOverviewDiff.OtherRewardPurchaseAmount =
            this._AccountOverviewNext.OtherRewardPurchaseAmount -
            this._AccountOverview.OtherRewardPurchaseAmount;
          this._AccountOverviewDiff.PurchaseAmount =
            this._AccountOverviewNext.PurchaseAmount -
            this._AccountOverview.PurchaseAmount;
          this._AccountOverviewDiff.RedeemAmount =
            this._AccountOverviewNext.RedeemAmount -
            this._AccountOverview.RedeemAmount;
          this._AccountOverviewDiff.RepeatingAppUsers =
            this._AccountOverviewNext.RepeatingAppUsers -
            this._AccountOverview.RepeatingAppUsers;
          this._AccountOverviewDiff.RewardAmount =
            this._AccountOverviewNext.RewardAmount -
            this._AccountOverview.RewardAmount;
          this._AccountOverviewDiff.RewardChargeAmount =
            this._AccountOverviewNext.RewardChargeAmount -
            this._AccountOverview.RewardChargeAmount;
          this._AccountOverviewDiff.TUCPlusRewardAmount =
            this._AccountOverviewNext.TUCPlusRewardAmount -
            this._AccountOverview.TUCPlusRewardAmount;
          this._AccountOverviewDiff.TUCPlusRewardChargeAmount =
            this._AccountOverviewNext.TUCPlusRewardChargeAmount -
            this._AccountOverview.TUCPlusRewardChargeAmount;
          this._AccountOverviewDiff.TUCPlusRewardClaimedAmount =
            this._AccountOverviewNext.TUCPlusRewardClaimedAmount -
            this._AccountOverview.TUCPlusRewardClaimedAmount;
          this._AccountOverviewDiff.Transactions =
            this._AccountOverviewNext.Transactions -
            this._AccountOverview.Transactions;
          this._AccountOverviewDiff.UniqueAppUsers =
            this._AccountOverviewNext.UniqueAppUsers -
            this._AccountOverview.UniqueAppUsers;
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public TURewardMerchant_Config: OList;
  TURewardMerchant_Setup() {
    this.TURewardMerchant_Config = {
      Id:"",
      Task: this._HelperService.AppConfig.Api.ThankUCash
        .GetTUCPlusRewardTransctions,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Title: "Reward Transactions",
      StatusType: "transaction",
      StartDate: this.StartTime,
      EndDate: this.EndTime,
      StartTime: this.StartTime,
      EndTime: this.EndTime,
      PageRecordLimit: 10,
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict(
        "",
        "ParentKey",
        this._HelperService.AppConfig.DataType.Text,
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      ),
      Type: this._HelperService.AppConfig.ListType.All,
      TableFields: [
        {
          DisplayName: "User",
          SystemName: "UserAccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Mobile Number",
          SystemName: "UserAccountMobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Type",
          SystemName: "TypeName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Card",
          SystemName: "CardBrandName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Bank",
          SystemName: "CardBankName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Card No",
          SystemName: "CardBinNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Invoice Amt",
          SystemName: "InvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Reward Amt",
          SystemName: "RewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "text-grey",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Charge",
          SystemName: "Charge",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: " text-success ",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Date",
          SystemName: "TransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
          IsDateSearchField: true
        },
        {
          DisplayName: "Store",
          SystemName: "SubParentDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        },
        {
          DisplayName: "Done By",
          SystemName: "CreatedByDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "UserAccountKey",
        }
      ]
    };

    if (this._HelperService.AppConfig.ActiveOwnerIsTucPlusEnabled == true) {
      this.TURewardMerchant_Config.Task = this._HelperService.AppConfig.Api.ThankUCash.GetTUCPlusRewardTransctions;
      this.TURewardMerchant_Config = this._DataHelperService.List_Initialize(
        this.TURewardMerchant_Config
      );
      this.TURewardMerchant_GetData();
    } else {
      this.TURewardMerchant_Config.Task = this._HelperService.AppConfig.Api.ThankUCash.GetTUCRewardTransctions;
      this.TURewardMerchant_Config = this._DataHelperService.List_Initialize(
        this.TURewardMerchant_Config
      );
      this.TURewardMerchant_GetData();
    }
    this.TURewardMerchant_Config = this._DataHelperService.List_Initialize(
      this.TURewardMerchant_Config
    );
    this.TURewardMerchant_GetData();
  }
  TURewardMerchant_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TURewardMerchant_Config
    );
    this.TURewardMerchant_Config = TConfig;
  }
  TURewardMerchant_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }

  public StoreSalesList_Config: OList;
  StoreSalesList_Setup() {
    this.StoreSalesList_Config = {
      Id:"",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Title: "Store Sales",
      StatusType: "default",
      StartDate: this.StartTime,
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      EndDate: this.EndTime,
      Type: this._HelperService.AppConfig.ListType.Owner,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Visits",
          SystemName: "Transactions",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Purchase",
          SystemName: "Purchase",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Reward",
          SystemName: "RewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Redeem",
          SystemName: "RedeemAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Charge",
          SystemName: "CommissionAmount",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        },
        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelMerchant.Store.Dashboard
        }
      ]
    };
    this.StoreSalesList_Config = this._DataHelperService.List_Initialize(
      this.StoreSalesList_Config
    );
    this.StoreSalesList_GetData();
  }
  StoreSalesListDate_ToggleOption(event: any, Type: any) {
    this.StoreSalesList_Config.StartDate = event.start;
    this.StoreSalesList_Config.EndDate = event.end;
    this.StoreSalesList_GetData();
  }
  StoreSalesList_ToggleOption(event: any, Type: any) {
    this.StoreSalesList_Config = this._DataHelperService.List_Operations(
      this.StoreSalesList_Config,
      event,
      Type
    );
    if (this.StoreSalesList_Config.RefreshData == true) {
      this.StoreSalesList_GetData();
    }
  }
  StoreSalesList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.StoreSalesList_Config
    );
    this.StoreSalesList_Config = TConfig;
  }
  StoreSalesList_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }

  public _AccountOverview: OAccountOverview = {
    AppUsers: 0,
    CardRewardPurchaseAmount: 0,
    CashRewardPurchaseAmount: 0,
    IssuerCommissionAmount: 0,
    OtherRewardPurchaseAmount: 0,
    PurchaseAmount: 0,
    RedeemAmount: 0,
    RepeatingAppUsers: 0,
    RewardAmount: 0,
    RewardChargeAmount: 0,
    TUCPlusRewardAmount: 0,
    TUCPlusRewardChargeAmount: 0,
    TUCPlusRewardClaimedAmount: 0,
    Transactions: 0,
    UniqueAppUsers: 0,
    PosOverview: []
  };

  public _AccountOverviewNext: OAccountOverview = {
    AppUsers: 0,
    CardRewardPurchaseAmount: 0,
    CashRewardPurchaseAmount: 0,
    IssuerCommissionAmount: 0,
    OtherRewardPurchaseAmount: 0,
    PurchaseAmount: 0,
    RedeemAmount: 0,
    RepeatingAppUsers: 0,
    RewardAmount: 0,
    RewardChargeAmount: 0,
    TUCPlusRewardAmount: 0,
    TUCPlusRewardChargeAmount: 0,
    TUCPlusRewardClaimedAmount: 0,
    Transactions: 0,
    UniqueAppUsers: 0,
    PosOverview: []
  };

  public _AccountOverviewDiff: OAccountOverview = {
    AppUsers: 0,
    CardRewardPurchaseAmount: 0,
    CashRewardPurchaseAmount: 0,
    IssuerCommissionAmount: 0,
    OtherRewardPurchaseAmount: 0,
    PurchaseAmount: 0,
    RedeemAmount: 0,
    RepeatingAppUsers: 0,
    RewardAmount: 0,
    RewardChargeAmount: 0,
    TUCPlusRewardAmount: 0,
    TUCPlusRewardChargeAmount: 0,
    TUCPlusRewardClaimedAmount: 0,
    Transactions: 0,
    UniqueAppUsers: 0,
    PosOverview: []
  };
}

export class OAccountOverview {
  public AppUsers: number;
  public RepeatingAppUsers: number;
  public UniqueAppUsers: number;
  public RewardChargeAmount: number;
  public TUCPlusRewardChargeAmount: number;
  public Transactions: number;
  public PurchaseAmount: number;
  public IssuerCommissionAmount: number;
  public CashRewardPurchaseAmount: number;
  public CardRewardPurchaseAmount: number;
  public OtherRewardPurchaseAmount: number;
  public RedeemAmount: number;
  public RewardAmount: number;
  public TUCPlusRewardAmount: number;
  public TUCPlusRewardClaimedAmount: number;
  public PosOverview: any[];
}
