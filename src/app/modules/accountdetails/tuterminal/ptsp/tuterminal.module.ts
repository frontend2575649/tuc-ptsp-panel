import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';



import { TUTerminalComponent } from "./tuterminal.component";
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MainPipe } from '../../../../service/main-pipe.module';

const routes: Routes = [
    {
        path: "",
        component: TUTerminalComponent,
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/terminal/ptsp/dashboard.module#TUDashboardModule" },
            { path: "dashboard/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/terminal/ptsp/dashboard.module#TUDashboardModule" },
            { path: 'sales/saleshistory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Terminal' }, loadChildren: '../../../dashboards/terminal/transactions/transactions.module#TransactionsModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUTerminalRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUTerminalRoutingModule,
        MainPipe,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        LeafletModule
    ],
    declarations: [TUTerminalComponent]
})
export class TUTerminalModule { }
