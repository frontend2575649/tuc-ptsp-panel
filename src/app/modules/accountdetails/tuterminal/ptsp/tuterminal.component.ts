import { Component, OnInit, HostListener } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
import * as L from "leaflet";
import { tileLayer, latLng } from 'leaflet';
declare var moment: any;

import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  FilterHelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-terminal",
  templateUrl: "./tuterminal.component.html"
})
export class TUTerminalComponent implements OnInit {
  slideOpen: any = false;
  _ShowMap: boolean = false;

  public _TerminalDetails: any =
    {
      ProviderIconUrl: null,
      MerchantIconUrl: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region toogle map
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    // if(this.slideOpen){
    //   this._HelperService._MapCorrection();
    // }
  }

  myFunction() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");
  }

  DisplayDiv() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }
  //#endregion

  ngOnInit() {

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveTerminal);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
    // this._ShowMap = true;
    // this._HelperService._InitMap();
    // this._ShowMap = false;
    // this._HelperService._MapCorrection();
    // this._ShowMap = true;
    Feather.replace();
    this._HelperService.ResetDateRange();
    this.GetTerminalsDetails();
   
    this.GetOverviews();
    // this.Block();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.PosTerminal;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.FullContainer = false;
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveTerminal);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

  }
  public OverviewData: any = {
    Transactions: 0,
    InvoiceAmount: 0.0
};
  GetOverviews(){
    var pData = {
        Task: 'getaccountoverview',
        StartTime: new Date(2017, 0, 1, 0, 0, 0, 0), // new Date(2017, 0, 1, 0, 0, 0, 0),
        EndTime:   moment().add(2, 'days'), // moment().add(2, 'days'),
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        AccountKey:  this._HelperService.AppConfig.ActiveOwnerKey,
        SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
        SubAccountKey:  this._HelperService.AppConfig.ActiveReferenceKey
      
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAnalytics, pData);
    _OResponse.subscribe(
        _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
                this.OverviewData = _Response.Result as any;
                
            }
            else {
                this._HelperService.NotifyError(_Response.Message);
            }
        },
        _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);

        });


}
  clicked() {
    this._HelperService.OpenModal("BlockPos");

  }

  GoTo_Overview(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal.Dashboard, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }


  GoTo_Transactions(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal.SalesHistory, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }


  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverviewlite',
      StartTime: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  GetTerminalsDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminalDetail,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._TerminalDetails = _Response.Result;


          if (_Response.Result.StoreLatitude != undefined && _Response.Result.StoreLongitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.StoreLatitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.StoreLongitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this._HelperService._ReLocate();

          // this._DateDiff = this._HelperService._DateRangeDifference(this._TerminalDetails.StartDate, this._TerminalDetails.EndDate);
          // this._TerminalDetails.StartDateS = this._HelperService.GetDateS(
          //   this._TerminalDetails.StartDate
          // );
          this._TerminalDetails.EndDateS = this._HelperService.GetDateS(
            this._TerminalDetails.EndDate
          );
          this._TerminalDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.CreateDate
          );
          this._TerminalDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.ModifyDate
          );
          this._TerminalDetails.StatusI = this._HelperService.GetStatusIcon(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusB = this._HelperService.GetStatusBadge(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusC = this._HelperService.GetStatusColor(
            this._TerminalDetails.StatusCode
          );

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public _AccountOverview: OAccountOverview =
    {
      Stores: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,

    }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "updateterminalstatus",
      ReferenceId: this._TerminalDetails.ReferenceId,
      ReferenceKey: this._TerminalDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess(_Response.Message);
          this._HelperService.CloseModal("BlockPos");
          this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminals]);

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

