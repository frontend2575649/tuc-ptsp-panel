import { Component, OnInit, HostListener, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of, Subscription } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
declare let $ : any; 

import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon,
    OUserDetails
} from "../../../../service/service";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
@Component({
    selector: "tu-merchant",
    templateUrl: "./tumerchant.component.html"
})

export class TUMerchantComponent implements OnInit {
  public _ShowMap = false;
  public ActivationProgress = 0;
  public ActivationTotalCount = 20;
  public ActivationCompletionCount = 0;
  public AccountActivationCheckList =
    {
      IsBusinessInformationComplete: false,
      IsConfigurationComplete: false,
      IsStoreComplete: false,
      IsBusinessCategoriesSet: false,
    }
  public _Merchantdetails:any= {
      RewardPercentage: null ,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      AccountCode:null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Address:null,
      Stores:null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      IconUrl:null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
  }
    slideOpen: any = false;
    _DateDiff: any = {};
  public _DateSubscription: Subscription = null;
  @ViewChild("offCanvas") divView: ElementRef;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { 

      this._HelperService.ShowDateRange = true;
    }

    changeSlide(): void {
        this.slideOpen = !this.slideOpen;
      }

      myFunction(){
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-HideDiv");
        element.classList.remove("Hm-DisplayDiv");

      }

      DisplayDiv(){
        var element = document.getElementById("StoresHide");
        element.classList.add("Hm-DisplayDiv");
        element.classList.remove("Hm-HideDiv");
      }

      
      options: any;

      ngOnDestroy() {
        this._DateSubscription.unsubscribe();
      }
  
    ngOnInit() {
      var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
  
        // $('#collapseOne').collapse('hide');
        Feather.replace();
        var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
        this._HelperService.ResetDateRange();
        this.GetMerchantDetails();
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.PosTerminal;
        this._HelperService.ContainerHeight = window.innerHeight;
        this.FormA_EditUser_Load();
        this.FormB_EditUser_Load();
        this.FormC_EditUser_Load();
        this._HelperService.ResetDateRange();
        this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {

    });
      
    }


    GetMerchantDetails() {
      this._HelperService.IsFormProcessing = true;
      var pData = {
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPMerchant,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
      }
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, pData);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.IsFormProcessing = false;
            this._Merchantdetails = _Response.Result;
            this._Merchantdetails.IconUrl = _Response.Result.IconUrl;
            if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
              this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
              this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
            } else {
              this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
              this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
            }
            // this._HelperService._ReLocate();
  
            this._DateDiff = this._HelperService._DateRangeDifference(this._Merchantdetails.StartDate, this._Merchantdetails.EndDate);
            this._Merchantdetails.StartDateS = this._HelperService.GetDateS(
              this._Merchantdetails.StartDate
            );
            this._Merchantdetails.EndDateS = this._HelperService.GetDateS(
              this._Merchantdetails.EndDate
            );
            this._Merchantdetails.CreateDateS = this._HelperService.GetDateTimeS(
              this._Merchantdetails.CreateDate
            );
            this._Merchantdetails.ModifyDateS = this._HelperService.GetDateTimeS(
              this._Merchantdetails.ModifyDate
            );
            this._Merchantdetails.StatusI = this._HelperService.GetStatusIcon(
              this._Merchantdetails.StatusCode
            );
            this._Merchantdetails.StatusB = this._HelperService.GetStatusBadge(
              this._Merchantdetails.StatusCode
            );
            this._Merchantdetails.StatusC = this._HelperService.GetStatusColor(
              this._Merchantdetails.StatusCode
            );
  
  
  
          }
          else {
            this.toogleIsFormProcessing(false);
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        }
      );
    }
    FormA_EditUser: FormGroup;

    FormA_EditUser_Show() {
      this._HelperService.OpenModal("FormA_EditUser_Content");
    }
    FormA_EditUser_Close() {
      this._HelperService.OpenModal("FormA_EditUser_Content");
    }
    FormA_EditUser_Load() {
      this._HelperService._FileSelect_Icon_Data.Width = 128;
      this._HelperService._FileSelect_Icon_Data.Height = 128;
  
      this._HelperService._FileSelect_Poster_Data.Width = 800;
      this._HelperService._FileSelect_Poster_Data.Height = 400;
  
      this.FormA_EditUser = this._FormBuilder.group({
        OperationType: 'new',
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
        AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
        RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
        OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
        OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
        DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
        Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
        ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
        EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
        RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])]
      });
    }
    FormA_EditUser_Clear() {
      this.FormA_EditUser.reset();
      this._HelperService._FileSelect_Icon_Reset();
      this._HelperService._FileSelect_Poster_Reset();
      this.FormA_EditUser_Load();
      this._HelperService.GetRandomNumber();
      this._HelperService.GeneratePassoword();
    }
    FormA_EditUser_Process(_FormValue: any) {
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V2.System,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this.toogleIsFormProcessing(false);
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Account updated successfully");
            if (_FormValue.OperationType == "close") {
              this.FormA_EditUser_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this.toogleIsFormProcessing(false);
          this._HelperService.HandleException(_Error);
        }
      );
    }
  
  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,

      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }
  FormB_EditUser_Clear() {
    this.FormB_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormB_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  FormB_EditUser_Process(_FormValue: any) {
    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "close") {
            this.FormB_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }


  FormC_EditUser: FormGroup;
  FormC_EditUser_Address: string = null;
  FormC_EditUser_Latitude: number = 0;
  FormC_EditUser_Longitude: number = 0;


  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  FormC_EditUser_PlaceMarkerClick(event) {
    this.FormC_EditUser_Latitude = event.coords.lat;
    this.FormC_EditUser_Longitude = event.coords.lng;
  }
  public FormC_EditUser_AddressChange(address: Address) {
    this.FormC_EditUser_Latitude = address.geometry.location.lat();
    this.FormC_EditUser_Longitude = address.geometry.location.lng();
    this.FormC_EditUser_Address = address.formatted_address;
  }

  FormC_EditUser_Show() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }
  FormC_EditUser_Close() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }
  FormC_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormC_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Latitude: 0,
      Longitude: 0,
      Address: [null, Validators.compose([Validators.required, Validators.maxLength(256), Validators.minLength(2)])],
      Configuration: [],
    });
  }
  FormC_EditUser_Clear() {
    this.FormC_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.FormC_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  FormC_EditUser_Process(_FormValue: any) {
    _FormValue.Longitude = this.FormC_EditUser_Longitude
    _FormValue.Latitude = this.FormC_EditUser_Latitude

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "close") {
            this.FormC_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public Get_UserAccountDetails(ShowHeader: boolean) {
    this.toogleIsFormProcessing(true);
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
      Reference: this._HelperService.GetSearchConditionStrict("", "ReferenceKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, "="
      )
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._UserAccount = _Response.Result as OUserDetails;

          this._UserAccount.DateOfBirth = this._HelperService.GetDateS(this._UserAccount.DateOfBirth);
          this._UserAccount.LastLoginDateS = this._HelperService.GetDateTimeS(this._UserAccount.LastLoginDate);
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._UserAccount.CreateDate);
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._UserAccount.ModifyDate);
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._UserAccount.StatusCode);
          this.toogleIsFormProcessing(false);

          this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          this.FormC_EditUser_Longitude = this._UserAccount.Longitude;
          setTimeout(() => {
            this.ToggleStoreSelect = false;

          }, 300);
          if (this._UserAccount.SubAccounts > 0) {
            this.AccountActivationCheckList.IsStoreComplete = true;
          }
          this.AccountActivationCheckList.IsBusinessInformationComplete = true;
          if (
            this._UserAccount.DisplayName == null
            || this._UserAccount.DisplayName == ''
            || this._UserAccount.Name == null
            || this._UserAccount.Name == ''

            || this._UserAccount.ContactNumber == null
            || this._UserAccount.ContactNumber == ''

            || this._UserAccount.EmailAddress == null
            || this._UserAccount.EmailAddress == ''

            || this._UserAccount.AccountOperationTypeCode == null
            || this._UserAccount.AccountOperationTypeCode == ''

            || this._UserAccount.IconUrl == null
            || this._UserAccount.IconUrl == ''
            || this._UserAccount.IconUrl.endsWith('defaulticon.png')
            || this._UserAccount.Description == null
            || this._UserAccount.Description == ''

            || this._UserAccount.Address == null
            || this._UserAccount.Address == ''

            || this._UserAccount.FirstName == null
            || this._UserAccount.FirstName == ''

            || this._UserAccount.LastName == null
            || this._UserAccount.LastName == ''

            || this._UserAccount.MobileNumber == null
            || this._UserAccount.MobileNumber == ''

            || this._UserAccount.EmailAddress == null
            || this._UserAccount.EmailAddress == ''

            || this._UserAccount.SecondaryEmailAddress == null
            || this._UserAccount.SecondaryEmailAddress == ''
          ) {
            this.AccountActivationCheckList.IsBusinessInformationComplete = false;
          }
          this.RefreshActivationCheckList(this._UserAccount);
        } else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public RefreshActivationCheckList(_UserAccount: OUserDetails) {

    if (_UserAccount.DisplayName != undefined) {
      var PData =
      {
        Name: 'Display Name',
        Value: _UserAccount.DisplayName,
        IsComplete: true,
      }
    }
    else {
      var PData =
      {
        Name: 'Display Name',
        Value: '',
        IsComplete: false,
      }
    }

  }
  public ToggleStoreSelect: boolean = false;
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
  }
  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };
    public _AccountOverview: OAccountOverview =

    {
        TerminalStatus :0,
        Others: 0,
        CardTransPerc: 0,
        CashTransPerc: 0,
        ActiveMerchants: 0,
        ActiveMerchantsDiff: 0,
        ActiveTerminals: 0,
        ActiveTerminalsDiff: 0,
        CashTransactionAmount: 0,
        CardTransactionsAmount: 0,
        CardRewardPurchaseAmount: 0,
        CardRewardPurchaseAmountDiff: 0,
        CashRewardPurchaseAmount: 0,
        CashRewardPurchaseAmountDiff: 0,
        Merchants: 0,
        PurchaseAmount: 0,
        PurchaseAmountDiff: 0,
        Terminals: 0,
        Transactions: 0,
        TransactionsDiff: 0,
        TotalTransactions: 0,
        TotalSale: 0,
        AverageTransactions: 0,
        AverageTransactionAmount: 0
    }


    

}

export class OAccountOverview {
  public TerminalStatus : number;
  public Others: number;
  public CardTransPerc: number;
  public CashTransPerc: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public AverageTransactions: number;
  public AverageTransactionAmount: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashTransactionAmount: number;
  public CardTransactionsAmount: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
  public TotalTransactions: number;
  public TotalSale: number;
}