import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from "agm-overlays"

import { TUStoreComponent } from "./tustore.component";
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MainPipe } from '../../../service/main-pipe.module';

const routes: Routes = [
  {
    path: "",
    component: TUStoreComponent,
    children: [
      {
        path: "/:referencekey/:referenceid",
        data: {
          permission: "getmerchant",
          PageName: "System.Menu.Store" ,
          menuoperations: "ManageMerchant",
          accounttypecode: "store"
        },
        loadChildren:
          "../../dashboards/store/dashboard/dashboard.module#DashboardModule"
      },
      { path: "dashboard/:referencekey/:referenceid", data: { permission: "getappuser",  PageName: "System.Menu.Store" , menuoperations: "ManageAppUser", accounttypecode: "merchant" }, loadChildren: "../../dashboards/store/dashboard/dashboard.module#DashboardModule" },

      { path: "terminals/:referencekey/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Store" }, loadChildren: "../../dashboards/store/terminals/terminals.module#TerminalsModule" },
      // { path: 'customers/:referencekey/:referenceid', data: { 'permission': 'rewardhistory', PageName: 'System.Menu.Stores' }, loadChildren: '../../../modules/accounts/tuappusers/store/tuappusers.module#TUAppUsersModule' },

      // { path: 'rewardhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardsHistory' }, loadChildren: '../../../modules/transactions/tureward/store/tureward.module#TURewardsModule' },
      // { path: 'rewardclaimhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardClaimHistory' }, loadChildren: '../../../modules/transactions/turewardclaim/store/turewardclaim.module#TURewardsClaimModule' },
      // { path: 'redeemhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RedeemHistory' }, loadChildren: '../../../modules/transactions/turedeem/store/turedeem.module#TURedeemModule' },
      { path: 'transactions/:referencekey/:referenceid', data: { 'permission': 'merchants', PageName: 'System.Menu.Store' }, loadChildren: '../../dashboards/store/transactions/transactions.module#TransactionsModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUStoreRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUStoreRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    AgmOverlays,
    GooglePlaceModule,
    MainPipe,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    }),
    LeafletModule,
  ],
  declarations: [TUStoreComponent]
})
export class TUStoreModule { }
