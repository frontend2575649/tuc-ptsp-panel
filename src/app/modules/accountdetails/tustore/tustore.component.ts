import { Component, OnInit, HostListener, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
declare let $: any;

import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  FilterHelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  OUserDetails
} from "../../../service/service";
import swal from "sweetalert2";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: "tu-store",
  templateUrl: "./tustore.component.html"
})
export class TUStoreComponent implements OnInit {
  _CurrentAddress: any = {};
  public _ShowMap = false;
  slideOpen: any = false;
  @ViewChild("offCanvas") divView: ElementRef;
  public _StoreDetails:any =
  {
    OwnerKey:null,
    ReferenceId: null,
    ReferenceKey: null,
    TypeCode: null,
    TypeName: null,
    SubTypeCode: null,
    SubTypeName: null,
    UserAccountKey: null,
    UserAccountDisplayName: null,
    Name: null,
    Description: null,
    StartDate: null,
    StartDateS: null,
    EndDate: null,
    EndDateS: null,
    SubTypeValue: null,
    MinimumInvoiceAmount: null,
    MaximumInvoiceAmount: null,
    MinimumRewardAmount: null,
    MaximumRewardAmount: null,
    ManagerKey: null,
    ManagerDisplayName: null,
    SmsText: null,
    Comment: null,
    CreateDate: null,
    CreatedByKey: null,
    CreatedByDisplayName: null,
    ModifyDate: null,
    ModifyByKey: null,
    ModifyByDisplayName: null,
    StatusId: null,
    StatusCode: null,
    StatusName: null,

    CreateDateS: null,
    ModifyDateS: null,
    StatusI: null,
    StatusB: null,
    StatusC: null,
  }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService:FilterHelperService,
  ) { }

  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
  }

  myFunction() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");

  }

  DisplayDiv() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }

  ngOnInit() {
    $('#collapseOne').collapse('hide'); 
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveStore);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }
    
    this._HelperService.Get_UserAccountDetails(false);

    Feather.replace();
    this.Form_EditUser_Load();
    this.GetBranches_List();
    this.GetMangers_List();
    this.GetSoresDetails();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.PTSPID;
    this._HelperService.ContainerHeight = window.innerHeight;
    
  }


  GoTo_Overview(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Store.Dashboard, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }

  GoTo_Transactions(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Store.SalesHistory, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }

  GoTo_Terminals(): void {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Store.Terminals, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId
    ]);
  }

  Form_EditUser: FormGroup;
  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;

  @ViewChild('placesStore') placesStore: GooglePlaceDirective;
  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
  }
  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;
    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      DisplayName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
      EmailAddress: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      Address: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(256)])],
      Latitude: 0,
      Longitude: 0,
      RegionKey: '',
      RegionAreaKey: '',
      CityKey: '',
      CityAreaKey: '',
      PostalCodeKey: '',
      CountValue: 0,
      AverageValue: 0,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      Configuration: [],
    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
      _FormValue.Latitude =  this.Form_EditUser_Latitude;
     _FormValue.Longitude =  this.Form_EditUser_Longitude;
    _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account created successfully");
          this.Form_EditUser_Clear();
          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        }
      ]
    }
    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        }
      ]
    }
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  

  
  GetSoresDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPStore,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId :this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._StoreDetails = _Response.Result;
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          this._StoreDetails.EndDateS = this._HelperService.GetDateS(
            this._StoreDetails.EndDate
          );
          this._StoreDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._StoreDetails.CreateDate
          );
          this._StoreDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._StoreDetails.ModifyDate
          );
          this._StoreDetails.StatusI = this._HelperService.GetStatusIcon(
            this._StoreDetails.StatusCode
          );
          this._StoreDetails.StatusB = this._HelperService.GetStatusBadge(
            this._StoreDetails.StatusCode
          );
          this._StoreDetails.StatusC = this._HelperService.GetStatusColor(
            this._StoreDetails.StatusCode
          );



        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
