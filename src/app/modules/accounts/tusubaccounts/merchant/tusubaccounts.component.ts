import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
    selector: "tu-tusubaccounts",
    templateUrl: "./tusubaccounts.component.html"
})
export class TUSubAccountsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.UserAccounts_Setup();
                this.Form_AddUser_Load();
                this.Form_UpdateUser_Load();
                this._HelperService.GetRandomNumber();
                this._HelperService.GeneratePassoword();
            }
        });


    }

    public UserAccounts_Config: OList;
    UserAccounts_Setup() {
        this.UserAccounts_Config = {
            Id:"",
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            Title: "Available Accounts",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '='),
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: "Name",
                    SystemName: "Name",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Mobile Number",
                    SystemName: "MobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "UserName",
                    SystemName: "UserName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Role",
                    SystemName: "RoleName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Last Login",
                    SystemName: "LastLoginDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                }
            ]
        };
        this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(
            this.UserAccounts_Config.SearchBaseCondition,
            "AccountTypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.AccountType.MerchantSubAccount,
            "="
        );
        this.UserAccounts_Config = this._DataHelperService.List_Initialize(
            this.UserAccounts_Config
        );
        this.UserAccounts_GetData();
    }
    UserAccounts_ToggleOption(event: any, Type: any) {
        this.UserAccounts_Config = this._DataHelperService.List_Operations(
            this.UserAccounts_Config,
            event,
            Type
        );
        if (this.UserAccounts_Config.RefreshData == true) {
            this.UserAccounts_GetData();
        }
    }
    UserAccounts_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.UserAccounts_Config
        );
        this.UserAccounts_Config = TConfig;
    }
    UserAccounts_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this._HelperService.RandomPassword = null;
        this._HelperService.Get_UserAccountDetails(true);
        this.Form_UpdateUser_Show();
    }


    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: ["Name asc"],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "StatusCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.Status.Active
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(
            _Select.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            "hcore.role",
            "="
        );
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(
            _Select
        ) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_AddUser.patchValue({
            RoleKey: event.value
        });
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Show() {
        this._HelperService.OpenModal("Form_AddUser_Content");
    }
    Form_AddUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.System.AdminUsers
        // ]);
        this._HelperService.OpenModal("Form_AddUser_Content");
    }
    Form_AddUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,

            AccountTypeCode: this._HelperService.AppConfig.AccountType
                .MerchantSubAccount,
            AccountOperationTypeCode: this._HelperService.AppConfig
                .AccountOperationType.Online,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource
                .System,
            OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
            RoleKey: ["7", Validators.required],
            UserName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(256)
                ])
            ],
            Password: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(20),
                    Validators.pattern(
                        this._HelperService.AppConfig.ValidatorRegex.Password
                    )
                ])
            ],
            SecondaryPassword: null,

            FirstName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            LastName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(10),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,
            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Owners: [],
            Configuration: []
        });
    }
    Form_AddUser_Clear() {
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        _FormValue.DisplayName = _FormValue.FirstName;
        _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V2.System,
            _FormValue
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Account created successfully");
                    this.Form_AddUser_Clear();
                    if (_FormValue.OperationType == "close") {
                        this.Form_AddUser_Close();
                    }
                    this.UserAccounts_Setup();
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    public isReadOnly = true;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        } else {
            this.isReadOnly = true;
        }
    }
    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Show() {
        this._HelperService.OpenModal("Form_UpdateUser_Content");
    }
    Form_UpdateUser_Close() {
        this._HelperService.CloseModal("Form_UpdateUser_Content");
    }
    Form_UpdateUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            RoleKey: ["7", Validators.required],
            UserName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(256)
                ])
            ],
            Password: null,
            AccessPin: null,
            DisplayName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(25)
                ])
            ],
            FirstName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            LastName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(10),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            StatusCode: this._HelperService.AppConfig.Status.Inactive,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: []
        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: "top",
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: "Click on udpate button to confirm changes",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.System,
                    _FormValue
                );
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Details updated successfully");
                            this.Form_UpdateUser_Close();
                            if (_FormValue.OperationType == "close") {
                            }
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
}
