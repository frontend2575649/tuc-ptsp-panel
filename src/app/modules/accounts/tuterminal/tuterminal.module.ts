import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from "@agm/core";

import { TUTerminalComponent } from "./tuterminal.component";
const routes: Routes = [
  {
    path: "",
    component: TUTerminalComponent,
    children: [
      {
        path: "/:referencekey",
        data: {
          permission: "getappuser",
          menuoperations: "ManageAppUser",
          accounttypecode: "posterminal"
        },
        loadChildren:
          "../tuterminal/dashboard/tudashboard.module#TUTerminalInfoDashboardModule"
      },
      {
        path: "dashboard/:referencekey",
        data: {
          permission: "getappuser",
          menuoperations: "ManageAppUser",
          accounttypecode: "posterminal"
        },
        loadChildren:
          "../tuterminal/dashboard/tudashboard.module#TUTerminalInfoDashboardModule"
      },



      // { path: 'rewardhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardsHistory' }, loadChildren: '../../../modules/transactions/tureward/terminal/tureward.module#TURewardsModule' },
      // { path: 'rewardclaimhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardClaimHistory' }, loadChildren: '../../../modules/transactions/turewardclaim/terminal/turewardclaim.module#TURewardsClaimModule' },
      // { path: 'redeemhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RedeemHistory' }, loadChildren: '../../../modules/transactions/turedeem/terminal/turedeem.module#TURedeemModule' },
      // { path: 'saleshistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.SalesHistory' }, loadChildren: '../../../modules/transactions/tusale/terminal/tusale.module#TUSaleModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUTerminalRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUTerminalRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    GooglePlaceModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo"
    })
  ],
  declarations: [TUTerminalComponent]
})
export class TUTerminalModule { }
