import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, FilterHelperService } from '../../../service/service';
import swal from 'sweetalert2';
import * as Feather from "feather-icons";
declare var $: any;

@Component({
    selector: 'hc-apps',
    templateUrl: './hcapps.component.html',
})
export class HCAppsComponent implements OnInit {
    @HostListener('window:resize', ['$event'])
    public ContainerHeight: any;
    onResize(event) {
        this.ContainerHeight = window.innerHeight;
    }
    public ResetFilterControls: boolean = true;

    _AdminDetails: any = {};
    isLoaded: boolean = false;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
        this._HelperService.AppConfig.ActiveReferenceKey = null;
    }
    ngOnInit() {
        // this.ContainerHeight = window.innerHeight;
        // this._HelperService.AppConfig.ShowHeader = false;
        // this._HelperService.FullContainer = true;
        Feather.replace();

        var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveApp);
        if (StorageDetails != null) {
            this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
            this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId
            this._HelperService.AppConfig.ActiveAccountId = StorageDetails.AccountId;
            this._HelperService.AppConfig.ActiveAccountKey = StorageDetails.AccountKey;

            this.ListApps_GetDetails();
            this.Form_AddApp_Load();
            this.Form_EditApp_Load();
            this.Form_EditAppKey_Load();
            // this.ListApps_Setup();
            // this.ListAppUsage_Setup();
            this.GetAppAddOs_List();
            // this.GetAddAppUserAccounts_List();
            this.Form_Add_Load();
            this.Form_Edit_Load();
            // this.ListAppVersions_Setup();
      
        } else {
          this._Router.navigate([
            this._HelperService.AppConfig.Pages.System.NotFound
          ]);
        }
    


        // this._ActivatedRoute.params.subscribe((params: Params) => {

 

        // });


    }

    //#region StoreFilter

    public ListAppUsage_Filter_Stores_Option: Select2Options;
    public ListAppUsage_Filter_Stores_Selected = null;
    ListAppUsage_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            // AccountKey: this._HelperService.UserAccount.AccountKey,
            // AccountId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
            ],
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.ListAppUsage_Filter_Stores_Option = {
            placeholder: "Select Store",
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    ListAppUsage_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.ListAppUsage_Config,
            this._HelperService.AppConfig.OtherFilters.Merchant.Owner
        );

        this.StoreEventProcessing(event);

    }

    StoreEventProcessing(event: any): void {
        if (event.value == this.ListAppUsage_Filter_Stores_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict(
                "",
                "StoreReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this.ListAppUsage_Filter_Stores_Selected,
                "="
            );
            this.ListAppUsage_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
                SearchCase,
                this.ListAppUsage_Config.SearchBaseConditions
            );
            this.ListAppUsage_Filter_Stores_Selected = null;
        } else if (event.value != this.ListAppUsage_Filter_Stores_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict(
                "",
                "StoreReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this.ListAppUsage_Filter_Stores_Selected,
                "="
            );
            this.ListAppUsage_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
                SearchCase,
                this.ListAppUsage_Config.SearchBaseConditions
            );
            this.ListAppUsage_Filter_Stores_Selected = event.data[0].ReferenceKey;
            this.ListAppUsage_Config.SearchBaseConditions.push(
                this._HelperService.GetSearchConditionStrict(
                    "",
                    "StoreReferenceKey",
                    this._HelperService.AppConfig.DataType.Text,
                    this.ListAppUsage_Filter_Stores_Selected,
                    "="
                )
            );
        }

        this.ListAppUsage_ToggleOption(
            null,
            this._HelperService.AppConfig.ListToggleOption.ResetOffset
        );
    }

    //#endregion

    public ListApps_Config: OList;
    public Valuelog: number;
    ListApps_Setup() {
        this.ListApps_Config =
        {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetApps,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.System,
            Title: 'Categories',
            StatusType: 'default',
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.App, "="),
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Os',
                    SystemName: 'HelperName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Versions',
                    SystemName: 'SubItemsCount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Requests',
                    SystemName: 'Count',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                // {
                //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
                //     SystemName: 'ModifyDate',
                //     DataType: this._HelperService.AppConfig.DataType.Date,
                //     Class: 'td-date',
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null,
                // },
                // {
                //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
                //     SystemName: 'ModifyByDisplayName',
                //     DataType: this._HelperService.AppConfig.DataType.Text,
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null,
                // },
            ]
        }
        this.ListApps_Config = this._DataHelperService.List_Initialize(this.ListApps_Config);
        this.ListApps_GetData();
    }
    ListApps_ToggleOption(event: any, Type: any) {
        this.ListApps_Config = this._DataHelperService.List_Operations(this.ListApps_Config, event, Type);
        if (this.ListApps_Config.RefreshData == true) {
            this.ListApps_GetData();
        }
    }

    timeout = null;
    ListApps_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {

            this.ListApps_Config = this._DataHelperService.List_Operations(this.ListApps_Config, event, Type);
            if (this.ListApps_Config.RefreshData == true) {
                this.ListApps_GetData();
            }

        }, this._HelperService.AppConfig.SearchInputDelay);

    }

    ListApps_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.ListApps_Config);
        this.ListApps_Config = TConfig;
    }
    ListApps_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId
        this._HelperService.AppConfig.ActiveAccountKey = ReferenceData.AccountKey
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.AccountId
        this.Valuelog = ReferenceData.LogRequest;

        this.ListApps_GetDetails();
    }
    Accountdisplay: boolean = false;
    ListApps_GetDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetApp,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,


            // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        this.isLoaded = false;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                this.isLoaded = true;

                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService._CoreCommon = _Response.Result as OCoreCommon;

                    if (_Response.Result.LogRequest == 1) {
                        this.SelectValue = true;
                    }
                    else {
                        this.SelectValue = false;

                    }
                    
                    this._HelperService._CoreCommon.LastRequestDateTime = this._HelperService.GetDateTimeS(this._HelperService._CoreCommon.LastRequestDateTime);

                    this._HelperService._CoreCommon.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._CoreCommon.CreateDate);
                    this._HelperService._CoreCommon.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._CoreCommon.ModifyDate);
                    this._HelperService._CoreCommon.StatusI = this._HelperService.GetStatusIcon(this._HelperService._CoreCommon.StatusCode);
                    this.GetAppUpdateOs_Option.placeholder = this._HelperService._CoreCommon.HelperName;
                    this._HelperService._CoreCommon.StatusI = this._HelperService.GetStatusIcon(this._HelperService._CoreCommon.StatusCode);
                    this._HelperService._CoreCommon.StatusB = this._HelperService.GetStatusBadge(this._HelperService._CoreCommon.StatusCode);
                    this._HelperService._CoreCommon.StatusC = this._HelperService.GetStatusColor(this._HelperService._CoreCommon.StatusCode);
                    this.Accountdisplay = false;
                    this._ChangeDetectorRef.detectChanges();

                    this.GetUpdateAppUserAccounts_Option.placeholder = this._HelperService._CoreCommon.AccountDisplayName;
                    this.Form_EditAppKey.controls['LogRequest'].setValue(_Response.Result.LogRequest);

                    this.Accountdisplay = true;
                    this.GetAddAppUserAccounts_List();
                    this._ChangeDetectorRef.detectChanges();
                    this.ListAppUsage_Setup();
                    this.ListAppVersions_Setup();
                    // this._HelperService.OpenModal('Form_EditApp_Content');
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    LogRequestValue: number = 0;
    SelectValue: boolean = false;
    CheckValue() {
        // console.log("value",this.SelectValue)
        this.SelectValue = !(this.SelectValue);
        if (this.SelectValue == true) {
            this.LogRequestValue = 1
            this.Form_AddApp.controls['LogRequest'].setValue(this.LogRequestValue);
            this.Form_EditAppKey.controls['LogRequest'].setValue(this.LogRequestValue);


        }
        else {
            this.LogRequestValue = 0
            this.Form_AddApp.controls['LogRequest'].setValue(this.LogRequestValue);
            this.Form_EditAppKey.controls['LogRequest'].setValue(this.LogRequestValue);



        }

    }
    Form_AddApp: FormGroup;
    Form_AddApp_Show() {
        this.Form_AddApp_Clear();
        this._HelperService.OpenModal('Form_AddApp_Content');
    }
    Form_AddApp_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_AddApp_Content');
    }
    Form_AddApp_Load() {
        this.Form_AddApp = this._FormBuilder.group({
            // AllowDuplicateName: false,
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveApp,
            // TypeCode: this._HelperService.AppConfig.HelperTypes.App,
            Name: [null, Validators.required],
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            // Description: [null, Validators.required],
            // LogRequest: [null, Validators.required],
            // UserAccountKey: null,
            // Value: this._HelperService.GenerateGuid(),
            LogRequest: 0,
            StatusCode: 'default.inactive',
        });
    }
    Form_AddApp_Clear() {
        this.Form_AddApp.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddApp_Load();
    }
    Form_AddApp_Process(_FormValue: any) {
        this._HelperService.ToggleField = true;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, _FormValue);

        _OResponse.subscribe(
            _Response => {
                this._HelperService.ToggleField = false;
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.ListApps_Setup();
                    this.Form_AddApp_Clear();
                    if (_FormValue.OperationType == 'edit') {
                        this.Form_AddApp_Close();
                        this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
                        this.ListApps_GetDetails();
                        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_AddApp_Close();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


  


    Form_EditApp: FormGroup;
    Form_EditApp_Show() {
        // this._HelperService.OpenModal('Form_EditApp_Content');
        this._HelperService.OpenModal('Form_Edit_Content');

    }
    Form_EditApp_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_EditApp_Content');
        this.ListApps_GetData();
    }
    Form_EditApp_Load() {

        this.Form_EditApp = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Core.UpdateApp,
            AllowDuplicateName: false,
            OperationType: 'new',
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            Name: [null, Validators.required],
            // Description: [null, Validators.required],
            // HelperCode: null,
            // UserAccountKey: null,'
            AccountKey: [null, Validators.required],
            AccountId: [null, Validators.required],
            LogRequest: 0,
            StatusCode: 'default.inactive',
        });
    }
    Form_EditApp_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_EditApp_Process1(_FormValue: any) {
        // _FormValue.AuthPin = result.value;

        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.Form_EditApp_Clear();
                    if (_FormValue.OperationType == 'close') {
                        this.Form_EditApp_Close();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });

    }

    Form_EditAppKey: FormGroup;
    Form_EditAppKey_Show() {
        this._HelperService.OpenModal('Form_EditAppKey_Content');
    }
    Form_EditAppKey_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_EditAppKey_Content');
    }
    Form_EditAppKey_Load() {
        this.Form_EditAppKey = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateApp,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            Name: [null, Validators.required],
            LogRequest: 0,
            StatusCode: 'default.inactive',
        });
    }
    Form_EditAppKey_Clear() {
        this.Form_EditAppKey.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_EditAppKey_Load();
    }
    Form_EditAppKey_Process(_FormValue: any) {
        this._HelperService.ToggleField = true;
        this._HelperService.IsFormProcessing = true;
        _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
        _FormValue.ReferenceId = this._HelperService.AppConfig.ActiveReferenceId;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, _FormValue);

        _OResponse.subscribe(
            _Response => {
                this._HelperService.ToggleField = false;
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.ListApps_Setup();
                    this.Form_EditAppKey_Clear();
                    if (_FormValue.OperationType == 'edit') {
                        this.Form_EditAppKey_Close();
                        // this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
                        this.ListApps_GetDetails();
                        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_EditAppKey_Close();
                    }
                    this.ListApps_GetDetails();
                    this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ResetKey(){
        this._HelperService.CloseModal('exampleModal');
        swal({
            title: this._HelperService.AppConfig.CommonResource.ResetKey,
            text: this._HelperService.AppConfig.CommonResource.ResetKeyHelp,
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            // input: 'password',
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            // inputAttributes: {
            //     autocapitalize: 'off',
            //     autocorrect: 'off',
            //     maxLength: "4",
            //     minLength: "4"
            // },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var p1Data = {
                    Task: this._HelperService.AppConfig.Api.Core.ResetAppKey,
                    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
                    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
                    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
                    // AuthPin: result.value
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, p1Data);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.ListApps_GetDetails();
                            this.Showkey();
                            // this._Router.navigate([
                            //     this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Administration.Apps]);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        }); 
    }



    Delete_Confirm() {
        swal({
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteApp,
                    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
                    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
                    AccountId: this._HelperService.AppConfig.ActiveOwnerId,


                    AuthPin: result.value
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._Router.navigate([
                                this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Apps]);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }

    Delete_AppVersionConfirm() {
        swal({
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteCoreCommon,
                    ReferenceKey: this._HelperService._SubCoreCommon.ReferenceKey,
                    AuthPin: result.value

                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this.Form_Edit_Close();
                            this.ListAppVersions_GetData();
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._HelperService.CloseModal('ModalDetails');
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {

                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }

    Showkey(){
        this._HelperService.OpenModal('exampleModal');
    }

    Form_Add: FormGroup;
    Form_Add_Show() {
        this.Form_Add_Clear();
        this._HelperService.OpenModal('Form_Add_Content');
    }
    Form_Add_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_Add_Content');
    }
    Form_Add_Load() {
        this.Form_Add = this._FormBuilder.group({
            AllowDuplicateName: false,
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveCoreCommon,
            TypeCode: this._HelperService.AppConfig.HelperTypes.AppVersion,
            ParentKey: this._HelperService._CoreCommon.ReferenceKey,
            Name: [null, Validators.required],
            Value: this._HelperService.GenerateGuid(),
            Sequence: 0,
            StatusCode: 'default.inactive',
        });
    }
    Form_Add_Clear() {
        this.Form_Add.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_Add_Load();
    }
    Form_Add_Process(_FormValue: any) {
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                    this.ListAppVersions_Setup();
                    this.Form_Add_Clear();
                    if (_FormValue.OperationType == 'edit') {
                        this.Form_Add_Close();
                        this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result.ReferenceKey;
                        //  this.TList_GetDetails();
                        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.Form_Add_Close();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    Form_Edit: FormGroup;
    Form_Edit_Show() {
        // this._HelperService.OpenModal('Form_Edit_Content');
    }
    Form_Edit_Close() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
        this._HelperService.CloseModal('Form_Edit_Content');
        this.ListAppVersions_GetData();
    }
    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            AllowDuplicateName: false,
            OperationType: 'new',
            ReferenceKey: this._HelperService._CoreParameter.ReferenceKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateCoreCommon,
            Name: [null, Validators.required],
            StatusCode: 'default.inactive',
        });
    }
    Form_Edit_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_Edit_Process(_FormValue: any) {
        swal({
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            showCancelButton: true,
            position: this._HelperService.AppConfig.Alert_Position,
            animation: this._HelperService.AppConfig.Alert_AllowAnimation,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
            allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService._SubCoreCommon.ReferenceKey;
                _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.Form_Edit_Clear();
                            if (_FormValue.OperationType == 'close') {
                                this.Form_Edit_Close();
                            }
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }


    public GetAppAddOs_Option: Select2Options;
    public GetAppUpdateOs_Option: Select2Options;
    GetAppAddOs_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpers,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'SystemName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'ParentCode', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.Os, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAppAddOs_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
        this.GetAppUpdateOs_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAppAddOs_ListChange(event: any) {
        this.Form_AddApp.patchValue(
            {
                HelperCode: event.value
            }
        );
    }

    public GetAddAppUserAccounts_Option: Select2Options;
    public GetUpdateAppUserAccounts_Option: Select2Options;
    GetAddAppUserAccounts_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                // {
                //     SystemName: 'AccountTypeName',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     Id: false,
                //     Text: true,
                // },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.AccountType.AppUser, '!=');
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.Status.Active, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAddAppUserAccounts_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
        this.GetUpdateAppUserAccounts_Option = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAddAppUserAccounts_ListChange(event: any) {
        this.Form_AddApp.patchValue(
            {
                // UserAccountKey: event.value
                AccountId: event.data[0].ReferenceId,
                AccountKey: event.data[0].ReferenceKey

            }
        );
    }
    GetUpdateAppUserAccounts_ListChange(event: any) {
        // console.log("event",event)
        // this.Form_EditApp.patchValue(
        //     {
        //         AccountId: event.data[0].ReferenceId,
        //         AccountKey: event.data[0].ReferenceKey
        //     }
        // );
    }

    public ListAppVersions_Config: OList;
    ListAppVersions_Setup() {
        this.ListAppVersions_Config =
        {
            Id: null,
            Sort: null,
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            Title: 'Versions',
            StatusType: 'default',
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "TypeCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.HelperTypes.AppVersion, "="),
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Requests',
                    SystemName: 'Count',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
                    SystemName: 'ModifyDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                // {
                //     DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
                //     SystemName: 'ModifyByDisplayName',
                //     DataType: this._HelperService.AppConfig.DataType.Text,
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null,
                // },
            ]
        }
        this.ListAppVersions_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.ListAppVersions_Config.SearchBaseCondition, "ParentKey", this._HelperService.AppConfig.DataType.Text, this._HelperService._CoreCommon.ReferenceKey, "="),
            this.ListAppVersions_Config = this._DataHelperService.List_Initialize(this.ListAppVersions_Config);
        if (this._HelperService._CoreCommon.ReferenceKey != undefined) {
            this.ListAppVersions_GetData();
        }
    }
    ListAppVersions_ToggleOption(event: any, Type: any) {
        this.ListAppVersions_Config = this._DataHelperService.List_Operations(this.ListAppVersions_Config, event, Type);
        if (this.ListAppVersions_Config.RefreshData == true) {
            this.ListAppVersions_GetData();
        }
    }
    timeoutV = null;
    ListAppVersions_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeoutV);

        this.timeoutV = setTimeout(() => {

            this.ListAppVersions_Config = this._DataHelperService.List_Operations(this.ListAppVersions_Config, event, Type);
            if (this.ListAppVersions_Config.RefreshData == true) {
                this.ListAppVersions_GetData();
            }

        }, this._HelperService.AppConfig.SearchInputDelay);

    }

    ListAppVersions_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.ListAppVersions_Config);
        this.ListAppVersions_Config = TConfig;
    }
    ListAppVersions_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService._SubCoreCommon.ReferenceKey = ReferenceKey;
        // this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this.ListAppVersions_GetDetails();
    }
    ListAppVersions_GetDetails() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommon,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService._SubCoreCommon.ReferenceKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService._SubCoreCommon = _Response.Result as OCoreCommon;
                    this._HelperService._SubCoreCommon.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._SubCoreCommon.CreateDate);
                    this._HelperService._SubCoreCommon.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._SubCoreCommon.ModifyDate);
                    this._HelperService._SubCoreCommon.StatusI = this._HelperService.GetStatusIcon(this._HelperService._SubCoreCommon.StatusCode);
                    this._HelperService.OpenModal('Form_Edit_Content');
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public ListAppUsage_Config: OList;
    ListAppUsage_Setup() {

        this.ListAppUsage_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.Core.GetRequestHistory,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.System,
            Title: 'Core Usage',
            StatusType: 'default',
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "AppKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, "="),
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Version',
                    SystemName: 'AppVersionName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'User',
                    SystemName: 'UserAccountDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Request On',
                    SystemName: 'RequestTime',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Response On',
                    SystemName: 'ResponseTime',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Time(sec)',
                    SystemName: 'ProcessingTime',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
            ],
            Sort: {
                SortDefaultName: null,
                SortDefaultColumn: 'ResponseTime',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            DefaultSortExpression: 'ResponseTime desc'
        }

        this.ListAppUsage_Config = this._DataHelperService.List_Initialize(this.ListAppUsage_Config);

        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Usage,
            this.ListAppUsage_Config
        );
        if (this._HelperService._CoreCommon.ReferenceKey != undefined) {
            this.ListAppUsage_GetData();
        }
    }
    ListAppUsage_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.ListAppUsage_Config.Sort.SortOptions.length; index++) {
                const element = this.ListAppUsage_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.ListAppUsage_Config
        );

        this.ListAppUsage_Config = this._DataHelperService.List_Operations(
            this.ListAppUsage_Config,
            event,
            Type
        );

        if (
            (this.ListAppUsage_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.ListAppUsage_GetData();
        }
    }
    timeoutU = null;
    ListAppUsage_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeoutU);

        this.timeoutU = setTimeout(() => {
            if (event != null) {
                for (let index = 0; index < this.ListAppUsage_Config.Sort.SortOptions.length; index++) {
                    const element = this.ListAppUsage_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }
                }
            }

            this._HelperService.Update_CurrentFilterSnap(
                event,
                Type,
                this.ListAppUsage_Config
            );

            this.ListAppUsage_Config = this._DataHelperService.List_Operations(
                this.ListAppUsage_Config,
                event,
                Type
            );

            if (
                (this.ListAppUsage_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.ListAppUsage_GetData();
            }

        }, this._HelperService.AppConfig.SearchInputDelay);

    }
    ListAppUsage_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.ListAppUsage_Config);
        this.ListAppUsage_Config = TConfig;
    }
    ListAppUsage_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        this.ListAppUsage_GetDetails();
    }
    ListAppUsage_GetDetails() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreUsage,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._CoreUsage = _Response.Result as OCoreUsage;
                    this._CoreUsage.RequestTime = this._HelperService.GetDateTimeS(this._CoreUsage.RequestTime);
                    this._CoreUsage.ResponseTime = this._HelperService.GetDateTimeS(this._CoreUsage.ResponseTime);
                    this._HelperService.OpenModal('ModalUsageDetailsContent');
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public _CoreUsage: OCoreUsage =
        {
            ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
        }


    //#region filterOperations

    Active_FilterValueChanged(event: any) {

        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetUsageConfig(this.ListAppUsage_Config);

        //#region setOtherFilters
        //this.SetOtherFilters();
        //#endregion
        this.ListAppUsage_GetData();

    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Usage(Type, index);
        this._FilterHelperService.SetUsageConfig(this.ListAppUsage_Config);

        this.SetOtherFilters();

        this.ListAppUsage_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Usage(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Usage
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            // confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Usage
                );
                this._FilterHelperService.SetUsageConfig(this.ListAppUsage_Config);
                this.ListAppUsage_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.ListAppUsage_GetData();

        if (ButtonType == 'Sort') {
            $("#ListAppUsage_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#ListAppUsage_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetUsageConfig(this.ListAppUsage_Config);
        this.SetOtherFilters();

        this.ListAppUsage_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    //#region filterOperationsV

    Active_FilterValueChangedV(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetUsageConfig(this.ListAppVersions_Config);

        //#region setOtherFilters
        //this.SetOtherFiltersV();
        //#endregion

        this.ListAppUsage_GetData();
    }

    RemoveFilterComponentV(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Version(Type, index);
        this._FilterHelperService.SetVersionConfig(this.ListAppVersions_Config);

        this.SetOtherFilters();

        this.ListAppVersions_GetData();
    }

    Save_NewFilterV() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Version(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Version
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_FilterV() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Version
                );
                this._FilterHelperService.SetVersionConfig(this.ListAppVersions_Config);
                this.ListAppVersions_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });

    }

    ApplyFiltersV(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.ListAppVersions_GetData();

        if (ButtonType == 'Sort') {
            $("#ListAppVersion_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#ListAppVersion_fdropdown").dropdown('toggle');
        }

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    ResetFiltersV(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetVersionConfig(this.ListAppVersions_Config);
        this.SetOtherFilters();

        this.ListAppVersions_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion


    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.ListAppUsage_Filter_Stores_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    SetOtherFilters(): void {
        this.ListAppUsage_Config.SearchBaseConditions = [];
        this.ListAppUsage_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            this.ListAppUsage_Filter_Stores_Selected = null;
            this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    SetOtherFiltersV(): void {
        this.ListAppUsage_Config.SearchBaseConditions = [];
        this.ListAppUsage_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
        if (CurrentIndex != -1) {
            this.ListAppUsage_Filter_Stores_Selected = null;
            this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    tabSwitched(name: string): void {
        if (name == 'usage') {
            this.ListAppUsage_Setup();
        } else if (name == 'version') {
            this.ListAppVersions_Setup();
        }
    }
}

export class OCoreUsage {
    public ReferenceId: number;
    public Reference: string;
    public ReferenceKey: string;
    public AppOwnerKey: string;
    public AppOwnerName: string;
    public AppKey: string;
    public AppName: string;
    public ApiKey: string;
    public ApiName: string;
    public AppVersionKey: string;
    public AppVersionName: string;
    public FeatureKey: string;
    public FeatureName: string;
    public UserAccountKey: string;
    public UserAccountDisplayName: string;
    public UserAccountIconUrl: string;
    public UserAccountTypeCode: string;
    public UserAccountTypeName: string;
    public SessionId: string;
    public SessionKey: string;
    public IpAddress: string;
    public Latitude: string;
    public Longitude: string;
    public Request: string;
    public Response: string;
    public RequestTime: Date;
    public ResponseTime: Date;
    public ProcessingTime: string;
    public StatusId: number;
    public StatusCode: string;
    public StatusName: string;
}