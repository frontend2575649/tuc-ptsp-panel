import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-turequesthistory",
  templateUrl: "./turequesthistory.component.html",
})
export class TURequestHistoryComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
   


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {



    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.InitBackDropClickEvent();

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey= params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      this._HelperService.AppConfig.ActiveAccountKey= params['accountkey'];
      this._HelperService.AppConfig.ActiveAccountId = params['accountid'];

      this.RequestHistory_Setup();
      // this.RequestHistory_Filter_Stores_Load();
      this.InitColConfig();


    });


    // this._HelperService.StopClickPropogation();
    this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
      this.RequestHistory_GetData();
    });
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  Form_AddUser_Open() {
    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
    //     .MerchantOnboarding,
    // ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region RequestHistory
  public CurrentRequest_Key: string;
  public CurrentRequest_Id : number;
  public RequestHistory_Config: OList;
  RequestHistory_Setup() {
    this.RequestHistory_Config = {
      Id: null,
      Type: "all",
      Task: this._HelperService.AppConfig.Api.Core.GetCoreUsages,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      Title: 'Core Usage',
      StatusType: 'Apprequestdefault',
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "AppKey", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, "="),
      // DefaultSortExpression: 'RequestTime desc',
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'RequestTime',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
            DisplayName: '#',
            SystemName: 'ReferenceId',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: true,
            Search: true,
            Sort: false,
            ResourceId: null,
        },
        {
            DisplayName: 'App Version',
            SystemName: 'AppVersionName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
        },
        {
          DisplayName: 'Ip Address',
          SystemName: 'IpAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
      },
      
        {
            DisplayName: 'User',
            SystemName: 'UserAccountDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: false,
            ResourceId: null,
        },
        {
            DisplayName: 'Request time',
            SystemName: 'RequestTime',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
           
        },
        {
            DisplayName: 'Response On',
            SystemName: 'ResponseTime',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
        },
        {
            DisplayName: 'Processing Time',
            SystemName: 'ProcessingTime',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
        },
    ]

    };
    this.RequestHistory_Config = this._DataHelperService.List_Initialize(
      this.RequestHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.RequestHistory,
      this.RequestHistory_Config
    );

    this.RequestHistory_GetData();
  }
  RequestHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.RequestHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.RequestHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.RequestHistory_Config


    );

    this.RequestHistory_Config = this._DataHelperService.List_Operations(
      this.RequestHistory_Config,
      event,
      Type
    );

    if (
      (this.RequestHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.RequestHistory_GetData();
    }

  }
  RequestHistory_GetData() {

    // var TConfig = this._DataHelperService.List_GetData(
    //   this.RequestHistory_Config
    // );
    // this.RequestHistory_Config = TConfig;

    var TConfig = this._DataHelperService.List_GetDataTur(
      this.RequestHistory_Config
    );
    this.RequestHistory_Config = TConfig;

    
  }
  RequestHistory_RowSelected(ReferenceData) {
    this.CurrentRequest_Key = ReferenceData.ReferenceKey;
    this.CurrentRequest_Id= ReferenceData.ReferenceId;

    this.ListAppUsage_GetDetails();

  }

  //#endregion

  //#region StoreFilter

  public RequestHistory_Filter_Stores_Option: Select2Options;
  public RequestHistory_Filter_Stores_Selected = null;
  RequestHistory_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      // AccountKey: this._HelperService.UserAccount.AccountKey,
      // AccountId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.RequestHistory_Filter_Stores_Option = {
      placeholder: "Select Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  RequestHistory_Filter_Stores_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.RequestHistory_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = null;
    } else if (event.value != this.RequestHistory_Filter_Stores_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.RequestHistory_Filter_Stores_Selected,
        "="
      );
      this.RequestHistory_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.RequestHistory_Config.SearchBaseConditions
      );
      this.RequestHistory_Filter_Stores_Selected = event.data[0].ReferenceKey;
      this.RequestHistory_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.RequestHistory_Filter_Stores_Selected,
          "="
        )
      );
    }

    this.RequestHistory_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.RequestHistory_Config.SearchBaseConditions = [];
    this.RequestHistory_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.RequestHistory_Filter_Stores_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.RequestHistory_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);

    this.SetOtherFilters();

    this.RequestHistory_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_RequestHistory(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.RequestHistory
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.RequestHistory
        );
        this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
        this.RequestHistory_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.RequestHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#RequestHistory_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#RequestHistory_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.RequestHistory_Config);
    this.SetOtherFilters();

    this.RequestHistory_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.RequestHistory_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }


  ListAppUsage_GetDetails() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetRequestHistoryDetails,
      ReferenceKey:this.CurrentRequest_Key,
      ReferenceId: this.CurrentRequest_Id,


      // Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.CurrentRequest_Key, '='),
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CoreUsage = _Response.Result as OCoreUsage;
          this._CoreUsage.RequestTime = this._HelperService.GetDateTimeS(this._CoreUsage.RequestTime);
          this._CoreUsage.ResponseTime = this._HelperService.GetDateTimeS(this._CoreUsage.ResponseTime);
          this.clicked()
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }



  public _CoreUsage: OCoreUsage =
    {
      Coordinates:null,
      ReferenceKey: null, ReferenceId: null, Reference: null, StatusCode: null, StatusId: null, StatusName: null, UserAccountKey: null, UserAccountIconUrl: null, UserAccountDisplayName: null, ApiKey: null, ApiName: null, AppKey: null, AppName: null, AppOwnerKey: null, AppOwnerName: null, AppVersionKey: null, AppVersionName: null, FeatureKey: null, FeatureName: null, IpAddress: null, Latitude: null, Longitude: null, ProcessingTime: null, Request: null, RequestTime: null, Response: null, ResponseTime: null, SessionId: null, SessionKey: null, UserAccountTypeCode: null, UserAccountTypeName: null,
    }


}


export class OCoreUsage {
  Coordinates?:any;
  public ReferenceId: number;
  public Reference: string;
  public ReferenceKey: string;
  public AppOwnerKey: string;
  public AppOwnerName: string;
  public AppKey: string;
  public AppName: string;
  public ApiKey: string;
  public ApiName: string;
  public AppVersionKey: string;
  public AppVersionName: string;
  public FeatureKey: string;
  public FeatureName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public UserAccountTypeCode: string;
  public UserAccountTypeName: string;
  public SessionId: string;
  public SessionKey: string;
  public IpAddress: string;
  public Latitude: string;
  public Longitude: string;
  public Request: string;
  public Response: string;
  public RequestTime: Date;
  public ResponseTime: Date;
  public ProcessingTime: string;
  public StatusId: number;
  public StatusCode: string;
  public StatusName: string;
}