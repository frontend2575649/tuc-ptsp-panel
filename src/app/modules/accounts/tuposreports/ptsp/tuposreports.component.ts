import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HelperService, DataHelperService } from 'src/app/service/service';

@Component({
  selector: 'app-tuposreports',
  templateUrl: './tuposreports.component.html',
})
export class TuposreportsComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {
  }

}
