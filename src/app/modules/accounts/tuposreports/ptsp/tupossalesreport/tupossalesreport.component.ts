import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { OResponse, HelperService, DataHelperService } from 'src/app/service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
declare var moment: any;
import * as Chart from 'chart.js'
import * as Feather from 'feather-icons';

@Component({
  selector: 'app-tupossalesreport',
  templateUrl: './tupossalesreport.component.html',
 
})
export class TupossalesreportComponent implements OnInit {

  Type = 1;
    StartTime = null;
    EndTime = null;
    StartTimeS = null;
    EndTimeS = null;
    LiteType = 1;
    LiteStartTime = null;
    LiteEndTime = null;
    LiteStartTimeS = null;
    LiteEndTimeS = null;
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;

    public todaysdate: string = null;
    public now: number;
    timeLeft: number = 60;
    interval;
    startTimer() {
        this._HelperService.AppConfig.ActiveOwnerId + '_';
        this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                this.timeLeft = 60;
            }
        }, 1000);
    }


    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
        this.todaysdate = this._HelperService.GetDateS(new Date());
        setInterval(() => {
            this.now = Date.now();
        }, 1);
    }

    public Options: any = {
      cornerRadius: 20,
      responsive: true,
      elements: {
        line: {
          tension: 0
        }
      },
      legend: {
        display: false,
        position: 'right',
      },
      ticks: {
        autoSkip: false
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              stacked: true,
              display: true
            },
            ticks: {
              autoSkip: false,
              fontSize: 11
            }
          }
        ],
        yAxes: [
          {
  
            gridLines: {
              stacked: true,
              display: true
            },
            ticks: {
              beginAtZero: true,
              fontSize: 11
            }
          }
        ]
      },
      plugins: {
        datalabels: {
          display: false,
          backgroundColor: "#ffffff47",
          color: "#798086",
          borderRadius: "2",
          borderWidth: "1",
          borderColor: "transparent",
          anchor: "end",
          align: "end",
          padding: 2,
          font: {
            size: 10,
            weight: 500
          },
          formatter: (value, ctx) => {
            const label = ctx.chart.data.labels[ctx.dataIndex];
            if (label != undefined) {
              return value;
            } else {
              return value;
            }
          }
        }
      }
    }
    public barChartLabels1 = ['9 AM ', '10 AM', '11 AM', '12 PM', '01 PM', "02 PM", "03 PM","04PM","05PM","06PM","07PM","08PM"];
    public barChartLabels2 = ['MON ', 'TUE', 'WED', 'THURS', 'FRI', "SAT"];
    public barChartLabels3 = ['JAN ', 'FEB', 'MAR', 'APR', 'MAY', "JUN", "JUL", "AUG","SEPT", "OCT", "NOV", "DEC"];
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData = [
    { data: [1200, 2100, 1900, 1200, 1800, 2900, 1600,2400,3200,3000,2300,4000], label: 'Remote', fill: false, backgroundColor: ["#bae755"] },
    { data: [1800, 2500, 3500, 2000, 2500, 3500, 2000,3000,3600,3500,3000,4200], label: 'Visit', fill: false ,},
  ];
    ngOnInit() {
    
        Feather.replace();
        this.startTimer();
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('day');
            this.EndTime = moment().endOf(1, 'day');
        }
        if (this.LiteStartTime == undefined) {
            this.LiteStartTime = moment().startOf('day');
            this.LiteEndTime = moment().endOf(1, 'day');
        }
        this.LoadData();
        
    }
    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 0) {
            SDate =
                {
                    start: moment().startOf('day').subtract(7, 'days'),
                    end: moment().endOf('day'),
                }
        }
        else if (Type == 1) {
            SDate =
                {
                    start: moment().startOf('day'),
                    end: moment().endOf('day'),
                }
        }
        else if (Type == 2) {
            SDate =
                {
                    start: moment().subtract(1, 'days').startOf('day'),
                    end: moment().subtract(1, 'days').endOf('day'),
                }
        }
        else if (Type == 3) {
            SDate =
                {
                    start: moment().startOf('isoWeek'),
                    end: moment().endOf('isoWeek'),
                }
        }
        else if (Type == 4) {
            SDate =
                {
                    start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                    end: moment().subtract(1, 'weeks').endOf('isoWeek'),
                }
        }
        else if (Type == 5) {
            SDate =
                {
                    start: moment().startOf('month'),
                    end: moment().endOf('month'),
                }
        }
        else if (Type == 6) {
            SDate =
                {
                    start: moment().startOf('month').subtract(1, 'month'),
                    end: moment().startOf('month').subtract(1, 'seconds'),
                }
        }
        else if (Type == 7) {
            SDate =
                {
                    start: new Date(2017, 0, 1, 0, 0, 0, 0),
                    end: moment().endOf('day'),
                }
        }
        this.StartTime = SDate.start;
        this.EndTime = SDate.end;

        this.LoadData();
    }
    LoadData() {
        this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
        this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
        this._HelperService.GetUserCounts(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetRewardsSummary(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetSalesSummary(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.AccountType.MerchantId, 0, 0, this.StartTime, this.EndTime);
    }

    GetStoreVisits() {
        var Data = {
            Task: "getaccountsaleshistoryhourly",
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: this._HelperService.GetDateCondition('', 'Date', this.StartTime, this.EndTime)
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var ChartResponse = _Response.Result.Data;
                    this._HelperService._UserOverviewPlot.HourlyVisitLabel = [];
                    this._HelperService._UserOverviewPlot.HourlyVisitData = [];
                    this._HelperService._UserOverviewPlot.HourlySalesData = [];
                    var TPlotDataSet = [];
                    var TSalesPlotDataSet = [];
                    var DataSetItemVisit = {
                        label: 'Visits',
                        data: [],
                        lineTension: 0.0,
                        fill: false,
                    };
                    var DataSetItemSale = {
                        label: 'Sale',
                        data: []
                    };
                    for (let index = 0; index < 24; index++) {
                        var dd = " am";
                        var h = index;
                        if (h >= 12) {
                            h = index - 12;
                            dd = " pm";
                        }
                        if (h == 0) {
                            h = 12;
                        }
                        var Hour = h + dd;
                        this._HelperService._UserOverviewPlot.HourlyVisitLabel.push(Hour);
                        var RData = ChartResponse.find(x => x.Hour == index);
                        if (RData != undefined && RData != null) {
                            DataSetItemVisit.data.push(RData.SuccessfulTransaction);
                            DataSetItemSale.data.push(RData.SuccessfulTransactionInvoiceAmount);
                        }
                        else {
                            DataSetItemVisit.data.push(0);
                            DataSetItemSale.data.push(0);
                        }
                    }
                    TPlotDataSet.push(DataSetItemVisit);
                    TSalesPlotDataSet.push(DataSetItemSale);
                    this._HelperService._UserOverviewPlot.HourlySalesData = TSalesPlotDataSet;
                    this._HelperService._UserOverviewPlot.HourlyVisitData = TPlotDataSet;
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public StoreLabel = [];
    public StoreData = [];
    public StoreSalesData = [];
    GetSalesByStore() {
        var SearchCondition = '';
        SearchCondition = this._HelperService.GetDateCondition('', 'Date', this.StartTime, this.EndTime);
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountTypeId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.AccountType.StoreId, '=');
        var Data = {
            Task: "getaccountstoresaleshistory",
            SearchCondition: SearchCondition,
            SortExpression: 'TotalTransaction desc',
            Offset: 0,
            Limit: 10,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var ChartResponse = _Response.Result.Data;
                    this.StoreLabel = [];
                    this.StoreData = [];
                    var TPlotDataSet = [];
                    var TSalesPlotDataSet = [];
                    var DataSetItemVisit = {
                        label: 'Visits',
                        data: [],

                    };
                    var DataSetItemSale = {
                        label: 'Sale',
                        data: []
                    };
                    if (ChartResponse != undefined) {
                        ChartResponse.forEach(RangeItem => {
                            this.StoreLabel.push(RangeItem.DisplayName);
                            DataSetItemVisit.data.push(RangeItem.TotalTransaction);
                            DataSetItemSale.data.push(Math.round(RangeItem.TotalInvoiceAmount));
                        });
                        TPlotDataSet.push(DataSetItemVisit);
                        DataSetItemSale.data = DataSetItemSale.data.sort((n2, n1) => n1 - n2);
                        TSalesPlotDataSet.push(DataSetItemSale);
                        this.StoreData = TPlotDataSet;
                        this.StoreSalesData = TSalesPlotDataSet;
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public BankCollections = [];
    GetBankCollections() {
        var SearchCondition = '';
        SearchCondition = this._HelperService.GetDateCondition('', 'Date', this.StartTime, this.EndTime);
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'UserAccountTypeId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.AccountType.PosTerminalId, '=');
        var Data = {
            Task: "getaccountbankcollectionhistory",
            SearchCondition: SearchCondition,
            SortExpression: 'CardInvoiceAmount desc',
            Offset: 0,
            Limit: 10,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.BankCollections = [];
                    if (_Response.Result.Data != undefined) {
                        _Response.Result.Data.forEach(element => {
                            if (element.IconUrl != undefined) {

                                element.IconUrl = element.IconUrl.replace('http://cdn.thankucash.com/http://cdn.thankucash.com/', 'http://cdn.thankucash.com/')
                            }
                        });
                        this.BankCollections = _Response.Result.Data;
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
}
