import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Routes, RouterModule } from '@angular/router';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { AgmCoreModule } from '@agm/core';
import { TuposactivityComponent } from './tuposactivity.component';
import { ChartsModule } from 'ng2-charts';
import { MainPipe } from '../../../../../service/main-pipe.module';

const routes: Routes = [{ path: "", component: TuposactivityComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TuposactivityRoutingModule {}
@NgModule({
  declarations: [TuposactivityComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TuposactivityRoutingModule,
    Select2Module,
    NgxPaginationModule,
    MainPipe,
    ChartsModule,
    MainPipe,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo',
    }),
    Daterangepicker,
    Ng2FileInputModule
  ]
})
export class TuposactivityModule { }
