import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import swal from "sweetalert2";
import { DataHelperService, FilterHelperService, HelperService, OList, OSelect } from "../../../../service/service";
declare var $: any;

@Component({
  selector: "tu-tumerchants",
  templateUrl: "./tumerchants.component.html"
})
export class TUMerchantsComponent implements OnInit {

  CheckMode: string;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { }
  ngOnInit() {
    this._HelperService.StopClickPropogation();

    // $('body').on('click', '.df-mode', (e) => {
    //   e.preventDefault();

    //   for (let index = 0; index < $('.df-mode').length; index++) {
    //     const element = $('.df-mode')[index];

    //     setTimeout(() => {
    //       if (element.classList.length == 3) {
    //         if (element.getAttribute('data-title') === 'dark') {
    //           this.CheckMode = 'dark';
    //         } else {
    //           this.CheckMode = 'light';
    //         }
    //       }
    //       Feather.replace();
    //     }, 50);
    //   }

    // });

    Feather.replace();
    this.MerchantsList_Setup();
    this.MerchantsList_Filter_Owners_Load();
    this.InitColConfig();
  }

  //#region ManipulateColumns 

  TempColumnConfig: any = [

    {
      Name: 'Status',
      Value: true
    },
    {
      Name: 'City',
      Value: true
    },
    {
      Name: 'Contact',
      Value: true
    },
    {
      Name: 'Stores',
      Value: true
    },
    {
      Name: 'POS',
      Value: true
    },
    {
      Name: 'ActivePOS',
      Value: true
    },
    {
      Name: 'RM',
      Value: true
    },
    {
      Name: 'Active Date',
      Value: true
    },
  ]

  ColumnConfig: any = [

    {
      Name: 'Status',
      Value: true
    },
    {
      Name: 'City',
      Value: true
    },
    {
      Name: 'Contact',
      Value: true
    },
    {
      Name: 'Stores',
      Value: true
    },
    {
      Name: 'POS',
      Value: true
    },
    {
      Name: 'ActivePOS',
      Value: true
    },
    {
      Name: 'RM',
      Value: true
    },
    {
      Name: 'Active Date',
      Value: true
    },
  ]

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage('MerchantTable');
    var ColConfigExist: boolean = MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(MerchantTableConfig.config);
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal('EditCol');
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage('MerchantTable', { config: this.ColumnConfig });
    this._HelperService.CloseModal('EditCol');
  }

  //#endregion

  //#region Merchants 

  public MerchantsList_Config: OList;
  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
      Title: "Merchants List",
      StatusType: "default",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerKey', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerKey
      // , '=='),
      Type: this._HelperService.AppConfig.ListType.All,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Sort:
      {
        SortDefaultName: 'Added on',
        SortDefaultColumn: 'CreateDate',
        SortDefaultOrder: 'desc'
      },
      TableFields: [
        {
          DisplayName: 'Merchant Name',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Contact No',
          SystemName: 'ContactNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'text-center',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: 'ThankUCash',
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Email Address',
          SystemName: 'EmailAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Address',
          SystemName: 'Address',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'City',
          SystemName: 'CityName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'RM',
          SystemName: 'RmDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Stores',
          SystemName: 'Stores',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'POS',
          SystemName: 'Terminals',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Active POS',
          SystemName: 'ActiveTerminals',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
      ]
    };
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.MerchantsList_Config
    );

    this.MerchantsList_GetData();
  }
  MerchantsList_ToggleOption(event: any, Type: any) {
   
    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config
    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData();
    }
  }

  MerchantsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;
  }
  MerchantsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveMerchant, {
      "ReferenceKey": ReferenceData.ReferenceKey,
      "ReferenceId": ReferenceData.ReferenceId,
      "DisplayName": ReferenceData.DisplayName,
      "AccountTypeCode": this._HelperService.AppConfig.AccountType.Merchant
    });

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Merchant.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);

  }

  //#endregion

  //#region Dropdown_Owners 

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = null;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: false,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.MerchantsList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.MerchantsList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.MerchantsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.MerchantsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.MerchantsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.MerchantsList_Config.SearchBaseConditions = [];
    // this.MerchantsList_Config.SearchBaseCondition = null;

    // var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    // if (CurrentIndex != -1) {
    //   this.MerchantsList_Filter_Owners_Selected = null;
    //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    // }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();
    this.MerchantsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },

      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }


    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: 'Delete View?',
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();
    this.ResetFilterUI();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData();
    this.ResetFilterUI();
    this._HelperService.StopClickPropogation();
  }

  //#endregion

  Form_AddUser_Open() {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.MerchantOnboarding]);
  }

  ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.MerchantsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

}