import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from "../../../../service/service";
import swal from 'sweetalert2';


@Component({
    selector: 'tu-appusers',
    templateUrl: './tuappusers.component.html',
})
export class TUAppUsersComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.AppUsersList_Setup();
            }
        });
    }
    public AppUsersList_Config: OList;
    AppUsersList_Setup() {
        this.AppUsersList_Config = {
            Id:"",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetAppUsers,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "Available Customers",
            StatusType: "default",
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            Type: this._HelperService.AppConfig.ListType.CreatedBy,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Mobile',
                    SystemName: 'MobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'Email',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Gender',
                    SystemName: 'GenderName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },

                {
                    DisplayName: 'Reward',
                    SystemName: 'TotalRewardAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'R. CLAIM',
                    SystemName: 'TucPlusRewardClaimAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Redeem',
                    SystemName: 'RedeemAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Tr',
                    SystemName: 'TotalTransaction',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Purchase',
                    SystemName: 'TotalInvoiceAmount',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                }
            ]

        };
        this.AppUsersList_Config = this._DataHelperService.List_Initialize(
            this.AppUsersList_Config
        );
        this.AppUsersList_GetData();
    }
    AppUsersList_ToggleOption(event: any, Type: any) {
        this.AppUsersList_Config = this._DataHelperService.List_Operations(
            this.AppUsersList_Config,
            event,
            Type
        );
        if (this.AppUsersList_Config.RefreshData == true) {
            this.AppUsersList_GetData();
        }
    }
    AppUsersList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.AppUsersList_Config
        );
        this.AppUsersList_Config = TConfig;
    }
    AppUsersList_RowSelected(ReferenceData) {
       
    }










}