import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
  OCoreCommon
} from "../../../service/service";
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: "tu-tufaqs",
  templateUrl: "./tufaqs.component.html",
})
export class TUFAQsComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {


  }
  @ViewChild("offCanvas") divView: ElementRef;

  ngOnInit() {
    $(document).ready(function(){
      // Add minus icon for collapse element which is open by default
      $(".collapse.show").each(function(){
        $(this).prev(".test").find(".fa").addClass("fa-minus").removeClass("fa-plus");
      });
      
      // Toggle plus minus icon on show hide of collapse element
      $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".test").find(".fa").removeClass("fa-plus").addClass("fa-minus");
      }).on('hide.bs.collapse', function(){
        $(this).prev(".test").find(".fa").removeClass("fa-minus").addClass("fa-plus");
      });
  });

    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.InitBackDropClickEvent();


    // this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {

      this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

      this.MerchnatCategories_Setup();
      this.InitColConfig();
      this.Form_Add_Load();
      this.Form_Edit_Load();
      this.MerchantsList_Filter_Owners_Load();
      this.Categories_GetDetails();

    });

  }
  viewfaqcategories: boolean = true;
  FaqCategories() {
    this.viewfaqcategories = !this.viewfaqcategories;
  }


  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  // Form_AddUser_Open() {
  //   this._Router.navigate([
  //     this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole
  //       .MerchantOnboarding,
  //   ]);
  // }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region FAQs
  public CurrentRequest_Key: string;

  public MerchnatCategories_Config: OList;
  MerchnatCategories_Setup() {
    this.MerchnatCategories_Config = {
      Id: null,
      Type: "all",
      Sort: null,
      Task: 'getfaqs',
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq,
      Title: 'Merchant Categories',
      StatusType: 'default',
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "CategoryId", this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, "="),
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Sequence',
          SystemName: 'Sequence',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Category Name',
          SystemName: 'CategoryName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Category Key',
          SystemName: 'CategoryKey',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
          SystemName: 'CreatedByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.ModifyDate,
          SystemName: 'ModifyDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.ModifyBy,
          SystemName: 'ModifyByDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
      ]

    };

    this.MerchnatCategories_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(this.MerchnatCategories_Config.SearchBaseCondition, "StatusCode", this._HelperService.AppConfig.DataType.Text, 'default.active', "=");

    this.MerchnatCategories_Config = this._DataHelperService.List_Initialize(
      this.MerchnatCategories_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Faq,
      this.MerchnatCategories_Config
    );

    this.MerchnatCategories_GetData();
  }
  MerchnatCategories_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.MerchnatCategories_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchnatCategories_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchnatCategories_Config


    );

    this.MerchnatCategories_Config = this._DataHelperService.List_Operations(
      this.MerchnatCategories_Config,
      event,
      Type
    );

    if (
      (this.MerchnatCategories_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchnatCategories_GetData();
    }

  }
  timeout = null;
  MerchnatCategories_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchnatCategories_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchnatCategories_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchnatCategories_Config


      );

      this.MerchnatCategories_Config = this._DataHelperService.List_Operations(
        this.MerchnatCategories_Config,
        event,
        Type
      );

      if (
        (this.MerchnatCategories_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchnatCategories_GetData();
      }

    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  MerchnatCategories_GetData() {

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchnatCategories_Config
    );
    this.MerchnatCategories_Config = TConfig;
  }
  MerchnatCategories_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;

    this.MerhchantCategories_GetDetails(ReferenceData);
  }

  //#endregion



  Details: any = {};
  MerhchantCategories_GetDetails(ReferenceData) {
    var pData = {
      Task: 'getfaq',
      ReferenceId: ReferenceData.ReferenceId,
      ReferenceKey: ReferenceData.ReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.Details = _Response.Result as OCoreCommon;
          this.Details.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._CoreCommon.CreateDate);
          this.Details.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._CoreCommon.ModifyDate);
          this.Details.StatusI = this._HelperService.GetStatusIcon(this._HelperService._CoreCommon.StatusCode);
          this.Form_Edit.controls['CategoryKey'].setValue(this.Details.CategoryKey);

          this.Form_Edit.controls['ReferenceKey'].setValue(this.Details.ReferenceKey);
          this.Form_Edit.controls['ReferenceId'].setValue(this.Details.ReferenceId);
          //this.clicked();
          this._HelperService.OpenModal('Form_Edit_Content');
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }
  Form_Add: FormGroup;
  Form_Add_Show() {
    this.Form_Add_Clear();
    this._HelperService.OpenModal('Form_Add_Content');
  }
  Form_Add_Close() {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    this._HelperService.CloseModal('Form_Add_Content');
  }
  Form_Add_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_Add = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      Task: 'savefaq',
      TypeCode: this._HelperService.AppConfig.HelperTypes.FAQs,
      Name: [null, Validators.required],
      Description: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      CategoryKey: [this._HelperService.AppConfig.ActiveReferenceKey, Validators.compose([Validators.required, Validators.minLength(1)])],
      Sequence: [0, Validators.compose([Validators.required, Validators.min(1)])],
      StatusCode: 'default.inactive'
    });
  }
  Form_Add_Clear() {
    this.Form_Add.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_Add_Load();
    this._HelperService._FileSelect_Icon_Reset();
  }
  Form_Add_Process(_FormValue: any) {
    _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.MerchnatCategories_Setup()
          this.Form_Add_Clear();
          if (_FormValue.OperationType == 'edit') {
            this.Form_Add_Close();
            // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AddCoreHelpers, _Response.Result.ReferenceKey]);
          }
          else if (_FormValue.OperationType == 'close') {
            this.Form_Add_Close();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  Form_Edit: FormGroup;
  Form_Edit_Show(ReferenceData) {
    this.MerhchantCategories_GetDetails(ReferenceData);
  }
  Form_Edit_Close() {
    this._HelperService.CloseModal('Form_Edit_Content');
    // this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreHelpers]);
    //this.unclick();
    // this.MerchnatCategories_Setup();
  }
  Form_Edit_Load() {
    this.Form_Edit = this._FormBuilder.group({
      AllowDuplicateName: false,
      OperationType: 'new',
      ReferenceId: null,
      ReferenceKey: null,
      Task: 'updatefaq',
      TypeCode: this._HelperService.AppConfig.HelperTypes.FAQCategories,
      Name: [null, Validators.required],
      Description: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      CategoryKey: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      Sequence: [0, Validators.compose([Validators.required, Validators.min(1)])],
      StatusCode: 'default.inactive'
    });
  }
  Form_Edit_Clear() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
  }
  Form_Edit_Process(_FormValue: any) {

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.Form_Edit_Clear();
          this.Form_Edit_Close();
          this.MerchnatCategories_GetData();
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });


  }


  //#region Get Category Details 

  CategoryDetails: any = {};
  Categories_GetDetails() {
    var pData = {
      Task: 'getfaqcategory',
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.CategoryDetails = _Response.Result as OCoreCommon;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion


  SetOtherFilters(): void {
    this.MerchnatCategories_Config.SearchBaseConditions = [];
    this.MerchnatCategories_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', "CategoryId", this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, "=");

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);

    //#region setOtherFilters
    // this.SetOtherFilters();
    //#endregion

    this.MerchnatCategories_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_RequestHistory(Type, index);
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
    this.SetOtherFilters();
    this.MerchnatCategories_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_RequestHistory(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Faq
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Faq
        );
        this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
        this.MerchnatCategories_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchnatCategories_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchnatCategories_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchnatCategories_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetRequestHistoryConfig(this.MerchnatCategories_Config);
    this.SetOtherFilters();

    this.MerchnatCategories_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  Delete_Confirm() {
    swal({
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      showCancelButton: true,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      input: 'password',
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      inputAttributes: {
        autocapitalize: 'off',
        autocorrect: 'off',
        maxLength: "4",
        minLength: "4"
      },
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.Core.DeleteCoreCommon,
          ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
          AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this.Form_Edit_Close();
              this._HelperService.NotifySuccess(_Response.Message);
              this._HelperService.CloseModal('ModalDetails');
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }

  public _CoreUsage: OCoreUsage =
    {
      Count: 0,
      CreateDate: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Data: null,
      Description: null,
      PlatformCharge: null,
      HelperCode: null,
      HelperName: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      Name: null,
      ParentCode: null,
      ParentKey: null,
      ParentName: null,
      PosterUrl: null,
      Reference: null,
      ReferenceKey: null,
      ReferenceId: null,
      Sequence: null,
      SubItemsCount: null,
      StatusCode: null,
      StatusId: null,
      StatusName: null,
      SubParentCode: null,
      SubParentKey: null,
      SubParentName: null,
      SubValue: null,
      SystemName: null,
      TypeCode: null,
      TypeName: null,
      UserAccountDisplayName: null,
      UserAccountIconUrl: null,
      UserAccountKey: null,
      Value: null,
      StatusI: null,
      CreateDateS: null,
      ModifyDateS: null

    }

  //#region Account Selector 

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = 0;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: 'getfaqcategoriesclient',
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SystemFaq,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {

    // console.log('event', event);
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      // this.MerchantsList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      this.Form_Add.controls['AccountTypeCode'].setValue(event.data[0].code);
      this.MerchantsList_Filter_Owners_Selected = event.value;
    }
  }

  //#endregion

  //#region Account Selector Edit
  public MerchantsList_Filter_Owners_Selected_Edit = 0;
  MerchantsList_Filter_Owners_Change_Edit(event: any) {

    if (event.value == this.MerchantsList_Filter_Owners_Selected_Edit) {
      // this.MerchantsList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.MerchantsList_Filter_Owners_Selected_Edit) {
      this.Form_Edit.controls['AccountTypeCode'].setValue(event.data[0].code);
      this.MerchantsList_Filter_Owners_Selected_Edit = event.value;
    }
  }

  //#endregion
}

export class OCoreUsage {
  public Reference: string;
  public ReferenceKey: string;
  public ReferenceId: string;
  public SystemName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public TypeCode: string;
  public TypeName: string;
  public HelperCode: string;
  public HelperName: string;
  public ParentKey: string;
  public ParentCode: string;
  public ParentName: string;
  public SubParentKey: string;
  public SubParentCode: string;
  public SubParentName: string;
  public PlatformCharge?: any;
  public Name: string;
  public Value: string;
  public SubValue: string;
  public Description: string;
  public Data: string;
  public Sequence: number = 0;
  public Count: number = 0;
  public SubItemsCount: number = 0;
  public IconUrl: string;
  public PosterUrl: string;
  public CreateDate: Date;
  public CreatedByKey: string;
  public CreatedByDisplayName: string;
  public CreatedByIconUrl: string;
  public ModifyDate: Date;
  public ModifyByKey: string;
  public ModifyByDisplayName: string;
  public ModifyByIconUrl: string;
  public StatusId: number = 0;
  public StatusCode: string;
  public StatusName: string;
  public StatusI: string;
  public CreateDateS: string;
  public ModifyDateS: string;
}