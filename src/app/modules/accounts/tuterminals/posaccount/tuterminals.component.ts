import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-tuterminals",
  templateUrl: "./tuterminals.component.html"
})
export class TUTerminalsComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this._HelperService.Get_UserAccountDetails(true);
        this.TerminalsList_Filter_Merchants_Load();
        this.TerminalsList_Filter_Stores_Load();
        this.TerminalsList_Filter_Banks_Load();
        this.TerminalsList_Setup();
      }
    });
  }

  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: "POSTerminals",
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      Title: "Available Terminals",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.All,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Acquirer',
          SystemName: 'AcquirerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },


        {
          DisplayName: 'Re. %',
          SystemName: 'RewardPercentage',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );
    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }
  }
  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
  }




  public TerminalsList_Filter_Merchant_Option: Select2Options;
  public TerminalsList_Filter_Merchant_Selected = 0;
  TerminalsList_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Merchant
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Merchant_Option = {
      placeholder: 'Filter by Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Merchants_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Merchant_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Merchant_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Merchant_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = 0;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: 'Filter by Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Store_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Sort by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


} 
