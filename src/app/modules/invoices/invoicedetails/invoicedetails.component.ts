import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon,
    OInvoiceDetails
} from "../../../service/service";
import swal from "sweetalert2";
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
    selector: "tu-invoicedetails",
    templateUrl: "./invoicedetails.component.html"
})
export class TUInvoiceDetailsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    public InvoiceKey = null;
    public UserAccountKey = null;

    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this.InvoiceKey = params["referencekey"];
            this.UserAccountKey = params['useraccountkey'];
            if (this.InvoiceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this.InvoiceList_Filter_TransactionTypes_Load();
                this.InvoiceList_Setup();
                this.Get_Invoice();
                this.Form_AddUser_Load();
            }
        });

    }

    public InvoiceList_Config: OList;
    InvoiceList_Setup() {
        this.InvoiceList_Config = {
            Id:"",
            Task: this._HelperService.AppConfig.Api.Core.GetUserInvoices,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
            Title: "Available Invoices",
            StatusType: "invoice",
            PageRecordLimit: 5,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Text, this.UserAccountKey, '='),
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: '#REF',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'User',
                    SystemName: 'UserAccountDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'Invoice',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },


                {
                    DisplayName: 'Amount',
                    SystemName: 'TotalAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-right',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,


                },
                {
                    DisplayName: 'Invoice Date',
                    SystemName: 'InvoiceDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: true,


                },

                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,


                },
                {
                    DisplayName: 'Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: false,
                    Search: false,
                    Sort: true,
                    ResourceId: null,


                },
            ]
        };
        this.InvoiceList_Config = this._DataHelperService.List_Initialize(
            this.InvoiceList_Config
        );
        this.InvoiceList_GetData();
    }
    InvoiceList_ToggleOption(event: any, Type: any) {
        this.InvoiceList_Config = this._DataHelperService.List_Operations(
            this.InvoiceList_Config,
            event,
            Type
        );
        if (this.InvoiceList_Config.RefreshData == true) {
            this.InvoiceList_GetData();
        }
    }
    InvoiceList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.InvoiceList_Config
        );
        this.InvoiceList_Config = TConfig;
    }
    InvoiceList_RowSelected(ReferenceData) {
        this.InvoiceKey = ReferenceData.ReferenceKey;
        this.UserAccountKey = ReferenceData.UserAccountKey;
        this.Get_Invoice();
    }





    public InvoiceList_Filter_TransactionType_Option: Select2Options;
    public InvoiceList_Filter_TransactionType_Selected = 0;
    InvoiceList_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "SystemName",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.InvoiceType
                }
            ]
        };

        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.InvoiceList_Filter_TransactionType_Option = {
            placeholder: 'Search By Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    InvoiceList_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.InvoiceList_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
            this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
            this.InvoiceList_Filter_TransactionType_Selected = event.value;
            this.InvoiceList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '='));
        }
        this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }


    Get_Invoice() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserInvoice,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this.InvoiceKey, '=')
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._OInvoiceDetails = null;
                    this._OInvoiceDetails = _Response.Result as OInvoiceDetails;
                    this._OInvoiceDetails.InvoiceDate = this._HelperService.GetDateS(this._OInvoiceDetails.InvoiceDate);
                    this._OInvoiceDetails.PaymentDate = this._HelperService.GetDateS(this._OInvoiceDetails.PaymentDate);
                    this._OInvoiceDetails.ModifyDateS = this._HelperService.GetDateTimeS(this._OInvoiceDetails.ModifyDate);
                    this._OInvoiceDetails.StatusI = this._HelperService.GetStatusIcon(this._OInvoiceDetails.Status);
                    this._OInvoiceDetails.StatusBadge = this._HelperService.GetStatusBadge(this._OInvoiceDetails.StatusCode);
                }
                else {

                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                // console.log(_Error);
            });
    }

    public IsInvoiceUpdating = false;
    OpenInvoiceUpdator() {
        this.IsInvoiceUpdating = true;
    }
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {

        this._HelperService._Icon_Cropper_Data.Width = 512;
        this._HelperService._Icon_Cropper_Data.Height = 512;
        this.Form_AddUser = this._FormBuilder.group({
            Task: 'completepaymentinvoice',
            PaymentModeCode: [null, Validators.compose([Validators.required])],
            PaymentApproverKey: this._HelperService.UserAccount.AccountKey,
            PaymentApprover: this._HelperService.UserAccount.DisplayName,
            PaymentReference: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Comment: null,
            PaymentProofContent: this._HelperService._Icon_Cropper_Data,
        });
    }
    Form_AddUser_Clear() {
        this.Get_Invoice();
        this.IsInvoiceUpdating = false;
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {

        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this.InvoiceKey;
                _FormValue.IconContent = this._HelperService._Icon_Cropper_Data;
                _FormValue.AuthPin = result.value;
                _FormValue.PaymentProofContent = this._HelperService._Icon_Cropper_Data;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Invoice updated successfully');
                            this.Form_AddUser_Clear();
                            this.Form_AddUser_Load();
                            this._HelperService.CloseModal('InvoiceUpdate');
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }


    htmltoPDF() {
        this._HelperService.OpenModal('ModalReport');
        setTimeout(() => {
            this.StartDownload();
        }, 1000);
    }

    StartDownload() {
        html2canvas(document.querySelector("#InvoiceContainer")).then(canvas => {
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;
            const contentDataURL = canvas.toDataURL('image/png');
            let pdf1 = new jsPDF('p', 'mm', 'a4');
            var position = 0;
            pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            pdf1.save('invoice_' + this._OInvoiceDetails.ReferenceId + '.pdf');
        });
    }
    _OInvoiceDetails: OInvoiceDetails =
        {
            PaymentDateS: null,
            StatusBadge: null,
            PaymentApproverDisplayName: null,
            PaymentApproverKey: null,
            PaymentDate: null,
            PaymentModeCode: null,
            PaymentModeName: null,
            PaymentProofUrl: null,
            PaymentReference: null,

            Amount: null,
            Charge: null,
            ChargePercentage: null,
            ComissionAmount: null,
            ComissionPercentage: null,
            Comment: null,
            CreateDate: null,
            CreatedByDisplayName: null,
            CreatedByKey: null,
            Description: null,
            DiscountAmount: null,
            DiscountPercentage: null,
            EndDate: null,
            FromAddress: null,
            FromContactNumber: null,
            FromEmailAddress: null,
            FromFax: null,
            FromName: null,
            InvoiceDate: null,
            InvoiceDateS: null,
            InoviceNumber: null,
            ModifyByDisplayName: null,
            ModifyByKey: null,
            ModifyDate: null,
            ModifyDateS: null,
            Name: null,
            ParentKey: null,
            ParentName: null,
            ReferenceId: null,
            ReferenceKey: null,
            StartDate: null,
            Status: null,
            StatusCode: null,
            StatusI: null,
            StatusName: null,
            ToAddress: null,
            ToContactNumber: null,
            ToEmailAddress: null,
            ToFax: null,
            ToName: null,
            TotalAmount: null,
            TotalItem: null,
            TypeCode: null,
            TypeName: null,
            UnitCost: null,
            UserAccountDisplayName: null,
            UserAccountIconUrl: null,
            UserAccountId: null,
            UserAccountKey: null,
            UserAccountTypeCode: null,
            UserAccountTypeName: null,
            Items: [],
        };


}
