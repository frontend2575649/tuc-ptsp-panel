import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon
} from "../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-invoices",
  templateUrl: "./tuinvoices.component.html"
})
export class TUInvoicesComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this.InvoiceList_Filter_Owners_Load();
    this.InvoiceList_Filter_TransactionTypes_Load();
    this.InvoiceList_Setup();

  }

  public InvoiceList_Config: OList;
  InvoiceList_Setup() {
    this.InvoiceList_Config = {
      Id:"",
      Task: this._HelperService.AppConfig.Api.Core.GetUserInvoices,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Title: "Available Invoices",
      StatusType: "invoice",
      Type: this._HelperService.AppConfig.ListType.All,
      Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
      TableFields: [
        {
          DisplayName: '#REF',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          },
        {
          DisplayName: 'User',
          SystemName: 'UserAccountDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          },
        {
          DisplayName: 'Invoice',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          },


        {
          DisplayName: 'Amount',
          SystemName: 'TotalAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          
         
        },
        {
          DisplayName: 'Invoice Date',
          SystemName: 'InvoiceDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          
         
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          
         
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          
         
        },
      ]
    };
    this.InvoiceList_Config = this._DataHelperService.List_Initialize(
      this.InvoiceList_Config
    );
    this.InvoiceList_GetData();
  }
  InvoiceList_ToggleOption(event: any, Type: any) {
    this.InvoiceList_Config = this._DataHelperService.List_Operations(
      this.InvoiceList_Config,
      event,
      Type
    );
    if (this.InvoiceList_Config.RefreshData == true) {
      this.InvoiceList_GetData();
    }
  }
  InvoiceList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.InvoiceList_Config
    );
    this.InvoiceList_Config = TConfig;
  }
  InvoiceList_RowSelected(ReferenceData) {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Invoices.Details, ReferenceData.ReferenceKey, ReferenceData.UserAccountKey]);
  }



  public InvoiceList_Filter_Owners_Option: Select2Options;
  public InvoiceList_Filter_Owners_Selected = 0;
  InvoiceList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.PosTerminal,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.Acquirer
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.InvoiceList_Filter_Owners_Option = {
      placeholder: 'Sort by User',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  InvoiceList_Filter_Owners_Change(event: any) {
    if (event.value == this.InvoiceList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_Owners_Selected, '=');
      this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
      this.InvoiceList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.InvoiceList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_Owners_Selected, '=');
      this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
      this.InvoiceList_Filter_Owners_Selected = event.value;
      this.InvoiceList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountKey', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_Owners_Selected, '='));
    }
    this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  public InvoiceList_Filter_TransactionType_Option: Select2Options;
  public InvoiceList_Filter_TransactionType_Selected = 0;
  InvoiceList_Filter_TransactionTypes_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "SystemName",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "ParentCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.HelperTypes.InvoiceType
        }
      ]
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.InvoiceList_Filter_TransactionType_Option = {
      placeholder: 'Search By Type',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  InvoiceList_Filter_TransactionTypes_Change(event: any) {
    if (event.value == this.InvoiceList_Filter_TransactionType_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
      this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
      this.InvoiceList_Filter_TransactionType_Selected = 0;
    }
    else if (event.value != this.InvoiceList_Filter_TransactionType_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '=');
      this.InvoiceList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.InvoiceList_Config.SearchBaseConditions);
      this.InvoiceList_Filter_TransactionType_Selected = event.value;
      this.InvoiceList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Number, this.InvoiceList_Filter_TransactionType_Selected, '='));
    }
    this.InvoiceList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


}
