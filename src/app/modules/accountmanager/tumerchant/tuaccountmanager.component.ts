import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OUserDetails } from '../../../service/service';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: 'tu-accountmanager',
    templateUrl: './tuaccountmanager.component.html',
})
export class TUAccountManagerComponent implements OnInit {


    public ActivationProgress = 0;
    public ActivationTotalCount = 20;
    public ActivationCompletionCount = 0;
    public AccountActivationCheckList =
        {
            IsBusinessInformationComplete: false,
            IsConfigurationComplete: false,
            IsStoreComplete: false,
            IsBusinessCategoriesSet: false,
        }

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Merchant;
        this._HelperService.ContainerHeight = window.innerHeight;
        this.Form_AddUser_Load();
        this.GetBusinessCategories();
        this.F_AddStore_Load();
        this.StoresList_Setup();
        this.TerminalsList_Setup();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Banks_Load();
        this.TUTr_Filter_Providers_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.Get_UserAccountDetails(true);
            }
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    public RefreshActivationCheckList(_UserAccount: OUserDetails) {

        if (_UserAccount.DisplayName != undefined) {
            var PData =
            {
                Name: 'Display Name',
                Value: _UserAccount.DisplayName,
                IsComplete: true,
            }
        }
        else {
            var PData =
            {
                Name: 'Display Name',
                Value: '',
                IsComplete: false,
            }
        }





    }




    public Get_UserAccountDetails(ShowHeader: boolean) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict(
                "",
                "ReferenceKey",
                this._HelperService.AppConfig.DataType.Text,
                this._HelperService.AppConfig.ActiveReferenceKey,
                "="
            )
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._UserAccount = _Response.Result as OUserDetails;

                    this._UserAccount.DateOfBirth = this._HelperService.GetDateS(this._UserAccount.DateOfBirth);
                    this._UserAccount.LastLoginDateS = this._HelperService.GetDateTimeS(this._UserAccount.LastLoginDate);
                    this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._UserAccount.CreateDate);
                    this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._UserAccount.ModifyDate);
                    this._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._UserAccount.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                    this.StoresList_Setup();
                    this.TerminalsList_Setup();
                    this.ToggleStoreSelect = true;
                    this.TUTr_Filter_Stores_Load();
                    this.GetMerchantBusinessCategories();
                    this.GetMerchantConfigurations();
                    setTimeout(() => {
                        this.ToggleStoreSelect = false;

                    }, 300);
                    if (this._UserAccount.SubAccounts > 0) {
                        this.AccountActivationCheckList.IsStoreComplete = true;
                    }
                    this.AccountActivationCheckList.IsBusinessInformationComplete = true;
                    if (
                        this._UserAccount.DisplayName == null
                        || this._UserAccount.DisplayName == ''
                        || this._UserAccount.Name == null
                        || this._UserAccount.Name == ''

                        || this._UserAccount.ContactNumber == null
                        || this._UserAccount.ContactNumber == ''

                        || this._UserAccount.EmailAddress == null
                        || this._UserAccount.EmailAddress == ''

                        || this._UserAccount.AccountOperationTypeCode == null
                        || this._UserAccount.AccountOperationTypeCode == ''

                        || this._UserAccount.IconUrl == null
                        || this._UserAccount.IconUrl == ''
                        || this._UserAccount.IconUrl.endsWith('defaulticon.png')
                        || this._UserAccount.Description == null
                        || this._UserAccount.Description == ''

                        || this._UserAccount.Address == null
                        || this._UserAccount.Address == ''

                        || this._UserAccount.FirstName == null
                        || this._UserAccount.FirstName == ''

                        || this._UserAccount.LastName == null
                        || this._UserAccount.LastName == ''

                        || this._UserAccount.MobileNumber == null
                        || this._UserAccount.MobileNumber == ''

                        || this._UserAccount.EmailAddress == null
                        || this._UserAccount.EmailAddress == ''

                        || this._UserAccount.SecondaryEmailAddress == null
                        || this._UserAccount.SecondaryEmailAddress == ''
                    ) {
                        this.AccountActivationCheckList.IsBusinessInformationComplete = false;
                    }
                    this.RefreshActivationCheckList(this._UserAccount);
                } else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
    }
    Form_AddUser_Show() {
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            ReferenceKey: null,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            IconContent: this._HelperService._Icon_Cropper_Data,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update profile ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._UserAccount.ReferenceKey;
                _FormValue.UserName = this._UserAccount.UserName;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.Icon_Crop_Clear();
                            this.Get_UserAccountDetails(true);
                            this._HelperService.NotifySuccess('Profile updated successfully.');
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });



    }


    F_AddStore: FormGroup;
    F_AddStore_Address: string = null;
    F_AddStore_Latitude: number = 0;
    F_AddStore_Longitude: number = 0;
    F_AddStore_PlaceMarkerClick(event) {
        this.F_AddStore_Latitude = event.coords.lat;
        this.F_AddStore_Longitude = event.coords.lng;
    }
    public F_AddStore_AddressChange(address: Address) {
        this.F_AddStore_Latitude = address.geometry.location.lat();
        this.F_AddStore_Longitude = address.geometry.location.lng();
        this.F_AddStore_Address = address.formatted_address;
    }
    F_AddStore_Show() {
    }
    F_AddStore_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Stores]);
    }
    F_AddStore_Load() {

        this.F_AddStore = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: this._HelperService.UserAccount.AccountKey,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.email, Validators.minLength(2)])],
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            Latitude: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
            Longitude: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Owners: [],
        });
    }
    F_AddStore_Clear() {
        this.F_AddStore.reset();
        this.F_AddStore_Load();
        this.F_AddStore_Latitude = 0;
        this.F_AddStore_Longitude = 0;
        this.F_AddStore_Address = "";
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();

        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    F_AddStore_Process(_FormValue: any) {
        _FormValue.OwnerKey = this._UserAccount.ReferenceKey;
        _FormValue.Name = _FormValue.DisplayName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess('Store added');
                    this.F_AddStore_Clear();
                    this.F_AddStore_Load();
                    this.ToggleStoreSelect = true;
                    setTimeout(() => {
                        this.ToggleStoreSelect = false;
                    }, 300);
                    if (_FormValue.OperationType == 'edit') {
                        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Store.Dashboard, _Response.Result.ReferenceKey]);
                    }
                    else if (_FormValue.OperationType == 'close') {
                        this.F_AddStore_Close();
                    }
                    this.StoresList_Setup();
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    public StoresList_Config: OList;
    StoresList_Setup() {
        this.StoresList_Config = {
            Id: "",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "Available Stores",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._UserAccount.ReferenceId, '='),
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'ContactNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Email Address',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'Address',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Terminals',
                    SystemName: 'Terminals',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.StoresList_Config = this._DataHelperService.List_Initialize(
            this.StoresList_Config
        );
        if (this._UserAccount.ReferenceId != undefined) {
            this.StoresList_GetData();
        }


    }
    StoresList_ToggleOption(event: any, Type: any) {
        this.StoresList_Config = this._DataHelperService.List_Operations(
            this.StoresList_Config,
            event,
            Type
        );
        if (this.StoresList_Config.RefreshData == true) {
            this.StoresList_GetData();
        }
    }
    StoresList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.StoresList_Config
        );
        this.StoresList_Config = TConfig;
    }
    StoresList_RowSelected(ReferenceData) {
        swal({
            position: 'top',
            title: 'Delete store ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteUserAccount,
                    ReferenceKey: ReferenceData.ReferenceKey
                }
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Store account deleted');
                            this.StoresList_Setup();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }


    public TerminalId: string = null;
    public TerminalStoreKey: string = null;
    public TerminalAcquirerKey: string = null;
    public TerminalProviderKey: string = null;
    public ToggleStoreSelect: boolean = false;
    public TUTr_Filter_Store_Option: Select2Options;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._UserAccount.ReferenceId, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Select Store',
            ajax: _Transport,
            multiple: false,
            allowClear: false,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this.TerminalStoreKey = event.value;
    }
    public TUTr_Filter_Bank_Option: Select2Options;
    TUTr_Filter_Banks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
                }]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Bank_Option = {
            placeholder: 'Select Bank (Acquirer)',
            ajax: _Transport,
            multiple: false,
            allowClear: false,
        };
    }

    TUTr_Filter_Banks_Change(event: any) {
        this.TerminalAcquirerKey = event.value;
    }

    public TUTr_Filter_Provider_Option: Select2Options;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Select  Provider (PTSP)',
            ajax: _Transport,
            multiple: false,
            allowClear: false,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        this.TerminalProviderKey = event.value;
    }

    AddTerminal() {
        if (this.TerminalId == null || this.TerminalId == "") {
            this._HelperService.NotifyError('Enter terminal id');
        }
        else if (this.TerminalStoreKey == null || this.TerminalStoreKey == "") {
            this._HelperService.NotifyError('Select store');
        }
        else if (this.TerminalAcquirerKey == null || this.TerminalAcquirerKey == "") {
            this._HelperService.NotifyError('Select bank (acquirer)');
        }
        else if (this.TerminalProviderKey == null || this.TerminalProviderKey == "") {
            this._HelperService.NotifyError('Select provider (ptps)');
        }
        else {
            this._HelperService.ToggleField = true;

            this._HelperService.GeneratePassoword();
            var PData = {
                Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
                AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
                RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
                OwnerKey: this.TerminalProviderKey,
                SubOwnerKey: this.TerminalStoreKey,
                BankKey: this.TerminalAcquirerKey,
                UserName: this.TerminalId,
                Name: this.TerminalId,
                DisplayName: this.TerminalId,
                Password: this._HelperService.RandomPassword,
                StatusCode: this._HelperService.AppConfig.Status.Inactive,
                Owners: [{
                    OwnerKey: this._UserAccount.ReferenceKey,
                },
                {
                    OwnerKey: this.TerminalStoreKey,
                },
                {
                    OwnerKey: this.TerminalProviderKey,
                },
                {
                    OwnerKey: this.TerminalAcquirerKey,
                }],
            };
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.ToggleField = false;
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Terminal added');
                        this.TerminalId = null;
                        this.TerminalsList_Setup();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.ToggleField = false;

                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    public TerminalsList_Config: OList;
    TerminalsList_Setup() {
        this.TerminalsList_Config = {
            Id:"",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "Available Terminals",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._UserAccount.ReferenceId, '='),
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'TID',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Provider',
                    SystemName: 'ProviderDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Acquirer',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'StoreDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Re. %',
                    SystemName: 'RewardPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.TerminalsList_Config = this._DataHelperService.List_Initialize(
            this.TerminalsList_Config
        );
        if (this._UserAccount.ReferenceId != undefined) {
            this.TerminalsList_GetData();
        }
    }
    TerminalsList_ToggleOption(event: any, Type: any) {
        this.TerminalsList_Config = this._DataHelperService.List_Operations(
            this.TerminalsList_Config,
            event,
            Type
        );
        if (this.TerminalsList_Config.RefreshData == true) {
            this.TerminalsList_GetData();
        }
    }
    TerminalsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.TerminalsList_Config
        );
        this.TerminalsList_Config = TConfig;
    }
    TerminalsList_RowSelected(ReferenceData) {
        swal({
            position: 'top',
            title: 'Delete Terminal ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteUserAccount,
                    ReferenceKey: ReferenceData.ReferenceKey
                }
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Terminal account deleted');
                            this.TerminalsList_Setup();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });
    }

    public OriginalBusinessCategories = [];
    public BusinessCategories = [];
    public MerchantBusinessCategories = [];
    GetBusinessCategories() {
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.OriginalBusinessCategories = _Response.Result.Data;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    GetMerchantBusinessCategories() {
        this.BusinessCategories = [];
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserParameters,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            "="
        );
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "UserAccountKey",
            this._HelperService.AppConfig.DataType.Text,
            this._UserAccount.ReferenceKey,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        this.MerchantBusinessCategories = _Response.Result.Data;
                        this.OriginalBusinessCategories.forEach(element => {
                            var ElementIndexSearch = this.MerchantBusinessCategories.findIndex(x => x.CommonKey == element.ReferenceKey);
                            if (ElementIndexSearch == -1) {
                                this.BusinessCategories.push(element);
                            }
                        });
                    }
                    if (this.MerchantBusinessCategories.length > 0) {
                        this.AccountActivationCheckList.IsBusinessCategoriesSet = true;
                    }
                    else {
                        this.AccountActivationCheckList.IsBusinessCategoriesSet = false;
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    SaveMerchantBusinessCategory(item) {
        if (item != '0') {
            var Setup =
            {
                Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
                TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
                UserAccountKey: this._UserAccount.ReferenceKey,
                CommonKey: item,
                StatusCode: 'default.active'
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
            _OResponse.subscribe(
                _Response => {
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Business category assigned to merchant');
                        this.BusinessCategories = [];
                        this.GetMerchantBusinessCategories();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    DeleteMerchantBusinessCategory(Item) {
        swal({
            position: 'top',
            title: 'Remove merchant business category  ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.Core.DeleteUserParameter,
                    ReferenceKey: Item.ReferenceKey
                }
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Merchant business category removed successfully')
                            this.GetMerchantBusinessCategories();
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    public _Configurations = {
        RewardPercentage: null,
        RewardMaximumInvoiceAmount: null,
        RewardDeductionTypeCode: null,
        RewardDeductionType: null,
        MaximumRewardDeductionAmount: null,

        ThankUPlus: null,
        ThankUPlusCriteriaType: null,
        ThankUPlusCriteriaTypeCode: null,
        ThankUPlusCriteriaValue: null,
        ThankUPlusMinimumTransferAmount: null,

        SettlmentSchedule: null,
        SettlmentScheduleCode: null,
        MinimumSettlementAmount: null
    };
    GetMerchantConfigurations() {
        var PData =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserParameters,
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
            "="
        );
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "UserAccountKey",
            this._HelperService.AppConfig.DataType.Text,
            this._UserAccount.ReferenceKey,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var ResposeData = _Response.Result.Data;
                        ResposeData.forEach(element => {
                            if (element.CommonSystemName == "rewardpercentage") {
                                this._Configurations.RewardPercentage = element.Value;
                            }
                            if (element.CommonSystemName == "rewardmaxinvoiceamount") {
                                this._Configurations.RewardMaximumInvoiceAmount = element.Value;
                            }
                            if (element.CommonSystemName == "rewarddeductiontype") {
                                this._Configurations.RewardDeductionType = element.HelperName;
                                this._Configurations.RewardDeductionTypeCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "maximumrewarddeductionamount") {
                                this._Configurations.MaximumRewardDeductionAmount = element.Value;
                            }
                            if (element.CommonSystemName == "settlementScheduletype") {
                                this._Configurations.SettlmentSchedule = element.HelperName;
                                this._Configurations.SettlmentScheduleCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "minimumsettlementamount") {
                                this._Configurations.MinimumSettlementAmount = element.Value;
                            }

                            if (element.CommonSystemName == "thankucashplus") {
                                this._Configurations.ThankUPlus = element.Value;
                            }
                            if (element.CommonSystemName == "thankucashplusrewardcriteria") {
                                this._Configurations.ThankUPlusCriteriaValue = element.Value;
                                this._Configurations.ThankUPlusCriteriaType = element.HelperName;
                                this._Configurations.ThankUPlusCriteriaTypeCode =
                                    element.HelperCode;
                            }
                            if (element.CommonSystemName == "thankucashplusmintransferamount") {
                                this._Configurations.ThankUPlusMinimumTransferAmount =
                                    element.Value;
                            }
                        });
                    }
                    this.AccountActivationCheckList.IsConfigurationComplete = true;
                    if (
                        this._Configurations.RewardPercentage == null
                        || this._Configurations.RewardMaximumInvoiceAmount == null
                        || this._Configurations.RewardDeductionTypeCode == null
                        || this._Configurations.SettlmentSchedule == null
                        || this._Configurations.MinimumSettlementAmount == null
                        || this._Configurations.RewardDeductionType == null
                        || this._Configurations.MaximumRewardDeductionAmount == null
                        || this._Configurations.ThankUPlus == null
                    ) {
                        this.AccountActivationCheckList.IsConfigurationComplete = false;
                    }
                    if (this._Configurations.ThankUPlus == 1) {
                        if (
                            this._Configurations.ThankUPlusCriteriaType == null
                            || this._Configurations.ThankUPlusCriteriaTypeCode == null
                            || this._Configurations.ThankUPlusCriteriaValue == null
                            || this._Configurations.ThankUPlusMinimumTransferAmount == null
                        ) {
                            this.AccountActivationCheckList.IsConfigurationComplete = false;
                        }
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    UpdateConfiguration(ConfigurationKey: string, ConfigurationValue: any, SubConfigurationValue: any, HelperCode: any) {
        this._HelperService.IsFormProcessing = true;
        var PostData = {
            Task: "saveaccountconfiguration",
            TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
            UserAccountKey: this._UserAccount.ReferenceKey,
            ConfigurationKey: ConfigurationKey,
            Value: ConfigurationValue,
            SubValue: SubConfigurationValue,
            StatusCode: this._HelperService.AppConfig.Status.Active,
            HelperCode: HelperCode,
        };
        this._HelperService.IsFormProcessing = false;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Configuration Updated");
                    this._HelperService.CloseModal("M_UpdateRewardPercentage");
                    this.GetMerchantConfigurations();
                } else {
                    this._HelperService.NotifySuccess(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
    RewardPercentage_Open() {
        this._HelperService.OpenModal('Modal_RewardPercentage');
    }
    RewardPercentage_Update() {
        if (
            this._Configurations.RewardPercentage == undefined ||
            this._Configurations.RewardPercentage == null ||
            this._Configurations.RewardPercentage == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter reward percentage"
            );
        } else if (isNaN(this._Configurations.RewardPercentage) == true) {
            this._HelperService.NotifyError(
                "Please enter valid reward percentage"
            );
        } else if (parseFloat(this._Configurations.RewardPercentage) < 0) {
            this._HelperService.NotifyError(
                "Reward percentage must be between 0 to 100"
            );
        } else if (parseFloat(this._Configurations.RewardPercentage) > 100) {
            this._HelperService.NotifyError(
                "Reward percentage must be between 0 to 100"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update reward percentage ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'rewardpercentage',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.RewardPercentage,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardPercentage");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }



    MaximumRewardInvoiceAmount_Open() {
        this._HelperService.OpenModal('Modal_RewardsMaximumInvoiceLimit');
    }
    MaximumRewardInvoiceAmount_Update() {
        if (
            this._Configurations.RewardMaximumInvoiceAmount == undefined ||
            this._Configurations.RewardMaximumInvoiceAmount == null ||
            this._Configurations.RewardMaximumInvoiceAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter maximum reward invoice amount"
            );
        } else if (isNaN(this._Configurations.RewardMaximumInvoiceAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid maximum reward invoice amount"
            );
        } else if (parseFloat(this._Configurations.RewardMaximumInvoiceAmount) < 0) {
            this._HelperService.NotifyError(
                "Maximum reward invoice amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.RewardMaximumInvoiceAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Maximum reward invoice amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update maximum reward invoice amount ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'rewardmaxinvoiceamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.RewardMaximumInvoiceAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardsMaximumInvoiceLimit");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }




    MinimumSettlementAmount_Open() {
        this._HelperService.OpenModal('Modal_MinimumSettlementAmount');
    }
    MinimumSettlementAmount_Update() {
        if (
            this._Configurations.MinimumSettlementAmount == undefined ||
            this._Configurations.MinimumSettlementAmount == null ||
            this._Configurations.MinimumSettlementAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter minimum settlement amount"
            );
        } else if (isNaN(this._Configurations.MinimumSettlementAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid minimum settlement amount"
            );
        } else if (parseFloat(this._Configurations.MinimumSettlementAmount) < 0) {
            this._HelperService.NotifyError(
                "Minimum settlement amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.MinimumSettlementAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Minimum settlement amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update minimum settlement amount ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'minimumsettlementamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.MinimumSettlementAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_MinimumSettlementAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }

    RewardWalletMaxDeductionAmt_Open() {
        this._HelperService.OpenModal('Modal_RewardsWalletMaximumAmountDeduction');
    }
    RewardWalletMaxDeductionAmt_Update() {
        if (
            this._Configurations.MaximumRewardDeductionAmount == undefined ||
            this._Configurations.MaximumRewardDeductionAmount == null ||
            this._Configurations.MaximumRewardDeductionAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter rewards wallet maximum amount deduction"
            );
        } else if (isNaN(this._Configurations.MaximumRewardDeductionAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid rewards wallet maximum amount deduction"
            );
        } else if (parseFloat(this._Configurations.MaximumRewardDeductionAmount) < 0) {
            this._HelperService.NotifyError(
                "Rewards wallet maximum amount deduction   must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.MaximumRewardDeductionAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Rewards wallet maximum amount deduction   must be between 0 to 10000000"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update rewards wallet maximum amount deduction ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'maximumrewarddeductionamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.MaximumRewardDeductionAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_RewardsWalletMaximumAmountDeduction");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }
    SettlementSchedule_Open() {
        this._HelperService.OpenModal('Modal_SettlementSchedule');
    }
    SettlementSchedule_Update() {
        swal({
            position: 'top',
            title: 'Update settlement Schedule?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: 'settlementScheduletype',
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                    Value: this._Configurations.SettlmentSchedule,
                    HelperCode: this._Configurations.SettlmentScheduleCode,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_SettlementSchedule");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    PaymentCollectionMethod_Open() {
        this._HelperService.OpenModal('Modal_PaymentCollectionMethod');
    }
    PaymentCollectionMethod_Update() {
        swal({
            position: 'top',
            title: 'Rewards Payments collection method ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: 'rewarddeductiontype',
                    Value: this._Configurations.RewardDeductionType,
                    HelperCode: this._Configurations.RewardDeductionTypeCode,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_SettlementSchedule");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    UpdateMerchantStatus() {
        swal({
            position: 'top',
            title: 'Activate merchant ?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "activateaccount",
                    UserAccountKey: this._UserAccount.ReferenceKey,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Merchant activated");
                            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);

                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }

    ThankUCashPlusStatus_Open() {
        this._HelperService.OpenModal('Modal_ThankUChashPlusStatus');
    }
    ThankUCashPlusStatus_Update() {
        swal({
            position: 'top',
            title: 'Update ThankUCash Plus Status?',
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService.IsFormProcessing = true;
                var PostData = {
                    Task: "saveaccountconfiguration",
                    TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                    UserAccountKey: this._UserAccount.ReferenceKey,
                    ConfigurationKey: "thankucashplus",
                    Value: this._Configurations.ThankUPlus,
                    StatusCode: this._HelperService.AppConfig.Status.Active,
                };

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Configuration Updated");
                            this._HelperService.CloseModal("Modal_ThankUChashPlusStatus");
                            this.GetMerchantConfigurations();
                        } else {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }


    TUCPlusRewardCriteria_Open() {
        this._HelperService.OpenModal('Modal_TUCPlusRewardCriteria');
    }
    TUCPlusRewardCriteria_Update() {
        if (
            this._Configurations.ThankUPlusCriteriaValue == undefined ||
            this._Configurations.ThankUPlusCriteriaValue == null ||
            this._Configurations.ThankUPlusCriteriaValue == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter minimum transfer amount"
            );
        } else if (isNaN(this._Configurations.ThankUPlusCriteriaValue) == true) {
            this._HelperService.NotifyError(
                "Please enter valid minimum transfer amount"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusCriteriaValue) < 0) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusCriteriaValue) > 10000000) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update reward criteria ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: "thankucashplusrewardcriteria",
                        HelperCode: this._Configurations.ThankUPlusCriteriaTypeCode,
                        Value: this._Configurations.ThankUPlusCriteriaValue,
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                    };
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_TUCPlusMinTransferAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }


    TUCPlusMinTransferAmount_Open() {
        this._HelperService.OpenModal('Modal_TUCPlusMinTransferAmount');
    }
    TUCPlusMinTransferAmount_Update() {
        if (
            this._Configurations.ThankUPlusMinimumTransferAmount == undefined ||
            this._Configurations.ThankUPlusMinimumTransferAmount == null ||
            this._Configurations.ThankUPlusMinimumTransferAmount == ""
        ) {
            this._HelperService.NotifyError(
                "Please enter minimum transfer amount"
            );
        } else if (isNaN(this._Configurations.ThankUPlusMinimumTransferAmount) == true) {
            this._HelperService.NotifyError(
                "Please enter valid minimum transfer amount"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusMinimumTransferAmount) < 0) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else if (parseFloat(this._Configurations.ThankUPlusMinimumTransferAmount) > 10000000) {
            this._HelperService.NotifyError(
                "Minimum transfer amount  must be between 0 to 10000000"
            );
        } else {
            swal({
                position: 'top',
                title: 'Update minimum transfer amount ?',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {
                    this._HelperService.IsFormProcessing = true;
                    var PostData = {
                        Task: "saveaccountconfiguration",
                        TypeCode: this._HelperService.AppConfig.HelperTypes.ConfigurationValue,
                        UserAccountKey: this._UserAccount.ReferenceKey,
                        ConfigurationKey: 'thankucashplusmintransferamount',
                        StatusCode: this._HelperService.AppConfig.Status.Active,
                        Value: this._Configurations.ThankUPlusMinimumTransferAmount,
                    };

                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.ThankU, PostData);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Configuration Updated");
                                this._HelperService.CloseModal("Modal_TUCPlusMinTransferAmount");
                                this.GetMerchantConfigurations();
                            } else {
                                this._HelperService.NotifySuccess(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });


        }
    }


    public _UserAccount: OUserDetails = {
        ContactNumber: null,
        SecondaryEmailAddress: null,
        ReferenceId: null,
        BankDisplayName: null,
        BankKey: null,
        SubOwnerAddress: null,
        SubOwnerLatitude: null,
        SubOwnerDisplayName: null,
        SubOwnerKey: null,
        SubOwnerLongitude: null,
        AccessPin: null,
        LastLoginDateS: null,
        AppKey: null,
        AppName: null,
        AppVersionKey: null,
        CreateDate: null,
        CreateDateS: null,
        CreatedByDisplayName: null,
        CreatedByIconUrl: null,
        CreatedByKey: null,
        Description: null,
        IconUrl: null,
        ModifyByDisplayName: null,
        ModifyByIconUrl: null,
        ModifyByKey: null,
        ModifyDate: null,
        ModifyDateS: null,
        PosterUrl: null,
        ReferenceKey: null,
        StatusCode: null,
        StatusI: null,
        StatusId: null,
        StatusName: null,
        AccountCode: null,
        AccountOperationTypeCode: null,
        AccountOperationTypeName: null,
        AccountTypeCode: null,
        AccountTypeName: null,
        Address: null,
        AppVersionName: null,
        ApplicationStatusCode: null,
        ApplicationStatusName: null,
        AverageValue: null,
        CityAreaKey: null,
        CityAreaName: null,
        CityKey: null,
        CityName: null,
        CountValue: null,
        CountryKey: null,
        CountryName: null,
        DateOfBirth: null,
        DisplayName: null,
        EmailAddress: null,
        EmailVerificationStatus: null,
        EmailVerificationStatusDate: null,
        FirstName: null,
        GenderCode: null,
        GenderName: null,
        LastLoginDate: null,
        LastName: null,
        Latitude: null,
        Longitude: null,
        MobileNumber: null,
        Name: null,
        NumberVerificationStatus: null,
        NumberVerificationStatusDate: null,
        OwnerDisplayName: null,
        OwnerKey: null,
        Password: null,
        Reference: null,
        ReferralCode: null,
        ReferralUrl: null,
        RegionAreaKey: null,
        RegionAreaName: null,
        RegionKey: null,
        RegionName: null,
        RegistrationSourceCode: null,
        RegistrationSourceName: null,
        RequestKey: null,
        RoleKey: null,
        RoleName: null,
        SecondaryPassword: null,
        SystemPassword: null,
        UserName: null,
        WebsiteUrl: null
    };





}