import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import swal from 'sweetalert2';

@Component({
    selector: 'tu-accountmanager',
    templateUrl: './tuaccountmanager.component.html',
})
export class TUAccountManagerComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
                this.Form_UpdateUser_Load();
            }
        });
    }

    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_UpdateUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }

    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Address: string = null;
    Form_UpdateUser_Latitude: number = 0;
    Form_UpdateUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_UpdateUser_PlaceMarkerClick(event) {
        this.Form_UpdateUser_Latitude = event.coords.lat;
        this.Form_UpdateUser_Longitude = event.coords.lng;
    }
    public Form_UpdateUser_AddressChange(address: Address) {
        this.Form_UpdateUser_Latitude = address.geometry.location.lat();
        this.Form_UpdateUser_Longitude = address.geometry.location.lng();
        this.Form_UpdateUser_Address = address.formatted_address;
    }
    Form_UpdateUser_Show() {
    }
    Form_UpdateUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Acquirers]);
    }
    Form_UpdateUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_UpdateUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            Password: [null, Validators.compose([Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            IconContent: this._HelperService._Icon_Cropper_Data,
        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_UpdateUser_Latitude = 0;
        this.Form_UpdateUser_Longitude = 0;
        this.Form_UpdateUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_UpdateUser_Load();
    }
    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Account details updated successfully');
                            this._HelperService.Get_UserAccountDetails(true);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });



    }
}