import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
declare var moment: any;
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon,
    OOverview
} from "../../../../service/service";
import { DaterangePickerComponent } from "ng2-daterangepicker";

@Component({
    selector: "tulivemerchants",
    templateUrl: "./tulivemerchants.component.html",
    styles: [
        `
      agm-map {
        height: 300px;
      }
    `
    ]
})
export class TULiveMerchantsComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) {
        this._HelperService.AppConfig.ShowMenu = false;
        this._HelperService.AppConfig.ShowHeader = false;
        this._HelperService.ContainerHeight = window.innerHeight;
        this.todaysdate = this._HelperService.GetDateS(new Date());
        setInterval(() => {
            this.now = Date.now();
        }, 1);
    }
    public GenderChart = [];
    public AutoRefresh = 0;
    ngOnInit() {
        this._HelperService.FullContainer = true;
        this.LoadData();
    }
    public RefreshTimer = null;
    ToggleAutoRefresh() {
        if (this.RefreshTimer) {
            clearInterval(this.RefreshTimer);
        }
        if (this.interval) {
            this.timeLeft = 120;
            clearInterval(this.interval);
        }
        this.RefreshTimer = setInterval(() => {
            if (this.AutoRefresh) {
                this.LoadData();
            }
        }, 120000);
        if (this.AutoRefresh) {
            this.startTimer();
        }
    }

    public todaysdate: string = null;
    public now: number;
    timeLeft: number = 120;
    interval;
    StartTime = null;
    EndTime = null;
    startTimer() {
        this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                this.timeLeft = 120;
            }
        }, 1000);
    }

    LoadData() {
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf("day");
            this.EndTime = moment().endOf("day");
        }
        this.GetLiveTerminals();
    }

    public AllTerminals = [];
    public LiveTerminals = {
        Data: [],
    };
    public TotalTerminals = 0;
    public LiveTerminalCount = 0;
    public IdleTerminalCount = 0;
    public DeadTerminalCount = 0;
    public Providers: any[] = [];
    public Merchants: any[] = [];
    public Stores: any[] = [];
    public Acquirers: any[] = [];
    public SelectedMerchant = '0';
    public SelectedTerminalStatus = '0';
    public SelectedStore = '0';
    public SelectedProvider = '0';

    GetLiveTerminals() {
        this.AllTerminals = [];
        this.LiveTerminals = {
            Data: [],
        };

        this.TotalTerminals = 0;
        this.LiveTerminalCount = 0;
        this.IdleTerminalCount = 0;
        this.DeadTerminalCount = 0;
        this.Providers = [];
        this.Merchants = [];
        this.Stores = [];
        this.Acquirers = [];
        this.SelectedMerchant = '0';
        this.SelectedTerminalStatus = '0';


        this.LiveTerminals = {
            Data: [],
        };
        this._HelperService.IsFormProcessing = true;
        var SearchCondition = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=');
        var Data = {
            Task: "getlivemerchants",
            Type: "acquirer",
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.LiveTerminals = _Response.Result;
                    this.AllTerminals = this.LiveTerminals.Data;
                    this.RefreshTerminalsData();
                    this.TUTr_Setup();

                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public TerminalViewChange() {
        var TempResults = this.AllTerminals;
        if (this.SelectedMerchant != '' && this.SelectedMerchant != '0') {
            TempResults = TempResults.filter(x => x.MerchantDisplayName == this.SelectedMerchant);
        }
        if (this.SelectedStore != '' && this.SelectedStore != '0') {
            TempResults = TempResults.filter(x => x.StoreDisplayName == this.SelectedStore);
        }
        if (this.SelectedProvider != '' && this.SelectedProvider != '0') {
            TempResults = TempResults.filter(x => x.ProviderDisplayName == this.SelectedProvider);
        }
        if (this.SelectedTerminalStatus != '' && this.SelectedTerminalStatus != '0') {
            TempResults = TempResults.filter(x => x.Type == this.SelectedTerminalStatus);
        }
        this.LiveTerminals.Data = TempResults;
        // else {
        //     if (this.SelectedMerchant != '' && this.SelectedMerchant != '0') {
        //         this.LiveTerminals.Data = this.AllTerminals.filter(x => x.MerchantDisplayName == this.SelectedMerchant);
        //     }
        //     else {
        //         this.LiveTerminals.Data = this.AllTerminals;
        //     }
        // }
        // this.RefreshTerminalsData();

    }

    public RefreshTerminalsData() {
        if (this.LiveTerminals != undefined) {
            this.TotalTerminals = this.LiveTerminals.Data.length;
            this.LiveTerminals.Data.forEach(element => {
                var TLive = 0;
                var TIdle = 0;
                var TDead = 0;
                var DifferenceInfo = this._HelperService.GetTimeDifference(element.LastTransactionDate, moment());
                if (DifferenceInfo.Days == 0) {
                    element.Type = 'live';
                    TLive = 1;
                    this.LiveTerminalCount = this.LiveTerminalCount + 1;
                }
                else if (DifferenceInfo.Days > 0 && DifferenceInfo.Days < 8) {
                    element.Type = 'idle';
                    TIdle = 1;
                    this.IdleTerminalCount = this.IdleTerminalCount + 1;
                }
                else {
                    element.Type = 'dead';
                    TDead = 1;
                    this.DeadTerminalCount = this.DeadTerminalCount + 1;
                }
                element.LastTransactionDateD = this._HelperService.GetDateS(element.LastTransactionDate);
                element.LastTransactionDateT = this._HelperService.GetTimeS(element.LastTransactionDate);
                if (element.LastTransactionDate != "0001-01-01 00:00:00.000000") {
                    element.LastTransactionDateDiffS = this._HelperService.GetTimeDifferenceS(element.LastTransactionDate, moment());
                    element.LastTransactionDateDiff = this._HelperService.GetTimeDifference(element.LastTransactionDate, moment());
                }
                element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);

                var ProviderIndex = this.Providers.findIndex(x => x.Name == element.ProviderDisplayName);
                if (ProviderIndex != -1) {
                    this.Providers[ProviderIndex].TotalTerminals = this.Providers[ProviderIndex].TotalTerminals + 1;
                    this.Providers[ProviderIndex].LiveTerminals = this.Providers[ProviderIndex].LiveTerminals + TLive;
                    this.Providers[ProviderIndex].IdleTerminals = this.Providers[ProviderIndex].IdleTerminals + TIdle;
                    this.Providers[ProviderIndex].DeadTerminals = this.Providers[ProviderIndex].DeadTerminals + TDead;
                    this.Providers[ProviderIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Providers[ProviderIndex].IconUrl = element.ProviderIconUrl;

                    this.Providers[ProviderIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Providers[ProviderIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Providers[ProviderIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Providers[ProviderIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;

                }
                else {
                    var PItem =
                    {
                        Name: element.ProviderDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.ProviderIconUrl,
                    };
                    this.Providers.push(PItem);
                }
                var AcquirerIndex = this.Acquirers.findIndex(x => x.Name == element.AcquirerDisplayName);
                if (AcquirerIndex != -1) {
                    this.Acquirers[AcquirerIndex].TotalTerminals = this.Acquirers[AcquirerIndex].TotalTerminals + 1;
                    this.Acquirers[AcquirerIndex].LiveTerminals = this.Acquirers[AcquirerIndex].LiveTerminals + TLive;
                    this.Acquirers[AcquirerIndex].IdleTerminals = this.Acquirers[AcquirerIndex].IdleTerminals + TIdle;
                    this.Acquirers[AcquirerIndex].DeadTerminals = this.Acquirers[AcquirerIndex].DeadTerminals + TDead;
                    this.Acquirers[AcquirerIndex].LastTransactionDate = element.LastTransactionDate;
                    if (element.AcquirerIconUrl != undefined) {
                        this.Acquirers[AcquirerIndex].IconUrl = element.AcquirerIconUrl;
                    }
                    this.Acquirers[AcquirerIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Acquirers[AcquirerIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Acquirers[AcquirerIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Acquirers[AcquirerIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                }
                else {
                    var PItem =
                    {
                        Name: element.AcquirerDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.AcquirerIconUrl,
                    };
                    this.Acquirers.push(PItem);
                }

                var MerchantIndex = this.Merchants.findIndex(x => x.Name == element.MerchantDisplayName);
                if (MerchantIndex != -1) {
                    this.Merchants[MerchantIndex].TotalTerminals = this.Merchants[MerchantIndex].TotalTerminals + 1;
                    this.Merchants[MerchantIndex].LiveTerminals = this.Merchants[MerchantIndex].LiveTerminals + TLive;
                    this.Merchants[MerchantIndex].IdleTerminals = this.Merchants[MerchantIndex].IdleTerminals + TIdle;
                    this.Merchants[MerchantIndex].DeadTerminals = this.Merchants[MerchantIndex].DeadTerminals + TDead;
                    this.Merchants[MerchantIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Merchants[MerchantIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Merchants[MerchantIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Merchants[MerchantIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Merchants[MerchantIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                    this.Merchants[MerchantIndex].IconUrl = element.MerchantIconUrl;
                }
                else {
                    var PItem =
                    {
                        Name: element.MerchantDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.MerchantIconUrl,
                    };
                    this.Merchants.push(PItem);
                }

                var StoreIndex = this.Stores.findIndex(x => x.Name == element.StoreDisplayName);
                if (StoreIndex != -1) {
                    this.Stores[StoreIndex].TotalTerminals = this.Stores[StoreIndex].TotalTerminals + 1;
                    this.Stores[StoreIndex].LiveTerminals = this.Stores[StoreIndex].LiveTerminals + TLive;
                    this.Stores[StoreIndex].IdleTerminals = this.Stores[StoreIndex].IdleTerminals + TIdle;
                    this.Stores[StoreIndex].DeadTerminals = this.Stores[StoreIndex].DeadTerminals + TDead;
                    this.Stores[StoreIndex].LastTransactionDate = element.LastTransactionDate;
                    this.Stores[StoreIndex].IconUrl = element.MerchantIconUrl;
                    this.Stores[StoreIndex].LastTransactionDateD = element.LastTransactionDateD;
                    this.Stores[StoreIndex].LastTransactionDateT = element.LastTransactionDateT;
                    this.Stores[StoreIndex].LastTransactionDateDiffS = element.LastTransactionDateDiffS;
                    this.Stores[StoreIndex].LastTransactionDateDiff = element.LastTransactionDateDiff;
                }
                else {
                    var PItem =
                    {
                        Name: element.StoreDisplayName,
                        TotalTerminals: 1,
                        LiveTerminals: TLive,
                        IdleTerminals: TIdle,
                        DeadTerminals: TDead,
                        LastTransactionDate: element.LastTransactionDate,
                        LastTransactionDateD: element.LastTransactionDateD,
                        LastTransactionDateT: element.LastTransactionDateT,
                        LastTransactionDateDiffS: element.LastTransactionDateDiffS,
                        LastTransactionDateDiff: element.LastTransactionDateDiff,
                        IconUrl: element.MerchantIconUrl,
                    };
                    this.Stores.push(PItem);
                }

            });
        }
    }


    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
            {
                Id:"",
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
                Title: 'Sales History',
                StatusType: 'transaction',
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                Type: this._HelperService.AppConfig.ListType.All,
                Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
                RefreshCount: false,
                PageRecordLimit: 8,
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false,
                        // NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                ]
            }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    // Overview = null;
    // GetPosTerminalsStatusOverview() {
    //     this._HelperService.IsFormProcessing = true;
    //     var Data = {
    //         Task: "getposterminalsstatusoverview",
    //         StartTime: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
    //         EndTime: this.EndTime, // moment().add(2, 'days'),
    //         UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(
    //         this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
    //         Data
    //     );
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.IsFormProcessing = false;
    //             if (_Response.Status == this._HelperService.StatusSuccess) {
    //                 this._AccountOverview = _Response.Result as OAccountOverview;
    //                 if (this._AccountOverview.PosOverview != undefined) {
    //                     this._AccountOverview.PosOverview.forEach(element => {
    //                         element.LastTransactionDateD = this._HelperService.GetDateS(
    //                             element.LastTransactionDate
    //                         );
    //                         element.LastTransactionDateT = this._HelperService.GetTimeS(
    //                             element.LastTransactionDate
    //                         );
    //                         element.LastTransactionDate = this._HelperService.GetDateTimeS(
    //                             element.LastTransactionDate
    //                         );
    //                     });
    //                 }
    //             } else {
    //                 this._HelperService.NotifyError(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.IsFormProcessing = false;
    //             this._HelperService.HandleException(_Error);
    //         }
    //     );
    // }


    public _AccountOverview: OAccountOverview = {
        AppUsers: 0,
        CardRewardPurchaseAmount: 0,
        CashRewardPurchaseAmount: 0,
        IssuerCommissionAmount: 0,
        OtherRewardPurchaseAmount: 0,
        PurchaseAmount: 0,
        RedeemAmount: 0,
        RepeatingAppUsers: 0,
        RewardAmount: 0,
        RewardChargeAmount: 0,
        TUCPlusRewardAmount: 0,
        TUCPlusRewardChargeAmount: 0,
        TUCPlusRewardClaimedAmount: 0,
        Transactions: 0,
        UniqueAppUsers: 0,
        PosOverview: []
    };
    // public PosTerminalsByTransactionsAsc = {
    //     TotalRecords: 0,
    //     Data: [],
    //     Offset: 0,
    //     Limit: 0
    // };
    // GetPosTerminalsByTransactionsAsc() {
    //     this.PosTerminalsByTransactions = {
    //         TotalRecords: 0,
    //         Data: [],
    //         Offset: 0,
    //         Limit: 0
    //     };
    //     this._HelperService.IsFormProcessing = true;
    //     // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'MerchantKey', 'text', this._HelperService.AppConfig.ActiveOwnerKey, '=');
    //     var SearchCondition = "";
    //     if (
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.Merchant ||
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.MerchantSubAccount
    //     ) {
    //         SearchCondition = this._HelperService.GetSearchConditionStrict(
    //             "",
    //             "MerchantKey",
    //             "text",
    //             this._HelperService.AppConfig.ActiveOwnerKey,
    //             "="
    //         );
    //     } else if (
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.Acquirer ||
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.AcquirerSubAccount
    //     ) {
    //         SearchCondition = this._HelperService.GetSearchConditionStrict(
    //             "",
    //             "AcquirerKey",
    //             "text",
    //             this._HelperService.AppConfig.ActiveOwnerKey,
    //             "="
    //         );
    //     } else if (
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.Store ||
    //         this._HelperService.UserAccount.AccountTypeCode ==
    //         this._HelperService.AppConfig.AccountType.StoreSubAccount
    //     ) {
    //         SearchCondition = this._HelperService.GetSearchConditionStrict(
    //             "",
    //             "StoreKey",
    //             "text",
    //             this._HelperService.AppConfig.ActiveOwnerKey,
    //             "="
    //         );
    //     } else {
    //         SearchCondition = this._HelperService.GetSearchConditionStrict(
    //             "",
    //             "MerchantKey",
    //             "text",
    //             this._HelperService.AppConfig.ActiveOwnerKey,
    //             "="
    //         );
    //     }
    //     SearchCondition = this._HelperService.GetSearchConditionStrict(
    //         SearchCondition,
    //         "Transactions",
    //         "text",
    //         "0",
    //         "="
    //     );

    //     var Data = {
    //         Task: "getposterminalsoverviewlist",
    //         StartDate: this.StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
    //         EndDate: this.EndTime, // moment().add(2, 'days'),
    //         // StartDate: new Date(2017, 0, 1, 0, 0, 0, 0),
    //         // EndDate: moment().add(2, 'days'),
    //         Type: "subowner",
    //         SortExpression: "LastTransactionDate asc",
    //         SearchCondition: SearchCondition, //this._HelperService.GetSearchConditionStrict('', 'MerchantKey', 'text', this._HelperService.AppConfig.ActiveOwnerKey, '='),
    //         ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(
    //         this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
    //         Data
    //     );
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.IsFormProcessing = false;
    //             if (_Response.Status == this._HelperService.StatusSuccess) {
    //                 this.PosTerminalsByTransactionsAsc = _Response.Result;
    //                 if (this.PosTerminalsByTransactionsAsc != undefined) {
    //                     this.PosTerminalsByTransactionsAsc.Data.forEach(element => {
    //                         element.LastTransactionDateD = this._HelperService.GetDateS(
    //                             element.LastTransactionDate
    //                         );
    //                         element.LastTransactionDateT = this._HelperService.GetTimeS(
    //                             element.LastTransactionDate
    //                         );
    //                         if (element.LastTransactionDate != "0001-01-01 00:00:00.000000") {
    //                             element.LastTransactionDateDiff = this._HelperService.GetTimeDifferenceS(
    //                                 element.LastTransactionDate,
    //                                 moment()
    //                             );
    //                         }
    //                         element.LastTransactionDate = this._HelperService.GetDateTimeS(
    //                             element.LastTransactionDate
    //                         );
    //                     });
    //                 }
    //             } else {
    //                 this._HelperService.NotifyError(_Response.Message);
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.IsFormProcessing = false;
    //             this._HelperService.HandleException(_Error);
    //         }
    //     );
    // }
}

export class OAccountOverview {
    public AppUsers: number;
    public RepeatingAppUsers: number;
    public UniqueAppUsers: number;
    public RewardChargeAmount: number;
    public TUCPlusRewardChargeAmount: number;
    public Transactions: number;
    public PurchaseAmount: number;
    public IssuerCommissionAmount: number;
    public CashRewardPurchaseAmount: number;
    public CardRewardPurchaseAmount: number;
    public OtherRewardPurchaseAmount: number;
    public RedeemAmount: number;
    public RewardAmount: number;
    public TUCPlusRewardAmount: number;
    public TUCPlusRewardClaimedAmount: number;
    public PosOverview: any[];
}
