import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TutransactionsComponent } from './tutransactions.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
const routes: Routes = [
  { path: '', component: TutransactionsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TutransactionsRoutingModule { }

@NgModule({
  declarations: [TutransactionsComponent],
  imports: [
    CommonModule,
    FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        TutransactionsRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
  ]
})
export class TutransactionsModule { }
