import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HelperService, DataHelperService, OList, OResponse, OSelect, FilterHelperService } from 'src/app/service/service';
import * as Feather from 'feather-icons';
import { Observable } from 'rxjs';
declare var moment: any;
import swal from 'sweetalert2';
declare var $:any;
@Component({
    selector: 'app-tutransactions',
    templateUrl: './tutransactions.component.html',

})
export class TutransactionsComponent implements OnInit {

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
    this._HelperService.ShowDateRange = false;

     }
    public merchantId = 0;
    public PtspId = 0;
    StartTime = null;
    StartTimeS = null;
    EndTime = null;
    public TodayStartTime = null;
    public TodayEndTime = null;
    ngOnInit() {
        if (this.StartTime == undefined) {
            this.StartTime = new Date(2017, 0, 1, 0, 0, 0, 0), moment();
            this.EndTime = moment().endOf('month');
        }
        if (this.TodayStartTime == undefined) {
            this.TodayStartTime = moment().startOf('day');
            this.TodayEndTime = moment().add(1, 'minutes');
        }
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
                if (this._HelperService.AppConfig.ActiveOwnerId != undefined && this._HelperService.AppConfig.ActiveOwnerId != null) {
                    this.merchantId = this._HelperService.AppConfig.ActiveReferenceId;
                    this.PtspId = this._HelperService.AppConfig.ActiveOwnerId;
                    this.TransactionsList_Setup();

                    this.TUTr_Filter_Merchants_Load();
                    this.TUTr_Filter_Stores_Load();
                    this.TUTr_Filter_CardBrands_Load();
                    this.TUTr_Filter_TransactionTypes_Load();
                    this.TUTr_Filter_CardBanks_Load();

                    // this._HelperService.GetAccountOverviewMain(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.StartTime, this.EndTime);
                }
                else {
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
                }
            } else {
                this.merchantId = this._HelperService.AppConfig.ActiveReferenceId;

                this.TransactionsList_Setup();

                this.TUTr_Filter_Merchants_Load();
                this.TUTr_Filter_Stores_Load();
                this.TUTr_Filter_CardBrands_Load();
                this.TUTr_Filter_TransactionTypes_Load();
                this.TUTr_Filter_CardBanks_Load();
            }
        });
        Feather.replace();
    }

    //#region Transactions 

    public TransactionsList_Config: OList;
    public MerchantId = 0;
    TransactionsList_Setup() {
        var SearchCondition = undefined
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ProviderId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=')
        this.TransactionsList_Config = {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
            Title: 'Sales History',
            StatusType: 'transaction',
            StatusName:'Success',
            SearchBaseCondition: SearchCondition,
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'TransactionDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: true,
                    IsDateSearchField: true,
                },
                {
                    DisplayName: 'Transaction Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Customer',
                    SystemName: 'UserDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'User Mobile Number',
                    SystemName: 'UserMobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: true,
                    Sort: false,
                },
                {
                    DisplayName: 'Invoice Amount',
                    SystemName: 'InvoiceAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Reward Amount',
                    SystemName: 'RewardAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Class: 'text-grey',
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'User Reward Amount',
                    SystemName: 'UserAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Commission Amount',
                    SystemName: 'CommissionAmount',
                    DataType: this._HelperService.AppConfig.DataType.Decimal,
                    Show: true,
                    Search: false,
                    Sort: false,
                },

                {
                    DisplayName: 'Card Number',
                    SystemName: 'AccountNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Type',
                    SystemName: 'CardBrandName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Card Bank Provider',
                    SystemName: 'CardBankName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'TUC Card Number',
                    SystemName: 'TUCCardNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Merchant',
                    SystemName: 'ParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Store',
                    SystemName: 'SubParentDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Acquirer / Bank',
                    SystemName: 'AcquirerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Terminal Provider',
                    SystemName: 'ProviderDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Issuer (Done by)',
                    SystemName: 'CreatedByDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Transaction Reference',
                    SystemName: 'ReferenceNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },


            ]
        };
        this.TransactionsList_Config = this._DataHelperService.List_Initialize(this.TransactionsList_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
            this.TransactionsList_Config
        );
        this.TransactionsList_GetData();
    }
    TransactionsList_GetData() {
        var TConfig = this._DataHelperService.List_GetDataTur(
            this.TransactionsList_Config
        );
        this.TransactionsList_Config = TConfig;
    }

    TransactionsList_ToggleOption(event: any, Type: any) {

        if (event != null) {
            for (let index = 0; index < this.TransactionsList_Config.Sort.SortOptions.length; index++) {
              const element = this.TransactionsList_Config.Sort.SortOptions[index];
              if (event.SystemName == element.SystemName) {
                element.SystemActive = true;
              }
              else {
                element.SystemActive = false;
              }
            }
          }

        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TransactionsList_Config);

        this.TransactionsList_Config = this._DataHelperService.List_Operations(this.TransactionsList_Config, event, Type);
        if (
            (this.TransactionsList_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TransactionsList_GetData();
        }

        this._HelperService.StopClickPropogation();
    }

    //#endregion

    SetOtherFilters(): void {

        var SearchCondition = undefined
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
        SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'ProviderId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=')

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Merchant_Selected = 0;
            this.MerchantEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBankEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_TransactionType_Selected = 0;
            this.TransactionTypeEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }

        CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_CardBrand_Selected = 0;
            this.CardBrandEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region drkopdowns 

    //#region Merchants 

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                // }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Merchants_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TransactionsList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Merchant
        );
        this.MerchantEventProcessing(event);
    }

    MerchantEventProcessing(event): void {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            // console.log('A', this.TUTr_Filter_Merchant_Selected);
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TransactionsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TransactionsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }

    //#endregion

    //#region Store 

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
            ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
            ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };

        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TransactionsList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TransactionsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TransactionsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion

    //#region TransactionType 

    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TransactionsList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.TransactionType
        );
        this.TransactionTypeEventProcessing(event);
    }

    TransactionTypeEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TransactionsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TransactionsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region CardBrand 

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TransactionsList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBrand
        );
        this.CardBrandEventProcessing(event);
    }

    CardBrandEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TransactionsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TransactionsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#region CardBank 

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TransactionsList_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.CardBank
        );
        this.CardBankEventProcessing(event);
    }

    CardBankEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TransactionsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TransactionsList_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TransactionsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TransactionsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#endregion

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetMerchantSalesConfig(this.TransactionsList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TransactionsList_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_MerchantSales(Type, index);
        this._FilterHelperService.SetMerchantSalesConfig(this.TransactionsList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TransactionsList_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                maxLength: "32",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.MerchantSales
                );
                this._FilterHelperService.SetMerchantSalesConfig(this.TransactionsList_Config);
                this.TransactionsList_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any,ButtonType:any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.TransactionsList_GetData();
        this.ResetFilterUI();

        if(ButtonType == 'Sort'){
            $("#MerchantsList_sdropdown").dropdown('toggle');
          }else if(ButtonType == 'Other'){
            $("#MerchantsList_fdropdown").dropdown('toggle');
          }

          this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetMerchantSalesConfig(this.TransactionsList_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion
        this.TransactionsList_GetData();
        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();
    }

    //#endregion

    ResetFilterControls: boolean = true;
    ResetFilterUI(): void {
      this.ResetFilterControls = false;
      this._ChangeDetectorRef.detectChanges();
  
      this.TUTr_Filter_Merchants_Load();
      this.TUTr_Filter_Stores_Load();
      this.TUTr_Filter_CardBrands_Load();
      this.TUTr_Filter_TransactionTypes_Load();
      this.TUTr_Filter_CardBanks_Load();
  
      this.ResetFilterControls = true;
      this._ChangeDetectorRef.detectChanges();
    }
}