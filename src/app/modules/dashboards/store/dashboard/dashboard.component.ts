import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, ViewChildren, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, BaseChartDirective } from 'ng2-charts';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit , OnDestroy {
    showBarChart: boolean = true;
    public _DateSubscription: Subscription = null;
    public _BarDateSubscription: Subscription = null;

    @ViewChild(DaterangePickerComponent)
    @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

    private picker: DaterangePickerComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,

    ) {
        this._HelperService.ShowDateRange = true;

    }

    public Options: any = {
        cornerRadius: 20,
        responsive: true,
        legend: {
            display: false,
            position: 'right',
        },
        ticks: {
            autoSkip: false
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        stacked: true,
                        display: false
                    },
                    ticks: {
                        autoSkip: false,
                        fontSize: 11
                    }
                }
            ],
            yAxes: [
                {

                    gridLines: {
                        stacked: true,
                        display: true
                    },
                    ticks: {
                        beginAtZero: true,
                        fontSize: 11
                    }
                }
            ]
        },
        annotation: {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                // value: 20,
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 4,
                label: {
                    enabled: false,
                    content: 'Test label'
                }
            }]
        },
        plugins: {
            datalabels: {
                backgroundColor: "#ffffff47",
                color: "#798086",
                borderRadius: "2",
                borderWidth: "1",
                borderColor: "transparent",
                anchor: "end",
                align: "end",
                padding: 2,
                font: {
                    size: 10,
                    weight: 500
                },
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    if (label != undefined) {
                        return value;
                    } else {
                        return value;
                    }
                }
            }
        }
    }
    public barChartLabels: Label[] = ['day 1 ', 'day 2', 'day 3', 'day 4', 'day 5', 'day 6', 'day 7'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];

    public barChartData: ChartDataSets[] = [
        // { data: [0, 0, 0, 0, 0, 0, 0], label: 'All' },
        { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
        // { data: [0, 0, 0, 0, 0, 0, 0], label: 'Ideal' },
        { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
        { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
        { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' },
    ];

    public TodayStartTime = null;
    public TodayEndTime = null;
    Type = 5;
    StartTime = null;
    StartTimeS = null;
    EndTime = null;
    EndTimeS = null;
    CustomType = 1;
    ngOnDestroy() {
        this._DateSubscription.unsubscribe();
        this._BarDateSubscription.unsubscribe();

    }

    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];

            if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.FullContainer = false;
                if (this.StartTime == undefined || this.TodayStartTime == null) {
                    this.StartTime = moment();
                    this.EndTime = moment();
                }

                if (this.TodayStartTime == undefined || this.TodayStartTime == null) {
                    this.TodayStartTime = moment().subtract(7, 'day').startOf('day');
                    this.TodayEndTime = moment().endOf('day');


                }
                this.LoadData();
                this.TerminalsList_Setup();
            }
        });
        this._HelperService.ResetDateRange();
        this._HelperService.ToogleRange('C');

        this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
            this.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveReferenceId, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.DateRangeStart, this._HelperService.DateRangeEnd);

        });
        this._BarDateSubscription = this._HelperService.BarDatechange.subscribe(value => {
            this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._HelperService.DateRangeStart), moment(this._HelperService.DateRangeEnd));
            this._ChangeDetectorRef.detectChanges();
        });
    }

    public _LastSevenDaysData: any = {};
    GetTerminalActivityHistory(UserAccountId, UserAccountKey, starttime, endtime) {

        this.barChartData[0].data = [];
        this.barChartData[1].data = [];
        this.barChartData[2].data = [];
        this.barChartData[3].data = [];
        this.barChartColors[0].backgroundColor = [];
        this.barChartColors[1].backgroundColor = [];
        this.barChartColors[2].backgroundColor = [];
        this.barChartColors[3].backgroundColor = [];

        var Data = {
            Task: "getterminalactivityhistory",
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            StartTime: this._HelperService.DateRangeStart,
            EndTime: this._HelperService.DateRangeEnd
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._LastSevenDaysData = _Response.Result;

                    this.showBarChart = false;
                    this._ChangeDetectorRef.detectChanges();
                    for (let i = 0; i < this._LastSevenDaysData.length; i++) {
                        // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;
                        this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Active;
                        this.barChartData[1].data[i] = this._LastSevenDaysData[i].Data.Idle;
                        this.barChartData[2].data[i] = this._LastSevenDaysData[i].Data.Dead;
                        this.barChartData[3].data[i] = this._LastSevenDaysData[i].Data.Inactive;

                        this.barChartColors[0].backgroundColor.push('#00CCCC')
                        this.barChartColors[1].backgroundColor.push('#FFC20A')
                        this.barChartColors[2].backgroundColor.push('#F10875')
                        this.barChartColors[3].backgroundColor.push('#0168FA')

                    }

                    // console.log('=>', this.barChartColors);

                    this.components.forEach(a => {
                        try {
                            if (a.chart) a.chart.update();
                        } catch (error) {
                            console.log('chartjs error');
                        }
                    });

                    this.showBarChart = true;
                    this._ChangeDetectorRef.detectChanges();

                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }
    LoadData() {
        this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
        this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
        this.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveReferenceId, this._HelperService.AppConfig.ActiveReferenceKey, this.StartTime, this.EndTime);
        this.GetTerminalActivityHistory(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.TodayStartTime, this.TodayEndTime)

    }

    GetAccountOverview(
        UserAccountId,
        UserAccountKey,
        SubUserAccountId,
        SubUserAccountKey,
        StartTime,
        EndTime
    ) {

        var Data = {
            Task: "getaccountoverview",
            StartTime: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
            AccountId: UserAccountId,
            AccountKey: UserAccountKey,
            SubAccountId: SubUserAccountId,
            SubAccountKey: SubUserAccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._AccountOverview = _Response.Result as OAccountOverview;

                        if (this._AccountOverview.TerminalStatus['Total'] != 0) {
                            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
                            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
                            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
                            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
                            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
                        } else {
                            this._AccountOverview["IdleTerminalsPerc"] = 0;
                            this._AccountOverview["ActiveTerminalsPerc"] = 0;
                            this._AccountOverview["DeadTerminalsPerc"] = 0;
                            this._AccountOverview["UnusedTerminalsPerc"] = 0;
                        }
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
                var other: any = {
                    Name: "Other",
                    Transactions: 0,
                    Amount: 0.0
                }

                for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
                    let element = this._AccountOverview['CardTypeSale'][index];
                    other.Transactions += element['Transactions'];
                    other.Amount += element['Amount'];
                }

                other.Transactions = this._AccountOverview["TotalTransactions"] - other['Transactions'];
                other.Amount = this._AccountOverview["TotalSale"] - other['Amount'];

                this._AccountOverview['Others'] = other;
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    public TerminalsList_Config: OList;
    TerminalsList_Setup() {
        this.TerminalsList_Config = {
            Id: "TerminalsList",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantsTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
            Title: "Available Terminals",
            StatusType: "default",
            Type: this._HelperService.AppConfig.ListType.All,
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Number, 'default.blocked', '=='),
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            ListType: 1,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            TableFields: [
                {
                    DisplayName: 'TerminalID',
                    SystemName: 'TerminalId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'MerchantID',
                    SystemName: 'MerchantId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'AcquirerId',
                    SystemName: 'AcquirerId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Acquirer',
                    SystemName: 'AcquirerName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'StoreId',
                    SystemName: 'StoreId',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'StoreName',
                    SystemName: 'StoreName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Address',
                    SystemName: 'StoreAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: 'tx-normal',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'StoreContactNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: 'text-center',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash',
                    NavigateField: "ReferenceKey",
                    NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
                },

                {
                    DisplayName: 'Last Tr',
                    SystemName: 'LastTransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    IsDateSearchField: false,
                },
                {
                    DisplayName: 'Added On',
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'StatusID',
                    SystemName: 'StatusId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: false,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Status',
                    SystemName: 'StatusCode',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: false,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Status',
                    SystemName: 'StatusName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: 'tx-normal',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.TerminalsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict( this.TerminalsList_Config.SearchBaseCondition , 'StoreId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceId,  '=='),

        this.TerminalsList_Config = this._DataHelperService.List_Initialize(
            this.TerminalsList_Config
        );
        this.TerminalsList_GetData();
    }
    TerminalsList_ToggleOption(event: any, Type: any) {
        this.TerminalsList_Config = this._DataHelperService.List_Operations(
            this.TerminalsList_Config,
            event,
            Type
        );
        if (this.TerminalsList_Config.RefreshData == true) {
            this.TerminalsList_GetData();
        }
    }

    TerminalsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.TerminalsList_Config
        );
        this.TerminalsList_Config = TConfig;

    }
    public _AccountOverview: OAccountOverview =
        {
            TerminalStatus: 0,
            Others: 0,
            CardTransPerc: 0,
            CashTransPerc: 0,
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            ActiveTerminalsDiff: 0,
            CashTransactionAmount: 0,
            CardTransactionsAmount: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            TotalTransactions: 0,
            TotalSale: 0,
            AverageTransactions: 0,
            AverageTransactionAmount: 0
        }


}

export class OAccountOverview {
    public TerminalStatus: number;
    public Others: number;
    public CardTransPerc: number;
    public CashTransPerc: number;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public AverageTransactions: number;
    public AverageTransactionAmount: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashTransactionAmount: number;
    public CardTransactionsAmount: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
    public TotalTransactions: number;
    public TotalSale: number;
}