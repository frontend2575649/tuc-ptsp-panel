import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import 'leaflet.markercluster';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OList, OResponse, OSelect } from "../../../../service/service";
import { FilterHelperService } from 'src/app/service/filterhelper.service';
declare var moment: any;
import swal from "sweetalert2";
declare var $: any;
@Component({
  selector: "tu-terminals",
  templateUrl: "./terminals.component.html"
})

export class TerminalsComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) { 
    this._HelperService.ShowDateRange = false;

  }

  public PtspId = 0;
  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      Terminals: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
    }

  StartTime = null;
  StartTimeS = null;
  EndTime = null;
  EndTimeS = null;
  public TodayStartTime = null;
  public TodayEndTime = null;
  public addressPoints: any[];
  Options: any;


  ngOnInit() {
    if (this.StartTime == undefined) {
      this.StartTime = moment().startOf('month');
      this.EndTime = moment().endOf('month');
    }

    if (this.TodayStartTime == undefined) {
      this.TodayStartTime = moment().startOf('day');
      this.TodayEndTime = moment().add(1, 'minutes');
    }

    Feather.replace();
    this._HelperService.StopClickPropogation();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
        if (this._HelperService.AppConfig.ActiveOwnerId != undefined && this._HelperService.AppConfig.ActiveOwnerId != null) {
          this.PtspId = this._HelperService.AppConfig.ActiveOwnerId;
          this.TerminalsList_Filter_Merchants_Load();
          this.TerminalsList_Filter_Stores_Load();
          this.TerminalsList_Filter_Banks_Load();
          this.TerminalsList_Setup();
          this.InitColumnConfig();
          // this._HelperService.GetAccountOverviewMain(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.StartTime, this.EndTime);
        }
        else {
          this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
        }
      } else {
        this.PtspId = this._HelperService.AppConfig.ActiveReferenceId;
        this.TerminalsList_Filter_Merchants_Load();
        this.TerminalsList_Filter_Stores_Load();
        this.TerminalsList_Filter_Banks_Load();
        this.TerminalsList_Setup();
        this.InitColumnConfig();
      }
    });

  }

  //#region ColumnsManipulation 

  TempColoumConfig: any = [

    {
      Name: 'Status',
      Value: true
    },
    {
      Name: 'Terminal ID',
      Value: true
    },
    {
      Name: 'Acquirer',
      Value: true
    },
   
    {
      Name: 'Last Tr.',
      Value: true
    },
    {
      Name: 'Added On',
      Value: true
    },
  ]

  ColoumConfig: any = [

    {
      Name: 'Status',
      Value: true
    },
    {
      Name: 'Terminal ID',
      Value: true
    },
    {
      Name: 'Acquirer',
      Value: true
    },
   
    {
      Name: 'Last Tr.',
      Value: true
    },
    {
      Name: 'Added On',
      Value: true
    },
  ]
  InitColumnConfig() {
    var temp = this._HelperService.GetStorage('TerminalTable');
    if (temp != undefined && temp != null) {
      this.ColoumConfig = temp.config;
      this.TempColoumConfig = JSON.parse(JSON.stringify(temp.config));
    }
  }

  EditColumnConfig() {
    this._HelperService.OpenModal('EditCol');
  }
  SaveColumnConfig() {
    this.ColoumConfig = JSON.parse(JSON.stringify(this.TempColoumConfig));
    this._HelperService.SaveStorage('TerminalTable', { config: this.ColoumConfig });
    this._HelperService.CloseModal('EditCol');
  }

  //#endregion

  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    // this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Merchant));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Merchant_Selected = null;
      this.MerchantEventProcess(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Provider));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Bank_Selected = null;
      this.BanksEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Store));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Store_Selected = null;
      this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  //#region Terminals 

  public TerminalsList_Config: OList;

  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort:
      {
        SortDefaultName: 'Added on',
        SortDefaultColumn: 'CreateDate',
        SortDefaultOrder: 'desc'
      },
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantsTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Title: "Available Terminals",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.All,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StoreId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
      TableFields: [
        {
          DisplayName: 'Terminal',
          SystemName: 'TerminalId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        
        {
          DisplayName: 'Acquirer',
          SystemName: 'AcquirerName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        
        {
          DisplayName: 'StoreName',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Address',
          SystemName: 'StoreAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Contact No',
          SystemName: 'StoreContactNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'text-center',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          DefaultValue: 'ThankUCash',
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: 'Added On',
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField: true,

          ResourceId: null,
        },
        {
          DisplayName: 'StatusID',
          SystemName: 'StatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TerminalsList_Config
    );

    this.TerminalsList_GetData();

  }


  TerminalsList_ToggleOptionPre(event: any, Type: any) {
    this.TerminalsList_Config.StartDate = event.start;
    this.TerminalsList_Config.EndDate = event.end;
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }

  }

  TerminalsList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
        const element = this.TerminalsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TerminalsList_Config);
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type,
      this._HelperService.AppConfig.FilterTypeOption.Terminal
    );
    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }
  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTerm(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }
  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveTerminal, {
      "ReferenceKey": ReferenceData.ReferenceKey,
      "ReferenceId": ReferenceData.ReferenceId,
      "DisplayName": ReferenceData.DisplayName,
      "AccountTypeCode": this._HelperService.AppConfig.AccountType.PosTerminal
    });

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
  }

  //#endregion

  //#region dropdowns 

  //#region merchant 

  public TerminalsList_Filter_Merchant_Option: Select2Options;
  public TerminalsList_Filter_Merchant_Selected = null;
  TerminalsList_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPTSPMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Merchant_Option = {
      placeholder: 'Filter by Merchant',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Merchant
    );

    this.MerchantEventProcess(event);


  }

  MerchantEventProcess(event: any): void {
    if (event.value == this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Merchant_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Merchant_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Merchant_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Merchant_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "MerchantId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Merchant_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region store 

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = null;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: 'Filter by Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region bank 

  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Sort by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Provider
    );
    this.BanksEventProcessing(event);
  }

  BanksEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  //#endregion

  //#region Apply/Reset Filters 

  //#endregion

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_POS(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal
        );
        this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();
    this.ResetFilterUI();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
    this.ResetFilterUI();
    this._HelperService.StopClickPropogation();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_POS(Type, index);
    this._FilterHelperService.SetPOSConfig(this.TerminalsList_Config);  
    //#region setOtherFilters
    this.SetOtherFilters();

    //#endregion
    this.TerminalsList_GetData();
  }

  //#endregion

  ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.TerminalsList_Filter_Merchants_Load();
    this.TerminalsList_Filter_Stores_Load();
    this.TerminalsList_Filter_Banks_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  public SelectedAppStatus = 0;
  ToggleActivityStatus(value: any) {
    if (value == 0) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ApplicationStatusId', this._HelperService.AppConfig.DataType.Number, this.SelectedAppStatus, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.SelectedAppStatus = 0;
    }
    else if (value != this.SelectedAppStatus) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ApplicationStatusId', this._HelperService.AppConfig.DataType.Number, this.SelectedAppStatus, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.SelectedAppStatus = value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ApplicationStatusId', this._HelperService.AppConfig.DataType.Number, value, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

}
export class OAccountOverview {

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
}
