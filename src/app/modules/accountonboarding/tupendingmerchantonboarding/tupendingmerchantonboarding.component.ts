import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon
} from "../../../service/service";
import swal from "sweetalert2";

@Component({
    selector: "tu-pendingmerchantonboarding",
    templateUrl: "./tupendingmerchantonboarding.component.html"
})
export class TUPendingMerchantOnboardingComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this.MerchantsList_Setup();
        this.MerchantsList_Filter_Owners_Load();
    }

    public MerchantsList_Config: OList;
    MerchantsList_Setup() {
        this.MerchantsList_Config = {
            Id:"",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "Available Merchants",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 1, '=='),
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'ContactNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: 'Email Address',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Stores',
                    SystemName: 'Stores',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Terminals',
                    SystemName: 'Terminals',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Re. %',
                    SystemName: 'RewardPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null,
                },


                {
                    DisplayName: 'Owner',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                },
            ]
        };
        this.MerchantsList_Config = this._DataHelperService.List_Initialize(
            this.MerchantsList_Config
        );
        this.MerchantsList_GetData();
    }
    MerchantsList_ToggleOption(event: any, Type: any) {
        this.MerchantsList_Config = this._DataHelperService.List_Operations(
            this.MerchantsList_Config,
            event,
            Type
        );
        if (this.MerchantsList_Config.RefreshData == true) {
            this.MerchantsList_GetData();
        }
    }
    MerchantsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.MerchantsList_Config
        );
        this.MerchantsList_Config = TConfig;
    }
    MerchantsList_RowSelected(ReferenceData) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchantManager, ReferenceData.ReferenceKey]);
    }

    public MerchantsList_Filter_Owners_Option: Select2Options;
    public MerchantsList_Filter_Owners_Selected = 0;
    MerchantsList_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.Merchant,
                this._HelperService.AppConfig.AccountType.Acquirer,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.PosAccount
            ]
            , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.MerchantsList_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    MerchantsList_Filter_Owners_Change(event: any) {
        if (event.value == this.MerchantsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
            this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
            this.MerchantsList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
            this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
            this.MerchantsList_Filter_Owners_Selected = event.value;
            this.MerchantsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '='));
        }
        this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }


}
