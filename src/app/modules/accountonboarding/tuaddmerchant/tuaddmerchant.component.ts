import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: 'tu-addmerchant',
    templateUrl: './tuaddmerchant.component.html',
})
export class TUAddMerchantComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.Form_AddUser_Load();
        this.GetRoles_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }
    GetRoles_ListChange(event: any) {
        this.Form_AddUser.patchValue(
            {
                RoleKey: event.value
            }
        );
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
    }
    Form_AddUser_Show() {
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchant]);
    }
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
            AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
            RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            SubOwnerKey: null,
            BankKey: null,
            Password: null,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            CountValue: 0,
            AverageValue: 0,
            ApplicationStatusCode: null,
            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,
            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,
            RewardPercentage: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            Owners: [],
            Configuration: [],
        });
    }
    Form_AddUser_Clear() {
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 300);
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService.Icon_Crop_Clear();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        var RewardPercentage = 0;
        RewardPercentage = parseFloat(_FormValue.RewardPercentage);
        if (RewardPercentage == undefined) {
            this._HelperService.NotifyError('Enter reward percentage');
        }
        else if (RewardPercentage == NaN) {
            this._HelperService.NotifyError('Enter valid reward percentage');
        }
        else if (isNaN(RewardPercentage) == true) {
            this._HelperService.NotifyError('Enter valid reward percentage');
        }
        else if (RewardPercentage > 100) {
            this._HelperService.NotifyError('Reward percentage must be between 0 to 100');
        }
        else if (RewardPercentage < 0) {
            this._HelperService.NotifyError('Reward percentage must be between 0 to 100');
        }
        else {

            var Config =
            {
                SystemName: 'rewardpercentage',
                Value: _FormValue.RewardPercentage,
            }
            _FormValue.Configurations = [Config];
            this._HelperService.GeneratePassoword();
            _FormValue.Password = this._HelperService.RandomPassword;
            _FormValue.UserName = _FormValue.EmailAddress;
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._HelperService.NotifySuccess('Merchant account created successfully.');
                        this.Form_AddUser_Clear();
                        if (_FormValue.OperationType == 'edit') {

                            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.PendingMerchantManager, _Response.Result.ReferenceKey]);
                        }
                        else if (_FormValue.OperationType == 'close') {
                            this.Form_AddUser_Close();
                        }
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }

    }
}