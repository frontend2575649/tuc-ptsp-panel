import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HelperService, OResponse } from '../../../service/service';
import { Address } from "ngx-google-places-autocomplete/objects/address";
import {
    HttpClientModule,
    HttpClient,
    HttpHeaders,
    HttpErrorResponse
} from "@angular/common/http";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { map } from "rxjs/operators";
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'registercomplete',
    templateUrl: './registercomplete.component.html',
})
export class RegisterCompleteComponent implements OnInit {
    public safeURL = null;
    public IsMobileNumber1 = true;
    public MobileNumber1 = null;
    public MobileNumber1Message = null;
    public MobileNumber2 = null;
    public MobileNumber3 = null;
    Form_AddUser_Processing = false;
    Form_AddUser: FormGroup;
    constructor(
        private _sanitizer: DomSanitizer,
        private _Http: HttpClient,
        public _FormBuilder: FormBuilder,
        public _TranslateService: TranslateService,
        public _Router: Router,
        public _HelperService: HelperService,
    ) {
        // this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl();
    }
    ngOnInit() {

    }

    NavigateToDashboard() {
        window.location.href = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant;

    }
}
