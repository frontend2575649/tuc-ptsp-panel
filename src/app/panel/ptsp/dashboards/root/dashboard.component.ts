import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService, FakeMissingTranslationHandler } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import { OSelect, DataHelperService, HelperService, OList, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview, OListResponse } from '../../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ChartsModule } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { google } from '@agm/core/services/google-maps-types';
import * as Feather from 'feather-icons';
declare var $: any;

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',

})

export class TUDashboardComponent implements OnInit {

  public TodayDate = this._HelperService.GetDateTime(new Date())


  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];

  public barChartData: ChartDataSets[] = [
    // { data: [0, 0, 0, 0, 0, 0, 0], label: 'All' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
    // { data: [0, 0, 0, 0, 0, 0, 0], label: 'Ideal' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' },
  ];

  showBarChart: boolean = true;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) {

  }
  public TodayStartTime = null;
  public TodayEndTime = null;

  Type = 5;
  StartTime = null;
  StartTimeS = null;
  EndTime = null;
  EndTimeS = null;
  CustomType = 1;
  PtspId = 0;

  ngOnInit() {

    Feather.replace();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      this.PtspId = this._HelperService.AppConfig.ActiveOwnerId;
      this.TerminalsList_Setup();

    });
    this._HelperService.FullContainer = false;
    if (this.StartTime == undefined || this.TodayStartTime == null) {
      this.StartTime = moment();
      this.EndTime = moment();
    }
    if (this.TodayStartTime == undefined || this.TodayStartTime == null) {
      this.TodayStartTime = moment().subtract(6, 'day').startOf('day');
      this.TodayEndTime = moment().endOf('day');
    }

    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.TodayStartTime), moment(this.TodayEndTime));
    this.LoadData();
    // this._HelperService.GetAccountCount(this._HelperService.UserAccount.AccountId, this._HelperService.UserAccount.AccountKey, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)


  }

  UserAnalytics_DateChange(Type) {
    this.Type = Type;
    var SDate;
    if (Type == 1) { // today
      SDate =
      {
        start: moment().startOf('day'),
        end: moment().endOf('day'),
      }
    }
    else if (Type == 2) { // yesterday
      SDate =
      {
        start: moment().subtract(1, 'days').startOf('day'),
        end: moment().subtract(1, 'days').endOf('day'),
      }
    }
    else if (Type == 3) {  // this week
      SDate =
      {
        start: moment().startOf('isoWeek'),
        end: moment().endOf('isoWeek'),
      }
    }
    else if (Type == 4) { // last week
      SDate =
      {
        start: moment().subtract(1, 'weeks').startOf('isoWeek'),
        end: moment().subtract(1, 'weeks').endOf('isoWeek'),
      }
    }
    else if (Type == 5) { // this month
      SDate =
      {
        start: moment().startOf('month'),
        end: moment().endOf('month'),
      }
    }
    else if (Type == 6) { // last month
      SDate =
      {
        start: moment().startOf('month').subtract(1, 'month'),
        end: moment().startOf('month').subtract(1, 'days'),
      }
    }
    else if (Type == 7) {
      SDate =
      {
        start: new Date(2017, 0, 1, 0, 0, 0, 0),
        end: moment().endOf('day'),
      }
    }

    this.StartTime = SDate.start;
    this.EndTime = SDate.end;
    this.LoadData();
  }
  LoadData() {
    this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
    this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
    this.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.StartTime, this.EndTime);
    this.GetTerminalActivityHistory(this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.ActiveOwnerKey, this.TodayStartTime, this.TodayEndTime)

  }
  public _LastSevenDaysData: any = {};
  GetTerminalActivityHistory(UserAccountId, UserAccountKey, starttime, endtime) {

    var Data = {
      Task: "getterminalactivityhistory",
      AccountId: UserAccountId,
      AccountKey: UserAccountKey,
      StartTime: starttime,
      EndTime: endtime
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LastSevenDaysData = _Response.Result;

          this.showBarChart = false;
          this._ChangeDetectorRef.detectChanges();
          for (let i = 0; i < this._LastSevenDaysData.length; i++) {
            this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Active;
            this.barChartData[1].data[i] = this._LastSevenDaysData[i].Data.Idle;
            this.barChartData[2].data[i] = this._LastSevenDaysData[i].Data.Dead;
            this.barChartData[3].data[i] = this._LastSevenDaysData[i].Data.Inactive;

          }

          this.showBarChart = true;
          this._ChangeDetectorRef.detectChanges();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetAccountOverview(
    UserAccountId,
    UserAccountKey,
    StartTime,
    EndTime
  ) {

    var Data = {
      Task: "getaccountoverview",
      StartTime: StartTime,
      EndTime: EndTime,
      AccountId: UserAccountId,
      AccountKey: UserAccountKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.TUCPtspAnalytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._AccountOverview = _Response.Result as OAccountOverview;

            if (this._AccountOverview.TerminalStatus['Total'] != 0) {
              var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
              this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
              this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
              this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
              this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
            } else {
              this._AccountOverview["IdleTerminalsPerc"] = 0;
              this._AccountOverview["ActiveTerminalsPerc"] = 0;
              this._AccountOverview["DeadTerminalsPerc"] = 0;
              this._AccountOverview["UnusedTerminalsPerc"] = 0;
            }
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort:
      {
        SortDefaultName: 'Added on',
        SortDefaultColumn: 'CreateDate',
        SortDefaultOrder: 'desc'
      },
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchantsTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
      Title: "Available Terminals",
      StatusType: "default",
      PageRecordLimit : this._HelperService.AppConfig.ListRecordLimit[5],
      Type: this._HelperService.AppConfig.ListType.All,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this._HelperService.UserAccount.AccountId, '=='),
      TableFields: [
        {
          DisplayName: 'TerminalID',
          SystemName: 'TerminalId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'MerchantID',
          SystemName: 'MerchantId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'AcquirerId',
          SystemName: 'AcquirerId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Acquirer',
          SystemName: 'AcquirerName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'StoreId',
          SystemName: 'StoreId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'StoreName',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Address',
          SystemName: 'StoreAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Contact No',
          SystemName: 'StoreContactNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'text-center',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: 'ThankUCash',
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },

        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: 'Added On',
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'StatusID',
          SystemName: 'StatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusCode',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: 'tx-normal',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );
    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }
  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTerm(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;

  }
  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelPTSP.Terminal.Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }
  public _OSalesHistory =
    {
      Labels: [],
      SaleColors: [],
      SaleDataSet: [],

      SaleCustomersColors: [],
      SaleCustomersDataSet: [],

      TransactionsDataSetColors: [],
      TransactionsDataSet: [],

      TransactionStatusDataSetColors: [],
      TransactionStatusDataSet: [],

      TransactionTypeDataSetColors: [],
      TransactionTypeDataSet: [],

      TransactionTypeCustomersDataSetColors: [],
      TransactionTypeCustomersDataSet: [],
    }

  public _AccountOverview: OAccountOverview =
    {
      ActiveStores: 0,
      TotalTerminals: 0,
      ActivePercentage: 0,
      IdlePercentage: 0,
      DeadPercentage: 0,
      InactivePercentage: 0,
      Total: 0,
      Active: 0,
      Idle: 0,
      Dead: 0,
      Inactive: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      TotalMerchants: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      TerminalStatus: 0,
      ActivePosUpgradeRequest: 0,
      TotalStores: 0
    }

}

export class OAccountOverview {
  public ActiveStores?: number;
  public ActivePosUpgradeRequest?: number;
  public TotalStores?: number;
  public TotalMerchants?: number;
  public TotalTerminals?: number;
  public ActivePercentage?: number;
  public IdlePercentage?: number;
  public DeadPercentage?: number;
  public InactivePercentage?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Active?: number;
  public Idle?: number;
  public Dead?: number;
  public Inactive?: number;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

