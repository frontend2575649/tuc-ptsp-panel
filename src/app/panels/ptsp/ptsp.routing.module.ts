import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TUPtspComponent } from "./ptsp.component";
import { HelperService } from "../../service/helper.service";
const routes: Routes = [
  {
    path: "provider",
    component: TUPtspComponent,
    canActivateChild: [HelperService],
    children: [
      { path: 'apps', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/hcapps/tuapplist/tuapplist.module#TUAppListModule' },
      { path: 'app', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/hcapps/hcapps.module#HCAppsModule' },
      { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/ptsp/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: "merchants", data: { permission: "terminals", PageName: "System.Menu.Merchants" }, loadChildren: "./../../modules/accounts/tumerchants/ptsp/tumerchants.module#TUMerchantsModule" },
      { path: 'terminals', data: { 'permission': 'terminals', PageName: 'System.Menu.Terminals' }, loadChildren: './../../modules/accounts/tuterminals/ptsp/tuterminals.module#TUTerminalsModule' },
      { path: 'terminal', data: { 'permission': 'terminals', PageName: 'System.Menu.Terminal' }, loadChildren: './../../modules/accountdetails/tuterminal/ptsp/tuterminal.module#TUTerminalModule' },
      { path: "merchant", data: { permission: "merchant", PageName: "System.Menu.Merchant" }, loadChildren: "./../../modules/accountdetails/tumerchant/ptsp/tumerchant.module#TUMerchantModule" },
      { path: "store", data: { permission: "merchant", PageName: "System.Menu.Merchant" }, loadChildren: "./../../modules/accountdetails/tustore/tustore.module#TUStoreModule" },
      { path: 'stores', data: { permission: 'stores', PageName: 'System.Menu.Stores' }, loadChildren: './../../modules/accounts/tustores/ptsp/tustores.module#TUStoresModule' },
      { path: 'posreports', data: { permission: 'stores', PageName: 'System.Menu.Stores' }, loadChildren: './../../modules/accounts/tuposreports/ptsp/tuposreports.module#TuposreportsModule' },
      { path: 'posreports/posactivity', data: { permission: 'stores', PageName: 'System.Menu.Activity' }, loadChildren: './../../modules/accounts/tuposreports/ptsp/tuposactivity/tuposactivity.module#TuposactivityModule' },
      { path: 'posreports/possalesreport', data: { permission: 'stores', PageName: 'System.Menu.PosReport' }, loadChildren: './../../modules/accounts/tuposreports/ptsp/tupossalesreport/tupossalesreport.module#TupossalesreportModule' },
      { path: 'posreports/postransactions', data: { permission: 'stores', PageName: 'System.Menu.Transactions' }, loadChildren: './../../modules/accounts/tuposreports/ptsp/tupostransactions/tupostransactions.module#TupostransactionsModule' },
      { path: 'commision', data: { permission: 'stores', PageName: 'System.Menu.CommissionHistory' }, loadChildren: './../../modules/accounts/tuposreports/ptsp/tucommisonhistory/tucommisionhistory.module#TuCommisionHistorysModule' },
      
      { path: "profile", data: { permission: "customers", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcprofile/hcprofile.module#HCProfileModule" },

      {
        path: "merchantonboarding",
        data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
        loadChildren:
          "./../../pages/useronboarding/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule"
      },
      // FAQs
      { path: 'faqcategories', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tufaqs/tufaqs.module#TUFAQsModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUptspRoutingModule { }
