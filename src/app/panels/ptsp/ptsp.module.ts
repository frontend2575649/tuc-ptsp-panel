import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TUPtspComponent } from './ptsp.component';
import { TUptspRoutingModule } from './ptsp.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        TUPtspComponent,
    ],
    imports: [
        TUptspRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [TUPtspComponent]
})
export class TUPtspModule { }
